//
//  DAMShareableView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 16/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMViewControllerBase.h"
#import "DAMMediaSaveView.h"
#import "DAMMediaSendView.h"

@interface DAMShareableView : DAMViewControllerBase<DAMMediaSaveViewDelegate, DAMMediaSendViewDelegate>

@property (nonatomic, retain) DAMMediaSaveView *saveView;
@property (nonatomic, retain) DAMMediaSendView *sendView;

//Methods for superclasses to override
- (void)setSelectionMode:(BOOL)selectionMode;       //optional
- (NSArray*)selectedMedia;                          //mandatory

@end
