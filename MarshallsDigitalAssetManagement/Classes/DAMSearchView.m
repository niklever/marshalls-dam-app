//
//  DAMSearchView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMSearchView.h"
//#import "ASIHTTPRequest.h"
//#import "JSONKit.h"
#import "DAMMediaViewBase.h"
#import "NSString+Utilities.h"
#import "DAMPredictiveViewController.h"

#define HIDE_PREDICTIVE_AFTER 30 //in seconds
#define TIME_FIRE_SEARCH 1 //in seconds

@interface DAMSearchView () {
    NSString *searchText;
}
@property (nonatomic, retain) NSString *searchText;
@property (nonatomic, retain) DAMBusinessAreaFiltersView *businessAreaFiltersView;

@end

@implementation DAMSearchView
//@synthesize searchBar;
@synthesize brochuresCount;
@synthesize sampleBookCount;
@synthesize photosCount;
@synthesize videosCount;
@synthesize caseStudiesCount;
@synthesize presentationsCount;
@synthesize searchText;
@synthesize businessAreaFiltersView;
@synthesize predictiveViewController;

#pragma mark - Dealloc

- (void)dealloc{
    
    self.predictiveViewController = nil;
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    NSLog(@"brochuresCount.font:%@", brochuresCount.font);
    float fontSize = 160;
    brochuresCount.font = [UIFont fontWithName:@"Myriad Pro" size:fontSize];
    sampleBookCount.font = [UIFont fontWithName:@"Myriad Pro" size:fontSize];
    photosCount.font = [UIFont fontWithName:@"Myriad Pro" size:fontSize];
    videosCount.font = [UIFont fontWithName:@"Myriad Pro" size:fontSize];
    caseStudiesCount.font = [UIFont fontWithName:@"Myriad Pro" size:fontSize];
    presentationsCount.font = [UIFont fontWithName:@"Myriad Pro" size:fontSize];

    [self setCountLabelsAlpha:0.0];
    
    brochuresCount.transform = CGAffineTransformMakeTranslation(0, 40);
    sampleBookCount.transform = CGAffineTransformMakeTranslation(0, 40);
    photosCount.transform = CGAffineTransformMakeTranslation(0, 40);
    videosCount.transform = CGAffineTransformMakeTranslation(0, 40);
    caseStudiesCount.transform = CGAffineTransformMakeTranslation(0, 40);
    presentationsCount.transform = CGAffineTransformMakeTranslation(0, 40);

    brochuresCount.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    sampleBookCount.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    photosCount.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    videosCount.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    caseStudiesCount.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    presentationsCount.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    
//    [damNavigator displaySearchBarWithDelegate:self];
    
    self.predictiveViewController = [[[DAMPredictiveViewController alloc] init] autorelease];
    self.predictiveViewController.view.frame = CGRectMake(129, 45, 337, 246);
    self.predictiveViewController.view.hidden = YES;
    self.predictiveViewController.delegate = self;
    [self.view addSubview:self.predictiveViewController.view];
    
    
    UITapGestureRecognizer *vtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleVideosTap:)];
    [videosCount addGestureRecognizer:vtap];
    [vtap release];
    
    UITapGestureRecognizer *brotap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBrochuresTap:)];
    [brochuresCount addGestureRecognizer:brotap];
    [brotap release];
    
    
    UITapGestureRecognizer *sampletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSamplesTap:)];
    [sampleBookCount addGestureRecognizer:sampletap];
    [sampletap release];
    
    UITapGestureRecognizer *cstap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCaseStudyTap:)];
    [caseStudiesCount addGestureRecognizer:cstap];
    [cstap release];
    
    UITapGestureRecognizer *phototap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePhotosTap:)];
    [photosCount addGestureRecognizer:phototap];
    [phototap release];
    
    UITapGestureRecognizer *pretap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePresentationTap:)];
    [presentationsCount addGestureRecognizer:pretap];
    [pretap release];
    
    searchIsBuzy = false;
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    /*ios 7 updated*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSearchBar:) name:@"k_Global-Search_showsSearchBar" object: nil];
    
}

- (void) showSearchBar:(NSNotification *)sender {
    [damNavigator displaySearchBarWithDelegate:self];
    damNavigator.searchBar.searchField.text = searchText;
    damNavigator.searchBar.cancelButton.hidden = YES;
    damNavigator.searchBar.businessAreaButton.hidden = NO;
    damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)handleVideosTap:(UIGestureRecognizer *)gestureRecognizer {
    [self launchMediaView: videosCount.tag];
}

- (void)handleBrochuresTap:(UIGestureRecognizer *)gestureRecognizer {
    [self launchMediaView: brochuresCount.tag];
}

- (void)handleSamplesTap:(UIGestureRecognizer *)gestureRecognizer {
    [self launchMediaView: sampleBookCount.tag];
}

- (void)handleCaseStudyTap:(UIGestureRecognizer *)gestureRecognizer {
    [self launchMediaView: caseStudiesCount.tag];
}

- (void)handlePhotosTap:(UIGestureRecognizer *)gestureRecognizer {
    [self launchMediaView: photosCount.tag];
}

- (void)handlePresentationTap:(UIGestureRecognizer *)gestureRecognizer {
    [self launchMediaView: presentationsCount.tag];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showSearchBar: nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
//    self.searchBar = nil;
    self.brochuresCount = nil;
    self.sampleBookCount = nil;
    self.photosCount = nil;
    self.videosCount = nil;
    self.caseStudiesCount = nil;
    self.presentationsCount = nil;
    self.searchText = nil;
    self.businessAreaFiltersView = nil;
}

- (void)setCountLabelsAlpha:(float)alpha {
    brochuresCount.alpha = alpha;
    sampleBookCount.alpha = alpha;
    photosCount.alpha = alpha;
    videosCount.alpha = alpha;
    caseStudiesCount.alpha = alpha;
    presentationsCount.alpha = alpha;
}

- (void)animateToCountLabelsAlpha:(float)alpha {
    [UIView beginAnimations:@"animateToCountLabelsAlpha" context:nil];
    [UIView setAnimationDuration:0.5];
    [self setCountLabelsAlpha:alpha];
    [UIView commitAnimations];
}

#pragma mark - UITextFieldDelegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    //hide the predictive
    [NSObject cancelPreviousPerformRequestsWithTarget: self];
    [self.predictiveViewController resetData];
    self.predictiveViewController.view.hidden = YES;
    [textField resignFirstResponder];
    
    if (self.searchText.length >= 3) {
        //TODO - this search should be done via JS api
        [NSObject cancelPreviousPerformRequestsWithTarget: self];
        [self updateQuery: self.searchText showPredictive: NO];
        //[self performSelector:@selector(updateQuery:) withObject: self.searchText afterDelay: 0.0];
    }
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    activeTextField = textField;
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.searchText = newText;
    if (newText.length >= 3) {
        //TODO - this search should be done via JS api
        [NSObject cancelPreviousPerformRequestsWithTarget: self];
        [self performSelector:@selector(updateQuery:) withObject: newText afterDelay: TIME_FIRE_SEARCH];
    } else {
        [NSObject cancelPreviousPerformRequestsWithTarget: self];
        [self.predictiveViewController resetData];
        self.predictiveViewController.view.hidden = YES;
        [self animateToCountLabelsAlpha:0.0f];
    }
    return YES;
}

- (void) updateQuery:(NSString *)query{
    [self updateQuery:query showPredictive: YES];
}

- (void) updateQueryWithoutPredictive:(NSString *)query{
    [self updateQuery:query showPredictive: NO];
}

- (void) updateQuery:(NSString *)query showPredictive:(BOOL) show{
    if (searchIsBuzy) {
        return;
    }
    searchIsBuzy = true;
    NSArray *businessAreas = [NSArray arrayWithObjects:@"domestic", @"commercial", @"group", nil];
    
    //NSArray *activeContainers = [[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas];
    
    [[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas callback:^(NSArray *activeContainers) {
        NSArray *filterAreas = [[JSMain sharedInstance] getBusinessAreaFilters];
        if (filterAreas != NULL && [filterAreas count] > 0) {
#ifdef DEBUG
            NSLog(@"Apply business filter area %@", filterAreas);
#endif
            activeContainers = filterAreas;
        }
        [[JSMain sharedInstance] globalSearch: query containers: activeContainers callback:^(NSDictionary *searchResults) {
            [self setSearchResults:searchResults];
            [self animateToCountLabelsAlpha:1.0f];
            
            //NSArray *businessAreas = [NSArray arrayWithObjects:@"domestic", @"commercial", nil];
            
            //NSArray *activeContainers = [[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas];
            /*
            NSArray *filterAreas = [[JSMain sharedInstance] getBusinessAreaFilters];
            if (filterAreas != NULL && [filterAreas count] > 0) {
#ifdef DEBUG
                NSLog(@"Apply business filter area %@", filterAreas);
#endif
                activeContainers = filterAreas;
            }*/
            
            if (show) {
                [self.predictiveViewController requestDataWithKey:query mediaType: NULL containers: activeContainers];
            }
            searchIsBuzy = false;
        }];
    }];
    
    
}

#pragma mark - 

- (void)predictiveViewController:(DAMPredictiveViewController *)predictiveViewController didChooseItem:(NSDictionary *)item{
#ifdef DEBUG
    NSLog(@"predictiveViewController.didChooseItem = %@", item);
#endif
    self.searchText = [item objectForKey:@"text"];
    damNavigator.searchBar.searchField.text = self.searchText;
    activeTextField.text = self.searchText;
    [self.predictiveViewController hideAfterTime: 0];
    [self updateQuery:self.searchText showPredictive:NO];
}

/*
- (void)requestFinished:(ASIHTTPRequest *)request {
    // Use when fetching text data
    NSString *responseString = [request responseString];
    NSDictionary *searchResults = [responseString objectFromJSONString];
    [self setSearchResults:searchResults];
    [self animateToCountLabelsAlpha:1.0f];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
#ifdef DEBUG
    NSLog(@"requestFailed error:%@", error);
#endif
    [self animateToCountLabelsAlpha:0.0f];
}
*/

- (void)setSearchResults:(NSDictionary*)searchResults {
//    {"Video":5,"Presentation":0,"Photo":1419,"Brochure":64,"Sample":0,"CaseStudy":60}
    brochuresCount.text = [NSString stringWithFormat:@"%@", [searchResults objectForKey:@"Brochure"]];
    sampleBookCount.text = [NSString stringWithFormat:@"%@", [searchResults objectForKey:@"Sample"]];
    photosCount.text = [NSString stringWithFormat:@"%@", [searchResults objectForKey:@"Photo"]];
    videosCount.text = [NSString stringWithFormat:@"%@", [searchResults objectForKey:@"Video"]];
    caseStudiesCount.text = [NSString stringWithFormat:@"%@", [searchResults objectForKey:@"CaseStudy"]];
    presentationsCount.text = [NSString stringWithFormat:@"%@", [searchResults objectForKey:@"Presentation"]];
}

- (IBAction)mediaButtonTapped:(id)sender {
    UIButton *button = sender;
    NSString *navName = @"";
    switch (button.tag) {
        case 1://brochures
            navName = kMediaType_Brochures;
            break;
        case 2:
            navName = kMediaType_Samples;
            break;
        case 0:
            navName = kMediaType_Photos;
            break;
        case 3:
            navName = kMediaType_Videos;
            break;
        case 4:
            navName = kMediaType_CaseStudies;
            break;
        case 5:
            navName = kMediaType_Presentations;
            break;
        default:
            break;
    }
    if (navName.length > 0) {
        [self launchMediaView: navName];
    }
}

- (void)launchMediaView:(NSString *)navigationName {
    NSString *queryString = damNavigator.searchText;
    DAMMediaViewBase *mediaView = [self launchMediaViewByNavigationName: navigationName queryString: queryString];
    //[mediaView sourceFromQuery:queryString];
}

- (void) searchBarBusinessAreaTapped {
#ifdef DEBUG
    NSLog(@"show business area");
#endif
    self.businessAreaFiltersView = [[DAMBusinessAreaFiltersView alloc] initWithNibName:@"DAMBusinessAreaFiltersView" bundle:nil];
    businessAreaFiltersView.delegate = self;
    [self.view addSubview:businessAreaFiltersView.view];
    [businessAreaFiltersView animateOnStage];
    [businessAreaFiltersView release];
}

- (void)businessAreaFiltersViewDidCancel {
    [businessAreaFiltersView.view removeFromSuperview];
    self.businessAreaFiltersView = nil;
    //    [self setInitialSwitchPositions];
}

- (void)businessAreaFiltersViewDidOK {
    [businessAreaFiltersView.view removeFromSuperview];
    self.businessAreaFiltersView = nil;
    //we need to update search query again
    //NSLog(@"query text [%@]", damNavigator.searchBar.searchField.text);
    [self performSelector:@selector(updateQueryWithoutPredictive:) withObject: damNavigator.searchBar.searchField.text afterDelay: TIME_FIRE_SEARCH];
}

- (void)modalViewOffstageAnimationDidFinish:(DAMModalViewBase*)modalView {
}


@end
