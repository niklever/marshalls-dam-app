//
//  ImageHelper.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 9/4/13.
//
//

#import <Foundation/Foundation.h>


@interface UIImage (ImageHelper)
- (UIImage *)scaledToSize:(CGSize)newSize;
- (UIImage *)crop:(CGRect)rect;
@end