//
//  DAMHomeView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//TODO - fix copyright messages

#import "DAMHomeView.h"
//#import "JSONKit.h"
#import "DAMMediaViewBase.h"
#import "DAMApplicationModel.h"
#import "DAMAssetView.h"

//#import "DAMInThePressController.h"
#import "DAMSalesPhotoViewController.h"

//const NSString *modelFileBase = @"Home_Configuration";

const float animationInterval = 7;
const float imageFlipAnimationDuration = 4.0;
const float mediaTypeButtonWidth = 323;
const float mediaTypeButtonHeight = 200;
const float mediaTypeButtonVerticalPad = 1;

@interface DAMHomeView () {
    UIAlertView *alertView;
    BOOL downloadInProgress;
    BOOL downloadPostponed;
    
    dispatch_queue_t updateUIQueue;
}

@property (nonatomic, copy) NSString *currentAvailableMedia;

- (void)setupMediaTypeCarousel;
- (void)timerFired:(id)sender;
- (void)nextMediaTypeView;
- (void)nextAnimationSegment;
- (void)mediaTypeButtonTapped:(id)sender;
@end

@implementation DAMHomeView

@synthesize animationTimer;
@synthesize notificationTimer;

@synthesize mainImage;
@synthesize mainImageNext;
@synthesize teaserTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        updateUIQueue = dispatch_queue_create("com.dolphin.marshalls", DISPATCH_QUEUE_SERIAL);
        
        //updateUIQueue = dispatch_get_main_queue();
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.progressView.hidden = YES;
    self.notificationLabel.hidden = YES;
    self.progressLabel.hidden = YES;
    self.activityIndicatorView.hidden = YES;
    self.noticeLabel.hidden = YES;
    
    
    if (kAppDelegate.dataModel) {
        mediaTypeIndex = -1;
        [self nextMediaTypeView];
    }
    
    /*
    NSString *modelFilePath = [[NSBundle mainBundle] pathForResource:(NSString*)modelFileBase ofType:@"json"];
    NSLog(@"DAMHomeView.viewDidLoad modelFilePath:%@", modelFilePath);
    NSError *error = nil;
    NSString *modelString = [NSString stringWithContentsOfFile:modelFilePath encoding:NSUTF8StringEncoding error:&error];
    if (!error) {
        self.dataModel = [modelString objectFromJSONString];
        //[JSSync sharedInstance].settings = [self.dataModel objectForKey:@"settings"];
        if (dataModel) {
            mediaTypeIndex = -1;
            [self nextMediaTypeView];
        } else {
#ifdef DEBUG
            NSLog(@"DAMHomeView.viewDidLoad.ERROR_PARSE_DATA_MODEL dataModel:%@", dataModel);
#endif
        }
    } else {
#ifdef DEBUG
        NSLog(@"DAMHomeView.viewDidLoad.ERROR_NO_MODEL");
#endif
    }*/
    
    //init single instance of DAMApplication model
    //[DAMApplicationModel model];
    
    UITapGestureRecognizer *teaserTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(teaserTapped:)];
    NSArray *recognizers = [NSArray arrayWithObject:teaserTapRecognizer];
    mainImage.userInteractionEnabled = YES;
    mainImage.gestureRecognizers = recognizers;

    teaserTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(teaserTapped:)];
    recognizers = [NSArray arrayWithObject:teaserTapRecognizer];
    mainImageNext.userInteractionEnabled = YES;
    mainImageNext.gestureRecognizers = recognizers;

    teaserTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(teaserTapped:)];
    recognizers = [NSArray arrayWithObject:teaserTapRecognizer];
    teaserTitle.userInteractionEnabled = YES;
    teaserTitle.gestureRecognizers = recognizers;
    
    
    
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    //[self notificationTimerFired: nil];
    
    
    [[DAMApplicationModel model] startSyncingWithEmail: [[JSMain sharedInstance] userEmail] forceRemove:false validUserCallback:^{
        //update user email
        
    } invalidUserCallBack:^{
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.mainImage = nil;
    self.mainImageNext = nil;
    self.teaserTitle = nil;
    self.mediaTypeCarousel = nil;
    //should be taken care of in viewWillDisappear, but just in case...
    [animationTimer invalidate];
    self.animationTimer = nil;
    self.notificationLabel = nil;
    self.notificationTimer = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //activityIndicatorView.hidden = YES;
    self.animationTimer = [NSTimer scheduledTimerWithTimeInterval:animationInterval target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];

    [[DAMApplicationModel model] settingsGetAvailableMediaTypes];
    
    //[[DAMApplicationModel model] enableOps];
    
    [self.view addSubview:damNavigator.view];
    [damNavigator setDelegate:self];
}


- (void)viewWillDisappear:(BOOL)animated {
    [animationTimer invalidate];
    self.animationTimer = nil;
    [notificationTimer invalidate];
    self.notificationTimer = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}

- (IBAction)viewNowTapped:(id)sender {
#ifdef DEBUG
    NSLog(@"DAMHomeView.viewNowTapped");
#endif
}

- (void)applicationDidBecomeActive:(id)sender {
    downloadPostponed = NO;
}

// -------------------------------- Private Category Implementation --------------------------------
//called by PhoneGap fire method when results are available
/*pKt updated to fix callback*/
- (void)setupMediaTypeCarousel:(NSMutableArray*)availableTypes{
    
     NSLog(@"DAMHomeView.setupMediaTypeCarousel. CALLED %@ vs %@", [availableTypes JSONString], self.currentAvailableMedia);
    //NSArray *availableTypes = (NSArray *)options;
    if (![[availableTypes JSONString] isEqualToString:self.currentAvailableMedia]) {
        
        NSLog(@"DAMHomeView.setupMediaTypeCarousel dataModel %@", kAppDelegate.dataModel);
        
        self.currentAvailableMedia = [availableTypes JSONString];
        NSArray *mediaTypes = [kAppDelegate.dataModel objectForKey:@"mediaTypes"];
        
        //dispatch_sync(updateUIQueue, ^{
            [self.mediaTypeCarousel.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        //});
        
        int buttonTag = 0;
        float yPos = 0;
        
        
        for (NSDictionary *mediaType in mediaTypes) {
            NSString *typeName = [mediaType objectForKey:@"navigationName"];
            
            if ([[mediaType nonNullValueForKey: @"hiddenFromCarousel"] isEqualToString: @"true"]) {
                continue;
            }
            
            if ([availableTypes containsObject:typeName]) {
                
                NSLog(@"adding carousel %@", typeName);
                
                NSString *imgName = [mediaType objectForKey:@"carouselImage"];
                
                UIButton *mediaTypeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, yPos, mediaTypeButtonWidth, mediaTypeButtonHeight)];
                mediaTypeButton.tag = buttonTag;
                [mediaTypeButton setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
                [mediaTypeButton addTarget:self action:@selector(mediaTypeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                mediaTypeButton.hidden = NO;
                [self.mediaTypeCarousel addSubview: mediaTypeButton];
                
                yPos += mediaTypeButtonHeight + mediaTypeButtonVerticalPad;
            }
            buttonTag++;
        }
        
        /* ================================================================== */
        /* ================================================================== */
        /* ================================================================== */
        
        /* adding more profile when is not having the server side support */
        
        
        float carouselHeight = self.mediaTypeCarousel.subviews.count * mediaTypeButtonHeight +  (self.mediaTypeCarousel.subviews.count - 1) * mediaTypeButtonVerticalPad;
        
        /* ================================================================== */
        /* ================================================================== */
        /* ================================================================== */
        
        if ([[JSMain sharedInstance] isIOS7]) {
            carouselHeight += 20;
        }
        
        NSLog(@"carouselHeight %f -- subview %d", carouselHeight, self.mediaTypeCarousel.subviews.count);
        
        self.mediaTypeCarousel.contentSize = CGSizeMake(mediaTypeButtonWidth, carouselHeight);
        
        //self.mediaTypeCarousel.hidden = YES;
    } else {
        NSLog(@"DAMHomeView.setupMediaTypeCarousel.CONFIG_UNCHANGED");
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void) updateNotificationLogs:(NSString *) msg {
    if (msg == NULL) {
        msg = @"Updating...";
    }
    
    NSLog(@"downloading view %d - msg %@", self.downloadingView.hidden, msg);
    
    dispatch_sync(updateUIQueue, ^{
        if ([msg isEqualToString:kContactAdministrator]) {
            self.noWifiView.hidden = YES;
            self.downloadingView.hidden = NO;
        }else{
            //self.downloadingView.hidden = NO;
        }
        
        if ([msg isEqualToString: kSyncingContainers]) {
            self.progressView.hidden = NO;
        }
        
        self.noticeLabel.hidden = YES;
        self.notificationLabel.hidden = NO;
        self.mediaTypeCarousel.hidden = YES;
        self.notificationLabel.text = msg;
    });
}

- (void)showUpdatingMessage:(id)sender {
    //[self updateLoading:sender];
    //[self performSelector:@selector(updateNotificationLogs:) withObject: sender afterDelay:0.5];
    [self updateNotificationLogs: sender];
}

- (void)resetDownloadingMsg:(id)sender {
    /*progressLabel.text = @"";
    progressLabel.hidden = YES;
    progressView.hidden = YES;
    activityIndicatorView.hidden = YES;
    */
    //[self performSelector:@selector(resetNotificationLogs) withObject: nil afterDelay:0.5];
    [self resetNotificationLogs];
}



- (void)resetNotificationLogs {
    dispatch_sync(updateUIQueue, ^{
        self.progressLabel.text = @"";
        self.progressLabel.hidden = YES;
        self.progressView.hidden = YES;
        self.activityIndicatorView.hidden = YES;
    });
}


#pragma mark - Notification

- (void)hideDowloadingProgress:(id)sender{
    dispatch_sync(updateUIQueue, ^{
        self.progressLabel.hidden = YES;
        self.progressView.hidden = YES;
        self.activityIndicatorView.hidden = YES;
    });
    
}

- (void)notificationTimerFired:(id)sender {
    //if (downloadInProgress) return;
    
    //[UIApplication sharedApplication].idleTimerDisabled = NO;
    
    if ([[JSMain sharedInstance] online]) {
        [[DAMApplicationModel model] settingsActiveBusinessAreas:^(NSArray *activeBusinessAreas) {
            dispatch_sync(updateUIQueue, ^{
                self.activityIndicatorView.hidden = YES;
            });
            
            if (activeBusinessAreas.count == 0) {
                dispatch_sync(updateUIQueue, ^{
                    self.notificationLabel.hidden = NO;
                    self.noticeLabel.hidden = YES;
                    self.mediaTypeCarousel.hidden = YES;
                    if ([[JSMain sharedInstance] userEmail] != NULL && [[[JSMain sharedInstance] userEmail] length] > 0) {
                        self.notificationLabel.text = kContactAdministrator;
                    }else{
                        self.notificationLabel.text = kUpdateEmailInSettings;
                    }
                });
            } else {
                
                int downloadQueueSize = [[DAMApplicationModel model] downloadQueueSize];
                
                NSLog(@"downloadQueueSize %d", downloadQueueSize);
                
                if (downloadQueueSize > 0) {
                    
                    if ([[JSMain sharedInstance] onWifi]) {
                        
                        NSLog(@"kickQueue..........................................");
                        dispatch_sync(updateUIQueue, ^{
                            self.noWifiView.hidden = YES;
                            self.downloadingView.hidden = NO;
                            self.notificationLabel.hidden = NO;
                            mediaTypeCarousel.hidden = YES;
                            self.activityIndicatorView.hidden = NO;
                            self.noticeLabel.hidden = NO;
                            
                            self.notificationLabel.text = [NSString stringWithFormat: kDownloadingItemRemained, downloadQueueSize];
                        });
                        
                        [[JSSync sharedInstance].downloadPlugin kickQueue];
                        
                    }else{
                        dispatch_sync(updateUIQueue, ^{
                            //some UI methods ej
                            self.remainedAssetsLabel.text = [NSString stringWithFormat: kItemRemained, downloadQueueSize];
                            self.noWifiView.hidden = NO;
                            self.downloadingView.hidden = YES;
                        });
                        
                    }
                } else {
                    //doneSyncing
                    //[self performSelectorOnMainThread: @selector(doneSyncing) withObject: nil waitUntilDone: NO];
                    //[self performSelectorOnMainThread: @selector(doneSyncing) withObject: nil afterDelay: 1.0];
                    //[self performSelector:@selector(doneSyncing) withObject: nil afterDelay: 1.0];
                    [self doneSyncing];
                }
            }
        }];
    }else{
        //[self performSelector:@selector(offlineAlert) withObject: nil afterDelay:1];
        [self offlineAlert];
    }
}

- (void)offlineAlert {
    dispatch_sync(updateUIQueue, ^{
        self.noWifiView.hidden = YES;
        self.downloadingView.hidden = NO;
        //offline just use what we got so far
        
        downloadInProgress = NO;
        self.notificationLabel.hidden = YES;
        self.mediaTypeCarousel.hidden = NO;
        
        
    });
    [self hideDowloadingProgress:nil];
}


#pragma mark - DONE Sync
- (void) doneSyncing {
    NSLog(@"doneSyncing ALL ASSETS");
    [self hideDowloadingProgress:nil];
    dispatch_sync(updateUIQueue, ^{
        downloadInProgress = NO;
        self.noticeLabel.hidden = YES;
        self.notificationLabel.hidden = YES;
        self.mediaTypeCarousel.hidden = NO;
        NSLog(@"mediaTypeCarousel doneSyncing subview %d", self.mediaTypeCarousel.subviews.count);
    });
}

- (void)timerFired:(id)sender {
//    NSLog(@"DAMHomeView.timerFired");
    
    [self nextMediaTypeView];
}

- (float)boundedRandom:(float)bound {
    return bound * (arc4random() % INT32_MAX) / INT32_MAX;
}

- (float)normalisedRandom {
    return [self boundedRandom:1.0f];
}

- (CGRect)startAnimBounds {
    float width = 1500;
    float height = 995;
    float aspect = width / height;
    float xStart = 200;
    return CGRectMake(xStart, 0, width - xStart, (width - xStart) / aspect);
}

- (CGRect)endAnimBounds {
    float width = 1500;
    float height = 995;
    float aspect = width / height;
    float xStart = 400;
    return CGRectMake(xStart, 0, width - xStart, (width - xStart) / aspect);
}

- (void)nextMediaTypeView {
    
    NSArray *mediaTypes = [kAppDelegate.dataModel objectForKey:@"mediaTypes"];
    int numMediaTypes = mediaTypes.count;
    
    mediaTypeIndex = (mediaTypeIndex + 1) % numMediaTypes;
    
    UIImageView *targetImageView = mediaTypeIndex % 2 == 0 ? mainImage : mainImageNext;
    UIImageView *oldImageView = mediaTypeIndex % 2 == 0 ? mainImageNext : mainImage;
    NSString *nextImage = [[mediaTypes objectAtIndex:mediaTypeIndex] objectForKey:@"mainImage"];
    
//    NSLog(@"DAMHomeView.nextMediaTypeView nextImage:%@", nextImage);
    
    targetImageView.image = [UIImage imageNamed:nextImage];
    targetImageView.alpha = 0;
    targetImageView.bounds = self.startAnimBounds;
    
    NSString *titleImage = [[mediaTypes objectAtIndex:mediaTypeIndex] objectForKey:@"teaserTitle"];
    teaserTitle.image = [UIImage imageNamed:titleImage];
    
    [UIView beginAnimations:@"imageFlip" context:nil];
    [UIView setAnimationDuration:imageFlipAnimationDuration];
    
    targetImageView.alpha = 1.0;
    oldImageView.alpha = 0.0;
    oldImageView.bounds = self.endAnimBounds;
    
    [UIView commitAnimations];
}

- (void)nextAnimationSegment {
    
}

- (void)teaserTapped:(id)sender {
    
    //clear business area filter
    [[JSMain sharedInstance] setBusinessAreaFilters: [NSArray array]];
    DAMMediaViewBase *mediaView = [self launchMediaViewByIndex:mediaTypeIndex];
    [mediaView sourceFromCarousel];
    
}

- (void)mediaTypeButtonTapped:(id)sender {
    
    [[DAMApplicationModel model] suspendOps];
    
    //clear business area filter
    [[JSMain sharedInstance] setBusinessAreaFilters: [NSArray array]];
    
    UIButton *button = sender;
    int buttonTag = button.tag;
    
    DAMMediaViewBase *mediaView = [self launchMediaViewByIndex:buttonTag];
    [mediaView sourceFromCarousel];
    
}

//We don't want default left button behaviour (pop navigation stack) in the Home view
//- (BOOL)leftButtonTapped:(id)sender {
//    [super leftButtonTapped:sender];
//}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
    DAMIDriveNavigatorViewController *iDrive;
    switch (index) {
        case 4:
            [self launch:@"DAMNewSettingsView" withNavigation:@"Setttings"];
            [[DAMApplicationModel model] suspendOps];
            break;
        case 3:
//            [self launch:@"DAMIDriveView" withNavigation:@"iDrive"];
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            [[DAMApplicationModel model] suspendOps];
            break;
        case 2:{
            //THIS IS SALES PHOTOS
            
            [[DAMApplicationModel model] suspendOps];
            
            //clear business area filter
            [[JSMain sharedInstance] setBusinessAreaFilters: [NSArray array]];
            
            DAMMediaViewBase *mediaView = [self launchMediaViewByIndex: 6];
            [mediaView sourceFromCarousel];

            
            /*DAMSalesPhotoViewController *controller =   [[DAMSalesPhotoViewController alloc] initWithNibName:@"DAMSalesPhotoViewController" bundle:nil];
            [controller setDamNavigation:self.damNavigator];
            [self.navigationController pushViewController:controller animated:YES];
            [controller release];
            break;
            */
            
            break;
            
        }
        case 1:
            [self lauchInfo];
            [[DAMApplicationModel model] suspendOps];
            break;
        case 0:
            //clear business area filter
            [[JSMain sharedInstance] setBusinessAreaFilters: [NSArray array]];
            [self launch:@"DAMSearchView" withNavigation:@""];
            [[DAMApplicationModel model] suspendOps];
            break;
    }
    
    
}

- (void)lauchInfo{
    DAMAssetView *assetView = [[DAMAssetView alloc] initWithNibName:@"DAMAssetView" bundle:nil];
    assetView.model = [NSDictionary dictionaryWithObjectsAndKeys: [JSMain getURLOfUserGuide] , @"url", @"How To Guide", @"title", nil];
    assetView.mediaType = @"UserGuide";
    
    [self pushView:assetView];
    //[self pushView:assetView withNavigation:@"HowToGuide-View"];
    [assetView release];
}

- (void)assetSelected:(NSDictionary*)asset {
    [self openAsset:asset];
}



- (void)setDisplayProgressView: (BOOL) display {
    dispatch_sync(updateUIQueue, ^{
        self.progressView.hidden = display;
    });
}
- (void)setDisplayActivityIndicatorView: (BOOL) display {
    dispatch_sync(updateUIQueue, ^{
        self.activityIndicatorView.hidden = display;
    });
}

@end
