//
//  DAMViewControllerBase.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMViewController.h"
#import "DAMMediaViewBase.h"
#import "DAMAssetView.h"
#import <QuartzCore/QuartzCore.h>

@interface DAMViewController ()

@end

@implementation DAMViewControllerBase
//@synthesize dataModel;

@synthesize damNavigator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationController.navigationBarHidden = YES;

//    [self.view addSubview:damNavigator.view];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    //self.dataModel = nil;
    self.damNavigator = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    //normally, views inherit the navigation from their parent in the pushView method.
    //Not so for the home screen (which has no parent). If the navigator has not been
    //set, create it now.
    
    if (!damNavigator) {
        //NSLog(@"DAMViewControllerBase.viewWillAppear creating navigator for self:%@", self);
        self.damNavigator = [[DAMNavigatorViewController alloc] initWithNibName:@"DAMNavigatorViewController" bundle:nil];
        //!!force creation of view before configuration navigation with Home.
        [self.view addSubview:damNavigator.view];
        damNavigator.delegate = self;
        [damNavigator setModel:@"navigationConfiguration"];
        [damNavigator pushNavigation:@"Home"];
        [damNavigator release];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}


- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if([[JSMain sharedInstance] isIOS7]) {
        [self adjustViewsForOrientation:toInterfaceOrientation];
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    //The device has already rotated, that's why this method is being called.
    UIInterfaceOrientation toOrientation   = [[UIDevice currentDevice] orientation];
    //fixes orientation mismatch (between UIDeviceOrientation and UIInterfaceOrientation)
    if (toOrientation == UIInterfaceOrientationLandscapeRight) toOrientation = UIInterfaceOrientationLandscapeLeft;
    else if (toOrientation == UIInterfaceOrientationLandscapeLeft) toOrientation = UIInterfaceOrientationLandscapeRight;
    
    UIInterfaceOrientation fromOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    [self willRotateToInterfaceOrientation:toOrientation duration:0.0];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self willAnimateRotationToInterfaceOrientation:toOrientation duration:[context transitionDuration]];
        [self adjustViewsForOrientation:toOrientation];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self didRotateFromInterfaceOrientation:fromOrientation];
    }];
    
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation {
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate adjustOrientation:orientation];
//    orientation = [UIApplication sharedApplication].statusBarOrientation;
//    BOOL isLandscapeLeft = orientation == UIDeviceOrientationLandscapeLeft;
//    if (isLandscapeLeft) {
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width , self.window.frame.size.height)];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = 20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }else{
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = -20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }
}

#pragma DAMNavigatorDelegate

- (BOOL)leftButtonTapped:(id)sender {
    
    /*ios 7 updated*/
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    
    NSLog(@"damNavigator.view %@", damNavigator.view);
    
    [damNavigator.view removeFromSuperview];

    //save a ref to the navigationController, because self.navigationController will be null
    //after popViewControllerAnimated
    
    UINavigationController *navigationController = self.navigationController;
    
    BOOL isPhotos = NO;
    
    if ([self isKindOfClass: [DAMAssetView class]]) {
        DAMAssetView *dav = (DAMAssetView *)self;
        isPhotos = [dav.mediaType isEqualToString:kMediaType_Photos] || [dav.mediaType isEqualToString:kMediaType_SalesPhotos];
    }
    if (isPhotos) {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController popViewControllerAnimated: NO];
        //[self.navigationController pushViewController:view animated: NO];
    }else {
        [self.navigationController popViewControllerAnimated:  kAnimateViewControllerPushes];
    }
    DAMViewControllerBase *newTop = (DAMViewControllerBase*)[navigationController topViewController];
    damNavigator.delegate = newTop;
    [newTop.view addSubview:damNavigator.view];
    
    //DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    [kAppDelegate.damViewController.homeView notificationTimerFired:nil];
    
    return YES;
}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMViewControllerBase.rightButtonTapped self:%@", self);
#endif
}

- (void)pushView:(DAMViewControllerBase*)view {
#ifdef DEBUG
    NSLog(@"DAMViewControllerBase.pushView view:%@", view);
#endif
    //the pushed view should inherit the parent's navigator
    view.damNavigator = self.damNavigator;
    damNavigator.delegate = view;
    [view.view addSubview:damNavigator.view];

    [self.navigationController pushViewController:view animated: kAnimateViewControllerPushes];
}

- (void)pushView:(DAMViewControllerBase*)view withNavigation:(NSString*)navigationName {
    
    NSLog(@"DAMViewControllerBase.pushView view:%@ vwithNavigation:%@", view, navigationName);
    
    //check we are not getting a double push of the same view:
    if ([self.navigationController.topViewController class] == [view class]) {
        NSLog(@"DAMViewControllerBase.pushView.ERROR_DOUBLE_PUSH");
        return;
    };
    //the pushed view should inherit the parent's navigator
    view.damNavigator = self.damNavigator;
    damNavigator.delegate = view;
    [view.view addSubview:damNavigator.view];

    [view.damNavigator pushNavigation:navigationName];
    
    BOOL isPhotos = NO;
    BOOL isBrochures = [navigationName isEqualToString:kMediaType_Brochures];
    
    if ([view isKindOfClass: [DAMAssetView class]]) {
        DAMAssetView *dav = (DAMAssetView *)view;
        isPhotos = [dav.mediaType isEqualToString:kMediaType_Photos] || [dav.mediaType isEqualToString:kMediaType_SalesPhotos];
    }
    if (isPhotos) {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromTop;
        
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:view animated:NO];
        //[self.navigationController pushViewController:view animated: NO];
    }else{
        if (isBrochures) {
            //need to perform init before push view
            [self.navigationController pushViewController:view animated: kAnimateViewControllerPushes];
            //then we need to reload data for ios 7
            
        }else{
            [self.navigationController pushViewController:view animated: kAnimateViewControllerPushes];
        }
        
    }
}

//Launch a media view by the index it appears in the Home_Configuration model (stored
//in dataModel).
- (DAMMediaViewBase*)launchMediaViewByIndex:(int)index {
    return [self launchMediaViewByIndex: index queryString: nil];
}

- (DAMMediaViewBase*)launchMediaViewByIndex: (int)index queryString: (NSString *)query{
    NSLog(@"launchMediaViewByIndex dataModel %@", kAppDelegate.dataModel);
    NSArray *mediaTypes = [kAppDelegate.dataModel objectForKey:@"mediaTypes"];
    if (!mediaTypes || [mediaTypes count] <= index) {
        NSLog(@"Invalid media index");
        return NULL;
    }
    NSDictionary *selectedMedia = [mediaTypes objectAtIndex:index];
    NSString *nextViewControllerClass = [selectedMedia objectForKey:@"viewController"];
    NSString *modelUrl = [selectedMedia objectForKey:@"model"];

    NSLog(@"DAMHomeView.mediaTypeButtonTapped nextViewControllerClass:%@ modelUrl:%@ selectedMedia %@", nextViewControllerClass, modelUrl, selectedMedia);
    DAMMediaViewBase *viewController = [[NSClassFromString(nextViewControllerClass) alloc] initWithNibName:nextViewControllerClass bundle:nil];
    viewController.dataModel = selectedMedia;
    /*IOS 7 FIXED*/
    
    [viewController mediaViewBaseInit];
    
    NSString *navigationName = [selectedMedia objectForKey:@"navigationName"];
    
    [self pushView:viewController withNavigation:navigationName];
    
    if ([[JSMain sharedInstance] isIOS7] && ([navigationName isEqualToString:kMediaType_Brochures] || [navigationName isEqualToString:kMediaType_Videos] || [navigationName isEqualToString:kMediaType_InThePress])) {
        //force init
        if ([[JSMain sharedInstance] isIOS7]) {
            [viewController hideUnusedCarousels];
        }
    }
    
    [viewController sourceFromQuery:query];
    
    return viewController;
}

#pragma launchMediaViewByNav Name

- (DAMMediaViewBase*)launchMediaViewByNavigationName: (NSString *)navName queryString: (NSString *)query {
    NSLog(@"launchMediaViewByIndex dataModel %@", kAppDelegate.dataModel);
    NSArray *mediaTypes = [kAppDelegate.dataModel objectForKey:@"mediaTypes"];
    
    if (!navName || navName.length == 0) {
        NSLog(@"Invalid media index");
        return NULL;
    }
    
    //NSDictionary *selectedMedia = [mediaTypes objectAtIndex:index];
    
    NSDictionary *selectedMedia = nil;
    for (NSDictionary *temp in mediaTypes) {
        if (![[temp objectForKey: @"navigationName"] isEqual: [NSNull null]] && [[temp objectForKey: @"navigationName"] isEqualToString: navName]) {
            selectedMedia = temp;
            break;
        }
    }
    if (!selectedMedia) {
        NSLog(@"Invalid navigation name %@", navName);
        return NULL;
    }
    
    
    NSString *nextViewControllerClass = [selectedMedia objectForKey:@"viewController"];
    NSString *modelUrl = [selectedMedia objectForKey:@"model"];
    
    NSLog(@"DAMHomeView.mediaTypeButtonTapped nextViewControllerClass:%@ modelUrl:%@ selectedMedia %@", nextViewControllerClass, modelUrl, selectedMedia);
    DAMMediaViewBase *viewController = [[NSClassFromString(nextViewControllerClass) alloc] initWithNibName:nextViewControllerClass bundle:nil];
    viewController.dataModel = selectedMedia;
    /*IOS 7 FIXED*/
    
    [viewController mediaViewBaseInit];
    
    NSString *navigationName = [selectedMedia objectForKey:@"navigationName"];
    
    [self pushView:viewController withNavigation:navigationName];
    
    if ([[JSMain sharedInstance] isIOS7] && ([navigationName isEqualToString:kMediaType_Brochures] || [navigationName isEqualToString:kMediaType_Videos] || [navigationName isEqualToString:kMediaType_InThePress])) {
        //force init
        if ([[JSMain sharedInstance] isIOS7]) {
            [viewController hideUnusedCarousels];
        }
    }
    
    [viewController sourceFromQuery:query];
    
    return viewController;
}


- (DAMViewControllerBase*)launch:(NSString*)controllerClass withNavigation:(NSString*)navigationName {
#ifdef DEBUG
    NSLog(@"DAMHomeView.launch controllerClass:%@ navigationName:%@", controllerClass, navigationName);
#endif
    DAMViewControllerBase *viewController = [[NSClassFromString(controllerClass) alloc] initWithNibName:controllerClass bundle:nil];
    viewController.dataModel = kAppDelegate.dataModel;
    [self pushView:viewController];
    
    return viewController;
}

- (void)openAsset:(NSDictionary*)asset {
    DAMAssetView *assetView = [[DAMAssetView alloc] initWithNibName:@"DAMAssetView" bundle:nil];
    assetView.model = asset;
    assetView.mediaType = [asset objectForKey:@"mediaType"];
    
    [self pushView:assetView withNavigation:@"Brochure-View"]; 
}

@end
