//
//  DAMHomeView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMViewControllerBase.h"
#import "DAMIDriveNavigatorViewController.h"
#import "JSSync.h"

@interface DAMHomeView : DAMViewControllerBase<DAMIDriveNavigatorViewDelegate, UIAlertViewDelegate, UIScrollViewDelegate> {
    
//    NSDictionary *dataModel;
    
    NSTimer *animationTimer;
    NSTimer *notificationTimer;
    int mediaTypeIndex;

    UIImageView *mainImage;
    UIImageView *mainImageNext;
    UIImageView *teaserTitle;
    UIScrollView *mediaTypeCarousel;
    
    NSString *msgUpdated;
}
//NSDictionary *dataModel;
//@property (nonatomic, retain) NSDictionary *dataModel;

@property (nonatomic, retain) NSTimer *animationTimer;
@property (nonatomic, retain) NSTimer *notificationTimer;

@property (nonatomic, retain) IBOutlet UIView *downloadingView;
@property (nonatomic, retain) IBOutlet UIView *noWifiView;
@property (nonatomic, retain) IBOutlet UILabel *remainedAssetsLabel;

@property (nonatomic, retain) IBOutlet UIImageView *mainImage;
@property (nonatomic, retain) IBOutlet UIImageView *mainImageNext;
@property (nonatomic, retain) IBOutlet UIImageView *teaserTitle;
@property (nonatomic, retain) IBOutlet UIScrollView *mediaTypeCarousel;

@property (nonatomic, retain) IBOutlet UILabel *notificationLabel;
@property (nonatomic, retain) IBOutlet UILabel *progressLabel;
@property (nonatomic, retain) IBOutlet UILabel *noticeLabel;
@property (nonatomic, retain) IBOutlet UIProgressView *progressView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicatorView;

- (IBAction)viewNowTapped:(id)sender;
- (void) updateNotificationLogs:(NSString *) msg;
- (void)hideDowloadingProgress:(id)sender;

- (void)notificationTimerFired:(id)sender;

- (void)resetNotificationLogs;
- (void)showUpdatingMessage:(id)sender;

- (void)setDisplayProgressView: (BOOL) display;
- (void)setDisplayActivityIndicatorView: (BOOL) display;

@end
