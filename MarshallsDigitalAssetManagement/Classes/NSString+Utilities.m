//
//  NSString+Utilities.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 14/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+Utilities.h"

@implementation NSString(Utilities)

#pragma mark utilities
- (NSString*)encodeURL
{
	NSString *newString = [NSMakeCollectable(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)self, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding))) autorelease];
	if (newString) {
		return newString;
	}
	return @"";
}

- (float) textWidthWithFont:(UIFont *)font{
    // Get the width of a string ...
                   // Get the width of a string when wrapping within a particular width
                   CGSize size = [self sizeWithFont: font
                                                 forWidth:300.0
                                            lineBreakMode:UILineBreakModeWordWrap];
    return size.width;
}

@end
