//
//  DAMSettingsView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMViewControllerBase.h"
#import "DAMEmailChooserView.h"
#import "DAMMediaChangeView.h"

@interface DAMSettingsView : DAMViewControllerBase<DAMNavigatorDelegate, DAMEmailChooserViewDelegate, UIPopoverControllerDelegate,
    DAMMediaChangeViewDelegate> {
    
}
@property (nonatomic, retain) IBOutlet UILabel *emailLabel;
@property (nonatomic, retain) IBOutlet UIButton *emailChoiceButton;

@property (nonatomic, retain) IBOutletCollection(UISwitch) NSArray *domesticSwitches;
@property (nonatomic, retain) IBOutletCollection(UISwitch) NSArray *commercialSwitches;

@property (nonatomic, retain) IBOutlet UISwitch *allContentSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *allDomesticSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *allCommercialSwitch;

- (IBAction)emailChoiceTapped:(id)sender;

- (IBAction)allContentValueChanged:(UISwitch*)sender;
- (IBAction)allDomesticValueChanged:(UISwitch*)sender;
- (IBAction)allCommercialValueChanged:(UISwitch*)sender;

- (IBAction)switchValueChanged:(UISwitch*)sender;

@end
