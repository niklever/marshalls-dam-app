//
//  DAMShareableView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 16/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMShareableView.h"

@interface DAMShareableView () {

    DAMMediaSaveView *saveView;
    DAMMediaSendView *sendView;
}
@end

@implementation DAMShareableView
@synthesize saveView;
@synthesize sendView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    self.sendView = nil;
    self.saveView = nil;
}

- (void)saveButtonTapped:(UIButton*)sender {
    NSArray *selectedAssets = [self selectedMedia];
    if (selectedAssets == NULL || [selectedAssets count] == 0) {
        [JSMain showAlertWithMessage:@"No media selected."];
    }else{
        self.saveView = [[DAMMediaSaveView alloc] initWithNibName:@"DAMMediaSaveView" bundle:nil];
        saveView.delegate = self;
        [self.view addSubview:saveView.view];
        [saveView animateOnStage];
    }
}

- (void)shareButtonTapped:(UIButton*)sender {
    NSArray *selectedAssets = [self selectedMedia];
    if (selectedAssets == NULL || [selectedAssets count] == 0) {
        [JSMain showAlertWithMessage:@"No media selected."];
    }else{
        //    [damNavigator clearCustomButtonView];
        self.sendView = [[DAMMediaSendView alloc] initWithNibName:@"DAMMediaSendView" bundle:nil];
        sendView.delegate = self;
        [self.view addSubview:sendView.view];
        [sendView animateOnStage];
    }
}

- (void)cancelButtonTapped:(UIButton*)sender {
    //    [damNavigator clearCustomButtonView];
    [damNavigator dismissRollout];
    
    [self setSelectionMode:NO];
}

- (void)setSelectionMode:(BOOL)selectionMode { }

- (void)saveActionedToFolder:(NSString*)folder {
#ifdef DEBUG
    NSLog(@"DAMBrochureView.saveActionedToFolder folder:%@", folder);
#endif
    [saveView.view removeFromSuperview];
    self.saveView = nil;
    
//    NSMutableArray *selectedAssets = [NSMutableArray arrayWithArray:topScrollView.selectedAssets];
//    [selectedAssets addObjectsFromArray:bottomScrollView.selectedAssets];
    
    NSArray *selectedAssets = [self selectedMedia];
    
    if (selectedAssets == NULL || [selectedAssets count] == 0) {
        [JSMain showAlertWithMessage:@"No media selected."];
        [damNavigator dismissRollout];
    }else{
        //Beware, setSelectionMode:NO clears the selectedAssets array.
        [self setSelectionMode:NO];
        //    [damNavigator clearCustomButtonView];
        [damNavigator dismissRollout];
        
        [[DAMApplicationModel model] iDriveCopyAssets:selectedAssets toFolder:folder];
    }
}

- (void)sendMediaTapped {
    
    NSArray *selectedAssets = [self selectedMedia];
    if (selectedAssets == NULL || [selectedAssets count] == 0) {
        [JSMain showAlertWithMessage:@"No media selected."];
    }else{
    
        NSMutableArray *mediaRefs = [NSMutableArray arrayWithCapacity:selectedAssets.count];
        
        for (NSDictionary *asset in selectedAssets) {
            NSString *mediaRef = [asset objectForKey:@"mediaRef"];
            [mediaRefs addObject:mediaRef];
        }
        [[DAMApplicationModel model] emailAssets:mediaRefs toRecipient:sendView.emailAddress.text withSubject:sendView.subject.text andMessage:sendView.message.text];
        
        //    set selection mode to NO
        [self setSelectionMode:NO];
    }
    
    [damNavigator dismissRollout];
}

- (void)saveCancelled {

}

- (void)cancelMediaSendTapped {
}

- (void)modalViewOffstageAnimationDidFinish:(DAMModalViewBase*)modalView {
    if (sendView) {
        [sendView.view removeFromSuperview];
        self.sendView = nil;
    }
    if (saveView) {
        [saveView.view removeFromSuperview];
        self.saveView = nil;
    }
}

@end
