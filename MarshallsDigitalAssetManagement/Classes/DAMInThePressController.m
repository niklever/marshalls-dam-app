//
//  DAMInThePressController.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/24/14.
//
//

#import "DAMInThePressController.h"
#import "DAMMediaSaveView.h"
#import "DAMMediaSendView.h"
#import "DAMIDriveNavigatorViewController.h"
#import "DAMAssetView.h"

#import "JSMain.h"
#import "DAMUIDefaults.h"

#import "DAMInthePressCell.h"

#define kIdentifier @"DAMInThePressCell"

@interface DAMInThePressController () <DAMMediaSendViewDelegate, DAMMediaSaveViewDelegate, DAMIDriveNavigatorViewDelegate>{
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UICollectionView *vCollection;
    
    NSMutableArray *arrInThePress;
    IBOutlet UIPageControl *pageControll;
    IBOutlet UIView *vShare;
    IBOutlet UIButton *btnShare;
    
    NSMutableArray *arrSelectedItem;
    BOOL isSelectedMode;
}

@end

@implementation DAMInThePressController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self setupBeforeLaunchPage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if([[JSMain sharedInstance] isIOS7]) {
        [self adjustViewsForOrientation:toInterfaceOrientation];
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation {
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    BOOL isLandscapeLeft = orientation == UIDeviceOrientationLandscapeLeft;
    if (isLandscapeLeft) {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width , self.window.frame.size.height)];
        CGRect viewBounds = [appDelegate.window bounds];
        viewBounds.origin.x = 20;
        viewBounds.size.height = viewBounds.size.height;
        appDelegate.window.frame = viewBounds;
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        CGRect viewBounds = [appDelegate.window bounds];
        viewBounds.origin.x = -20;
        viewBounds.size.height = viewBounds.size.height;
        appDelegate.window.frame = viewBounds;
    }
}


- (void)dealloc {
    [lblTitle release];
    [vCollection release];
    [arrInThePress release];
    [arrSelectedItem release];
    [pageControll release];
    [vShare release];
    [btnShare release];
    [super dealloc];
}
- (void)viewDidUnload {
    [lblTitle release];
    lblTitle = nil;
    [vCollection release];
    vCollection = nil;
    [super viewDidUnload];
}

#pragma mark - IBACTION
- (IBAction)didClickHome:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)didClickExport:(id)sender {
    isSelectedMode  =   YES;
    [self showShareView];
}

- (IBAction)didClickIdrive:(id)sender {
    
    /*
     carouselImage = "menu-brochures.png";
     mainImage = "brochures.png";
     model = Brochures;
     navigationName = Brochures;
     teaserTitle = "title-brochures.png";
     title = BROCHURES;
     viewController = DAMBrochureView;
     
     */
    
    DAMIDriveNavigatorViewController *iDriveController  =   [[DAMIDriveNavigatorViewController alloc] initWithNibName:@"DAMIDriveNavigatorViewController" bundle:nil];
    [_damNavigation setIsNewCase:YES];
    [iDriveController setDamNavigator:_damNavigation];
    [iDriveController.view addSubview:_damNavigation.view];
    [iDriveController.damNavigator pushNavigation:@"iDrive"];
    [iDriveController setDelegate:self];
    
    
    
    [self.navigationController pushViewController:iDriveController animated:YES];
    [iDriveController release];
}

- (IBAction)didClickSearchOn:(id)sender {
    
    DAMAssetView *assetView = [[DAMAssetView alloc] initWithNibName:@"DAMAssetView" bundle:nil];
    assetView.model = [NSDictionary dictionaryWithObjectsAndKeys: [JSMain getURLOfUserGuide] , @"url", @"How To Guide", @"title", nil];
    assetView.mediaType = @"UserGuide";
    
    [_damNavigation setIsNewCase:YES];
    [assetView setDamNavigator:_damNavigation];
    [assetView.view addSubview:_damNavigation.view];
    [assetView.damNavigator pushNavigation:@"HowToGuide-View"];
    
    [self.navigationController pushViewController:assetView animated:YES];
    [assetView release];
    
}

- (IBAction)didClickCancel:(id)sender {
    
    [self hideShareView];
    
}

- (IBAction)didClickSend:(id)sender {
    
    if([arrSelectedItem count] == 0){
        [self showErrorMessage];
    }
    else{
        /* open send view */
        DAMMediaSendView *sendView  =   [[DAMMediaSendView alloc] initWithNibName:@"DAMMediaSendView" bundle:nil];
        sendView.delegate   =   self;
        [self addChildViewController:sendView];
        [self.view addSubview:sendView.view];
        [sendView animateOnStage];
        [sendView release];
    }
    
}

- (IBAction)didClickSave:(id)sender {
    
    if([arrSelectedItem count] == 0){
        [self showErrorMessage];
    }
    else{
        /* open save view */
        DAMMediaSaveView *saveView  =   [[DAMMediaSaveView alloc] initWithNibName:@"DAMMediaSaveView" bundle:nil];
        saveView.delegate           =   self;
        [self addChildViewController:saveView];
        [self.view addSubview:saveView.view];
        [saveView animateOnStage];
        [saveView release];
    }
    
}

#pragma mark - private method

- (void) setupBeforeLaunchPage{
    lblTitle.font = [[DAMUIDefaults defaults] navigatorTitleFont];
    
    [vCollection registerNib:[UINib nibWithNibName:@"DAMInThePressCell" bundle:nil] forCellWithReuseIdentifier:kIdentifier];
    
    arrInThePress   =   [NSMutableArray new];
    
    
    
    
    for(int index = 0; index < 10; index++ ){
        NSDictionary *dict  =   [NSDictionary dictionaryWithObjectsAndKeys:@"Marshalls Making The Headlines", @"title", @"Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat", @"description", @"http://www.marshalls.co.uk/dam-svc/Thumbnails/ded7e420-d75c-4a98-bb8b-8c71bf6b3be2.png", @"thumbnail", nil];
        
        [arrInThePress addObject:dict];
    }
    
    kAppDelegate.inThePressCount    =   [arrInThePress count];
    
    int page    =   [arrInThePress count] / itemPerPage;
    
    if([arrInThePress count] % itemPerPage != 0){
        page    =   page + 1;
    }
    
    [pageControll setNumberOfPages:page];
    
    arrSelectedItem =   [NSMutableArray new];
}

- (void) showShareView{
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         
                         [btnShare setAlpha:0.0];
                         [vShare setFrame:CGRectMake(540, 0, vShare.frame.size.width, vShare.frame.size.height)];
                         
                     } completion:^(BOOL finished) {
                         
                         [btnShare setHidden:YES];
                         
                     }];
    
}

- (void) hideShareView{
    
    [btnShare setHidden:NO];
    isSelectedMode  =   NO;
    [arrSelectedItem removeAllObjects];
    [vCollection reloadData];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         
                         [btnShare setAlpha:1.0];
                         [vShare setFrame:CGRectMake(1024, 0, vShare.frame.size.width, vShare.frame.size.height)];
                         
                     } completion:^(BOOL finished) {
                         
                         
                     }];
}


- (void) showErrorMessage{
    UIAlertView *alvError   =   [[UIAlertView alloc] initWithTitle:nil
                                                           message:@"no media selected."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
    [alvError show];
    [alvError release];
}


#pragma mark - UICollectionView Data source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrInThePress count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DAMInThePressCell *cell =   (DAMInThePressCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kIdentifier forIndexPath:indexPath];
    
    NSDictionary *itemDict  =   [arrInThePress objectAtIndex:indexPath.row];
    
    [cell loadDataWithImage:[itemDict objectForKey:@"thumbnail"]
                   andTitle:[itemDict objectForKey:@"title"]
             andDescription:[itemDict objectForKey:@"description"]];
    
    [cell showHideCheckImage:[arrSelectedItem containsObject:[NSNumber numberWithInt:indexPath.row]]];
    
    
    return cell;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DAMInThePressCell *cell =   (DAMInThePressCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSNumber *itemIndex     =   [NSNumber numberWithInt:indexPath.row];
    
    if(isSelectedMode){
        /* add item into selected box */
        if([arrSelectedItem containsObject:itemIndex]){
            [arrSelectedItem removeObject:itemIndex];
            [cell showHideCheckImage:NO];
        }
        else{
            [arrSelectedItem addObject:[NSNumber numberWithInt:indexPath.row]];
            [cell showHideCheckImage:YES];
        }
    }
    else{
        /* open the pdf view to read file */
        /*
         {
         HasCrop = 0;
         Priority = 0;
         UpdatedDate = "2013-08-07T16:57:20.147";
         container = Stonemarket;
         mediaRef = "ded7e420-d75c-4a98-bb8b-8c71bf6b3be2";
         mediaType = Brochures;
         tags =     (
         nv,
         marshalls,
         brochure,
         2013
         );
         thumbnail = "/Thumbnails/ded7e420-d75c-4a98-bb8b-8c71bf6b3be2.png";
         title = "Brochure private market 2013";
         url = "/AssetStore/ded7e420-d75c-4a98-bb8b-8c71bf6b3be2.pdf";
         }
         
         */
        
        NSDictionary *dictItem  =   [NSDictionary dictionaryWithObjectsAndKeys:
                                     [NSNumber numberWithInt:0], @"HasCrop",
                                     [NSNumber numberWithInt:0], @"Priority",
                                     @"2013-08-07T16:57:20.147", @"UpdatedDate",
                                     @"Stonemarket", @"container",
                                     @"ded7e420-d75c-4a98-bb8b-8c71bf6b3be2", @"mediaRef",
                                     kMediaType_Brochures, @"mediaType",
                                     @"/Thumbnails/ded7e420-d75c-4a98-bb8b-8c71bf6b3be2.png", @"thumbnail",
                                     @"Brochure private market 2013", @"title",
                                     @"/AssetStore/ded7e420-d75c-4a98-bb8b-8c71bf6b3be2.pdf", @"url", nil];
        
        DAMAssetView *assetView = [[DAMAssetView alloc] initWithNibName:@"DAMAssetView" bundle:nil];
        [_damNavigation setIsNewCase:YES];
        [assetView setDamNavigator:_damNavigation];
        [assetView.view addSubview:_damNavigation.view];
        [assetView.damNavigator pushNavigation:kMediaType_Brochures];
        assetView.model = dictItem;
        assetView.mediaType = kMediaType_Brochures;
        
        [self.navigationController pushViewController:assetView animated:YES];
        [assetView release];
        
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
    [pageControll setCurrentPage:nearestNumber];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
    [pageControll updateCurrentPageDisplay];
}

#pragma mark - UICoverFlowLayout Delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(340, 310);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0f;
}


#pragma mark - DAMSendViewDelegate
- (void)sendMediaTapped:(DAMMediaSendView *)sendView{
    
    NSMutableArray *mediaRefs   =   [[NSMutableArray alloc] init];
    [arrSelectedItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSNumber *indexSelected =   (NSNumber *)obj;
        [mediaRefs addObject:[arrInThePress objectAtIndex:[indexSelected intValue]]];
        
    }];
    
    
    [[DAMApplicationModel model] emailAssets:mediaRefs toRecipient:sendView.emailAddress.text withSubject:sendView.subject.text andMessage:sendView.message.text];
    
    [mediaRefs release];
    mediaRefs   =   nil;
    
    [self hideShareView];
    
    [sendView.view removeFromSuperview];
    [sendView removeFromParentViewController];
}

- (void)cancelMediaSendTapped:(DAMMediaSendView *)sendView{
    [sendView.view removeFromSuperview];
    [sendView removeFromParentViewController];
}


#pragma mark - DAMMedidaSaveDelegate
- (void)saveActioned:(DAMMediaSaveView *)saveView ToFolder:(NSString*)folder{
    
    NSMutableArray *mediaRefs   =   [[NSMutableArray alloc] init];
    [arrSelectedItem enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSNumber *indexSelected =   (NSNumber *)obj;
        [mediaRefs addObject:[arrInThePress objectAtIndex:[indexSelected intValue]]];
        
    }];
    
    [[DAMApplicationModel model] iDriveCopyAssets:mediaRefs toFolder:folder];
    
    
    [mediaRefs release];
    mediaRefs   =   nil;
    
    [self hideShareView];
    
    [saveView.view removeFromSuperview];
    [saveView removeFromParentViewController];
    
}

- (void)saveCancelled:(DAMMediaSaveView *)saveView{
    
    [saveView.view removeFromSuperview];
    [saveView removeFromParentViewController];
    
}

#pragma mark - iDriveControllerDelegate
- (void)assetSelected:(NSDictionary*)asset{
    
}

@end
