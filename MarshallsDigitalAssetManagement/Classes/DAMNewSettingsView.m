//
//  DAMNewSettingsView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Hoa Truong Huu on 1/8/13.
//
//

#import "DAMNewSettingsView.h"
#import "JSDBV2.h"
#import "NSDictionary_JSONExtensions.h"
#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"
#import "JSMain.h"

#define TIME_FIRE_UPDATE_SETTING 1.5//3 //2 seconds

@interface DAMNewSettingsView () {
    
    __weak IBOutlet UIScrollView *scrollView;
}

@property (nonatomic, retain) NSArray *config;

@property (nonatomic, retain) UIPopoverController *emailChoicePopover;

@property (nonatomic, retain) NSMutableArray *containersTurnedOn;
@property (nonatomic, retain) NSMutableArray *containersTurnedOff;

@property (nonatomic, retain) DAMMediaChangeView *mediaChangeView;

@property (nonatomic, retain) NSArray *buttons;

@end



@implementation DAMNewSettingsView

@synthesize videosButton,samplesButton, brochuresButton;
@synthesize config;
@synthesize emailChoiceButton;
@synthesize emailTextField;
@synthesize emailChoicePopover;
@synthesize containersTurnedOff;
@synthesize containersTurnedOn;
@synthesize mediaChangeView;
@synthesize buttons;
//@synthesize emailCheckingRequest;

- (void)dealloc{
    
    self.config = nil;
    
    self.emailChoiceButton = nil;
    
    self.emailTextField = nil;
    
    self.emailChoicePopover = nil;
    
    self.containersTurnedOff = nil;
    
    self.containersTurnedOn = nil;
    
    self.mediaChangeView = nil;
    
    self.buttons = nil;
    
    [super dealloc];
}

#pragma mark - IBAction

- (IBAction)buttonTapped:(UIButton *)button{
    button.selected = !button.selected;
}

- (IBAction)emailChoiceTapped:(id)sender {
    DAMEmailChooserView *chooserView = [[DAMEmailChooserView alloc] initWithNibName:@"DAMEmailChooserView" bundle:nil];
    chooserView.delegate = self;
    self.emailChoicePopover = [[[UIPopoverController alloc] initWithContentViewController:chooserView] autorelease];
    self.emailChoicePopover.delegate = self;
    CGRect presentRect = CGRectMake(0, self.emailChoiceButton.frame.size.height / 2, 0, 0);
    [self.emailChoicePopover presentPopoverFromRect:presentRect inView:self.emailChoiceButton permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    [chooserView release];
}




- (IBAction)downloadAllVideosTapped:(id)sender {
    isCheckVideoButtonDirty = true;
    BOOL state = [[JSMain sharedInstance] isDownloadAllVideos];
    //update state
    [[JSMain sharedInstance] updateDownloadAllVideos: !state];
    
    /*[self setActiveButtonStateOfMediaType: !state ? kMediaType_Videos : @""];*/

    [self updateDownloadAllButtonStates];
}

- (IBAction)downloadAllBrochuresTapped:(id)sender {
    isCheckBrochuresButtonDirty = true;
    BOOL state = [[JSMain sharedInstance] isDownloadAllBrochures];
    //update state
    [[JSMain sharedInstance] updateDownloadAllBrochures:!state];
    /*[self setActiveButtonStateOfMediaType: !state ? kMediaType_Brochures : @""];*/
    [self updateDownloadAllButtonStates];
}

- (IBAction)downloadAllSamplesTapped:(id)sender {
    isCheckSamplesButtonDirty = true;
    BOOL state = [[JSMain sharedInstance] isDownloadAllSamples];
    //update state
    [[JSMain sharedInstance] updateDownloadAllSamples:!state];
    /*[self setActiveButtonStateOfMediaType: !state ? kMediaType_Samples : @""];*/
    [self updateDownloadAllButtonStates];
}

- (void)updateDownloadAllButtonStates {
    [self updateVideosButtonState];
    [self updateBrochuresButtonState];
    [self updateSamplesButtonState];
}



- (IBAction)emailAdminstratorTapped:(id)sender{
    if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
		controller.mailComposeDelegate = self;
        [controller setSubject:@"App settings change request"];

//        [controller setMessageBody:message isHTML:YES];
        [controller setToRecipients:[NSArray arrayWithObjects:@"appsettings@marshalls.co.uk", nil]];
        
		if (controller) {
            
            DAMAppDelegate *appDelegate = (DAMAppDelegate *)[UIApplication sharedApplication].delegate;
            
            [appDelegate.damViewController presentModalViewController:controller animated:YES];
        }
		[controller release];
	}else {
        [JSMain showAlertWithMessage:@"Please set up a mail account to send message."];
	}
}


#pragma mark Private Function

- (void) containersList: (void (^)(NSArray *))callbackFunc {
    //NSMutableArray * result = [[JSDBV2 sharedInstance] sql:@"SELECT * FROM containers" params:nil];
    [[JSDBV2 sharedInstance] executeQuery:@"SELECT * FROM containers" params:nil successCallback:^(NSMutableArray * result) {
        callbackFunc([result sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"businessArea" ascending:NO]]]);
    }];
}

/*
- (NSArray *)containersList {
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM containers"];
    return [[[JSDBV2 sharedInstance] sql:sql params:nil success:nil fail:nil] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"businessArea" ascending:NO]]];
 
    //NSArray *containers =  [[JSDB sharedInstance] sql:[NSString stringWithFormat:@"SELECT * FROM containers"] params:nil success:nil fail:nil];
    //return containers;
}*/

/*
- (NSDictionary*)configForTag:(int)tag {
    NSDictionary *result = nil;
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM containers WHERE tag=%d", tag];
    //pKt updated to use common functions in JSDB
    //NSArray *rows = [[DAMApplicationModel model] getRows:sql];
    NSArray *rows = [[JSDB sharedInstance] sql:sql params:nil success:nil fail:nil];
    if (rows!=nil &&rows.count>0){
        result = [rows objectAtIndex:0];
    }
    
    return result;
}
*/
 

- (void)actionChanges {
#ifdef DEBUG
    NSLog(@"DAMSettingsView.actionChanges");
#endif
    [[JSMain sharedInstance] setUserEmail: self.emailTextField.text];
    [[DAMApplicationModel model] startSyncingWithEmail:self.emailTextField.text forceRemove:true validUserCallback:^{
        
    } invalidUserCallBack:^{
    }];
}

- (void)confirmDelete {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                      message:@"Do you want to change your email address?"
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message addButtonWithTitle:@"Cancel"];
    [message show];
}

- (void)contactChosen:(NSDictionary*)contactModel {
    NSString *email = [contactModel objectForKey:@"email"];
    //[[DAMApplicationModel model] setUserEmail:email];
    self.emailTextField.text = [NSString stringWithFormat:@"%@", email];
    [self.emailChoicePopover dismissPopoverAnimated:YES];
    
    /*if (![JSMain validateEmail:email]) {
        [JSMain showAlertWithMessage:@"Please check your email address."];
    }*/
    
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.emailChoicePopover = nil;
}

- (void)modalViewOffstageAnimationDidFinish:(DAMModalViewBase*)modalView {
}

#pragma mark Public Function

- (void) quitScreen {
    [[DAMApplicationModel model] enableOps];
    
    [super leftButtonTapped: nil];
    [damNavigator popNavigation];
}

- (BOOL)leftButtonTapped:(id)sender {
    //cancel perform
    /*
    if (self.emailCheckingRequest != NULL && self.emailCheckingRequest.isExecuting) {
        [self.emailCheckingRequest cancel];
        self.emailCheckingRequest = nil;
    }*/
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(performUpdateUserSettingsFromServer) object:nil];
    
    
    NSString *email = self.emailTextField.text;
    if ([email length] > 0 && ![JSMain validateEmail:email]) {
        [JSMain showAlertWithMessage:@"Please check your email address."];
        //enable left button again
        damNavigator.leftButton.enabled = YES;
        return NO;
    }else{
        
    }
    
    /*abort current download*/
    [[JSSync sharedInstance] abortQueue: nil
                               callback:^{
                                   NSLog(@"abort queue");
                               }];
    
    if ([[JSMain sharedInstance] online]) {
        //abort current performselector
        [[JSMain sharedInstance] setValidUserEmail: @""];
        
        [[DAMApplicationModel model] startSyncingWithEmail: self.emailTextField.text forceRemove:false validUserCallback:^{
            //update user email
            
#ifdef DEBUG
            NSLog(@"performUpdateUserSettingsFromServer update useremail to == %@", self.emailTextField.text);
#endif
            
            [[JSMain sharedInstance] setUserEmail: self.emailTextField.text];
            [[JSMain sharedInstance] setValidUserEmail: self.emailTextField.text];
            
            if (![[JSMain sharedInstance] onWifi] && ([[JSMain sharedInstance] isDownloadAllVideos] || [[JSMain sharedInstance] isDownloadAllSamples] || [[JSMain sharedInstance] isDownloadAllBrochures])) {
                [JSMain showAlertWithMessage: kNoWifiToDownloadAssets];
                if (isCheckVideoButtonDirty) {
                    //reset Videos
                    [[JSMain sharedInstance] updateDownloadAllVideos: false];
                }
                if (isCheckSamplesButtonDirty) {
                    //reset Videos
                    [[JSMain sharedInstance] updateDownloadAllSamples:false];
                }
                if (isCheckBrochuresButtonDirty) {
                    //reset Videos
                    [[JSMain sharedInstance] updateDownloadAllBrochures:false];
                }
            }
            
            [self quitScreen];
            //[damNavigator leftButtonTapped:self];
            
        } invalidUserCallBack:^{
            if ([[JSMain sharedInstance] userEmail] == NULL || [[[JSMain sharedInstance] userEmail] length] == 0) {
                [[JSMain sharedInstance] setUserEmail: self.emailTextField.text];
                [self quitScreen];
            }else{
                //show alert
                if (hasUserTypeSomething)
                    [self confirmDelete];
                else {
                    [self quitScreen];
                }
            }
        }];
    }else{
        if (![[JSMain sharedInstance] onWifi] && ([[JSMain sharedInstance] isDownloadAllVideos] || [[JSMain sharedInstance] isDownloadAllSamples] || [[JSMain sharedInstance] isDownloadAllBrochures])) {
            if (isCheckVideoButtonDirty) {
                //reset Videos
                [[JSMain sharedInstance] updateDownloadAllVideos: false];
            }
            if (isCheckSamplesButtonDirty) {
                //reset Videos
                [[JSMain sharedInstance] updateDownloadAllSamples:false];
            }
            if (isCheckBrochuresButtonDirty) {
                //reset Videos
                [[JSMain sharedInstance] updateDownloadAllBrochures:false];
            }
        }
        
        [JSMain showAlertWithMessage: kNoWifiToDownloadAssets];
        [self quitScreen];
    }
    return NO;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"OK"]){
        [self actionChanges];
        //[super leftButtonTapped: self];
        [self quitScreen];
    }else{
#ifdef DEBUG
        NSLog(@"Cancel remove was called.");
#endif
        //need to enable X button again
        damNavigator.leftButton.enabled = YES;
    }
}

#pragma mark MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)_result
                        error:(NSError*)error;
{
	/*
	 @constant   MFMailComposeResultCancelled   User canceled the composition.
	 @constant   MFMailComposeResultSaved       User successfully saved the message.
	 @constant   MFMailComposeResultSent        User successfully sent/queued the message.
	 @constant   MFMailComposeResultFailed      User's attempt to save or send was unsuccessful.
	 */
	if (_result == MFMailComposeResultSent) {
        //		[Utils showAlertViewWithTitle:nil withMessage:@"You successfully sent/queued the message."];
	}else if (_result == MFMailComposeResultFailed) {
        [JSMain showAlertWithTitle:@"Warning" message:@"Your attempt to save or send was unsuccessful."];
	}else if (_result == MFMailComposeResultSaved) {
        [JSMain showAlertWithTitle:nil message:@"You successfully saved the message."];
	}
    
    DAMAppDelegate *appDelegate = (DAMAppDelegate *)[UIApplication sharedApplication].delegate;
    
	[appDelegate.damViewController dismissModalViewControllerAnimated:YES];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //need to fire time interval checking
    NSString *tempEmail = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_performCheckingEmailSettings) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(_performCheckingEmailSettings:) withObject:tempEmail afterDelay:TIME_FIRE_UPDATE_SETTING];
    isEmailFieldDirty = TRUE;
    hasUserTypeSomething = TRUE;
    return YES;
}

- (void)_performCheckingEmailSettings:(NSString *)userEmail{
    if (userEmail && [userEmail length] > 0 && [JSMain validateEmail:userEmail]) {
        [[APICommon sharedInstance] getUserSettings: userEmail
                                    successCallback:^(id resp) {
                                        NSDictionary *dictionary = resp;
#ifdef DEBUG
                                        NSLog(@"_performCheckingEmailSettings.dictionary = %@", dictionary);
#endif
                                        
                                        [self containersList:^(NSArray * containers) {
                                            NSMutableArray *userContainers = [NSMutableArray arrayWithCapacity: [containers count]];
                                            
                                            if (dictionary != NULL) {
                                                NSArray *selectedContainers = [dictionary objectForKey:@"Specialities"];
                                                
                                                BOOL isOn = false;
                                                for (NSDictionary *con in containers ) {
                                                    NSMutableDictionary * vCon = [NSMutableDictionary dictionaryWithDictionary: con];
                                                    isOn = false;
                                                    for (NSString *name in selectedContainers) {
                                                        if ([[con objectForKey:@"name"] isEqual:name]) {
                                                            isOn = true;
                                                            break;
                                                        }
                                                    }
                                                    [vCon setValue: (isOn ? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0]) forKey:@"state"];
                                                    [userContainers addObject: vCon];
                                                }
                                            }else{
                                                //null user then all container must reset
                                                for (NSDictionary *con in containers ) {
                                                    NSMutableDictionary * vCon = [NSMutableDictionary dictionaryWithDictionary: con];
                                                    [vCon setValue: [NSNumber numberWithInt:0] forKey:@"state"];
                                                    [userContainers addObject: vCon];
                                                }
                                            }
                                            [self layoutContainers: userContainers];
                                        }];
                                    }
                                    andFailCallback:^(id msg) {
                                        NSLog(@"FAILED to get user Settings");
                                    }];
        
        
        //NSString *url = [NSString stringWithFormat:@"%@/webapi/UserSettings/%@",[JSMain apiHost], userEmail];
#ifdef DEBUG
        NSLog(@"_performCheckingEmailSettings from API %@", url);
#endif
        
        /*
        if (self.emailCheckingRequest != NULL && self.emailCheckingRequest.isExecuting) {
            [self.emailCheckingRequest cancel];
            self.emailCheckingRequest = nil;
        }
        
        self.emailCheckingRequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString: url]];
        [self.emailCheckingRequest setCompletionBlock:^{
#ifdef DEBUG
            NSLog(@"_performCheckingEmailSettings.responseString = %@", emailCheckingRequest.responseString);
#endif
            NSDictionary *dictionary = [NSDictionary dictionaryWithJSONString:emailCheckingRequest.responseString error:nil];
#ifdef DEBUG
            NSLog(@"_performCheckingEmailSettings.dictionary = %@", dictionary);
#endif
            
            [self containersList:^(NSArray * containers) {
                NSMutableArray *userContainers = [NSMutableArray arrayWithCapacity: [containers count]];
                
                if (dictionary != NULL) {
                    NSArray *selectedContainers = [dictionary objectForKey:@"Specialities"];
                    
                    BOOL isOn = false;
                    for (NSDictionary *con in containers ) {
                        NSMutableDictionary * vCon = [NSMutableDictionary dictionaryWithDictionary: con];
                        isOn = false;
                        for (NSString *name in selectedContainers) {
                            if ([[con objectForKey:@"name"] isEqual:name]) {
                                isOn = true;
                                break;
                            }
                        }
                        [vCon setValue: (isOn ? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0]) forKey:@"state"];
                        [userContainers addObject: vCon];
                    }
                }else{
                    //null user then all container must reset
                    for (NSDictionary *con in containers ) {
                        NSMutableDictionary * vCon = [NSMutableDictionary dictionaryWithDictionary: con];
                        [vCon setValue: [NSNumber numberWithInt:0] forKey:@"state"];
                        [userContainers addObject: vCon];
                    }
                }
                [self layoutContainers: userContainers];
            }];
            
            //NSArray *containers =  [[JSDB sharedInstance] sql:[NSString stringWithFormat:@"SELECT * FROM containers"] params:nil success:nil fail:nil];
        }];
        
        [self.emailCheckingRequest setFailedBlock:^{
#ifdef DEBUG
            NSLog(@"FAILED to get user Settings");
#endif
        }];
        [self.emailCheckingRequest startAsynchronous];
         */
    }else{
#ifdef DEBUG
        NSLog(@"Input email %@ is invalid...ignore checking!", userEmail);
#endif
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView{
    [super loadView];
}

- (void) layoutContainers:(NSArray *)containers {
    
    [scrollView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)obj;
            if (button.tag == 5000)
                [obj performSelector:@selector(removeFromSuperview)];
        }
    }];
    
    //NSArray *containers = (_containers ? _containers : [self containersList]);
    //NSLog(@"containers = %@", containers);
    NSMutableArray *groups = [NSMutableArray array];
    NSMutableArray *group;
    NSMutableArray *groupScapes = [NSMutableArray array];
    NSString *businessArea = nil;
    NSDictionary *dic;
    for (dic in containers) {
        if ([[dic objectForKey:@"name"] isEqual:@"Rail"] || [[dic objectForKey:@"name"] isEqual:@"Educational"] || [[dic objectForKey:@"name"] isEqual:@"Home"] || [[dic objectForKey:@"name"] isEqual:@"Retail"] || [[dic objectForKey:@"name"] isEqual:@"Cycling"] || [[dic objectForKey:@"name"] isEqual:@"Cycle"]) {
            [groupScapes addObject: dic];
        }else{
            if (![businessArea isEqual:[dic objectForKey:@"businessArea"]]) {
                businessArea = [dic objectForKey:@"businessArea"];
                group = [NSMutableArray array];
                [groups addObject:group];
            }
            
            [group addObject:dic];
        }
    }
    [groups addObject:groupScapes];
    
    
    float startX = 34.0f;
    float startY = 266.0;//266;
    NSArray *groupY = [NSMutableArray arrayWithObjects:[NSNumber numberWithFloat: 260.0],[NSNumber numberWithFloat: 340.0],[NSNumber numberWithFloat: 425.0],[NSNumber numberWithFloat: 690.0], nil];
    
    
    float width = 250.0f;
    float height = 40.0f;
    float offsetX = 0.0f;
    float offsetY = 18.0f;
    //float groupOffsetY = 46.0f;
    float x = 0.0f;
    float y = 0.0f;
    int column = 4;
    UIFont *font = [UIFont fontWithName:@"Arial" size:20];
    UIColor *color = [UIColor colorWithRed:(141.0f / 255.0f) green:(141.0f / 255.0f) blue:(141.0f / 255.0f) alpha:1.0f];
    CGRect rect = CGRectMake(x, y, width, height);
    //NSMutableArray *tmpButtons = [NSMutableArray array];
    UIButton *lstBtn = nil;
    for (int i = 0 ; i < groups.count; i++) {
        group = [groups objectAtIndex:i];
        startX = 34;
        //startY = i == 0 ? group1Y : y + height + groupOffsetY;
        if (i>=groupY.count) break;
        startY = [[groupY objectAtIndex:i] floatValue];
        for (int j = 0; j < group.count; j++) {
            
            dic = [group objectAtIndex:j];
            
            x = startX + (j % column) * (width + offsetX);
            y = startY + (j / column) * (height + offsetY);
            
            //NSLog(@"startY = %f, y = %f row = %d", startY, y, (j / column));
            
            rect.origin = CGPointMake(x, y);
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setImage:[UIImage imageNamed:@"btn_uncheck.png"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"btn_check.png"] forState:UIControlStateSelected];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.backgroundColor = [UIColor clearColor];
            button.titleEdgeInsets = UIEdgeInsetsMake(3, 8, 0, 0);
            button.frame = rect;
            button.titleLabel.font = font;
            [button setTitleColor:color forState:UIControlStateNormal];
            button.tag = [[dic objectForKey:@"tag"] intValue];
            [button setTitle:[[dic nonNullValueForKey:@"name"] uppercaseString] forState:UIControlStateNormal];
            button.userInteractionEnabled = NO;
            //[button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            button.selected = [[dic objectForKey:@"state"] intValue];
            button.tag = 5000;
            [scrollView addSubview:button];
            lstBtn = button;
            //[tmpButtons addObject:button];
        }
    }
    // start remove me now
//    rect.origin = CGPointMake(0, y + height + offsetY);
//    lstBtn = [[UIButton alloc] initWithFrame:rect];
//    [lstBtn setTitle:@"ABC" forState:UIControlStateNormal];
//    [lstBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
//    [scrollView addSubview:lstBtn];
    
    // end remove me now
    
    //self.buttons = [NSArray arrayWithArray:tmpButtons];
    
    [scrollView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)obj;
            button.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Light" size:button.titleLabel.font.pointSize];
        } else if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel *)obj;
            label.font = [UIFont fontWithName:@"MyriadPro-Light" size:label.font.pointSize];
        }
    }];
    // update conntent size of scrollView
    if(lstBtn)
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, lstBtn.frame.origin.y +  height + offsetY);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    damNavigator.delegate = self;
    
    self.emailTextField.textColor = [[DAMUIDefaults defaults] textColor];
    self.emailTextField.font = [[DAMUIDefaults defaults] textFont];
    NSString *userEmail = [[DAMApplicationModel model] userEmail];
    if (userEmail && userEmail.length > 0) {
        self.emailTextField.text = [NSString stringWithFormat:@"%@", userEmail];
    } else {
//        self.emailTextField.text = @"Please add your email address or select from your contacts";
        self.emailTextField.text = @"";
    }
    self.emailTextField.delegate = self;
    
    isEmailFieldDirty = TRUE;
    isCheckBrochuresButtonDirty = false;
    isCheckSamplesButtonDirty = false;
    isCheckVideoButtonDirty = false;
    hasUserTypeSomething = FALSE;
    
    bSamplesFinished = NO;
    bVideosFinished = NO;
    bBrochuresFinished = NO;
    
    //[self layoutContainers:NULL];
    
     NSString *validUserEmail = [[JSMain sharedInstance] validUserEmail];
    
    [self containersList:^(NSArray *data) {
        [self layoutContainers: data];
        
        [self remainedAssetDownload: kMediaType_Samples callback:^(int samplesRemained) {
            if (samplesRemained == 0 && validUserEmail.length > 0) {
                [[JSMain sharedInstance] updateDownloadAllSamples:NO];
            }
            
            [self remainedAssetDownload: kMediaType_Videos callback:^(int videoRemained) {
                if (videoRemained == 0 && validUserEmail.length > 0) {
                    [[JSMain sharedInstance] updateDownloadAllVideos: NO];
                }
                
                [self remainedAssetDownload: kMediaType_Brochures callback:^(int brochuresRemained) {
                    if (brochuresRemained == 0 && validUserEmail.length > 0) {
                        [[JSMain sharedInstance] updateDownloadAllBrochures: NO];
                    }
                }];
            }];
            
        }];
    }];
    //int videoRemained = [[JSSync sharedInstance] remainedAssetDownload:kMediaType_Videos];
    //int brochuresRemained = [[JSSync sharedInstance] remainedAssetDownload:kMediaType_Brochures];
    //int samplesRemained = [[JSSync sharedInstance] remainedAssetDownload:kMediaType_Samples];
    /*update DOWNLOAD all button states*/
    [self updateDownloadAllButtonStates];
    
    
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
    
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void) updateSamplesButtonState {
    //int samplesRemained = [[JSSync sharedInstance] remainedAssetDownload:kMediaType_Samples];
    [self remainedAssetDownload: kMediaType_Samples callback:^(int samplesRemained) {
    NSString *validUserEmail = [[JSMain sharedInstance] validUserEmail];
    BOOL videoState = [[JSMain sharedInstance] isDownloadAllVideos];
    BOOL samplesState = [[JSMain sharedInstance] isDownloadAllSamples];
    BOOL brochuresState = [[JSMain sharedInstance] isDownloadAllBrochures];
    if (samplesRemained == 0 && validUserEmail.length > 0 /*&& samplesState*/) {
        [self.samplesButton setBackgroundImage:[UIImage imageNamed: @"settings_samples_x.png"] forState: UIControlStateNormal];
        self.samplesButton.userInteractionEnabled = NO;
        self.samplesButton.enabled = NO;
        self.samplesButton.alpha = 0.8f;
        [[JSMain sharedInstance] updateDownloadAllSamples: NO];
    }else if (samplesState){
        [self.samplesButton setBackgroundImage:[UIImage imageNamed: @"settings_samples_x.png"] forState: UIControlStateNormal];
        self.samplesButton.userInteractionEnabled = YES;
        self.samplesButton.enabled = YES;
        self.samplesButton.alpha = 1.0f;
    }else {
        [self.samplesButton setBackgroundImage:[UIImage imageNamed: @"settings_samples.png"] forState: UIControlStateNormal];
        if (videoState || brochuresState) {
            self.samplesButton.userInteractionEnabled = NO;
            self.samplesButton.enabled = NO;
            self.samplesButton.alpha = 0.8f;
        }else{
            self.samplesButton.userInteractionEnabled = YES;
            self.samplesButton.enabled = YES;
            self.samplesButton.alpha = 1.0f;
        }
    }
    }];
}

- (void) updateBrochuresButtonState {
    //int brochuresRemained = [[JSSync sharedInstance] remainedAssetDownload:kMediaType_Brochures];
    [self remainedAssetDownload: kMediaType_Brochures callback:^(int brochuresRemained) {
    NSString *validUserEmail = [[JSMain sharedInstance] validUserEmail];
    BOOL videoState = [[JSMain sharedInstance] isDownloadAllVideos];
    BOOL samplesState = [[JSMain sharedInstance] isDownloadAllSamples];
    BOOL brochuresState = [[JSMain sharedInstance] isDownloadAllBrochures];
    if (brochuresRemained == 0 && validUserEmail.length > 0 /*&& brochuresState*/) {
        [self.brochuresButton setBackgroundImage:[UIImage imageNamed: @"settings_brochures_x.png"] forState: UIControlStateNormal];
        self.brochuresButton.userInteractionEnabled = NO;
        self.brochuresButton.enabled = NO;
        self.brochuresButton.alpha = 0.8f;
        [[JSMain sharedInstance] updateDownloadAllBrochures: NO];
    }else if (brochuresState){
        [self.brochuresButton setBackgroundImage:[UIImage imageNamed: @"settings_brochures_x.png"] forState: UIControlStateNormal];
        self.brochuresButton.userInteractionEnabled = YES;
        self.brochuresButton.enabled = YES;
        self.brochuresButton.alpha = 1.0f;
    }else {
        [self.brochuresButton setBackgroundImage:[UIImage imageNamed: @"settings_brochures.png"] forState: UIControlStateNormal];
        if (samplesState || videoState) {
            self.brochuresButton.userInteractionEnabled = NO;
            self.brochuresButton.enabled = NO;
            self.brochuresButton.alpha = 0.8f;
        }else{
            
            self.brochuresButton.userInteractionEnabled = YES;
            self.brochuresButton.enabled = YES;
            self.brochuresButton.alpha = 1.0f;
        }
    }
    }];
}


- (void) remainedAssetDownload:(NSString *)mt callback: (void (^)(int total))callbackFunc {
    
    NSString *sql = [NSString stringWithFormat:@"SELECT count(0) AS count FROM download_queue_original where mediaType='%@'", mt];
    
    //NSArray * result = [[JSDBV2 sharedInstance] map: sql params: nil];
    
   [[JSDBV2 sharedInstance] executeQuery: sql params:nil successCallback:^(NSArray *result) {
       int temp = 0;
       if (result && [result count] > 0) {
           NSDictionary *temp = [result objectAtIndex:0];
           temp = [[temp objectForKey:@"count"] intValue];
       }
       callbackFunc(temp);
   }];
}

- (void) updateVideosButtonState {
    //int videoRemained = [[JSSync sharedInstance] remainedAssetDownload:kMediaType_Videos];
    [self remainedAssetDownload: kMediaType_Videos callback:^(int videoRemained) {
        NSString *validUserEmail = [[JSMain sharedInstance] validUserEmail];
        BOOL videoState = [[JSMain sharedInstance] isDownloadAllVideos];
        BOOL samplesState = [[JSMain sharedInstance] isDownloadAllSamples];
        BOOL brochuresState = [[JSMain sharedInstance] isDownloadAllBrochures];
        if (videoRemained == 0 && validUserEmail.length > 0 /*&& videoState*/) {
            [self.videosButton setBackgroundImage:[UIImage imageNamed: @"settings_video_x.png"] forState: UIControlStateNormal];
            self.videosButton.userInteractionEnabled = NO;
            self.videosButton.enabled = NO;
            self.videosButton.alpha = 0.8f;
            [[JSMain sharedInstance] updateDownloadAllVideos: NO];
        }else if (videoState){
            [self.videosButton setBackgroundImage:[UIImage imageNamed: @"settings_video_x.png"] forState: UIControlStateNormal];
            self.videosButton.userInteractionEnabled = YES;
            self.videosButton.enabled = YES;
            self.videosButton.alpha = 1.0f;
        }else {
            [self.videosButton setBackgroundImage:[UIImage imageNamed: @"settings_video.png"] forState: UIControlStateNormal];
            if (samplesState || brochuresState) {
                self.videosButton.userInteractionEnabled = NO;
                self.videosButton.enabled = NO;
                self.videosButton.alpha = 0.8f;
            }else{
                
                self.videosButton.userInteractionEnabled = YES;
                self.videosButton.enabled = YES;
                self.videosButton.alpha = 1.0f;
            }
        }
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
