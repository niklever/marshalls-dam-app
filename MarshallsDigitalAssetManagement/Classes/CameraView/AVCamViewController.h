/*
     File: AVCamViewController.h
 Abstract: View controller for camera interface.
  Version: 3.1


 */

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import "DAMAddToLibraryView.h"

@interface AVCamViewController : GAITrackedViewController<UIImagePickerControllerDelegate, UIPopoverControllerDelegate>

@end
