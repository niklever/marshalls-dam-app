
#import "AVCamViewController.h"

#import "DAMAppDelegate.h"
#import "DAMSalesPhotoGalleryViewController.h"
#import "MBProgressHUD.h"
#import "PBJVision.h"
#import "PBJVisionUtilities.h"

@interface AVCamViewController ()<PBJVisionDelegate>
{
    IBOutlet UIButton *btnThumbnail;
    AVCaptureVideoPreviewLayer *_previewLayer;
    
    ALAssetsLibrary *library;
    NSArray *imageArray;
    NSMutableArray *mutableArray;
    
    BOOL isIOS8;
}
@property (nonatomic, strong)DAMAddToLibraryView *addLibraryView;
@property (nonatomic, strong) UIPopoverController *pcPopoverController;
// For use in the storyboards.
@property (nonatomic, weak) IBOutlet UIView *previewView;

- (IBAction)snapStillImage:(id)sender;
- (IBAction)didClickCancel:(id)sender;
- (IBAction)didClickPhotoThumbnail:(id)sender;


@end

@implementation AVCamViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    __block int count = 0;
    
    [self getAllPictures];
    
    [self createCameraLayer];
}


- (void) createCameraLayer{
    
    _previewLayer = [[PBJVision sharedInstance] previewLayer];
    _previewLayer.frame = self.previewView.bounds;
    //NSLog(@"live view %@", NSStringFromCGRect(_previewLayer.frame));
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    /*[self.previewView.layer.sublayers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [(CALayer *)obj removeFromSuperlayer];
    }];
    */
    [self.previewView.layer addSublayer:_previewLayer];
    
    [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"createCameraLayer" withLabel: @"done" value: nil];
}

- (void)_startCapture
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [[PBJVision sharedInstance] startVideoCapture];
}

- (void)_pauseCapture
{
    [[PBJVision sharedInstance] pauseVideoCapture];
}

- (void)_resumeCapture
{
    [[PBJVision sharedInstance] resumeVideoCapture];
}

- (void)_endCapture
{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [[PBJVision sharedInstance] endVideoCapture];
}

- (void)_resetCapture
{
    [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"resetCapture" withLabel: @"started" value: nil];
    
    PBJVision *vision = [PBJVision sharedInstance];
    vision.delegate = self;

    [vision setCameraMode:PBJCameraModePhoto];
    
    int orientation = [UIApplication sharedApplication].statusBarOrientation;
    //NSLog(@"orientation %d", orientation);
    [vision setCameraOrientation:orientation];
    
    [vision setFocusMode:PBJFocusModeContinuousAutoFocus];
    [vision setOutputFormat:PBJOutputFormatWidescreen];
    
    [vision setPresentationFrame: self.previewView.frame];
    [vision setThumbnailEnabled: YES];
    
    [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"resetCapture" withLabel: [NSString stringWithFormat:@"done device orientation %d", orientation] value: nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self _resetCapture];
    [[PBJVision sharedInstance] startPreview];
    
    self.screenName = @"Live Camera View";
}

- (void)viewDidDisappear:(BOOL)animated
{
    
    [[PBJVision sharedInstance] stopPreview];
    [self didFinishedCaptureVideoFromCamera];
}

- (BOOL)shouldAutorotate
{
	// Disable autorotation of the interface when recording is in progress.
	//return ![self lockInterfaceRotation];
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    //The device has already rotated, that's why this method is being called.
    UIInterfaceOrientation toOrientation   = [[UIDevice currentDevice] orientation];
    //fixes orientation mismatch (between UIDeviceOrientation and UIInterfaceOrientation)
    if (toOrientation == UIInterfaceOrientationLandscapeRight) toOrientation = UIInterfaceOrientationLandscapeLeft;
    else if (toOrientation == UIInterfaceOrientationLandscapeLeft) toOrientation = UIInterfaceOrientationLandscapeRight;
    
    UIInterfaceOrientation fromOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    [self willRotateToInterfaceOrientation:toOrientation duration:0.0];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self willAnimateRotationToInterfaceOrientation:toOrientation duration:[context transitionDuration]];
        [self adjustViewsForOrientation:toOrientation];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self didRotateFromInterfaceOrientation:fromOrientation];
    }];
    
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation {
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate adjustOrientation:orientation];
//    orientation = [UIApplication sharedApplication].statusBarOrientation;
//    BOOL isLandscapeLeft = orientation == UIDeviceOrientationLandscapeLeft;
//    if (isLandscapeLeft) {
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width , self.window.frame.size.height)];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = 20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }else{
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = -20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }
}


#pragma mark Actions

- (IBAction)snapStillImage:(id)sender
{
    
    [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"imageCapture" withLabel: @"started" value: nil];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[PBJVision sharedInstance] setOutputFormat:PBJOutputFormatWidescreen];
    
    [[PBJVision sharedInstance] capturePhoto];
    
    
    
    NSLog(@"Do Take Picture");
}

- (IBAction)didClickCancel:(id)sender {
    //[[self session] stopRunning];
    [_previewLayer removeFromSuperlayer];
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)didClickPhotoThumbnail:(id)sender {
    /* open photo galery thumbnail */
    
    /*
    DAMSalesPhotoGalleryViewController *controller  =   [[DAMSalesPhotoGalleryViewController alloc] initWithNibName:@"DAMSalesPhotoGalleryViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    
    */
    
    if (!self.pcPopoverController) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        self.pcPopoverController = [[UIPopoverController alloc]
                                              initWithContentViewController:picker];
    }
    
    [self.pcPopoverController presentPopoverFromRect: [sender bounds]
                                            inView: sender
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated: YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    
    [self showAddToServer: selectedImage andImageTitle: [info [UIImagePickerControllerReferenceURL] lastPathComponent] needStopCamera: YES];
    
    [self.pcPopoverController dismissPopoverAnimated: YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Do Capture Video
- (void)didFinishedCaptureVideoFromCamera{
    [[PBJVision sharedInstance] endVideoCapture];
}

#pragma mark Do Capture Picture

- (UIImage *)imageInSize:(UIImage *) src size:(CGSize)size{
    float r = size.height /size.width;
    float ir = src.size.height / src.size.width;
    float width = size.width;
    float height = size.height;
    if (ir < r) {
        height = ceilf(size.width * src.size.height / src.size.width);
    } else {
        width = ceilf(size.height * src.size.width / src.size.height);
    }
    CGRect rect = CGRectMake((size.width - width)/2, (size.height - height)/2, width, height);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.f);
    [src drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)didFinishedCapturePictureFromCamera:(UIImage *)capturedImage{
    
    UIImage *image = capturedImage;
    
    [btnThumbnail setBackgroundImage:image forState:UIControlStateNormal];
    
    ALAssetsLibrary *assets =   [[ALAssetsLibrary alloc] init];
    
    [assets writeImageToSavedPhotosAlbum:[image CGImage]
                             orientation:(ALAssetOrientation)[image imageOrientation]
                         completionBlock:^(NSURL *assetURL, NSError *error) {
                             /* Moving to post media screen when user finish take image */
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                             
                             if (!error) {
                                 
                                 NSLog(@"path %@", assetURL);
                                 
                                 [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"imageCapture" withLabel: [NSString stringWithFormat:@"saved success to %@", [assetURL absoluteString]] value: nil];
                                 
                                 /*
                                 DAMSalesPhotoGalleryViewController *controller  =   [[DAMSalesPhotoGalleryViewController alloc] initWithNibName:@"DAMSalesPhotoGalleryViewController" bundle:nil];
                                 [controller setSeletedImage:image];
                                 [controller setImageTitle:[assetURL lastPathComponent]];
                                 [self.navigationController pushViewController:controller animated:YES];
                                  
                                 */
                                 [self showAddToServer: image andImageTitle: [assetURL lastPathComponent] needStopCamera: NO];
                             }else{
                                 [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"imageCapture" withLabel: [NSString stringWithFormat:@"saved failed to album %@", [error localizedDescription]] value: nil];
                                 /*
                                 DAMSalesPhotoGalleryViewController *controller  =   [[DAMSalesPhotoGalleryViewController alloc] initWithNibName:@"DAMSalesPhotoGalleryViewController" bundle:nil];
                                 [controller setSeletedImage:image];
                                 [controller setImageTitle: @"img"];
                                 [self.navigationController pushViewController:controller animated:YES];
                                 */
                                 [self showAddToServer: image andImageTitle: @"img" needStopCamera: NO];
                             }
                             
                             
                             /*[assets assetForURL:assetURL
                                     resultBlock:^(ALAsset *asset) {
                                         DAMSalesPhotoGalleryViewController *controller  =   [[DAMSalesPhotoGalleryViewController alloc] initWithNibName:@"DAMSalesPhotoGalleryViewController" bundle:nil];
         [controller setSeletedImage:image];
         [controller setImageTitle:[[asset defaultRepresentation] filename]];
                                         [self.navigationController pushViewController:controller animated:YES];
         }
                failureBlock:^(NSError *error) {
         
         }];*/
                         }];
}

- (void)vision:(PBJVision *)vision capturedPhoto:(NSDictionary *)photoDict error:(NSError *)error
{
    if (!error) {
        UIImage *capturedImage = [photoDict objectForKey:PBJVisionPhotoImageKey];
        //save to gallery to review
        //NSLog(@"capturedImage %@", NSStringFromCGSize(capturedImage.size));
        [self didFinishedCapturePictureFromCamera: capturedImage];
    }else{
        [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"imageCapture" withLabel: [NSString stringWithFormat:@"failed to capture image %@", [error localizedDescription]] value: nil];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}


#pragma mark - Add to Lib
- (void) showAddToServer:(UIImage *)image andImageTitle:(NSString *)title needStopCamera: (BOOL) flag{
    if(!self.addLibraryView){
        self.addLibraryView =   [[[NSBundle mainBundle] loadNibNamed:@"DAMAddToLibraryView" owner:nil options:nil] objectAtIndex:0];
        [self.addLibraryView setFrame:CGRectMake(0, self.view.frame.size.height, self.addLibraryView.frame.size.width, self.addLibraryView.frame.size.height)];
        [self.view addSubview: self.addLibraryView];
    }
    
    [self.addLibraryView createCancelBlock:^{
        //_previewView.hidden = NO;
        [self.addLibraryView removeFromSuperview];
        self.addLibraryView = nil;
        //resume camera
        [[PBJVision sharedInstance] unfreezePreview];
        //[self createCameraLayer];
        
        [self _resetCapture];
        [[PBJVision sharedInstance] startPreview];
        
        [self.previewView setNeedsDisplay];
    }];
    
    [self.addLibraryView createSaveBlock:^{
        [self didClickCancel: nil];
    }];
    
    if (flag) {
        [[PBJVision sharedInstance] stopPreview];
        [[PBJVision sharedInstance] endVideoCapture];
    }
    
    /* scale image follow new size 1280 x 1024 before send to server */
    int     extraScalse         =   1;//IS_RETINA ? 2 : 1;
    float   standardWidth       =   1280 * extraScalse;
    float   standardHeight       =   1024 * extraScalse;
    float   ratio               =   1;
    float   newHeight           =   image.size.height;
    float   newWidth            =   image.size.width;
    
    if(image.size.width > image.size.height && image.size.width > standardWidth){
        
        ratio       =   standardWidth / image.size.width;
        newWidth    =   standardWidth;
        newHeight   =   newHeight * ratio;
        image       =  [self imageWithImage:image scaledToSize:CGSizeMake(newWidth, newHeight)];
    }else if(image.size.height > image.size.width && image.size.height > standardHeight){
        
        ratio       =   standardHeight / image.size.height;
        newHeight    =   standardHeight;
        newWidth   =   newWidth * ratio;
        image       =  [self imageWithImage:image scaledToSize:CGSizeMake(newWidth, newHeight)];
    }
    
    
    [self.addLibraryView setThumbnailImage:image];
    [self.addLibraryView setImageTitle:title];
    [self.addLibraryView animationShowUp];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


-(void)getAllPictures
{
    imageArray=[[NSArray alloc] init];
    mutableArray =[[NSMutableArray alloc]init];
    NSMutableArray* assetURLDictionaries = [[NSMutableArray alloc] init];
    
    library = [[ALAssetsLibrary alloc] init];
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                
                NSURL *url= (NSURL*) [[result defaultRepresentation]url];
                
                [library assetForURL:url
                         resultBlock:^(ALAsset *asset) {
                             [btnThumbnail setBackgroundImage:[UIImage imageWithCGImage:[asset thumbnail]]
                                                     forState:UIControlStateNormal];
                             
                             [btnThumbnail setBackgroundImage:[UIImage imageWithCGImage:[asset thumbnail]]
                                                     forState:UIControlStateHighlighted];
                             *stop = YES;
                         }
                        failureBlock:^(NSError *error){
                            NSLog(@"operation was not successfull!");
                            [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"getAllPictures failed 1" withLabel: [NSString stringWithFormat:@"%@", [error localizedDescription]] value: nil];
                        } ];
                
            }
        }
    };
    
    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            if ([[group valueForProperty:@"ALAssetsGroupPropertyType"] intValue] == ALAssetsGroupSavedPhotos) {
                [group enumerateAssetsUsingBlock:assetEnumerator];
                [assetGroups addObject:group];
                *stop = YES;
            }
        }
    };
    
    assetGroups = [[NSMutableArray alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {
                             NSLog(@"There is an error");
                             [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"getAllPictures failed 2" withLabel: [NSString stringWithFormat:@"%@", [error localizedDescription]] value: nil];
                         }];
}

@end
