//
//  DAMMAPViewController.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/31/14.
//
//

#import "DAMMAPViewController.h"
#import "DAMAppDelegate.h"
#import "DAMMarkerPhotoView.h"

#import <GoogleMaps/GoogleMaps.h>

@interface DAMMAPViewController ()<GMSMapViewDelegate>{
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UIView *vMapContent;
}
- (IBAction)didClickClose:(id)sender;

@end

@implementation DAMMAPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    GMSCameraPosition *camera = nil;
    
    if([_arrLocation count] > 0){
        NSDictionary *item  =   [_arrLocation firstObject];
        camera = [GMSCameraPosition cameraWithLatitude:[[item objectForKey:@"latitude"] floatValue]
                                             longitude:[[item objectForKey:@"longitude"] floatValue]
                                                  zoom:12];
    }
    else{
        camera = [GMSCameraPosition cameraWithLatitude:53.6926785
                                             longitude:-1.8328717
                                                  zoom:12];
    }
    
    CGRect frame    =   self.view.frame;
    
    if([[JSMain sharedInstance] isIOS7]) {
        frame.size.height   =   self.view.frame.size.height - 20;
    }
    
    
    GMSMapView *mapView = [GMSMapView mapWithFrame:frame camera:camera];
    [mapView setDelegate:self];
    
    [self.view insertSubview:mapView atIndex:0];
    
    [_arrLocation enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSDictionary *item  =   (NSDictionary *)obj;
        
        GMSMarker *marker   = [[GMSMarker alloc] init];
        marker.position     = CLLocationCoordinate2DMake([[item objectForKey:@"latitude"] floatValue], [[item objectForKey:@"longitude"] floatValue]);
        marker.icon         = [self drawText:[NSString stringWithFormat:@"%d", idx + 1]
                                     inImage:[UIImage imageNamed:@"blackCircle"]
                                     atPoint:CGPointMake(7,2.5)];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.snippet         = [NSString stringWithFormat:@"%d", idx];
        marker.map = mapView;
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [lblTitle release];
    [_arrLocation release];
    [super dealloc];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if([[JSMain sharedInstance] isIOS7]) {
        [self adjustViewsForOrientation:toInterfaceOrientation];
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    //The device has already rotated, that's why this method is being called.
    UIInterfaceOrientation toOrientation   = [[UIDevice currentDevice] orientation];
    //fixes orientation mismatch (between UIDeviceOrientation and UIInterfaceOrientation)
    if (toOrientation == UIInterfaceOrientationLandscapeRight) toOrientation = UIInterfaceOrientationLandscapeLeft;
    else if (toOrientation == UIInterfaceOrientationLandscapeLeft) toOrientation = UIInterfaceOrientationLandscapeRight;
    
    UIInterfaceOrientation fromOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    [self willRotateToInterfaceOrientation:toOrientation duration:0.0];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self willAnimateRotationToInterfaceOrientation:toOrientation duration:[context transitionDuration]];
        [self adjustViewsForOrientation:toOrientation];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self didRotateFromInterfaceOrientation:fromOrientation];
    }];
    
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation {
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate adjustOrientation:orientation];
//    orientation = [UIApplication sharedApplication].statusBarOrientation;
//    BOOL isLandscapeLeft = orientation == UIDeviceOrientationLandscapeLeft;
//    if (isLandscapeLeft) {
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width , self.window.frame.size.height)];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = 20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }else{
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = -20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }
}

#pragma mark - private method
-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{
    
    UIFont *font = [UIFont systemFontOfSize:12];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void) showImageWithMarker:(NSDictionary *) item{
    __block DAMMarkerPhotoView *photoView   =   [[[NSBundle mainBundle] loadNibNamed:@"MarkerPhotoView" owner:nil options:nil] objectAtIndex:0];
    [photoView setTransform:CGAffineTransformMakeScale(0, 0)];
    [photoView createDissmissBlock:^{
        [UIView animateWithDuration:0.25
                         animations:^{
                             [photoView setTransform:CGAffineTransformMakeScale(0, 0)];
                         } completion:^(BOOL finished) {
                             [photoView removeFromSuperview];
                             photoView  =   nil;
                         }];
    }];
    
    [self.view addSubview:photoView];
    
    NSString *url   =   [NSString stringWithFormat:@"%@%@", [[DAMApplicationModel model] apiHost], [item objectForKey:@"url"]];
    
    
    [photoView showImage: url];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         [photoView setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                     } completion:^(BOOL finished) {
                     }];
    
}

#pragma mark - IBACTION
- (IBAction)didClickClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - map view delegate
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    [self showImageWithMarker:[_arrLocation objectAtIndex:[marker.snippet intValue]]];
    return YES;
    
}

@end
