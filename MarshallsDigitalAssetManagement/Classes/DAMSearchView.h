//
//  DAMSearchView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMViewControllerBase.h"
//#import "DAMSearchBarView.h"
#import "DAMBusinessAreaFiltersView.h"
#import "DAMPredictiveViewController.h"

@interface DAMSearchView : DAMViewControllerBase<UITextFieldDelegate,DAMSearchBarViewDelegate, DAMBusinessAreaFiltersViewDelegate, DAMPredictiveViewControllerDelegate> {
//    DAMSearchBarView *searchBar;
    
    UILabel *brochuresCount;
    UILabel *sampleBookCount;
    UILabel *photosCount;
    UILabel *videosCount;
    UILabel *caseStudiesCount;
    UILabel *presentationsCount;
    UITextField *activeTextField;
    BOOL searchIsBuzy;
}
//@property(nonatomic, retain) DAMSearchBarView *searchBar;
@property(nonatomic, retain) IBOutlet UILabel *brochuresCount;
@property(nonatomic, retain) IBOutlet UILabel *sampleBookCount;
@property(nonatomic, retain) IBOutlet UILabel *photosCount;
@property(nonatomic, retain) IBOutlet UILabel *videosCount;
@property(nonatomic, retain) IBOutlet UILabel *caseStudiesCount;
@property(nonatomic, retain) IBOutlet UILabel *presentationsCount;
@property (nonatomic, retain) DAMPredictiveViewController *predictiveViewController;

- (IBAction)mediaButtonTapped:(id)sender;

@end
