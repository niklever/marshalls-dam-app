//
//  DAMNewSettingsView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Hoa Truong Huu on 1/8/13.
//
//

#import <UIKit/UIKit.h>
//#import "ASIHTTPRequest.h"
#import "DAMViewControllerBase.h"
#import "DAMEmailChooserView.h"
#import "DAMMediaChangeView.h"
#import <MessageUI/MessageUI.h>



@interface DAMNewSettingsView : DAMViewControllerBase<DAMNavigatorDelegate, DAMEmailChooserViewDelegate, UIPopoverControllerDelegate,
DAMMediaChangeViewDelegate, MFMailComposeViewControllerDelegate, UITextFieldDelegate> {
    BOOL isEmailFieldDirty;
    BOOL isCheckVideoButtonDirty;
    BOOL isCheckBrochuresButtonDirty;
    BOOL isCheckSamplesButtonDirty;
    BOOL hasUserTypeSomething;
    //ASIHTTPRequest *emailCheckingRequest;
    
    BOOL bVideosFinished; //videos
    BOOL bSamplesFinished; // samples
    BOOL bBrochuresFinished; // brochures
}
//@property (nonatomic, retain) ASIHTTPRequest *emailCheckingRequest;
@property (nonatomic, retain) IBOutlet UIButton *emailChoiceButton;

@property (nonatomic, retain) IBOutlet UIButton *videosButton;
@property (nonatomic, retain) IBOutlet UIButton *brochuresButton;
@property (nonatomic, retain) IBOutlet UIButton *samplesButton;

@property (nonatomic, retain) IBOutlet UITextField *emailTextField;

- (IBAction)emailChoiceTapped:(id)sender;
- (IBAction)emailAdminstratorTapped:(id)sender;

- (IBAction)downloadAllVideosTapped:(id)sender;
- (IBAction)downloadAllBrochuresTapped:(id)sender;
- (IBAction)downloadAllSamplesTapped:(id)sender;

@end
