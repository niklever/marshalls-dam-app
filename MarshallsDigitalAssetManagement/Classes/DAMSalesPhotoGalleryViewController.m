//
//  DAMSalesPhotoGalleryViewController.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/30/14.
//
//

#import "DAMSalesPhotoGalleryViewController.h"

#import "DAMAppDelegate.h"
#import "MBProgressHUD.h"
#import "DAMPhotoGalleryCell.h"

#import "DAMAddToLibraryView.h"
#include <AssetsLibrary/AssetsLibrary.h>
#import "ALAsset+ALAsset_Date.h"

#define kPhotoCellIdentifier    @"DAMPhotoGalleryCell"

@interface DAMSalesPhotoGalleryViewController () <UIImagePickerControllerDelegate>{
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UICollectionView *vCollection;
    DAMAddToLibraryView *addLibraryView;
    
    NSArray *arrPhoto;
    
    ALAssetsLibrary *library;
    NSArray *imageArray;
    NSMutableArray *mutableArray;
    
}

-(void)allPhotosCollected:(NSArray*)imgArray;

- (IBAction)didClickClose:(id)sender;

@end

static int count=0;

@implementation DAMSalesPhotoGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [lblTitle setFont:[[DAMUIDefaults defaults] navigatorTitleFont]];
    [self registerCell];
    
    //[self listFileAtPath:nil];
    
    if(_seletedImage){
        [self showAddToServer:_seletedImage andImageTitle:_imageTitle];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    [self getAllPictures];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [lblTitle release];
    [vCollection release];
//    [arrPhoto release];
    [super dealloc];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if([[JSMain sharedInstance] isIOS7]) {
        [self adjustViewsForOrientation:toInterfaceOrientation];
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    //The device has already rotated, that's why this method is being called.
    UIInterfaceOrientation toOrientation   = [[UIDevice currentDevice] orientation];
    //fixes orientation mismatch (between UIDeviceOrientation and UIInterfaceOrientation)
    if (toOrientation == UIInterfaceOrientationLandscapeRight) toOrientation = UIInterfaceOrientationLandscapeLeft;
    else if (toOrientation == UIInterfaceOrientationLandscapeLeft) toOrientation = UIInterfaceOrientationLandscapeRight;
    
    UIInterfaceOrientation fromOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    [self willRotateToInterfaceOrientation:toOrientation duration:0.0];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self willAnimateRotationToInterfaceOrientation:toOrientation duration:[context transitionDuration]];
        [self adjustViewsForOrientation:toOrientation];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self didRotateFromInterfaceOrientation:fromOrientation];
    }];
    
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation {
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate adjustOrientation:orientation];
//    orientation = [UIApplication sharedApplication].statusBarOrientation;
//    BOOL isLandscapeLeft = orientation == UIDeviceOrientationLandscapeLeft;
//    if (isLandscapeLeft) {
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width , self.window.frame.size.height)];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = 20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }else{
//        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
//        CGRect viewBounds = [appDelegate.window bounds];
//        viewBounds.origin.x = -20;
//        viewBounds.size.height = viewBounds.size.height;
//        appDelegate.window.frame = viewBounds;
//    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"CAMERA ROLL";
}

#pragma mark - IBACTION
- (IBAction)didClickClose:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - private method

- (void) registerCell{
    [vCollection registerNib:[UINib nibWithNibName:@"DAMPhotoGalleryCell" bundle:nil] forCellWithReuseIdentifier:kPhotoCellIdentifier];
}

/*
-(void)listFileAtPath:(NSString *)path
{
    __block int count = 0;
    
    NSMutableArray *mutableArray =[[NSMutableArray alloc]init];
    NSMutableArray* assetURLDictionaries = [[NSMutableArray alloc] init];
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                
                NSURL *url= (NSURL*) [[result defaultRepresentation]url];
                
                [library assetForURL:url
                         resultBlock:^(ALAsset *asset) {
                             [mutableArray addObject:asset];
                             
                             if ([mutableArray count]==count)
                             {
                                 NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
                                 
                                 arrPhoto=[[NSArray alloc] initWithArray:[mutableArray sortedArrayUsingDescriptors:@[sort]]];
                                 [mutableArray release];
                                  kAppDelegate.photoGalleryCount  =   [arrPhoto count];
                                 [vCollection reloadData];
                             }
                         }
                        failureBlock:^(NSError *error){
                            [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"listFileAtPath" withLabel: [NSString stringWithFormat:@"%@", [error localizedDescription]] value: nil];
                                NSLog(@"operation was not successfull!");
                        } ];
                
            }
        }
    };
    
    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
            [assetGroups addObject:group];
            count +=[group numberOfAssets];
        }
    };
    
    assetGroups = [[NSMutableArray alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {NSLog(@"There is an error");}];
}
*/

-(void)getAllPictures
{
    imageArray=[[NSArray alloc] init];
    mutableArray =[[NSMutableArray alloc]init];
    NSMutableArray* assetURLDictionaries = [[NSMutableArray alloc] init];
    
    library = [[ALAssetsLibrary alloc] init];
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                
                NSURL *url= (NSURL*) [[result defaultRepresentation]url];
                
                [library assetForURL:url
                         resultBlock:^(ALAsset *asset) {
                             [mutableArray addObject:[UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]]];
                             
                             if ([mutableArray count]==count)
                             {
                                 imageArray=[[NSArray alloc] initWithArray:mutableArray];
                                 [self allPhotosCollected:imageArray];
                             }
                         }
                        failureBlock:^(NSError *error){
                            NSLog(@"operation was not successfull!");
                            [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"getAllPictures failed 1" withLabel: [NSString stringWithFormat:@"%@", [error localizedDescription]] value: nil];
                        } ];
                
            }
        }
    };
    
    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
            [assetGroups addObject:group];
            count=[group numberOfAssets];
        }
    };
    
    assetGroups = [[NSMutableArray alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {
                             NSLog(@"There is an error");
                             [TrackingHelper sendEventWithCategory: self.screenName  withAction: @"getAllPictures failed 2" withLabel: [NSString stringWithFormat:@"%@", [error localizedDescription]] value: nil];
                         }];
}

-(void)allPhotosCollected:(NSArray*)imgArray
{
    //write your code here after getting all the photos from library...
    //NSLog(@"all pictures are %@",imgArray.count);
    arrPhoto = [NSArray arrayWithArray: imgArray];
    [vCollection reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) showAddToServer:(UIImage *)image andImageTitle:(NSString *)title{
    if(!addLibraryView){
        addLibraryView =   [[[NSBundle mainBundle] loadNibNamed:@"DAMAddToLibraryView" owner:nil options:nil] objectAtIndex:0];
        [addLibraryView setFrame:CGRectMake(0, self.view.frame.size.height, addLibraryView.frame.size.width, addLibraryView.frame.size.height)];
        [self.view addSubview:addLibraryView];
    }
    
    [addLibraryView setThumbnailImage:image];
    [addLibraryView setImageTitle:title];
    [addLibraryView animationShowUp];
}

#pragma mark - uicollectionview datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrPhoto count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DAMPhotoGalleryCell *cell   =   (DAMPhotoGalleryCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kPhotoCellIdentifier forIndexPath:indexPath];
    
    ALAsset *asset  =   [arrPhoto objectAtIndex:indexPath.row];
    
    [cell loadData:[UIImage imageWithCGImage:[asset thumbnail]]];
    
    return cell;
    
}

#pragma mark - uicollectionview delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ALAsset *asset =   [arrPhoto objectAtIndex:indexPath.row];
    [self showAddToServer:[UIImage imageWithCGImage:[asset thumbnail]] andImageTitle:[[asset defaultRepresentation] filename]];
}

#pragma mark - uicollection flow layout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(200, 200);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 10;
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 00;
    
}

@end
