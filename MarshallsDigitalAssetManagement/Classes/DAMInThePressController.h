//
//  DAMInThePressController.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/24/14.
//
//

#import <UIKit/UIKit.h>
#import "DAMNavigatorViewController.h"

@interface DAMInThePressController : UIViewController

@property (nonatomic, assign)DAMNavigatorViewController *damNavigation;

@end
