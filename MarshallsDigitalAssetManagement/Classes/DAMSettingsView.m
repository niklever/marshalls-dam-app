//
//  DAMSettingsView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMSettingsView.h"
#import "DAMCheckBox.h"
#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"

@interface DAMSettingsView () {
    
    NSArray *config;
    
    UILabel *emailLabel;
    UIButton *emailChoiceButton;
    UIPopoverController *emailChoicePopover;

    NSArray *domesticSwitches;
    NSArray *commercialSwitches;
    
    NSMutableArray *containersTurnedOn;
    NSMutableArray *containersTurnedOff;
    
    UISwitch *allContentSwitch;
    UISwitch *allDomesticSwitch;
    UISwitch *allCommercialSwitch;
    
    DAMMediaChangeView *mediaChangeView;
}
@property (nonatomic, retain) NSArray *config;

@property (nonatomic, retain) UIPopoverController *emailChoicePopover;

@property (nonatomic, retain) NSMutableArray *containersTurnedOn;
@property (nonatomic, retain) NSMutableArray *containersTurnedOff;

@property (nonatomic, retain) DAMMediaChangeView *mediaChangeView;

@end

@implementation DAMSettingsView
@synthesize config;
@synthesize emailLabel;
@synthesize emailChoiceButton;
@synthesize emailChoicePopover;
@synthesize domesticSwitches;
@synthesize commercialSwitches;
@synthesize containersTurnedOn;
@synthesize containersTurnedOff;
@synthesize allContentSwitch;
@synthesize allDomesticSwitch;
@synthesize allCommercialSwitch;
@synthesize mediaChangeView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    damNavigator.delegate = self;
    
    emailLabel.textColor = [[DAMUIDefaults defaults] textColor];
    emailLabel.font = [[DAMUIDefaults defaults] textFont];
    NSString *userEmail = [[DAMApplicationModel model] userEmail];
    if (userEmail && userEmail.length > 0) {
        emailLabel.text = [NSString stringWithFormat:@"my eMail: %@", userEmail];
    } else {
        emailLabel.text = @"Please select your email address from your contacts";
    }
     
    UIColor *tintColour = [UIColor colorWithRed:136/255.0f green:41/255.0f blue:61/255.0f alpha:1];
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UISwitch class]]) {
            UISwitch *switchView = (UISwitch *)view;
            switchView.onTintColor = tintColour;
            
            //TODO - remove hard-coded switch initial position
            switchView.on = NO;
        }
    }
    [self setInitialSwitchPositions];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.config = nil;

    self.emailLabel = nil;
    self.emailChoiceButton = nil;
    self.emailChoicePopover = nil;
    self.domesticSwitches = nil;
    self.commercialSwitches = nil;
    self.containersTurnedOn = nil;
    self.containersTurnedOff = nil;
    self.allContentSwitch = nil;
    self.allDomesticSwitch = nil;
    self.allCommercialSwitch = nil;

    self.mediaChangeView = nil;
}

//TODO - switchForTag not optimal, but only small number of switches atm.
- (UISwitch*)switchForTag:(int)tag {
    UISwitch *result = nil;
    int index = 0;
    while (!result && index < self.view.subviews.count) {
        UIView *candidate = [self.view.subviews objectAtIndex:index];
        if ([candidate isKindOfClass:[UISwitch class]] && candidate.tag == tag) {
            result = (UISwitch *)candidate;
        }
        index++;
    }
    return result;
}

- (NSDictionary*)configForTag:(int)tag {
//    NSDictionary *result = nil;
//    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM containers WHERE tag=%d", tag];
//    //pKt updated to use common functions in JSDB
//    //NSArray *rows = [[DAMApplicationModel model] getRows:sql];
//    NSArray *rows = [[JSDB sharedInstance] sql:sql params:nil success:nil fail:nil];
//    if (rows!=nil &&rows.count>0){
//        result = [rows objectAtIndex:0];
//    }
//    return result;
}

- (BOOL)allSwitchesOnInContainer:(NSArray*)container {
    BOOL result = YES;
    int index = 0;
    while (result && index < container.count) {
        UISwitch *uiSwitch = [container objectAtIndex:index];
        result &= uiSwitch.on;
        index++;
    }
    return result;
}

- (void)setMasterSwitches {
    allDomesticSwitch.on = [self allSwitchesOnInContainer:domesticSwitches];
    allCommercialSwitch.on = [self allSwitchesOnInContainer:commercialSwitches];
    allContentSwitch.on = allDomesticSwitch.on && allCommercialSwitch.on;
}

- (void)setInitialSwitchPositions {
    self.config = [[DAMApplicationModel model] settingsGetContainerConfig];
    for (NSDictionary *container in config) {
        int tag = [[container objectForKey:@"tag"] intValue];
        bool state = [[container objectForKey:@"state"] boolValue];
        UISwitch *uiSwitch = [self switchForTag:tag];
        if (uiSwitch != nil) {
            uiSwitch.on = state;
        } else {
            NSLog(@"DAMSettingsView.setInitialSwitchPositions.ERROR_CANNOT_FIND_UISWITCH container:%@", container);
        }
    }
    [self setMasterSwitches];
}

- (IBAction)allContentValueChanged:(UISwitch*)sender {
    [self allCommercialValueChanged:sender];
    [self allDomesticValueChanged:sender];
    allDomesticSwitch.on = sender.on;
    allCommercialSwitch.on = sender.on;
}

- (IBAction)allCommercialValueChanged:(UISwitch*)sender {
    for (UISwitch *commSwitch in commercialSwitches) {
        commSwitch.on = sender.on;
    }
}

- (IBAction)allDomesticValueChanged:(UISwitch*)sender {
    for (UISwitch *dommSwitch in domesticSwitches) {
        dommSwitch.on = sender.on;
    }
}

- (IBAction)switchValueChanged:(UISwitch*)sender {
    [self setMasterSwitches];
}

- (NSArray*)containerNamesForSwitches:(NSArray*)switches {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:switches.count];
    for (UISwitch *uiSwitch in switches) {
        NSDictionary *switchConfig = [self configForTag:uiSwitch.tag];
        if (switchConfig) {
            [result addObject:[switchConfig objectForKey:@"name"]];
        }
    }
    return result;
}

- (BOOL)leftButtonTapped:(id)sender {
    
    BOOL result;
    
    self.containersTurnedOn = [NSMutableArray arrayWithCapacity:100];
    self.containersTurnedOff = [NSMutableArray arrayWithCapacity:100];
    
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UISwitch class]]) {
            UISwitch *uiSwitch = (UISwitch *)view;
            NSDictionary *switchConfig = [self configForTag:uiSwitch.tag];
            
            NSLog(@"......switchConfig = %@", switchConfig);
            
            if (uiSwitch.on != [[switchConfig objectForKey:@"state"] boolValue]) {
                NSString *containerName = [switchConfig objectForKey:@"name"];
                if (!containerName) {
                    NSLog(@"DAMSettingsView.leftButtonTapped.ERROR_BAD_CONTAINER_NAME tag:%i", uiSwitch.tag);
                } else {
                    if (uiSwitch.on) {
                        [containersTurnedOn addObject:containerName];
                    } else {
                        [containersTurnedOff addObject:containerName];
                    }
                }
            }
        }
    }
    if (containersTurnedOff.count > 0) {
        //[self confirmDelete];
        [self isIDriveImpactedByRemoval];
        result = NO;
    }else{
        [self actionChanges];
        result = [super leftButtonTapped:sender];
    }
    
    return result;
}

- (void)confirmDelete {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                      message:@"Removing sections will remove the content from your iPad. To restore this it will need to be downloaded again."
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message addButtonWithTitle:@"Cancel"];
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"OK"]){
        [self actionChanges];
    }else{
        NSLog(@"Cancel remove was called.");
    }
    [super leftButtonTapped:nil];
}


- (void)actionChanges {
    NSLog(@"DAMSettingsView.actionChanges");
    
    if (containersTurnedOff.count > 0) {
        [[DAMApplicationModel model] settingsRemoveContainerNames:containersTurnedOff];
    }
    if (containersTurnedOn.count > 0) {
        [[DAMApplicationModel model] settingsAddContainerNames:containersTurnedOn];
    }
}
 
- (void)isIDriveImpactedByRemoval {
    NSLog(@"DAMSettingsView.isIDriveImpactedByRemoval");
    DAMApplicationModel *model = [DAMApplicationModel model];
    //[model registerCommand:self];
    [model iDriveListFilesAffectedByRemovingContainers:containersTurnedOff toPlugin:@"DAMSettingsView" andMethod:@selector(iDriveChangeCallback:)];
}

- (void)iDriveChangeCallback:(NSMutableArray*)options {
    NSLog(@"DAMSettingsView.iDriveChangeCallback");
    ////[[DAMApplicationModel model] unregisterCommand:self];
    
    NSArray *filesImpacted = (NSArray *)options;
    if (filesImpacted.count > 0) {
        [self showChangeImpactOnIDrive:filesImpacted];
    } else {
        [self actionChanges];
        self.config = [[DAMApplicationModel model] settingsGetContainerConfig];
        [damNavigator leftButtonTapped:self];
    }
}

- (void)showChangeImpactOnIDrive:(NSArray*)filesImpacted {
    self.mediaChangeView = [[DAMMediaChangeView alloc] initWithNibName:@"DAMMediaChangeView" bundle:nil];
    mediaChangeView.delegate = self;
    mediaChangeView.model = filesImpacted;
    [self.view addSubview:mediaChangeView.view];
    [mediaChangeView animateOnStage];
    [mediaChangeView release];
}

- (void)mediaChangeViewDidCancel {
    [mediaChangeView.view removeFromSuperview];
    self.mediaChangeView = nil;
    [self setInitialSwitchPositions];
}

- (void)mediaChangeViewDidOK {
    [mediaChangeView.view removeFromSuperview];
    self.mediaChangeView = nil;
    [self actionChanges];
    //Now must set switched to new state, so that when
    //leftButtonTapped: is called again, no changes are
    //detected.
    
    self.config = [[DAMApplicationModel model] settingsGetContainerConfig];
    [damNavigator leftButtonTapped:self];
}


- (IBAction)emailChoiceTapped:(id)sender {
    DAMEmailChooserView *chooserView = [[DAMEmailChooserView alloc] initWithNibName:@"DAMEmailChooserView" bundle:nil];
    chooserView.delegate = self;
    self.emailChoicePopover = [[UIPopoverController alloc] initWithContentViewController:chooserView];
    emailChoicePopover.delegate = self;
    CGRect presentRect = CGRectMake(0, emailChoiceButton.frame.size.height / 2, 0, 0);
    [emailChoicePopover presentPopoverFromRect:presentRect inView:emailChoiceButton permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    [chooserView release];
    [emailChoicePopover release];
}

- (void)contactChosen:(NSDictionary*)contactModel {
    NSString *email = [contactModel objectForKey:@"email"];
    [[DAMApplicationModel model] setUserEmail:email];
    emailLabel.text = [NSString stringWithFormat:@"my eMail: %@", email];
    [emailChoicePopover dismissPopoverAnimated:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.emailChoicePopover = nil;
}

- (void)modalViewOffstageAnimationDidFinish:(DAMModalViewBase*)modalView {
}

@end
