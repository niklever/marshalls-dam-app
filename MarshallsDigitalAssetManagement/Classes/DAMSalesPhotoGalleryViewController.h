//
//  DAMSalesPhotoGalleryViewController.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/30/14.
//
//

#import <UIKit/UIKit.h>
#include <AssetsLibrary/AssetsLibrary.h> 
#import "GAITrackedViewController.h"

@interface DAMSalesPhotoGalleryViewController : GAITrackedViewController

@property (nonatomic, assign)UIImage *seletedImage;
@property (nonatomic, retain)NSString *imageTitle;

@end
