//
//  DAMViewControllerBase.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMNavigatorViewController.h"
#import "GAITrackedViewController.h"

@class DAMMediaViewBase;
@class DAMSearchBarView;

@interface DAMViewControllerBase : GAITrackedViewController<DAMNavigatorDelegate> {
    
    //NSDictionary *dataModel;

    DAMNavigatorViewController *damNavigator;
}

@property (nonatomic, retain) NSDictionary *dataModel;

@property (nonatomic, retain) DAMNavigatorViewController *damNavigator;

- (void)pushView:(DAMViewControllerBase*)view;
- (void)pushView:(DAMViewControllerBase*)view withNavigation:(NSString*)navigationName;

- (DAMMediaViewBase*)launchMediaViewByIndex:(int)index;


- (DAMMediaViewBase*)launchMediaViewByNavigationName: (NSString *)navName queryString: (NSString *)query;

- (DAMViewControllerBase*)launch:(NSString*)controllerClass withNavigation:(NSString*)navigationName;

- (void)openAsset:(NSDictionary*)asset;

@end
