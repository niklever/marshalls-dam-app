//
//  DAMSalesPhotoViewController.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/28/14.
//
//

#import "DAMSalesPhotoViewController.h"
#import "DAMIDriveNavigatorViewController.h"
#import "AVCamViewController.h"

#import "JSDBV2.h"
#import "DAMAppDelegate.h"

#import "DAMAssetView.h"
#import "DAMMediaSendView.h"
#import "DAMMediaSaveView.h"
#import "DAMMAPViewController.h"


#define kSalePhotoIdentifier    @"DAMSalesPhotoCell"

@interface DAMSalesPhotoViewController ()<DAMIDriveNavigatorViewDelegate, DAMMediaSendViewDelegate, DAMMediaSaveViewDelegate, UITextFieldDelegate>  {
    
    DAMPhotoScrollView *photoScrollView;
    
    IBOutlet UIView *vSearchView;
    
    IBOutlet UITextField *txtSearch;
    IBOutlet UICollectionView *vCollection;
    IBOutlet UIView *vShare;
    IBOutlet UIButton *btnShare;
}

@property (nonatomic, retain)NSArray *arrPhoto;

- (IBAction)didClickClearSearch:(id)sender;
- (IBAction)didClickShare:(id)sender;
- (IBAction)didClickIDrive:(id)sender;
- (IBAction)didClickTag:(id)sender;
- (IBAction)didClickMapPin:(id)sender;
- (IBAction)didClickCamera:(id)sender;
- (IBAction)didClickHome:(id)sender;
- (IBAction)didClickCancel:(id)sender;
- (IBAction)didClickSend:(id)sender;
- (IBAction)didClickSave:(id)sender;


@end

@implementation DAMSalesPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self borderSearchView];
    [self getSalePhoto];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [vSearchView release];
    [txtSearch release];
    [vCollection release];
    [_arrPhoto release];
    [vShare release];
    [btnShare release];
    [super dealloc];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if([[JSMain sharedInstance] isIOS7]) {
        [self adjustViewsForOrientation:toInterfaceOrientation];
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation)orientation {
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    BOOL isLandscapeLeft = orientation == UIDeviceOrientationLandscapeLeft;
    if (isLandscapeLeft) {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width , self.window.frame.size.height)];
        CGRect viewBounds = [appDelegate.window bounds];
        viewBounds.origin.x = 20;
        viewBounds.size.height = viewBounds.size.height;
        appDelegate.window.frame = viewBounds;
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        CGRect viewBounds = [appDelegate.window bounds];
        viewBounds.origin.x = -20;
        viewBounds.size.height = viewBounds.size.height;
        appDelegate.window.frame = viewBounds;
    }
}

#pragma mark - IBACTION
- (IBAction)didClickClearSearch:(id)sender {
    
    [txtSearch setText:@""];
    
}

- (IBAction)didClickShare:(id)sender {
    [txtSearch resignFirstResponder];
    [self showShareView];
}

- (IBAction)didClickIDrive:(id)sender {
    
    [txtSearch resignFirstResponder];
    
    DAMIDriveNavigatorViewController *iDriveController  =   [[DAMIDriveNavigatorViewController alloc] initWithNibName:@"DAMIDriveNavigatorViewController" bundle:nil];
    [_damNavigation setIsNewCase:YES];
    [iDriveController setDamNavigator:_damNavigation];
    [iDriveController.view addSubview:_damNavigation.view];
    [iDriveController.damNavigator pushNavigation:@"iDrive"];
    [iDriveController setDelegate:self];
    
    [self.navigationController pushViewController:iDriveController animated:YES];
    [iDriveController release];
}

- (IBAction)didClickTag:(id)sender {
    [txtSearch resignFirstResponder];
}

- (IBAction)didClickMapPin:(id)sender {
    [txtSearch resignFirstResponder];
    
    DAMMAPViewController *controller    =   [[DAMMAPViewController alloc] initWithNibName:@"DAMMAPViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (IBAction)didClickCamera:(id)sender {
    [txtSearch resignFirstResponder];
    
    AVCamViewController *controller =   [[AVCamViewController alloc] initWithNibName:@"AVCamViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (IBAction)didClickHome:(id)sender {
    
    [txtSearch resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)didClickCancel:(id)sender {
    [self hideShareView];
}

- (IBAction)didClickSend:(id)sender {
    if([photoScrollView.selectedAssets count] == 0){
        [self showErrorMessage];
    }
    else{
        /* open send view */
        DAMMediaSendView *sendView  =   [[DAMMediaSendView alloc] initWithNibName:@"DAMMediaSendView" bundle:nil];
        sendView.delegate   =   self;
        [self addChildViewController:sendView];
        [self.view addSubview:sendView.view];
        [sendView animateOnStage];
        [sendView release];
    }
}

- (IBAction)didClickSave:(id)sender {
    
    if([photoScrollView.selectedAssets count] == 0){
        [self showErrorMessage];
    }
    else{
        /* open save view */
        DAMMediaSaveView *saveView  =   [[DAMMediaSaveView alloc] initWithNibName:@"DAMMediaSaveView" bundle:nil];
        saveView.delegate           =   self;
        [self addChildViewController:saveView];
        [self.view addSubview:saveView.view];
        [saveView animateOnStage];
        [saveView release];
    }
    
}

#pragma mark - private method
- (void) borderSearchView{
    vSearchView.layer.cornerRadius  =   5;
    vSearchView.clipsToBounds       =   YES;
    
    txtSearch.font       = [UIFont fontWithName:@"MyriadPro-Regular" size:17];
    
    
//    if ([txtSearch respondsToSelector:@selector(setAttributedPlaceholder:)]) {
//        UIColor *color = [UIColor colorWithWhite:1 alpha:0.5];
//        txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtSearch.placeholder attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: txtSearch.font}];
//    } else {
//        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
//        // TODO: Add fall-back code to set placeholder color.
//    }

}

- (void) getSalePhoto{
    
    photoScrollView = [[DAMPhotoScrollView alloc] initWithFrame:CGRectMake(0, 0, 1024, 748)];
    [self.view insertSubview: photoScrollView atIndex:0];
    photoScrollView.backgroundColor = [UIColor blackColor];
    photoScrollView.delegate = self;
    [photoScrollView loadImageMetaData:nil];
    [photoScrollView release];
    
}

- (void) showShareView{
    
    [photoScrollView setSelecting:YES];
    [UIView animateWithDuration:0.5
                     animations:^{
                         [btnShare setAlpha:0.0];
                         [vShare setFrame:CGRectMake(417, vShare.frame.origin.y, vShare.frame.size.width, vShare.frame.size.height)];
                     } completion:^(BOOL finished) {
                         [btnShare setHidden:YES];
                     }];
    
}

- (void) hideShareView{
    
    [btnShare setHidden:NO];
    [photoScrollView setSelecting:NO];
    [photoScrollView layoutSubviews];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [btnShare setAlpha:1.0];
                         [vShare setFrame:CGRectMake(1024, vShare.frame.origin.y, vShare.frame.size.width, vShare.frame.size.height)];
                     } completion:^(BOOL finished) {
                     }];
}

- (void) showErrorMessage{
    UIAlertView *alvError   =   [[UIAlertView alloc] initWithTitle:nil
                                                           message:@"no media selected."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
    [alvError show];
    [alvError release];
}

#pragma mark - PhotoScrollViewDelegate
- (void)imageTapped:(NSDictionary*)image{
    
    [txtSearch resignFirstResponder];
    
    [(NSMutableDictionary *)image setObject: [NSString stringWithFormat:@"%f", photoScrollView.scrollView.contentOffset.x] forKey:@"offsetX"];

    DAMAssetView *assetView = [[DAMAssetView alloc] initWithNibName:@"DAMAssetView" bundle:nil];
    [_damNavigation setIsNewCase:YES];
    [assetView setDamNavigator:_damNavigation];
    [assetView.view addSubview:_damNavigation.view];
    [assetView.damNavigator pushNavigation:@"Brochure-View"];
    assetView.model = image;
    assetView.mediaType = [image objectForKey:@"mediaType"];;
    
    [self.navigationController pushViewController:assetView animated:YES];
    [assetView release];
}

- (void)layoutFinished{
    
}

- (void)noResults:(NSDictionary *) result{
    
}

- (void)clearNoResultsView {

}

#pragma mark - DAMSendViewDelegate
- (void)sendMediaTapped:(DAMMediaSendView *)sendView{

    [[DAMApplicationModel model] emailAssets:photoScrollView.selectedAssets toRecipient:sendView.emailAddress.text withSubject:sendView.subject.text andMessage:sendView.message.text];
    
    [self hideShareView];
    
    [sendView.view removeFromSuperview];
    [sendView removeFromParentViewController];
}

- (void)cancelMediaSendTapped:(DAMMediaSendView *)sendView{
    
    [sendView.view removeFromSuperview];
    [sendView removeFromParentViewController];
}

#pragma mark - DAMMedidaSaveDelegate
- (void)saveActioned:(DAMMediaSaveView *)saveView ToFolder:(NSString*)folder{
    
    [[DAMApplicationModel model] iDriveCopyAssets:photoScrollView.selectedAssets toFolder:folder];
    
    [self hideShareView];
    
    [saveView.view removeFromSuperview];
    [saveView removeFromParentViewController];
    
}

- (void)saveCancelled:(DAMMediaSaveView *)saveView{
    
    [saveView.view removeFromSuperview];
    [saveView removeFromParentViewController];
    
}


#pragma mark - UITEXTFIELD DELEGATE
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self hideShareView];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}


@end
