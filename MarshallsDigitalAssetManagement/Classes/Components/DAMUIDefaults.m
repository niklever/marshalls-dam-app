//
//  DAMUIDefaults.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMUIDefaults.h"

@implementation DAMUIDefaults
@synthesize textFont;
@synthesize iDriveFont;
@synthesize iDriveFreespaceFont;
@synthesize navigatorTitleFont;
@synthesize carouselLabelFont;
@synthesize thumbnailTitleFont;
@synthesize thumbnailSubtitleFont;
@synthesize modalTitleFont;
@synthesize mediaSendFont;
@synthesize busAreasFont;
@synthesize textColor;
@synthesize tagLabelFont;

DAMUIDefaults *singleton;

+ (DAMUIDefaults*)defaults {
    if (singleton == nil) {
        singleton = [[DAMUIDefaults alloc] init];
    }
    return singleton;
}

- (id)init {
    self = [super init];
    if (self) {
        int fontSizeH1 = 24;
        int fontSizeH2 = 20;
        int fontSizeH3 = 18;
        int fontSizeH4 = 18;
        int fontSizeH5 = 13;
        textFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH2]; [textFont retain];
        iDriveFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH2]; [iDriveFont retain];
        iDriveFreespaceFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH5]; [iDriveFreespaceFont retain];
        navigatorTitleFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH1]; [navigatorTitleFont retain];
        carouselLabelFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH1]; [carouselLabelFont retain];
        thumbnailTitleFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH2]; [thumbnailTitleFont retain];
        thumbnailSubtitleFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH3]; [thumbnailSubtitleFont retain];
        modalTitleFont = [UIFont fontWithName:@"MyriadPro-Semibold" size:fontSizeH1]; [modalTitleFont retain];
        mediaSendFont = [UIFont fontWithName:@"MyriadPro-Regular" size:fontSizeH1]; [mediaSendFont retain];
        busAreasFont = [UIFont fontWithName:@"MyriadPro-Semibold" size:fontSizeH3]; [busAreasFont retain];
        
        tagLabelFont = [UIFont fontWithName:@"MyriadPro-Light" size:fontSizeH4]; [tagLabelFont retain];

        textColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]; [textColor retain];
    }
    return self;
}

- (void)dealloc {
    [tagLabelFont release];
    [textFont release];
    [iDriveFont release];
    [iDriveFreespaceFont release];
    [navigatorTitleFont release];
    [carouselLabelFont release];
    [thumbnailTitleFont release];
    [thumbnailSubtitleFont release];
    [modalTitleFont release];
    [textColor release];
    [busAreasFont release];
    [super dealloc];
}

- (float)textPlaceholderFadeTime {
    return 0.25f;
}

@end
