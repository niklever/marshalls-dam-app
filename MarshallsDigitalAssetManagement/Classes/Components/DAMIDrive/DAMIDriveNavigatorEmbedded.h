//
//  DAMIDriveNavigatorEmbedded.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 19/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMApplicationModel.h"
#import "DAMBreadCrumbView.h"
#import "DAMIDriveTableView.h"
#import "DAMViewControllerBase.h"

@protocol DAMIDriveNavigatorViewDelegate <NSObject>

- (void)assetSelected:(NSDictionary*)asset;

@end

@interface DAMIDriveNavigatorEmbedded : UIViewController<DAMBreadCrumbViewDelegate, DAMIDriveTableViewDelegate> {
    
    id<DAMIDriveNavigatorViewDelegate> delegate;
    
    DAMBreadCrumbView *breadcrumb;
    
    UIView *breadcrumbPlaceholder;
    DAMIDriveTableView *navigator1;
    DAMIDriveTableView *navigator2;
    int visibleNavigatorIndex;
}
@property (nonatomic, assign) id<DAMIDriveNavigatorViewDelegate> delegate;

@property (nonatomic, retain) DAMBreadCrumbView *breadcrumb;

@property (nonatomic, retain) IBOutlet UIView *breadcrumbPlaceholder;
@property (nonatomic, retain) IBOutlet DAMIDriveTableView *navigator1;
@property (nonatomic, retain) IBOutlet DAMIDriveTableView *navigator2;

//- (IBAction)testRefresh:(id)sender;
//- (IBAction)testSelectToggle:(id)sender;

@end
