//
//  DAMIDriveNavigatorViewController.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMApplicationModel.h"
#import "DAMBreadCrumbView.h"
#import "DAMIDriveTableView.h"
#import "DAMViewControllerBase.h"
#import "DAMMediaSendView.h"
#import "DAMIDriveMoveToView.h"
#import "DAMMediaChangeView.h"

@protocol DAMIDriveNavigatorViewDelegate <NSObject>

- (void)assetSelected:(NSDictionary*)asset;

@end

@interface DAMIDriveNavigatorViewController : DAMViewControllerBase<DAMBreadCrumbViewDelegate, DAMIDriveTableViewDelegate, DAMMediaSendViewDelegate, DAMIDriveMoveToViewDelegate, DAMMediaChangeViewDelegate> {
    
    id<DAMIDriveNavigatorViewDelegate> delegate;
    
    DAMBreadCrumbView *breadcrumb;
    
    UIView *breadcrumbPlaceholder;
    DAMIDriveTableView *navigator1;
    DAMIDriveTableView *navigator2;
    int visibleNavigatorIndex;
    
    DAMMediaSendView *sendView;
    DAMIDriveMoveToView *moveToView;
}
@property (nonatomic, assign) id<DAMIDriveNavigatorViewDelegate> delegate;

@property (nonatomic, retain) DAMBreadCrumbView *breadcrumb;

@property (nonatomic, retain) IBOutlet UIView *breadcrumbPlaceholder;
@property (nonatomic, retain) IBOutlet DAMIDriveTableView *navigator1;
@property (nonatomic, retain) IBOutlet DAMIDriveTableView *navigator2;

@property (nonatomic, retain) DAMMediaSendView *sendView;
@property (nonatomic, retain) DAMIDriveMoveToView *moveToView;

@end
