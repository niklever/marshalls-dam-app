//
//  DAMBreadCrumbView.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMBreadCrumbView.h"
#import "DAMUIDefaults.h"

#define defaultBreadcrumbSize 16

@interface DAMBreadCrumbView () {
    
    float breadCrumbXPos;
}

@end

@implementation DAMBreadCrumbView
@synthesize delegate;
@synthesize currentPath;
@synthesize horizontalSeparator;
@synthesize homeButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.currentPath = [NSMutableArray arrayWithCapacity:defaultBreadcrumbSize];
        
        breadCrumbXPos = 148;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.currentPath = nil;
    self.horizontalSeparator = nil;
    self.homeButton = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation = UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (NSString*)currentFolder {
    return [NSString stringWithFormat:@"/%@", [currentPath componentsJoinedByString:@"/"]];
}

- (NSString*)subFolderOfCurrent:(NSString*)subFolder {
    NSMutableArray *tmpPath = [NSMutableArray arrayWithArray:currentPath];
    [tmpPath addObject:subFolder];
    return [NSString stringWithFormat:@"/%@", [tmpPath componentsJoinedByString:@"/"]];
}

- (NSString*)navigateToSubFolder:(NSString*)folder {
    NSString *topItem = [[folder componentsSeparatedByString:@"/"] lastObject];
    [(NSMutableArray*)currentPath addObject:topItem];
    
    [self addUIForTopItem];
    
    return self.currentFolder;
}

- (void)addUIForTopItem {
//    int fontSize = 18;
    float textPadding = 10;
    float buttonY = 21;
    float buttonHeight = 40;
    float separatorWidth = 48;
    float folderImageWidth = 55;
    
    UIFont *font = [DAMUIDefaults defaults].iDriveFont;
    UIColor *textColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];
    
    UILabel *separator = [[UILabel alloc] initWithFrame:CGRectMake(breadCrumbXPos, 0, separatorWidth, self.view.frame.size.height)];
    separator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    separator.font = font;
    separator.text = @">";
    separator.textAlignment = UITextAlignmentCenter;
    separator.textColor = textColor;
    [self.view addSubview:separator];
    [separator release];

    breadCrumbXPos += separatorWidth;
    UIButton *folderButton = [[UIButton alloc] initWithFrame:CGRectMake(breadCrumbXPos, buttonY, folderImageWidth, buttonHeight)];
    folderButton.tag = currentPath.count;
    [folderButton setBackgroundImage:[UIImage imageNamed:@"folder-small.png"] forState:UIControlStateNormal];
    [folderButton addTarget:self action:@selector(breadcrumbTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:folderButton];
    [folderButton release];
    
    breadCrumbXPos += folderImageWidth;
    
    //TODO - need to calculate folderLabelWidth dynamically
    breadCrumbXPos += textPadding;
    NSString *folderText = [currentPath lastObject];
    CGSize textSize = [folderText sizeWithFont:font];
    UILabel *folderLabel = [[UILabel alloc] initWithFrame:CGRectMake(breadCrumbXPos, 0, textSize.width, self.view.frame.size.height)];
    folderLabel.font = font;
    folderLabel.text = folderText;
    folderLabel.textColor = textColor;
    folderLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:folderLabel];
    [folderLabel release];
    
    breadCrumbXPos += textSize.width;
}

- (IBAction)breadcrumbTapped:(UIButton*)sender {
    int level = sender.tag;
    int numPopped = currentPath.count - level;
    if (numPopped > 0) {
        for (int i = 0; i < numPopped; i++) {
            //remove folder label, button and separator from subviews:
            [[self.view.subviews lastObject] removeFromSuperview];
            [[self.view.subviews lastObject] removeFromSuperview];
            [[self.view.subviews lastObject] removeFromSuperview];
        }
        UIView *rightmostView = [self.view.subviews lastObject];
        breadCrumbXPos = rightmostView.frame.origin.x + rightmostView.frame.size.width;
        NSRange subRange = NSMakeRange(currentPath.count - numPopped, numPopped);
        [(NSMutableArray*)currentPath removeObjectsInRange:subRange];
        if (delegate) {
            [delegate pathChangedTo:self.currentFolder];
        }
    }
}

@end
