//
//  DAMIDriveTableView.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 17/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"
#import "DAMIDriveTableView.h"
#import "DAMIDriveTableViewCell.h"

@interface DAMIDriveTableView() {
    DAMIDriveTableViewCell *addNewFolderCell;
    UITextField *addNewFolderTextField;
}
@end

@implementation DAMIDriveTableView
@synthesize parentPlugin;
@synthesize navigationDelegate;
@synthesize currentFolderContents;
@synthesize addNewFolderUI;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self sharedInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self sharedInit];
    }
    return self;
}

- (void)sharedInit {
    self.delegate = self;
    self.dataSource = self;
}

- (void)dealloc {
    if (self.currentFolderContents && [self.currentFolderContents count] > 0) {
        //self.currentFolderContents = nil;
        [self.currentFolderContents release];
    }
    self.parentPlugin = nil;
    self.addNewFolderUI = nil;
    [super dealloc];
}

- (BOOL)selectionMode { return selectionMode; }

- (void)setSelectionMode:(BOOL)newSelectionMode {
    selectionMode = newSelectionMode;
    self.allowsMultipleSelection = selectionMode;
    if (!selectionMode) {
        //reset any disclosure icons that may have been set to selected
        [self reloadData];
    }
}

- (BOOL)folderSelectionMode { return folderSelectionMode; }

- (void)setFolderSelectionMode:(BOOL)newFolderSelectionMode {
    folderSelectionMode = newFolderSelectionMode;
}

- (NSArray*)selectedAssets {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:self.indexPathsForSelectedRows.count];
    for (NSIndexPath *indexPath in self.indexPathsForSelectedRows) {
        int row = indexPath.row;
        [result addObject:[currentFolderContents objectAtIndex:row]];
    }
    return result;
}

- (void)removeSelectedAssets {
    if (currentFolderContents && [(NSMutableArray*)currentFolderContents count] > 0) {
        [(NSMutableArray*)currentFolderContents removeObjectsInArray:self.selectedAssets];
    }
    
    //TODO - animate removal of iDrive assets
    [self reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return folderSelectionMode ? currentFolderContents.count + 1 : currentFolderContents.count;
}

- (CGFloat)cellHeight {
    return folderSelectionMode ? 74 : 103;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    int row = indexPath.row;
    DAMIDriveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"iDriveNavigatorCell"];
    if (cell == NULL) {
        cell = [[DAMIDriveTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"iDriveNavigatorCell"];
#ifdef DEBUG
        NSLog(@"createCell row:%i", row);
#endif
    }
    if (row < currentFolderContents.count) {
        NSDictionary *currentItem = [currentFolderContents objectAtIndex:row];
        int itemType = [[currentItem objectForKey:@"type"] intValue];
        if (itemType == 1) {
            [self setupCellForFolder:cell withItem:currentItem];
        } else {
            //file
            [self setupCellForFile:cell withItem:currentItem];
        }
        if (selectionMode) {
            NSArray *selectedRows = tableView.indexPathsForSelectedRows;
//            NSLog(@"selectedRows:%@", selectedRows);
            if ([selectedRows containsObject:indexPath]) {
                cell.disclosure.image = [UIImage imageNamed:@"AssetSelectedBadge.png"];
            }
        }
        cell.textLabel.font = [DAMUIDefaults defaults].iDriveFont;
        cell.textLabel.textColor = [DAMUIDefaults defaults].textColor;
    } else {
        //show the "add a new folder" row
        cell.thumbnail.image = [UIImage imageNamed:@"folder-add-.png"];
        cell.title.text = @"Add a new folder";
        CGRect frame = cell.thumbnail.frame;
        frame.size.width = cell.thumbnail.image.size.width;
        frame.size.height = cell.thumbnail.image.size.height;
        //        frame.origin.y = (self.cellHeight - frame.size.height) / 2;
        cell.thumbnail.frame = frame;
        [self verticallCentreView:cell.thumbnail];
        [self verticallCentreView:cell.title];

        cell.disclosure.hidden = YES;
        cell.added.hidden = YES;
        cell.type.hidden = YES;
    }
    
    //UIImage *pattern = [UIImage imageNamed:@"bg.png"];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    //[cell.contentView setBackgroundColor:[UIColor colorWithPatternImage:pattern]];
    cell.contentView.backgroundColor = [UIColor clearColor];
    //[cell.contentView setBackgroundColor:[UIColor clearColor]];
    return cell;
}

- (void)verticallCentreView:(UIView*)view {
    CGRect frame = view.frame;
    frame.origin.y = (self.cellHeight - frame.size.height) / 2;
    view.frame = frame;

}

- (NSString*)formatTimeForItem:(NSDictionary*)item {
    NSString *result = @"Not available";
    NSString *ctime = [item objectForKey:@"ctime"];
    if (ctime) {
        //client request to change dateformat http://gemini.zinofi.co.uk/project/J0223/25/item/226
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        NSDate *currentDate = [df dateFromString: ctime];
        //[dateFormatter release];
        
        //NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd/MM/yyyy"];
        NSString *displayDate = [df stringFromDate: currentDate];
        [df release];
#ifdef DEBUG
        NSLog(@"ctime == %@ displayDate == %@ currentDate == %@", ctime, displayDate, currentDate);
#endif
        
        result = [NSString stringWithFormat:@"Added %@", displayDate];//[ctime substringToIndex:10]];
    }
    return result;
}

- (void)setupCellForFolder:(DAMIDriveTableViewCell *)cell withItem:(NSDictionary *)item {
    //folder
    NSString *folderPath = [item objectForKey:@"path"];
    NSMutableArray *pathComps = [NSMutableArray arrayWithArray:[folderPath componentsSeparatedByString:@"/"]];
    
    cell.title.text = [pathComps lastObject];
    cell.added.text = [self formatTimeForItem:item];
    cell.disclosure.image = [UIImage imageNamed:@"DAM_iDrive_Disclosure.png"];
    if (!folderSelectionMode) {
        cell.thumbnail.image = [UIImage imageNamed:@"folder-lrg.png"];
        cell.type.text = @"Folder";
        //TODO - folder added date
    } else {
        cell.thumbnail.image = [UIImage imageNamed:@"folder-small.png"];
        CGRect frame = cell.thumbnail.frame;
        frame.size.width = cell.thumbnail.image.size.width;
        frame.size.height = cell.thumbnail.image.size.height;
//        frame.origin.y = (self.cellHeight - frame.size.height) / 2;
        cell.thumbnail.frame = frame;
        [self verticallCentreView:cell.thumbnail];
        [self verticallCentreView:cell.title];
        [self verticallCentreView:cell.disclosure];
        
        frame = cell.disclosure.frame;
        frame.origin.x = 564;
        cell.disclosure.frame = frame;
        cell.added.hidden = YES;
        cell.type.hidden = YES;
    }
}

- (void)setupCellForFile:(DAMIDriveTableViewCell *)cell withItem:(NSDictionary *)item {
    NSString *mediaType = [item objectForKey:@"mediaType"];
    if ([mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos]) {
//        NSString *mediaRef = [item objectForKey:@"mediaRef"];
//        NSString *imageUrl = [NSString stringWithFormat:@"/AssetStore/%@.jpg?MaxWidth=500", mediaRef];
        NSString *imageUrl = [[DAMApplicationModel model] thumbnailForAsset:item inView:nil];
        
        [cell.thumbnail setUrl:imageUrl andPlaceholderIndex:2];
    } else {
        [cell.thumbnail setUrl:[item objectForKey:@"thumbnail"] andPlaceholderIndex:2];
    }
    NSString *title = [item objectForKey:@"title"];
    cell.title.text = title;
    cell.type.text = mediaType;
    cell.added.text = [self formatTimeForItem:item];
    cell.disclosure.image = nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    int row = indexPath.row;
    if (row == currentFolderContents.count) {
        DAMIDriveTableViewCell *cell = (DAMIDriveTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        [self setupAddNewFolderUIForCell:cell];
    } else {
        [self dismissAddNewFolderUI];
        if (selectionMode) {
            DAMIDriveTableViewCell *cell = (DAMIDriveTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            cell.disclosure.image = [UIImage imageNamed:@"selected.png"];
        } else {
            NSDictionary *currentItem = [currentFolderContents objectAtIndex:row];
            int itemType = [[currentItem objectForKey:@"type"] intValue];
            if (itemType == 1) {
                //folder
                NSString *selectedFolder = [currentItem objectForKey:@"path"];
                if (navigationDelegate) {
                    NSString *newFolder = [navigationDelegate viewNavigatedToFolder:selectedFolder];
                    if (folderSelectionMode) {
                        //[[DAMApplicationModel model] iDriveListFolderFolders:newFolder toPlugin:parentPlugin];
                        [[DAMApplicationModel model] iDriveListFolderFolders:newFolder delegate: parentPlugin];
                    } else {
                        //[[DAMApplicationModel model] iDriveListFolder:newFolder toPlugin:parentPlugin];
                        [[DAMApplicationModel model] iDriveListFolder:newFolder delegate: parentPlugin];
                    }
                }
            } else {
                if (navigationDelegate) {
                    [navigationDelegate viewSelectedAsset:currentItem];
                }
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (selectionMode) {
        DAMIDriveTableViewCell *cell = (DAMIDriveTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        int row = indexPath.row;
        NSDictionary *currentItem = [currentFolderContents objectAtIndex:row];
        int itemType = [[currentItem objectForKey:@"type"] intValue];
        if (itemType == 1) {
            cell.disclosure.image = [UIImage imageNamed:@"DAM_iDrive_Disclosure.png"];
        } else {
            cell.disclosure.image = nil;
        }
    }
}

- (void)setupAddNewFolderUIForCell:(DAMIDriveTableViewCell *)cell {
//    DAMIDriveTableViewCell *cell = (DAMIDriveTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];

    addNewFolderCell = cell;
    
    self.addNewFolderUI = [[UIView alloc] initWithFrame:cell.frame];
//    addNewFolderUI.backgroundColor = [UIColor redColor];
    addNewFolderUI.backgroundColor = [UIColor clearColor];
    [self addSubview:addNewFolderUI];
    
    //animate the table upwards to make room for the keyboard, if necessary
    float keyboardTopY = 400;
    UIView *rootView = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    CGPoint cellBottomEdgeInWindowCoords = [cell convertPoint:CGPointMake(0, cell.frame.size.height) toView:rootView];
    if (cellBottomEdgeInWindowCoords.y > keyboardTopY) {
        float requiredUpwardTranslation = cellBottomEdgeInWindowCoords.y - keyboardTopY;
        [UIView beginAnimations:@"requiredUpwardTranslation" context:nil];
        [UIView setAnimationDuration:0.5];
        self.transform = CGAffineTransformMakeTranslation(0, -requiredUpwardTranslation);
        [UIView commitAnimations];
    }

    float textFieldHeight = 31;
    float yFudge = 4;
    
    float tfY = (cell.frame.size.height - textFieldHeight) / 2;
    CGRect textFieldFrame = CGRectMake(cell.title.frame.origin.x, tfY + yFudge, cell.title.frame.size.width, cell.title.frame.size.height);    
    addNewFolderTextField = [[UITextField alloc] initWithFrame:textFieldFrame];
    
    addNewFolderTextField.font = [DAMUIDefaults defaults].iDriveFont;
    addNewFolderTextField.textColor = [DAMUIDefaults defaults].textColor;
    
    addNewFolderTextField.delegate = self;
    
    [addNewFolderUI addSubview:addNewFolderTextField];
    [addNewFolderTextField release];
    
    [addNewFolderTextField becomeFirstResponder];
    
    //810, 42
    
    float addButtonX = 405;
    float buttonY = 21;
    
    float buttonWidth = 90;
    float buttonHeight = 35;
    float buttonPadding = 10;
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(addButtonX, buttonY, buttonWidth, buttonHeight)];
    [addButton setBackgroundImage:[UIImage imageNamed:@"btn-add.png"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [addNewFolderUI addSubview:addButton];
    [addButton release];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(addButtonX + buttonWidth + buttonPadding, buttonY, buttonWidth, buttonHeight)];
    [cancelButton setBackgroundImage:[UIImage imageNamed:@"btn-cancel.png"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [addNewFolderUI addSubview:cancelButton];
    [cancelButton release];
}

- (void)dismissAddNewFolderUI {
    if (addNewFolderUI) {
        [addNewFolderUI removeFromSuperview];
        self.addNewFolderUI = nil;
//        addNewFolderCell.title.alpha = 1.0;

        [UIView beginAnimations:@"dismissAddNewFolderUI" context:nil];
        [UIView setAnimationDuration:0.5];
        self.transform = CGAffineTransformIdentity;
        addNewFolderCell.title.alpha = 1.0;
        [UIView commitAnimations];

        addNewFolderCell = nil;
        addNewFolderTextField = nil;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
    [self addButtonTapped:textField];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (addNewFolderCell) {
        NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [UIView beginAnimations:@"shouldChangeCharactersInRange" context:nil];
        [UIView setAnimationDuration:[[DAMUIDefaults defaults] textPlaceholderFadeTime]];
        if (newText.length > 0) {
            addNewFolderCell.title.alpha = 0.0;
        } else {
            addNewFolderCell.title.alpha = 1.0;
        }
        [UIView commitAnimations];
    }
    return YES;
}

- (void)addButtonTapped:(id)sender {
    if (navigationDelegate) {
        [navigationDelegate folderAdded:addNewFolderTextField.text];
    }
//    [self reloadData];
    [self dismissAddNewFolderUI];
}

- (void)cancelButtonTapped:(id)sender {
    [self dismissAddNewFolderUI];
//    addNewFolderCell.title.alpha = 1.0;
//    addNewFolderCell = nil;
//    addNewFolderTextField = nil;
}

@end
