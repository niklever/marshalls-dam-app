//
//  DAMIDriveNavigatorViewController.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMIDriveNavigatorViewController.h"
#import "DAMFreeSpaceView.h"

@interface DAMIDriveNavigatorViewController () {
    
    DAMMediaChangeView *mediaChangeView;
}
@property (nonatomic, retain) DAMMediaChangeView *mediaChangeView;

@end

@implementation DAMIDriveNavigatorViewController
@synthesize delegate;
@synthesize breadcrumb;
@synthesize breadcrumbPlaceholder;
@synthesize navigator1;
@synthesize navigator2;
@synthesize sendView;
@synthesize moveToView;
@synthesize mediaChangeView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.breadcrumb = [[DAMBreadCrumbView alloc] initWithNibName:@"DAMBreadCrumbView" bundle:nil];
        breadcrumb.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.breadcrumbPlaceholder addSubview:breadcrumb.view];
    
    //self.navigator1.parentPlugin = @"DAMIDriveNavigatorViewController";
    //self.navigator2.parentPlugin = @"DAMIDriveNavigatorViewController";
    self.navigator1.backgroundColor = [UIColor clearColor];
    self.navigator2.backgroundColor = [UIColor clearColor];
    self.navigator1.parentPlugin = self;
    self.navigator2.parentPlugin = self;
    visibleNavigatorIndex = 0;
    
    DAMFreeSpaceView *freeSpace = [[DAMFreeSpaceView alloc] initWithNibName:@"DAMFreeSpaceView" bundle:nil];
    
    [damNavigator.customButtonView addSubview:freeSpace.view];
    [damNavigator layoutCustomButtonsRightToLeft];
    [freeSpace release];
    
    ////[[DAMApplicationModel model] registerCommand:self];
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [damNavigator setDelegate:self];
    
//    //[[DAMApplicationModel model] registerCommand:self];
//    NSString *currentFolder = [breadcrumb currentFolder];
//    NSLog(@"DAMIDriveNavigatorViewController.viewWillAppear currentFolder:%@", currentFolder);
//    [[DAMApplicationModel model] iDriveListFolder:currentFolder toPlugin:@"DAMIDriveNavigatorViewController"];
    [self refresh];
    
    ////[[DAMApplicationModel model] registerCommand:self];
    [self performSelector:@selector(checkDeletions) withObject:nil afterDelay:2];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.breadcrumb = nil;
    self.breadcrumbPlaceholder = nil;
    self.navigator1 = nil;
    self.navigator2 = nil;
    self.sendView = nil;
    self.moveToView = nil;
    self.mediaChangeView = nil;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    ////[[DAMApplicationModel model] unregisterCommand:self];
}

- (void)refresh {
    NSString *currentFolder = [breadcrumb currentFolder];
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.refresh currentFolder:%@", currentFolder);
#endif
    //[[DAMApplicationModel model] iDriveListFolder:currentFolder toPlugin:@"DAMIDriveNavigatorViewController"];
    [[DAMApplicationModel model] iDriveListFolder:currentFolder delegate:self];
    
    //[[DAMApplicationModel model] iDriveListFolder:currentFolder toPlugin:@"DAMIDriveNavigatorViewController"];
}

- (DAMIDriveTableView*)visibleNavigator {
    NSArray *navigators = [NSArray arrayWithObjects:navigator1, navigator2, nil];
    return [navigators objectAtIndex:visibleNavigatorIndex];
}

- (void)flipVisibleNavigator:(BOOL)animateRight {
    NSArray *navigators = [NSArray arrayWithObjects:navigator1, navigator2, nil];
    DAMIDriveTableView *visibleNav = [navigators objectAtIndex:visibleNavigatorIndex];
    DAMIDriveTableView *offscreenNav = [navigators objectAtIndex:(visibleNavigatorIndex+1) % 2];
    
    float offscreenStartPos = animateRight ? 1024 : -1024;
    float onScreenEndPos = animateRight ? -1024 : 1024;
    offscreenNav.transform = CGAffineTransformMakeTranslation(offscreenStartPos, 0);
    
    [UIView beginAnimations:@"iDriveNavigate" context:nil];
    [UIView setAnimationDuration:0.5];
    
    visibleNav.alpha = 0;
    offscreenNav.alpha = 1;
    visibleNav.transform = CGAffineTransformMakeTranslation(onScreenEndPos, 0);
    offscreenNav.transform = CGAffineTransformIdentity;
    
    [UIView commitAnimations];
    
    visibleNavigatorIndex = (visibleNavigatorIndex + 1) % 2;
}

- (void)pathChangedTo:(NSString*)path {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.pathChangedTo path:%@", path);
#endif
    [self flipVisibleNavigator:NO];
    //[[DAMApplicationModel model] iDriveListFolder:path toPlugin:@"DAMIDriveNavigatorViewController"];
    [[DAMApplicationModel model] iDriveListFolder:path delegate:self];
}

- (void)lsResults:(id)dict {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.lsResults 2 params:%@", dict);
#endif
    //TODO Seems a bit weird this, I know... need to look into it
    //NSArray *downloadItems = (NSArray *)dict;
    NSArray *downloadItems = (NSArray *)dict;
    self.visibleNavigator.currentFolderContents = [downloadItems mutableCopy];
    [self.visibleNavigator reloadData];
}

- (NSString*)viewNavigatedToFolder:(NSString*)folder {
    
    [self flipVisibleNavigator:YES];
    NSString *newFolder = [breadcrumb navigateToSubFolder:folder];
    return newFolder;
}

- (void)viewSelectedAsset:(NSDictionary*)asset {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.viewSelectedAsset asset:%@", asset);
#endif
    if (delegate) {
        [delegate assetSelected:asset];
    }
}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.rightButtonTapped self:%@", self);
#endif
    [self addIDriveButtons];
    [navigator1 setSelectionMode:YES];
    [navigator2 setSelectionMode:YES];
}

- (void)addIDriveButtons {

    NSArray *buttonSpec = [NSArray arrayWithObjects:@"btn-cancel.png", @"cancelTapped:", @"exp-btn-move.png", @"moveTapped:",
                           @"exp-btn-send.png", @"sendTapped:", @"exp-btn-delete.png", @"deleteTapped:", nil];
    [damNavigator addRolloutButtons:buttonSpec forRightButtonIndex:0 withTarget:self];
    
}

- (void)deleteTapped:(id)sender {
    [[DAMApplicationModel model] iDriveDelete:[self.visibleNavigator selectedAssets]];
    [self.visibleNavigator removeSelectedAssets];
    [damNavigator dismissRollout];
    [navigator1 setSelectionMode:NO];
    [navigator2 setSelectionMode:NO];
}

- (void)sendTapped:(id)sender {
    self.sendView = [[DAMMediaSendView alloc] initWithNibName:@"DAMMediaSendView" bundle:nil];
    sendView.delegate = self;
    [self.view addSubview:sendView.view];
    [sendView release];
    [sendView animateOnStage];
}

- (void)moveTapped:(id)sender {
    self.moveToView = [[DAMIDriveMoveToView alloc] initWithNibName:@"DAMIDriveMoveToView" bundle:nil];
    moveToView.delegate = self;
    [self.view addSubview:moveToView.view];
    [moveToView release];
    [moveToView animateOnStage];
}

- (void)cancelTapped:(id)sender {
    [damNavigator dismissRollout];
    [navigator1 setSelectionMode:NO];
    [navigator2 setSelectionMode:NO];
}

- (void)sendMediaTapped {
    NSArray *selectedAssets = [self.visibleNavigator selectedAssets];
    NSMutableArray *mediaRefs = [NSMutableArray arrayWithCapacity:selectedAssets.count];
    for (NSDictionary *asset in selectedAssets) {
        NSString *mediaRef = [asset objectForKey:@"mediaRef"];
        [mediaRefs addObject:mediaRef];
    }
    [[DAMApplicationModel model] emailAssets:mediaRefs toRecipient:sendView.emailAddress.text withSubject:sendView.subject.text andMessage:sendView.message.text];

    [damNavigator dismissRollout];
    [navigator1 setSelectionMode:NO];
    [navigator2 setSelectionMode:NO];
}

- (void)cancelMediaSendTapped {
    [damNavigator dismissRollout];
    [navigator1 setSelectionMode:NO];
    [navigator2 setSelectionMode:NO];
}

- (void)offstageAnimationDidFinish {
    //To change the template use AppCode | Preferences | File Templates.
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.offstageAnimationDidFinish");
#endif
}

- (void)moveToCancelTapped {
}

- (void)moveToMoveTapped:(NSString*)folder {
    [[DAMApplicationModel model] iDriveMove:[self.visibleNavigator selectedAssets] toFolder:folder];

    [self performSelector:@selector(refresh) withObject:nil afterDelay:1.0];

    [damNavigator dismissRollout];
    [navigator1 setSelectionMode:NO];
    [navigator2 setSelectionMode:NO];
}

- (void)modalViewOffstageAnimationDidFinish:(DAMModalViewBase*)modalView {
    if (sendView) {
        [sendView.view removeFromSuperview];
        self.sendView = nil;
    }
    if (moveToView) {
        [moveToView.view removeFromSuperview];
        self.moveToView = nil;
    }
    if (mediaChangeView) {
        [mediaChangeView.view removeFromSuperview];
        self.mediaChangeView = nil;
    }
}

- (IBAction)deleteTest:(id)sender {
    DAMApplicationModel *model = [DAMApplicationModel model];
    //UIWebView *webView = model.webView;
    //[webView stringByEvaluatingJavaScriptFromString:@"sync.testDeletion()"];
}

- (void)checkDeletions {
    //[[DAMApplicationModel model] reportCMSDeletionsToPlugin:@"DAMIDriveNavigatorViewController" andMethod: @selector(deletionsDetected:)];
    [[DAMApplicationModel model] reportCMSDeletionsToPlugin: self andMethod: @selector(deletionsDetected:)];
}


//- (void)deletionsDetected:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options {
- (void)deletionsDetected:(NSMutableArray*)options {
    NSArray *deletions = options;
    
    if (deletions && deletions.count > 0) {
        self.mediaChangeView = [[DAMMediaChangeView alloc] initWithNibName:@"DAMMediaChangeView" bundle:nil];
        mediaChangeView.delegate = self;
        mediaChangeView.model = deletions;
        [self.view addSubview:mediaChangeView.view];
        mediaChangeView.modalTitle.text = @"MEDIA REMOVED";
        mediaChangeView.titleLabel.text = @"The following media have been removed by head office.";
        mediaChangeView.cancelButton.hidden = YES;
        [mediaChangeView.okButton setBackgroundImage:[UIImage imageNamed:@"btn-close.png"] forState:UIControlStateNormal];
        [mediaChangeView animateOnStage];
        [mediaChangeView release];
    }
}

//not used, becaus we hide the cancel button, but needed to implement the delegate
- (void)mediaChangeViewDidCancel { }

- (void)mediaChangeViewDidOK {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.mediaChangeViewDidOK");
#endif
    [[DAMApplicationModel model] clearCMSDeletions];
    [damNavigator clearIDriveAnimation];
}


@end
