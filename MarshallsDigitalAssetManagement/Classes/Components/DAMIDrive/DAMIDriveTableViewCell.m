//
//  DAMIDriveTableViewCell.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMIDriveTableViewCell.h"
#import "DAMUIDefaults.h"

@implementation DAMIDriveTableViewCell
@synthesize view;
@synthesize thumbnail;
@synthesize title;
@synthesize added;
@synthesize type;
@synthesize disclosure;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        [[NSBundle mainBundle] loadNibNamed:@"DAMIDriveTableViewCell" owner:self options:nil];
        
        title.font = [DAMUIDefaults defaults].iDriveFont;
        added.font = [DAMUIDefaults defaults].iDriveFont;
        type.font = [DAMUIDefaults defaults].iDriveFont;
        
        title.textColor = [DAMUIDefaults defaults].textColor;
        title.highlightedTextColor = [DAMUIDefaults defaults].textColor;
        added.textColor = [DAMUIDefaults defaults].textColor;
        added.highlightedTextColor = [DAMUIDefaults defaults].textColor;
        type.textColor = [DAMUIDefaults defaults].textColor;
        type.highlightedTextColor = [DAMUIDefaults defaults].textColor;
        
        view.backgroundColor = [UIColor clearColor];
        
        [self insertSubview:view atIndex:0];
        
        //TODO - use selectedBackgroundView for adding ticks in select mode
        UIView *selectedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        selectedView.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView = selectedView;
        [selectedView release];
        
        //self.backgroundColor = [UIColor greenColor];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc {
    self.thumbnail = nil;
    self.title = nil;
    self.added = nil;
    self.type = nil;
    self.disclosure = nil;

    [super dealloc];
}

@end
