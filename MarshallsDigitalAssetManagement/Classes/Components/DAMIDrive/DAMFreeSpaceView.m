//
//  DAMFreeSpaceView.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 17/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMFreeSpaceView.h"
#import "DAMUIDefaults.h"
#import "JSMain.h"


@interface DAMFreeSpaceView ()

@end

@implementation DAMFreeSpaceView
@synthesize freeSpaceLabel;
@synthesize freeSpaceProgress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if([[JSMain sharedInstance] isIOS7])
        {
            self.freeSpaceProgress.frame = CGRectMake(self.freeSpaceProgress.frame.origin.x, self.freeSpaceProgress.frame.origin.y, self.freeSpaceProgress.frame.size.width, 50);
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    freeSpaceLabel.font = [[DAMUIDefaults defaults] iDriveFreespaceFont];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

    self.freeSpaceLabel = nil;
    self.freeSpaceProgress = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateFreeDiskspace];
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

-(void)updateFreeDiskspace {
    float totalSpace = 0.0f;
    float totalFreeSpace = 0.0f;
    NSError *error = nil;  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];  
    
    if (dictionary) {  
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];  
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes floatValue];
        totalFreeSpace = [freeFileSystemSizeInBytes floatValue];
#ifdef DEBUG
        NSLog(@"Memory Capacity of %f MiB with %f MiB Free memory available.", ((totalSpace/1024.0f)/1024.0f), ((totalFreeSpace/1024.0f)/1024.0f));
#endif
        float totalSpaceGB = totalSpace / (1024 * 1024 * 1024);
        float totalFreeSpaceGB = totalFreeSpace / (1024 * 1024 * 1024);
        float totalUsedSpaceGB = totalSpaceGB - totalFreeSpaceGB;
        freeSpaceLabel.text = [NSString stringWithFormat:@"%.0fGB / %.0fGB", totalUsedSpaceGB, totalSpaceGB];
        freeSpaceProgress.progress = (totalSpace - totalFreeSpace) / totalSpace;
    } else {
#ifdef DEBUG
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %d", [error domain], [error code]);
#endif
    }  
//    
//    return totalFreeSpace;
}

@end
