//
//  DAMIDriveNavigatorEmbedded.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMIDriveNavigatorEmbedded.h"

@interface DAMIDriveNavigatorEmbedded ()
@end

@implementation DAMIDriveNavigatorEmbedded
@synthesize delegate;
@synthesize breadcrumb;
@synthesize breadcrumbPlaceholder;
@synthesize navigator1;
@synthesize navigator2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.breadcrumb = [[DAMBreadCrumbView alloc] initWithNibName:@"DAMBreadCrumbView" bundle:nil];
        breadcrumb.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.breadcrumbPlaceholder addSubview:breadcrumb.view];
    breadcrumb.horizontalSeparator.hidden = YES;
    breadcrumb.view.frame = breadcrumbPlaceholder.frame;
    
    //self.navigator1.parentPlugin = @"DAMIDriveNavigatorEmbedded";
    //self.navigator2.parentPlugin = @"DAMIDriveNavigatorEmbedded";
    self.navigator1.parentPlugin = self;
    self.navigator2.parentPlugin = self;
    visibleNavigatorIndex = 0;

    navigator1.folderSelectionMode = YES;
    navigator2.folderSelectionMode = YES;
    
    ////[[DAMApplicationModel model] registerCommand:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    //[[DAMApplicationModel model] registerCommand:self];
    NSString *currentFolder = [breadcrumb currentFolder];
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.viewWillAppear currentFolder:%@", currentFolder);
#endif
    //[[DAMApplicationModel model] iDriveListFolderFolders: currentFolder toPlugin:@"DAMIDriveNavigatorEmbedded"];
    [[DAMApplicationModel model] iDriveListFolderFolders: currentFolder delegate:self];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.breadcrumb = nil;
    self.breadcrumbPlaceholder = nil;
    self.navigator1 = nil;
    self.navigator2 = nil;
}

- (DAMIDriveTableView*)visibleNavigator {
    NSArray *navigators = [NSArray arrayWithObjects:navigator1, navigator2, nil];
    return [navigators objectAtIndex:visibleNavigatorIndex];
}

BOOL selectionModeEmbedded = NO;

- (void)flipVisibleNavigator:(BOOL)animateRight {
    NSArray *navigators = [NSArray arrayWithObjects:navigator1, navigator2, nil];
    DAMIDriveTableView *visibleNav = [navigators objectAtIndex:visibleNavigatorIndex];
    DAMIDriveTableView *offscreenNav = [navigators objectAtIndex:(visibleNavigatorIndex+1) % 2];
    
    float navigatorTableWidth = 604;
    
    float offscreenStartPos = animateRight ? navigatorTableWidth : -navigatorTableWidth;
    float onScreenEndPos = animateRight ? -navigatorTableWidth : navigatorTableWidth;
    CGRect frame = offscreenNav.frame;
    frame.origin.x = offscreenStartPos;
    offscreenNav.frame = frame;
    
    [UIView beginAnimations:@"iDriveNavigate" context:nil];
    [UIView setAnimationDuration:0.5];
    
    visibleNav.alpha = 0;
    offscreenNav.alpha = 1;
    frame = visibleNav.frame;
    frame.origin.x = onScreenEndPos;
    visibleNav.frame = frame;

    frame = offscreenNav.frame;
    frame.origin.x = 0;
    offscreenNav.frame = frame;
    
    [UIView commitAnimations];
    
    visibleNavigatorIndex = (visibleNavigatorIndex + 1) % 2;
}

- (void)pathChangedTo:(NSString*)path {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.pathChangedTo path:%@", path);
#endif
    [self.visibleNavigator dismissAddNewFolderUI];
    [self flipVisibleNavigator:NO];
    //[[DAMApplicationModel model] iDriveListFolderFolders:path toPlugin:@"DAMIDriveNavigatorEmbedded"];
    [[DAMApplicationModel model] iDriveListFolderFolders:path delegate:self];
}

- (void)lsResults:(id)dict {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorEmbedded.lsResults params:%@", [dict class]);
#endif
    //TODO Seems a bit weird this, I know... need to look into it
    NSArray *downloadItems = (NSArray *)dict;
    self.visibleNavigator.currentFolderContents = [downloadItems mutableCopy];
    [self.visibleNavigator reloadData];
}

- (NSString*)viewNavigatedToFolder:(NSString*)folder {
    
    [self flipVisibleNavigator:YES];
    NSString *newFolder = [breadcrumb navigateToSubFolder:folder];
    return newFolder;
}

- (void)viewSelectedAsset:(NSDictionary*)asset {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.viewSelectedAsset asset:%@", asset);
#endif
    if (delegate) {
        [delegate assetSelected:asset];
    }
}

- (void)folderAdded:(NSString*)folderName {
#ifdef DEBUG
    NSLog(@"DAMIDriveNavigatorViewController.folderAdded folderName:%@", folderName);
#endif
    NSString *newFolder = [breadcrumb subFolderOfCurrent:folderName];
    [[DAMApplicationModel model] iDriveCreateFolder:newFolder];
    [self performSelector:@selector(updateFolderList) withObject:nil afterDelay:1.0];
}

- (void)updateFolderList {
    //[[DAMApplicationModel model] iDriveListFolderFolders:breadcrumb.currentFolder toPlugin:@"DAMIDriveNavigatorEmbedded"];
    [[DAMApplicationModel model] iDriveListFolderFolders:breadcrumb.currentFolder delegate:self];
}

@end
