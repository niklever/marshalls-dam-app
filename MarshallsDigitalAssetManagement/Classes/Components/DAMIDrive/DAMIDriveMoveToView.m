//
//  DAMIDriveMoveToView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 20/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMIDriveMoveToView.h"

@interface DAMIDriveMoveToView ()

@end

@implementation DAMIDriveMoveToView
@synthesize delegate;
@synthesize iDriveContainer;
@synthesize iDrive;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.iDrive = [[DAMIDriveNavigatorEmbedded alloc] initWithNibName:@"DAMIDriveNavigatorEmbedded" bundle:nil];
    iDrive.view.frame = CGRectMake(0, 0, iDriveContainer.frame.size.width, iDriveContainer.frame.size.height);
    [iDriveContainer addSubview:iDrive.view];
    [iDrive release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.iDriveContainer = nil;
    self.iDrive = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (IBAction)cancelTapped:(id)sender {
    [self animateOffStage];
    if (delegate) {
        [delegate moveToCancelTapped];
    }
}

- (IBAction)moveTapped:(id)sender {
    [self animateOffStage];
    if (delegate) {
        [delegate moveToMoveTapped:iDrive.breadcrumb.currentFolder];
    }
}

@end
