//
//  DAMFreeSpaceView.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 17/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMFreeSpaceView : UIViewController {
    
    UILabel *freeSpaceLabel;
    UIProgressView *freeSpaceProgress;
}
@property (nonatomic, retain) IBOutlet UILabel *freeSpaceLabel;
@property (nonatomic, retain) IBOutlet UIProgressView *freeSpaceProgress;

-(void)updateFreeDiskspace;

@end
