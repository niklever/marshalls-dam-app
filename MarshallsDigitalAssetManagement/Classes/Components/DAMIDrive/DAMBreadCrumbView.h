//
//  DAMBreadCrumbView.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMBreadCrumbViewDelegate <NSObject>

- (void)pathChangedTo:(NSString*)path;

@end

@interface DAMBreadCrumbView : UIViewController {
    
    id<DAMBreadCrumbViewDelegate> delegate;
    
    NSArray *currentPath;
    UIImageView *horizontalSeparator;
    UIButton *homeButton;
}
@property (nonatomic, assign) id<DAMBreadCrumbViewDelegate> delegate;

@property (nonatomic, retain) NSArray *currentPath;
@property (nonatomic, retain) IBOutlet UIImageView *horizontalSeparator;
@property (nonatomic, retain) IBOutlet UIButton *homeButton;

- (NSString*)currentFolder;
- (NSString*)subFolderOfCurrent:(NSString*)subFolder;
- (NSString*)navigateToSubFolder:(NSString*)folder;

- (IBAction)breadcrumbTapped:(UIButton*)sender;

@end
