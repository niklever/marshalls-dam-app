//
//  DAMIDriveMoveToView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 20/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMIDriveNavigatorEmbedded.h"
#import "DAMModalViewBase.h"

@protocol DAMIDriveMoveToViewDelegate <NSObject>

- (void)moveToCancelTapped;
- (void)moveToMoveTapped:(NSString*)folder;

@end

@interface DAMIDriveMoveToView : DAMModalViewBase {
    
    id<DAMIDriveMoveToViewDelegate> delegate;
    
    UIView *iDriveContainer;
    DAMIDriveNavigatorEmbedded *iDrive;
}
@property (nonatomic, assign) id<DAMIDriveMoveToViewDelegate> delegate;

@property (nonatomic, retain) IBOutlet UIView *iDriveContainer;
@property (nonatomic, retain) DAMIDriveNavigatorEmbedded *iDrive;

- (IBAction)cancelTapped:(id)sender;
- (IBAction)moveTapped:(id)sender;

@end
