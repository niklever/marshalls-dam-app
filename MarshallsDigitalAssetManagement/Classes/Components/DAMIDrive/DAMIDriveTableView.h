//
//  DAMIDriveTableView.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 17/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMIDriveTableViewDelegate <NSObject>

- (NSString*)viewNavigatedToFolder:(NSString*)folder;
- (void)viewSelectedAsset:(NSDictionary*)asset;
- (void)folderAdded:(NSString*)folderName;

@end

@interface DAMIDriveTableView : UITableView<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {

    //NSString *parentPlugin;
    id parentPlugin;
    id<DAMIDriveTableViewDelegate> navigationDelegate;
    
    BOOL selectionMode;
    BOOL folderSelectionMode;
    //NSArray *currentFolderContents;
    NSMutableArray *currentFolderContents;
    
    UIView *addNewFolderUI;
}
//@property (nonatomic, assign) NSString *parentPlugin;
@property (nonatomic, assign) id parentPlugin;
@property (nonatomic, assign) IBOutlet id<DAMIDriveTableViewDelegate> navigationDelegate;
@property BOOL selectionMode;
@property BOOL folderSelectionMode;
@property (nonatomic, retain) NSMutableArray *currentFolderContents;

@property (nonatomic, retain) UIView *addNewFolderUI;

- (NSArray*)selectedAssets;

- (void)removeSelectedAssets;

- (void)dismissAddNewFolderUI;

@end
