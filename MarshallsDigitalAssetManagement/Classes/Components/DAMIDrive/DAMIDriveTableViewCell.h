//
//  DAMIDriveTableViewCell.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMImageView.h"

@interface DAMIDriveTableViewCell : UITableViewCell {
    
    UIView *view;
    
//    BOOL selectionMode;
    
    DAMImageView *thumbnail;
    UILabel *title;
    UILabel *added;
    UILabel *type;
    UIImageView *disclosure;
}
@property (nonatomic, retain) IBOutlet UIView *view;

//@property BOOL selectionMode;

@property (nonatomic, retain) IBOutlet DAMImageView *thumbnail;
@property (nonatomic, retain) IBOutlet UILabel *title;
@property (nonatomic, retain) IBOutlet UILabel *added;
@property (nonatomic, retain) IBOutlet UILabel *type;
@property (nonatomic, retain) IBOutlet UIImageView *disclosure;

//- (void)setSelectionMode:(BOOL)selectionMode;

@end
