//
//  DAMMapAnnotationView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 13/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMImageView.h"
#import "DAMImageView.h"

@interface DAMMapAnnotationView : UIViewController {
    
    DAMImageView *image;
    UILabel *titleLabel;
    UILabel *locationLabel;
    UIButton *disclosureButton;
}
@property(nonatomic, retain) IBOutlet DAMImageView *image;
@property(nonatomic, retain) IBOutlet UILabel *titleLabel;
@property(nonatomic, retain) IBOutlet UILabel *locationLabel;
@property(nonatomic, retain) IBOutlet UIButton *disclosureButton;

@end
