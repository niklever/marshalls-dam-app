//
//  DAMIndexStripView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DAMIndexStripView;

@protocol DAMIndexStripViewDelegate <NSObject>

- (void)indexStrip:(DAMIndexStripView*)indexStrip indexChangedTo:(int)index;

@end

@interface DAMIndexStripView : UIImageView

@property (nonatomic, assign) id<DAMIndexStripViewDelegate> delegate;
@property int numItems;

@end
