//
//  DAMIndexStripView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMIndexStripView.h"

@interface DAMIndexStripView() {
    id<DAMIndexStripViewDelegate> delegate;
    
    int numItems;
    int lastSelected;
}
- (void)updateIfNeeded:(UITouch*)touch;

@end

@implementation DAMIndexStripView
@synthesize delegate;
@synthesize numItems;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self sharedInit];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self sharedInit];
    return self;
}

- (void)sharedInit {
    if (self) {
        // Initialization code
        
        self.userInteractionEnabled = YES;
        numItems = 0;
        lastSelected = -1;
    }
}

- (void)dealloc {
    self.delegate = nil;
    [super dealloc];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.image = [UIImage imageNamed:@"tags-strip-on.png"];
    
    UITouch *touch = [touches anyObject];
    
    [self updateIfNeeded:touch];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    
    [self updateIfNeeded:touch];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    self.image = [UIImage imageNamed:@"tags-strip.png"];

}

- (void)updateIfNeeded:(UITouch*)touch {
    if (numItems > 0) {
        CGPoint touchPoint = [touch locationInView:self];
        int touchIndex = numItems * touchPoint.x / self.frame.size.width;
        if (touchIndex != lastSelected && touchIndex >=0 && touchIndex < numItems) {
            lastSelected = touchIndex;
            if (delegate) {
                [delegate indexStrip:self indexChangedTo:lastSelected];
            }
        }
    }
}

@end
