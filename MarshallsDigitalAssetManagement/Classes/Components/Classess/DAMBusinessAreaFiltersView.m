//
//  DAMBusinessAreaFiltersView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 1/14/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "DAMBusinessAreaFiltersView.h"

#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"
#import "JSDBV2.h"
#import "JSMain.h"
#import "DAMTagLabel.h"
#import "NSString+Utilities.h"

#define VERTICAL_PADDING 42

@interface DAMBusinessAreaFiltersView () {
    id<DAMBusinessAreaFiltersViewDelegate> delegate;
    
    UILabel *titleLabel;
    UILabel *commercialLabel;
    UILabel *landscapesLabel;
    UIButton *cancelButton;
    UIButton *okButton;
    
    UIView *containerPlacerView;
    
    //NSArray *mainGroup;
    //NSArray *commercialGroup;
    NSArray *landscapesGroup;
    //NSMutableArray *selectedContainers;
}

@end

@implementation DAMBusinessAreaFiltersView
@synthesize delegate;
@synthesize titleLabel;
@synthesize commercialLabel;
@synthesize cancelButton;
@synthesize okButton;
@synthesize landscapesLabel;
@synthesize containerPlacerView;
@synthesize selectedContainers;
@synthesize gridContents;
@synthesize devider1;
@synthesize devider2;
@synthesize deviderGroup;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //mainGroup = [NSArray arrayWithObjects:@"Marshalls Domestic",@"Stonemarket", nil];
        //commercialGroup = [NSArray arrayWithObjects:@"Paving",@"Block Paving",@"Machine Lay",@"Water Management",@"Kerb",@"Traffic Management",@"Street Furniture",@"Mortars & Screeds",@"Walling",@"Natural Stone",@"General",@"Sustainability",@"Internal Flooring", nil];
        landscapesGroup = [NSArray arrayWithObjects:@"Rail",@"Educational",@"Home",@"Retail", @"Cycling" , @"Cycle", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    titleLabel.textColor = [[DAMUIDefaults defaults] textColor];
    titleLabel.font = [[DAMUIDefaults defaults] thumbnailTitleFont];
    
    commercialLabel.textColor = [[DAMUIDefaults defaults] textColor];
    commercialLabel.font = [[DAMUIDefaults defaults] busAreasFont];
    
    landscapesLabel.textColor = [[DAMUIDefaults defaults] textColor];
    landscapesLabel.font = [[DAMUIDefaults defaults] busAreasFont];
    
    NSArray *temp = [[JSMain sharedInstance] getBusinessAreaFilters];
    
    if (temp == NULL) {
        self.selectedContainers = [NSMutableArray arrayWithCapacity: 100];
    }else{
        self.selectedContainers = [NSMutableArray arrayWithArray: temp];
    }
    
    
    
    //setup tag label
    //NSArray *rows = [[JSDBV2 sharedInstance] sql:@"select * from containers" params: nil ];
    
    [[JSDBV2 sharedInstance] executeQuery:@"select * from containers"
                                   params: nil
                                  successCallback:^(NSMutableArray *rows) {
        [self layoutContainers: rows];
        [self checkingTagStatuses];
    }];
    
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)layoutContainers:(NSArray *)rows {
    NSMutableArray *mainContainers = [NSMutableArray array];
    NSMutableArray *commercialContainers = [NSMutableArray array];
    NSMutableArray *landscapesContainers = [NSMutableArray array];
    NSMutableArray *groupContainers = [NSMutableArray array];
    BOOL hasOn = false;
    for (NSDictionary *row in rows) {
        if ([[row objectForKey:@"state"] intValue] == 1) {
            hasOn = true;
            NSString *cname = [row objectForKey:@"name"];
            
            if ([[row nonNullValueForKey:@"businessArea"] isEqualToString: @"Domestic"]) {
                [mainContainers addObject: cname];
            }else if ([self isString:cname existInArray: landscapesGroup]) {
                [landscapesContainers addObject: cname];
            }else if ([[row nonNullValueForKey:@"businessArea"] isEqualToString: @"Commercial"]) {
                [commercialContainers addObject: cname];
            }else{
                [groupContainers addObject: cname];
            }
            
            /*if ([self isString:cname existInArray:mainGroup]) {
                [mainContainers addObject: cname];
            }else if ([self isString:cname existInArray: commercialGroup]) {
                [commercialContainers addObject: cname];
            }else if ([self isString:cname existInArray: landscapesGroup]) {
                [landscapesContainers addObject: cname];
            }*/
            
        }
    }
    if (!hasOn) {
        titleLabel.text = @"Have no any containers actived.";
        commercialLabel.hidden = YES;
        landscapesLabel.hidden = YES;
        devider1.hidden = YES;
        devider2.hidden = YES;
        return;
    }
    float contentHeight = 0;
    //layout tags
    float x1Pos,x2Pos,yPos;
    x1Pos = 39;
    x2Pos = 336;
    yPos = 10;
    contentHeight += yPos;
    float labelWidth = 160;
    BOOL isCol1 = true;
    
    if (groupContainers.count > 0) {
        for (NSString *container in groupContainers) {
            container = [container uppercaseString];
            labelWidth = [container textWidthWithFont: [DAMUIDefaults defaults].tagLabelFont] + 50;
            DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(isCol1 ? x1Pos : x2Pos, yPos, labelWidth, 40)];
            tagLabel.delegate = self;
            tagLabel.label.text = container;
            [gridContents addSubview: tagLabel];
            
            //set selected
            if ([selectedContainers containsObject: [container capitalizedString]]) {
                [tagLabel setSelected];
            }
            [tagLabel release];
            isCol1 = !isCol1;
        }
        yPos += VERTICAL_PADDING;
        contentHeight += VERTICAL_PADDING * 2;
        //deviderGroup.hidden = NO;
        //deviderGroup.frame = CGRectMake(deviderGroup.frame.origin.x, yPos + 10, deviderGroup.frame.size.width,deviderGroup.frame.size.height);
    }else{
        deviderGroup.hidden = YES;
    }
    
    isCol1 = true;
    //yPos -= 10;
    
    if ([mainContainers count] > 0) {
        for (NSString *container in mainContainers) {
            container = [container uppercaseString];
            labelWidth = [container textWidthWithFont: [DAMUIDefaults defaults].tagLabelFont] + 50;
            DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(isCol1 ? x1Pos : x2Pos, yPos, labelWidth, 40)];
            tagLabel.delegate = self;
            tagLabel.label.text = container;
            [gridContents addSubview: tagLabel];
            
            //set selected
            if ([selectedContainers containsObject: [container capitalizedString]]) {
                [tagLabel setSelected];
            }
            [tagLabel release];
            isCol1 = !isCol1;
        }
        yPos += VERTICAL_PADDING;
        contentHeight += VERTICAL_PADDING * 2;
    }else{
        devider1.hidden = YES;
    }
    isCol1 = true;
    
    devider1.frame = CGRectMake(devider1.frame.origin.x, yPos + 10, devider1.frame.size.width,devider1.frame.size.height);
    yPos -= 10;
    
    if ([commercialContainers count] > 0) {
        //commercial text
        yPos = yPos + 30;
        
        
        commercialLabel.frame = CGRectMake(x1Pos, yPos, commercialLabel.frame.size.width,commercialLabel.frame.size.height);
        
        commercialLabel.hidden = YES;
        
        //replace by commercial label
        labelWidth = [commercialLabel.text textWidthWithFont: commercialLabel.font] + 50;
        DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(x1Pos, yPos, labelWidth, 40)];
        tagLabel.label.font = commercialLabel.font;
        tagLabel.delegate = self;
        tagLabel.tag = 98;
        tagLabel.label.text = commercialLabel.text;
        [gridContents addSubview: tagLabel];
        [tagLabel release];
        
        yPos = yPos + VERTICAL_PADDING;
        contentHeight += VERTICAL_PADDING * 2;
        for (NSString *container in commercialContainers) {
            container = [container uppercaseString];
            labelWidth = [container textWidthWithFont: [DAMUIDefaults defaults].tagLabelFont] + 50;
            DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(isCol1 ? x1Pos : x2Pos, yPos, labelWidth, 40)];
            //        tagLabel.textAlignment = UITextAlignmentCenter;
            //        tagLabel.text = tagString;
            tagLabel.delegate = self;
            tagLabel.label.text = container;
            tagLabel.tag = 100;
            //[containerPlacerView addSubview:tagLabel];
            [gridContents addSubview: tagLabel];
            //set selected
            if ([selectedContainers containsObject: [container capitalizedString]]) {
                [tagLabel setSelected];
            }
            [tagLabel release];
            if (!isCol1) {
                yPos += VERTICAL_PADDING;
                contentHeight += VERTICAL_PADDING;
            }
            isCol1 = !isCol1;
        }
        if (!isCol1)
            yPos += VERTICAL_PADDING;
        contentHeight += VERTICAL_PADDING;
    }else{
        commercialLabel.hidden = YES;
        devider2.hidden = YES;
    }
    
    devider2.frame = CGRectMake(devider2.frame.origin.x, yPos + 10, devider2.frame.size.width,devider2.frame.size.height);
    x1Pos = 39;
    
    if ([landscapesContainers count] > 0) {
        //commercial text
        yPos = yPos + 20;
        landscapesLabel.frame = CGRectMake(x1Pos, yPos, landscapesLabel.frame.size.width,landscapesLabel.frame.size.height);
        landscapesLabel.hidden = YES;
        //replace by commercial label
        labelWidth = [landscapesLabel.text textWidthWithFont: landscapesLabel.font] + 50;
        DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(x1Pos, yPos, labelWidth, 40)];
        tagLabel.delegate = self;
        tagLabel.tag = 99;
        tagLabel.label.font = landscapesLabel.font;
        tagLabel.label.text = landscapesLabel.text;
        [gridContents addSubview: tagLabel];
        [tagLabel release];
        
        yPos = yPos + VERTICAL_PADDING;
        contentHeight += VERTICAL_PADDING;
        isCol1 = true;
        BOOL colNo = 1;
        for (NSString *container in landscapesContainers) {
            container = [container uppercaseString];
            labelWidth = [container textWidthWithFont: [DAMUIDefaults defaults].tagLabelFont] + 50;
            DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(colNo == 1 ? x1Pos : colNo == 2 ? x1Pos + 200 : x1Pos + 400, yPos, labelWidth, 40)];
            //        tagLabel.textAlignment = UITextAlignmentCenter;
            //        tagLabel.text = tagString;
            tagLabel.delegate = self;
            tagLabel.tag = 101;
            tagLabel.label.text = container;
            //[containerPlacerView addSubview:tagLabel];
            [gridContents addSubview: tagLabel];
            //set selected
            if ([selectedContainers containsObject: [container capitalizedString]]) {
                [tagLabel setSelected];
            }
            [tagLabel release];
            colNo++;
            if (!isCol1) {
                
            }
            isCol1 = !isCol1;
            if (colNo > 3) {
                colNo = 1;
                yPos += VERTICAL_PADDING;
                contentHeight += VERTICAL_PADDING;
            }
        }
    }else{
        landscapesLabel.hidden = YES;
        devider2.hidden = YES;
    }
    gridContents.contentSize = CGSizeMake(640, contentHeight);
    //gridContents.scrollEnabled = YES;
}


- (BOOL) isString:(NSString *) str existInArray:(NSArray *) array {
    for(NSString *temp in array) {
        if ([temp isEqualToString: str]) {
            return TRUE;
        }
    }
    return FALSE;
}

- (void) checkingTagStatuses {
    //checking if all commercial field checked?
    __block BOOL isAllCommercialChecked = YES;
    [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DAMTagLabel class]]) {
            DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
            if (tagLabel.tag == 100 && ![tagLabel isSelected]) {
                isAllCommercialChecked = NO;
            }
        }
    }];
    [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DAMTagLabel class]]) {
            DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
            if (tagLabel.tag == 98){
                if (isAllCommercialChecked) {
                    [tagLabel setSelected];
                }else{
                    [tagLabel setUnSelected];
                }
            }
        }
    }];
    
    __block BOOL isAllLandscapeChecked = YES;
    [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DAMTagLabel class]]) {
            DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
            if (tagLabel.tag == 101 && ![tagLabel isSelected]) {
                isAllLandscapeChecked = NO;
            }
        }
    }];
    [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[DAMTagLabel class]]) {
            DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
            if (tagLabel.tag == 99){
                if (isAllLandscapeChecked) {
                    [tagLabel setSelected];
                }else{
                    [tagLabel setUnSelected];
                }
            }
        }
    }];
}

- (void)tagSelected:(NSString*)tag {
    //process select all tag if user tap on Commercial or landscape
    if ([[tag uppercaseString] isEqualToString: [commercialLabel.text uppercaseString]]) {
        //do select all commercial
        [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[DAMTagLabel class]]) {
                DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
                if (tagLabel.tag == 100) {
                    //set selected
                    [tagLabel setSelected];
                    if (![self.selectedContainers containsObject: [tagLabel.label.text capitalizedString]]) {
                        [self.selectedContainers addObject: [tagLabel.label.text capitalizedString]];
                    }
                }
            }
        }];
        return;
    }else if ([[tag uppercaseString] isEqualToString: [landscapesLabel.text uppercaseString]]) {
        //do select all commercial
        [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[DAMTagLabel class]]) {
                DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
                if (tagLabel.tag == 101) {
                    //set selected
                    [tagLabel setSelected];
                    if (![self.selectedContainers containsObject: [tagLabel.label.text capitalizedString]]) {
                        [self.selectedContainers addObject: [tagLabel.label.text capitalizedString]];
                    }
                }
            }
        }];
        return;
    }
    
    [self checkingTagStatuses];
    
    
    //set selected
    if (![self.selectedContainers containsObject: [tag capitalizedString]]) {
        [self.selectedContainers addObject: [tag capitalizedString]];
    }
}

- (void)tagDeselected:(NSString*)tag {
    
    if ([[tag uppercaseString] isEqualToString: [commercialLabel.text uppercaseString]]) {
        //do select all commercial
        [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[DAMTagLabel class]]) {
                DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
                if (tagLabel.tag == 100) {
                    //set selected
                    [tagLabel setUnSelected];
                    if ([self.selectedContainers containsObject: [tagLabel.label.text capitalizedString]]) {
                        [self.selectedContainers removeObject: [tagLabel.label.text capitalizedString]];
                    }
                }
            }
        }];
        return;
    }else if ([[tag uppercaseString] isEqualToString: [landscapesLabel.text uppercaseString]]) {
        //do select all commercial
        [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[DAMTagLabel class]]) {
                DAMTagLabel *tagLabel = (DAMTagLabel *)obj;
                if (tagLabel.tag == 101) {
                    //set selected
                    [tagLabel setUnSelected];
                    if ([self.selectedContainers containsObject: [tagLabel.label.text capitalizedString]]) {
                        [self.selectedContainers removeObject: [tagLabel.label.text capitalizedString]];
                    }
                }
            }
        }];
        return;
    }
    
    [self checkingTagStatuses];

    
    if ([self.selectedContainers containsObject: [tag capitalizedString]]) {
        [self.selectedContainers removeObject: [tag capitalizedString]];
    }
}

- (void) removeSelectedTag:(NSString *)tagString{
    [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[DAMTagLabel class]]){
            DAMTagLabel *tag    =   (DAMTagLabel *)obj;
            if([[tag.label.text capitalizedString] isEqualToString:[tagString capitalizedString]]){
                [tag setUnSelected];
                [self.selectedContainers removeObject:[tagString capitalizedString]];
            }
        }
    }];
}

- (void) selectTagsFollowSelecteTags:(NSMutableArray *)arrTags{
    [self.selectedContainers removeAllObjects];
    [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[DAMTagLabel class]]){
            DAMTagLabel *tag    =   (DAMTagLabel *)obj;
            [tag setUnSelected];
        }
    }];
    
    [arrTags enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *stringTag =   obj;
        [gridContents.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if([obj isKindOfClass:[DAMTagLabel class]]){
                DAMTagLabel *tag    =   (DAMTagLabel *)obj;
                if([[tag.label.text capitalizedString] isEqualToString:[stringTag capitalizedString]]){
                    [tag setSelected];
                    [self.selectedContainers addObject:stringTag];
                }
            }
        }];
    }];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.titleLabel = nil;
    self.cancelButton = nil;
    self.okButton = nil;
    self.selectedContainers = nil;
}


- (void)verticallCentreView:(UIView*)view {
    CGRect frame = view.frame;
    frame.origin.y = (frame.size.height) / 2;
    view.frame = frame;
    
}

- (IBAction)cancelTapped:(id)sender {
    
    if (delegate) {
        if([delegate respondsToSelector:@selector(businessAreaFiltersViewDidCancel:)]){
            [self animateOffStage:^{
                [delegate businessAreaFiltersViewDidCancel:self];
            }];
        }
        else{
            [self animateOffStage];
            [delegate businessAreaFiltersViewDidCancel];
        }
    }
}

- (IBAction)okTapped:(id)sender {
    if (delegate) {
        //save selected into main
#ifdef DEBUG
        NSLog(@"selectedContainers when close filters == %@", selectedContainers);
#endif
        //NSLog(@"selectedContainers when close filters == %@ == count == %d", selectedContainers, selectedContainers.count);
        
        [[JSMain sharedInstance] setBusinessAreaFilters: selectedContainers];
        
        if([delegate respondsToSelector:@selector(businessAreaFiltersViewDidOK:)]){
            [self animateOffStage:^{
                [delegate businessAreaFiltersViewDidOK:self];
            }];
        }
        else{
            [self animateOffStage];
            [delegate businessAreaFiltersViewDidOK];
        }
    }
}

@end
