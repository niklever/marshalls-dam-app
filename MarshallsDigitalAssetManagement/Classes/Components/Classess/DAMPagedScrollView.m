//
//  DAMPagedScrollView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DAMPagedScrollView.h"
#import "DAMImageView.h"
#import "DAMThumbnailBaseView.h"

@interface DAMPagedScrollView() {
    
    UIImageView *noResultsView;
}
@property (nonatomic, retain) UIImageView *noResultsView;

- (void)createPageControl:(int)numPages;
- (void)fireAssetOpened:(UIView*)sender;

@end

@implementation DAMPagedScrollView
@synthesize model;
@synthesize mediaType;
@synthesize delegate;
@synthesize layoutDelegate;
@synthesize carousel;
@synthesize pageControlEnabled;
//@synthesize selectedAssets;

@synthesize scrollView;
@synthesize pageControlBackground;

@synthesize loadedPages;
@synthesize noResultsView;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self sharedInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self sharedInit];
    }
    return self;
}

const float pageControlBackgroundHeight = 39;

- (void)sharedInit {
    [super sharedInit];
//    self.pageControlBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DAMPageViewBG.png"]];
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    scrollView.pagingEnabled = YES;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.delegate = self;
    
    
    NSLog(@"self.scrollView %@ == self.frame %@", NSStringFromCGRect(self.scrollView.frame), self.frame);
    
    
    [self addSubview:scrollView];
    [scrollView release];
    
//    self.selectedAssets = [NSMutableArray arrayWithCapacity:10];
    
    self.loadedPages = [NSMutableDictionary dictionaryWithCapacity:100];
}

- (void)dealloc {
    self.model = nil;
    self.mediaType = nil;
    self.carousel = nil;
//    self.selectedAssets = nil;
    self.pageControlBackground = nil;
    self.scrollView = nil;
    self.loadedPages = nil;
    self.noResultsView = nil;
    [super dealloc];
}

- (UIView*)viewContainingSelectableItems { return scrollView; }

- (NSString*)assetTitleFromDict:(NSDictionary*)dict {
    NSObject *obj = [dict objectForKey:@"title"];
    if (obj == [NSNull null]) {
        obj = [dict objectForKey:@"mediaRef"];
    }
    return [NSString stringWithFormat:@"%@", obj];
}

- (void)clearContent {
    [(NSMutableDictionary*)self.loadedPages removeAllObjects];
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [UIView beginAnimations:@"clearCarousels" context:nil];
    [UIView setAnimationDuration:1.0];
    self.scrollView.contentOffset = CGPointZero;
    [UIView commitAnimations];
    [self clearNoResultsView];
    
    [self clearPageControl];
    [pageControlBackground removeFromSuperview];
    self.pageControlBackground = NULL;
    
    //[self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

- (NSString*)getNoResultsImageFile {
    NSString *result = @"search-noresults-white.png";
    NSDictionary *map = [NSDictionary dictionaryWithObjectsAndKeys:@"search-noresults-black.png", kMediaType_Photos,@"search-noresults-black.png", kMediaType_SalesPhotos,
            @"search-noresults-black.png", kMediaType_Samples, nil];
    if ([map objectForKey:mediaType]) {
        result = [map objectForKey:mediaType];
    }
    return result;
}

- (void)displayNoResults {
    if (!noResultsView) {
        NSString *image = [self getNoResultsImageFile];
        self.noResultsView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:image]];
        noResultsView.center = scrollView.center;
        [self addSubview:noResultsView];
        
        NSLog(@"displayNoResults self.scrollView %@ == self.frame %@", NSStringFromCGRect(self.scrollView.frame), self);
        
        [noResultsView release];
        [self clearPageControl];
    }
}

- (void)clearNoResultsView {
    if (noResultsView) {
        [noResultsView removeFromSuperview];
        self.noResultsView = nil;
    }
}

- (void)setupWithModel:(NSDictionary*)aModel forPage:(int)page {

    //[self clearContent];
    
    self.model = aModel;
    
    int pageCount = [[model objectForKey:@"pageCount"] intValue];
    
    //bail with error overlay if no results
    if (pageCount == 0) {
        [self createPageControlBackground];
        [self displayNoResults];
        return;
    }
    
    
    
    if (pageControlEnabled && loadedPages.allKeys.count == 0) {
        [self createPageControl:pageCount];
    }
    NSNumber *pageNum = [NSNumber numberWithInt:page];
    [(NSMutableDictionary*)loadedPages setObject:@"" forKey:pageNum];
    
    scrollView.contentSize = CGSizeMake(pageCount * scrollView.frame.size.width, scrollView.frame.size.height);

#ifdef DEBUG
    NSLog(@"scrollView rect == %f,%f,%f,%f", scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height);
#endif
    
    
    //If we have a layoutDelegate (ie DAMPhotoView) pass off to that to lay out the assets
    if (layoutDelegate) {
        [layoutDelegate layoutModel:aModel onView:self withPage:page];
    } else {
        NSArray *assets = [model objectForKey:@"assets"];
        [self standardLayoutForPage:page withAssets:assets];
        [self addAssetGestureRecognizersToView:scrollView];
    }
}

//TODO oldLayoutForAsset should be removed soon
- (UIView*)oldLayoutForAsset:(NSDictionary*)asset withTag:(int)tag andParentView:(UIView*)parentView {

//    float textHeight = 80;
    if (asset) {
        NSString *thumbnailUrl = [asset objectForKey:@"thumbnail"];
        DAMImageView *thumbnail = [[DAMImageView alloc] initWithUrl:thumbnailUrl andFrame:CGRectZero];
        thumbnail.model = asset;
        thumbnail.tag = tag;
    //    thumbnail.frame = CGRectMake(xPos, thumbnailTop, thumbnailWidth, thumbnailHeight);
        [parentView addSubview:thumbnail];
        [thumbnail release];

        return thumbnail;
    }else {
        return NULL;
    }
}

- (UIView*)createThumbnailForAsset:(NSDictionary*)asset withTag:(int)tag andParentView:(UIView*)parentView {
    UIView *result = nil;
    NSDictionary *classForThumbnail = [NSDictionary dictionaryWithObjectsAndKeys:@"DAMSampleBookThumbnailView", kMediaType_Samples,
            @"DAMPresentationThumbnailView", kMediaType_Presentations, @"DAMBrochureThumbnailView", kMediaType_Brochures,@"DAMInThePressThumbnailView", kMediaType_InThePress, @"DAMVideoThumbnailView", kMediaType_Videos,  nil];
    
    NSString *className = [classForThumbnail objectForKey:mediaType];
    if (className) {
        DAMThumbnailBaseView *thumbnail = [[NSClassFromString(className) alloc] initWithNibName:className bundle:nil];
        thumbnail.model = asset;
        [thumbnail updateDisplay];
        [parentView addSubview:thumbnail.view];
        result = thumbnail.view;
    }
    return result;
}

- (float)xPaddingForThumbnail {
    float result = 70;
    NSDictionary *xPadding = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:5], kMediaType_Samples,
            [NSNumber numberWithInt:2], kMediaType_Presentations, [NSNumber numberWithInt:2], kMediaType_Brochures, [NSNumber numberWithInt:2], kMediaType_InThePress, [NSNumber numberWithInt:2], kMediaType_Videos, nil];
    id obj = [xPadding objectForKey:mediaType];
    if (obj) {
        result = [obj floatValue];
    }
    return result;
}

- (float)yPaddingForThumbnail {
    float result = 0;
    NSDictionary *yPadding = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:5], kMediaType_Samples,
            [NSNumber numberWithInt:0], kMediaType_Presentations, [NSNumber numberWithInt:0], kMediaType_Brochures, [NSNumber numberWithInt:0], kMediaType_InThePress, [NSNumber numberWithInt:20], kMediaType_Videos, nil];
    id obj = [yPadding objectForKey:mediaType];
    if (obj) {
        result = [obj floatValue];
    }
    return result;
}

- (BOOL)padThumbnailLeftColumn {
    BOOL result = YES;
    NSDictionary *pad = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], kMediaType_Presentations, [NSNumber numberWithInt:0], kMediaType_Samples,
        [NSNumber numberWithInt:0], kMediaType_Brochures, [NSNumber numberWithInt:0], kMediaType_InThePress, [NSNumber numberWithInt:0], kMediaType_Videos, nil];
    id obj = [pad objectForKey:mediaType];
    if (obj) {
        result = [obj boolValue];
    }
    return result;
}

- (BOOL)padThumbnailTopRow {
    BOOL result = YES;
    NSDictionary *pad = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], kMediaType_Presentations, [NSNumber numberWithInt:0], kMediaType_Samples, [NSNumber numberWithInt:0], kMediaType_Videos,[NSNumber numberWithInt:0], kMediaType_InThePress, nil];
    id obj = [pad objectForKey:mediaType];
    if (obj) {
        result = [obj boolValue];
    }
    return result;
}

- (void)standardLayoutForPage:(int)page withAssets:(NSArray*)assets {
    
    //TODO - wrap this layout code into "standard" grid layout manager
    //TODO - this is not really useful any more with multiple models
    //for each pages
    float xPadding = [self xPaddingForThumbnail];
    float yPadding = [self yPaddingForThumbnail];
    float thumbnailWidth = 243;
    float thumbnailHeight = 342 / 2;
    
    //remove all child for this page first
    for (UIView *view in scrollView.subviews) {
        if (view.tag == page) {
            [view removeFromSuperview];
        }
    }
    
    if ([mediaType compare:kMediaType_Samples] == NSOrderedSame) {
        float xPos = page * self.frame.size.width;
        //TODO - hack to put spaces on interiors pages of sample view
        if ([mediaType compare:kMediaType_Samples] == NSOrderedSame) {
            if (page > 0) {
                xPos += page * xPadding;
            }
        }
        
        if (self.padThumbnailLeftColumn) {
            xPos += xPadding;
        }
        float yPos = self.padThumbnailTopRow ? yPadding : 0;
        int assetIndex = 0;
        UIView *thumbnail;
        int itemOnPage = 0;//must be 9 items for each page and we should add some default if missing
        //phase 2 client requested to order Sample Books by column
        
        NSLog(@"DAMPagedScrollView.standardLayoutForPage.Samples asset length == %d", [assets count]);
        
        for (NSDictionary *asset in assets) {
            
            thumbnail = [self createThumbnailForAsset:asset withTag: page andParentView:scrollView];
            itemOnPage++;
#ifdef DEBUG
            NSLog(@"DAMPagedScrollView.standardLayoutForPage.Samples thumbnailPos:(%f,%f) mediaType %@", xPos, yPos, mediaType);
#endif
            if (thumbnail) {
                CGRect thumbnailFrame = thumbnail.frame;
                thumbnailFrame.origin.x = xPos;
                thumbnailFrame.origin.y = yPos;
                thumbnail.frame = thumbnailFrame;
                //xPos += thumbnail.frame.size.width + xPadding;
                yPos += thumbnail.frame.size.height + yPadding;
                
            } else {
                thumbnail = [self oldLayoutForAsset:asset withTag:assetIndex andParentView:scrollView];
                thumbnail.frame = CGRectMake(xPos, yPos, thumbnailWidth, thumbnailHeight);
                //xPos += thumbnailWidth + xPadding;
                yPos += thumbnailHeight + yPadding;
            }
            if (yPos + thumbnail.frame.size.height > 768) {
                //move to next page then we reset itemOnPage
                yPos = self.padThumbnailTopRow ? yPadding : 0;
                xPos += xPadding + thumbnail.frame.size.width;
            }
            if (itemOnPage % 9 == 0) {
                //move to next page
                itemOnPage = 0;
            }
            assetIndex++;
        }
        if (itemOnPage < 9) {
            for (int i = itemOnPage ; i < 9 ; i++) {
                thumbnail = [self createThumbnailForAsset: nil withTag: page andParentView:scrollView];
#ifdef DEBUG
                NSLog(@"DAMPagedScrollView.standardLayoutForPage.Samples thumbnailPos:(%f,%f) mediaType %@", xPos, yPos, mediaType);
#endif
                if (thumbnail) {
                    CGRect thumbnailFrame = thumbnail.frame;
                    thumbnailFrame.origin.x = xPos;
                    thumbnailFrame.origin.y = yPos;
                    thumbnail.frame = thumbnailFrame;
                    //xPos += thumbnail.frame.size.width + xPadding;
                    yPos += thumbnail.frame.size.height + yPadding;
                } else {
                    thumbnail = [self oldLayoutForAsset: nil withTag:assetIndex andParentView:scrollView];
                    thumbnail.frame = CGRectMake(xPos, yPos, thumbnailWidth, thumbnailHeight);
                    //xPos += thumbnailWidth + xPadding;
                    yPos += thumbnailHeight + yPadding;
                }
                if (yPos + thumbnail.frame.size.height > 768) {
                    //move to next page then we reset itemOnPage
                    yPos = self.padThumbnailTopRow ? yPadding : 0;
                    xPos += xPadding + thumbnail.frame.size.width;
                }
            }
        }
    }else{
        float xPos = page * self.frame.size.width;
        //TODO - hack to put spaces on interiors pages of sample view
        if ([mediaType compare:kMediaType_Samples] == NSOrderedSame) {
            if (page > 0) {
                xPos += page * xPadding;
            }
        }
        
        if (self.padThumbnailLeftColumn) {
            xPos += xPadding;
        }
        float yPos = self.padThumbnailTopRow ? yPadding : 0;
        int assetIndex = 0;
        //order as normal
        for (NSDictionary *asset in assets) {
            UIView *thumbnail = [self createThumbnailForAsset:asset withTag: page andParentView:scrollView];
            NSLog(@"DAMPagedScrollView.standardLayoutForPage thumbnailPos:(%f,%f) mediaType %@", xPos, yPos, mediaType);
            if (thumbnail) {
                CGRect thumbnailFrame = thumbnail.frame;
                thumbnailFrame.origin.x = xPos;
                thumbnailFrame.origin.y = yPos;
                thumbnail.frame = thumbnailFrame;
                xPos += thumbnail.frame.size.width + xPadding;
            } else {
                thumbnail = [self oldLayoutForAsset:asset withTag:assetIndex andParentView:scrollView];
                thumbnail.frame = CGRectMake(xPos, yPos, thumbnailWidth, thumbnailHeight);
                xPos += thumbnailWidth + xPadding;
            }
            if (xPos - page * self.frame.size.width > 1024) {
                xPos = page * self.frame.size.width;
                //TODO - hack to put spaces on interiors pages of sample view
                if ([mediaType compare:kMediaType_Samples] == NSOrderedSame) {
                    if (page > 0) {
                        xPos += page * xPadding;
                    }
                }
                if (self.padThumbnailLeftColumn) {
                    xPos += xPadding;
                }
                yPos += yPadding + thumbnail.frame.size.height;
            }
            assetIndex++;
        }
    }
}

//- (void)addAssetGestureRecognizersToView:(UIView*)view {
//    for (UIView *subView in view.subviews) {
//        UIView *thumbnail = (UIView *)subView;
//        
//        thumbnail.userInteractionEnabled = YES;
//            
//        UIPinchGestureRecognizer *gestureRecog = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(imagePinch:)];
//        gestureRecog.delegate = self;
//        [thumbnail addGestureRecognizer:gestureRecog];
//        [gestureRecog release];
//            
//        UITapGestureRecognizer *tapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
//        tapRecog.delegate = self;
//        [thumbnail addGestureRecognizer:tapRecog];
//        [tapRecog release];
//    }
//}

- (void)createPageControlBackground {
    if (!pageControlBackground) {
        self.pageControlBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DAMPageViewBG.png"]];
        CGRect backgroundFrame = pageControlBackground.frame;
        backgroundFrame.origin.y = self.frame.size.height - pageControlBackgroundHeight;
        pageControlBackground.frame = backgroundFrame;
        [self addSubview:pageControlBackground];
        [pageControlBackground release];
    }
}

- (void) clearPageControl {
    [pageControlBackground.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

- (void)createPageControl:(int)numPages {

    [self createPageControlBackground];
    self.scrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - pageControlBackgroundHeight);
    
    [self clearPageControl];
    //NSLog(@"numPages == %d", numPages);
    float dotWidth = 15;
    float pageControlWidth = numPages * dotWidth;
    float pageLeft = (self.frame.size.width - pageControlWidth) / 2;
    float x = pageLeft;
    for (int i = 0; i < numPages; i++) {
        NSString *imageFile = i == 0 ? @"caro-ind-on.png" : @"caro-ind-off.png";
        UIImageView *dot = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageFile]];
        CGRect rect = dot.frame;
        rect.origin.x = x;
        rect.origin.y = 14;
        dot.frame = rect;
        [pageControlBackground addSubview:dot];
        
        x += dotWidth;
    }
}

- (void)updatePageControlToPage:(int)page {
    //TODO - fix the page control - this is slooow
    int index = 0;
    for (UIImageView *dotImage in pageControlBackground.subviews) {
        NSString *imageFile = index == page ? @"caro-ind-on.png" : @"caro-ind-off.png";
        dotImage.image = [UIImage imageNamed:imageFile];
        index++;
    }
}

- (void)loadModelForPage:(int)pageNumber {
    @synchronized(loadedPages) {
        if (delegate) {
            NSNumber *pageNum = [NSNumber numberWithInt:pageNumber];
            if ([loadedPages objectForKey:pageNum] == NULL) {
//                NSLog(@"pageNum:%@, loadedPages:%@", pageNum, loadedPages);
                [(NSMutableDictionary*) loadedPages setObject:@"" forKey:pageNum];
//                NSLog(@"pageNum:%@, loadedPages:%@", pageNum, loadedPages);
                [delegate requestModelForPage:pageNumber onCarousel:carousel];
            } else {
                //            NSLog(@"Ignoring too many requests for pageNum:%i", pageNumber);
            }
        }
        [self removeDistantPagesFromPage:pageNumber];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)pScrollView {

    int leftEdgePageIndex = MAX(0, pScrollView.contentOffset.x / pScrollView.frame.size.width);
    
    //want to do max pages clamp on here, but that breaks down for Photos
    int rightEdgePageIndex = (pScrollView.contentOffset.x + pScrollView.frame.size.width - 1) / pScrollView.frame.size.width;

    int pageControlNumber = (pScrollView.contentOffset.x + pScrollView.frame.size.width / 2.0f) / pScrollView.frame.size.width;
    [self updatePageControlToPage:pageControlNumber];

    [self loadModelForPage:leftEdgePageIndex];
    [self loadModelForPage:rightEdgePageIndex];
    
    //set content offset to force page move horizontal only
    [pScrollView setContentOffset: CGPointMake(pScrollView.contentOffset.x, 0)];
}

- (void)removeDistantPagesFromPage:(int)page {
    NSArray *loadedPageKeys = [loadedPages allKeys];
    for (id obj in loadedPageKeys) {
        int pageNum = [obj intValue];
        if (fabs(pageNum - page) > 1) {
            id pageObj = [loadedPages objectForKey:obj];
            if ([pageObj isKindOfClass:[UIView class]]) {
                UIView *view = pageObj;
                [view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                [view removeFromSuperview];
//                NSNumber *pageKey = [NSNumber numberWithInt:page];
                [(NSMutableDictionary*)loadedPages removeObjectForKey:obj];
#ifdef DEBUG
                NSLog(@"DAMPagedScrollView.removeDistantPagesFromPage page:%i, pageNum:%i", page, pageNum);
#endif
            }
        }
    }
}

//const float openScaleThreshold = 3.0;
//
//- (void)imagePinch:(UIPinchGestureRecognizer*)gestureRecognizer {
//
//    if (!isSelecting) {
//        [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
//        
//        if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
//            gestureRecognizer.view.transform = CGAffineTransformMakeScale(gestureRecognizer.scale, gestureRecognizer.scale);
//        } else {
//            if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
//                [UIView beginAnimations:@"imageReturnedToCarousel" context:nil];
//                [UIView setAnimationDuration:0.25];
//                gestureRecognizer.view.transform = CGAffineTransformIdentity;
//                [UIView commitAnimations];
//            
//                if (gestureRecognizer.scale > openScaleThreshold) {
//                    [self fireAssetOpened:gestureRecognizer.view];
//                }
//            }
//        }
//    }
//}
//
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
//    
////    NSLog(@"otherGestureRecognizer:%@", otherGestureRecognizer);
//    
//    // if the gesture recognizers are on different views, don't allow simultaneous recognition
//    if (gestureRecognizer.view != otherGestureRecognizer.view)
//        return NO;
//    
//    // if either of the gesture recognizers is the long press, don't allow simultaneous recognition
//    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
//        return NO;
//    
//    return YES;
//}
//- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
//    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
//        UIView *piece = gestureRecognizer.view;
//        CGPoint locationInView = [gestureRecognizer locationInView:piece];
//        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
//        
////        UIView *piece = [scrollView.subviews objectAtIndex:0];
//        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
//        piece.center = locationInSuperview;
//    }
//}

//- (void)imageTapped:(UIGestureRecognizer *)gestureRecognizer {
//    NSLog(@"imageTapped gestureRecognizer:%@", gestureRecognizer);
//
//    if (self.isSelecting) {
//        [self toggleSelection:gestureRecognizer.view];
//    } else {
//        [self fireAssetOpened:gestureRecognizer.view];
//    }
//}

- (void)fireAssetOpened:(UIView*)view{
#ifdef DEBUG
    NSLog(@"view.class:%@", view.class);
#endif
    NSDictionary *asset = nil;
    int index = -1;
    if ([view respondsToSelector:@selector(model)]) {
        asset = [view performSelector:@selector(model)];
    } else {
        //TODO - should be able to get rid of tag-based accessed to model soon??
        NSArray *assets = [model objectForKey:@"assets"];
        index = view.tag;
        asset = [assets objectAtIndex:index];
    }
    if (asset && delegate) {
        [delegate assetOpened:asset atIndex:index fromPagedScrollView:self];
    }
}

//- (BOOL)isSelecting { return isSelecting; }
//
//- (void)setSelecting:(BOOL)newValue {
//    isSelecting = newValue;
//    if (!isSelecting) {
//        for (UIView *view in scrollView.subviews) {
////            NSLog(@"DAMPagedScrollView.setSelecting view:%@", view);
//            if ([view respondsToSelector:@selector(cancelSelection)]) {
//                [view performSelector:@selector(cancelSelection)];
//            }
//        }
//        [(NSMutableArray*)selectedAssets removeAllObjects];
//    }
//}
//
//- (void)toggleSelection:(UIView*)view {
//    if ([view respondsToSelector:@selector(toggleSelection)]) {
//        [view performSelector:@selector(toggleSelection)];
//        
//        BOOL selected = [[view performSelector:@selector(isSelected)] boolValue];
//        NSDictionary *viewModel = (NSDictionary *)[view performSelector:@selector(model)];
//        if (selected) {
//            [(NSMutableArray*)selectedAssets addObject:viewModel];
//        } else {
//            [(NSMutableArray*)selectedAssets removeObject:viewModel];
//        }
//    }
//}

@end
