//
//  DAMSelectableView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 28/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DAMSelectableView.h"

@interface DAMSelectableView() {

    BOOL isSelecting;
    NSArray *selectedAssets;
    
}
//CLients to override this method
- (void)fireAssetOpened:(UIView*)sender;

@end

@implementation DAMSelectableView
@synthesize selectedAssets;

- (void)sharedInit {
    self.selectedAssets = [NSMutableArray arrayWithCapacity:10];
}

- (void)dealloc {
    self.selectedAssets = nil;
    [super dealloc];
}

- (void)fireAssetOpened:(UIView*)sender {}

- (UIView*)viewContainingSelectableItems { return nil; }

- (void)addAssetGestureRecognizersToView:(UIView*)view {
    for (UIView *subView in view.subviews) {
        UIView *thumbnail = (UIView *)subView;
        
        thumbnail.userInteractionEnabled = YES;
        
        for (UIGestureRecognizer *recognizer in thumbnail.gestureRecognizers) {
            [thumbnail removeGestureRecognizer:recognizer];
        }
        
        /*UIPinchGestureRecognizer *gestureRecog = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(imagePinch:)];
        gestureRecog.delegate = self;
        [thumbnail addGestureRecognizer:gestureRecog];
        [gestureRecog release];*/
        
        UITapGestureRecognizer *tapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
        tapRecog.delegate = self;
        [thumbnail addGestureRecognizer:tapRecog];
        [tapRecog release];
    }
}

const float openScaleThreshold = 3.0;

- (void)imagePinch:(UIPinchGestureRecognizer*)gestureRecognizer {
    
    if (!isSelecting) {
        [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
        
        if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
            gestureRecognizer.view.transform = CGAffineTransformMakeScale(gestureRecognizer.scale, gestureRecognizer.scale);
        } else {
            if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
                [UIView beginAnimations:@"imageReturnedToCarousel" context:nil];
                [UIView setAnimationDuration:0.25];
                gestureRecognizer.view.transform = CGAffineTransformIdentity;
                [UIView commitAnimations];
                
                if (gestureRecognizer.scale > openScaleThreshold) {
                    [self fireAssetOpened:gestureRecognizer.view];
                }
            }
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    //    NSLog(@"otherGestureRecognizer:%@", otherGestureRecognizer);
    
    // if the gesture recognizers are on different views, don't allow simultaneous recognition
    if (gestureRecognizer.view != otherGestureRecognizer.view)
        return NO;
    
    // if either of the gesture recognizers is the long press, don't allow simultaneous recognition
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        return NO;
    
    return YES;
}
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        //        UIView *piece = [scrollView.subviews objectAtIndex:0];
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

- (void)imageTapped:(UIGestureRecognizer *)gestureRecognizer {
#ifdef DEBUG
    NSLog(@"DAMSelectableView.imageTapped gestureRecognizer:%@", gestureRecognizer);
#endif
    if (self.isSelecting) {
        [self toggleSelection:gestureRecognizer.view];
    } else {
        [self fireAssetOpened:gestureRecognizer.view];
    }
}

- (BOOL)isSelecting { return isSelecting; }

- (void)setSelecting:(BOOL)newValue {
    isSelecting = newValue;
    if (!isSelecting) {
        UIView *parentView = [self viewContainingSelectableItems];
        for (UIView *view in parentView.subviews) {
            
            //            NSLog(@"DAMPagedScrollView.setSelecting view:%@", view);
            if ([view respondsToSelector:@selector(cancelSelection)]) {
                [view performSelector:@selector(cancelSelection)];
            }
        }
        [(NSMutableArray*)selectedAssets removeAllObjects];
    }
}

- (void)toggleSelection:(UIView*)view {
    if ([view respondsToSelector:@selector(toggleSelection)]) {
        [view performSelector:@selector(toggleSelection)];
        
        BOOL selected = [[view performSelector:@selector(isSelected)] boolValue];
        NSDictionary *viewModel = (NSDictionary *)[view performSelector:@selector(model)];
        if (selected) {
            [(NSMutableArray*)selectedAssets addObject:viewModel];
        } else {
            [(NSMutableArray*)selectedAssets removeObject:viewModel];
        }
    }
}

@end
