//
//  DAMImageScrollingView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 17/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMImageScrollingViewDelegate

- (void)imageSingleTapped;

@end
    
@interface DAMImageScrollingView : UIScrollView<UIScrollViewDelegate> {
    
}
@property (nonatomic, assign) CGRect originalFrame;
@property (nonatomic, assign) id<DAMImageScrollingViewDelegate> gestureDelegate;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) NSDictionary *model;

- (void)showThumbImage:(NSString *) thumbUrl model:(NSDictionary *) imgModel;
- (void)showThumbImageForSamples:(NSString *) thumbUrl model:(NSDictionary *) imgModel;

@end
