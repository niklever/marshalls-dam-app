//
//  DAMPredictiveViewController.m
//  MarshallsDigitalAssetManagement
//
//  Created by Hoa Truong Huu on 1/16/13.
//
//

#import "DAMPredictiveViewController.h"
#import "JSMain.h"
#import <QuartzCore/QuartzCore.h>

@interface DAMPredictiveViewController ()

@property (nonatomic, retain) NSArray *items;
@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSTimer *timer;

@end

@implementation DAMPredictiveViewController
@synthesize items;
@synthesize key;
@synthesize contentTableView;
@synthesize delegate;
@synthesize timer;
@synthesize searchField;
#pragma mark - Dealloc

- (void)dealloc{
    
    self.contentTableView.delegate = nil;
    self.contentTableView.dataSource = nil;
    self.contentTableView = nil;
    
    self.items = nil;

    [super dealloc];
}

#pragma mark Public Function

- (void)requestDataWithKey:(NSString *)_key mediaType:(NSString *)_mediaType containers:(NSArray *)_containers{
    if (self.key && [self.key isEqualToString:_key]) {
        return;
    }
    
    self.key = _key;
    
//    NSDictionary *item;
//    NSMutableArray *tmpArray = [NSMutableArray array];
//    for (int i = 0; i < 10 ; i++) {
//        item = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@ %d", self.key, i] forKey:@"text"];
//        [tmpArray addObject:item];
//    }
//    
//    
    [[JSMain sharedInstance] predictiveKeysWithKey:self.key mediaType:_mediaType containers: _containers callback:^(NSArray *results) {
        if (results == NULL || [results count] == 0) {
#ifdef DEBUG
            NSLog(@"PredictiveViewController no results found");
#endif
            [self hideAfterTime:0];
        }else{
            self.view.hidden = NO;
            [self reloadDataWithItems:results];
        }
    }];
}

- (void)resetData{
    self.items = nil;
    self.key = @"";
}

- (void)reloadData{
    
    self.contentTableView.contentOffset = CGPointZero;
    [self.contentTableView reloadData];
}

- (void)reloadDataWithItems:(NSArray *)_items{
    
    self.items = _items;
    [self reloadData];
    
}

- (void)hideAfterTime:(float)time{
#ifdef DEBUG
    NSLog(@"hideAfterTime = %f.............", time);
#endif
    if (self.timer) {
        [self.timer invalidate];
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(hide:) userInfo:nil repeats:NO];
}

- (void)hide:(id)sender{
#ifdef DEBUG
    NSLog(@"hide.............");
#endif
    self.view.hidden = YES;
}

#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	
    //    NSLog(@"numberOfRowsInSection locations count = %d", self.locations.count);
    
	return (self.items ? self.items.count : 0);
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CellIdentifier = @"LocationIdentifier";
	
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textAlignment = UITextAlignmentLeft;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
	}
    
    
    NSDictionary *item = (NSDictionary *)[self.items objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [item objectForKey:@"text"];
    
    
    return cell;
}



#pragma mark Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48.0f;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(predictiveViewController:didChooseItem:)]) {
        [self.delegate predictiveViewController:self didChooseItem:[self.items objectAtIndex:indexPath.row]];
    }
    
    [self.contentTableView deselectRowAtIndexPath:indexPath animated:YES];
	
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:@"DAMPredictiveViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.layer.cornerRadius = 10;
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
