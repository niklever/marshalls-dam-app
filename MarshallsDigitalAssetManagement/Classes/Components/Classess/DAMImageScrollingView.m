//
//  DAMImageScrollingView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 17/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMImageScrollingView.h"
#import "DAMImageView.h"
#import "UIImageView+WebCache.h"
#import "ImageHelper.h"
#import <QuartzCore/QuartzCore.h>

@interface DAMImageScrollingView() {
    
    id<DAMImageScrollingViewDelegate> gestureDelegate;
}

@end

@implementation DAMImageScrollingView
@synthesize gestureDelegate;
@synthesize model;
@synthesize originalFrame;

#define ZOOM_STEP 1.5

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
        self.delegate = self;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //    NSLog(@"DAMMapScrollView.layoutSubviews");
    
    CGSize boundsSize = self.bounds.size;
    UIView *mapView = [self.subviews objectAtIndex:0];
    CGRect frameToCentre = mapView.frame;
    
    //centre horizontally
    if (frameToCentre.size.width < boundsSize.width)
        frameToCentre.origin.x = (boundsSize.width - frameToCentre.size.width) / 2;
    else
        frameToCentre.origin.x = 0;
    
    //centre vertically
    if (frameToCentre.size.height < boundsSize.height)
        frameToCentre.origin.y = (boundsSize.height - frameToCentre.size.height) / 2;
    else
        frameToCentre.origin.y = 0;
    
    mapView.frame = frameToCentre;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)pScrollView { 
    UIView *viewForZoom = [pScrollView.subviews objectAtIndex:0];
    return viewForZoom;
}

- (UIImage*)image {
    if (self.subviews.count > 0) {
        UIImageView *imageView = (UIImageView *)[self.subviews objectAtIndex:0];
        
        return  imageView.image;
    }
    return nil;
}


- (void)showThumbImageForSamples:(NSString *) thumbUrl model:(NSDictionary *) imgModel{
    CGSize imgSize = CGSizeMake([[imgModel objectForKey:@"width"] floatValue], [[imgModel objectForKey:@"height"] floatValue]);
    CGSize imageSize = imgSize;
#ifdef DEBUG
    NSLog(@"imageSize:(%f,%f)", imageSize.width, imageSize.height);
#endif
    
    float x,y,tw,th;
    x = 0;
    y = 0;
    tw = [[imgModel objectForKey:@"width"] floatValue];
    th = [[imgModel objectForKey:@"height"] floatValue];
    
    //self.contentSize = imgSize;
    
    //NSLog(@"content size == %f, %f, x== %f, y== %f", self.contentSize.width, self.contentSize.height, self.contentOffset.x, self.contentOffset.y);
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //self.frame = CGRectMake(x,y,tw,th);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, imgSize.width, imgSize.height)];
    //UIImageView *imageView = [[UIImageView alloc] initWithFrame: self.frame];
    [imageView sd_setImageWithURL:[NSURL URLWithString:thumbUrl]
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                //imageView.image = [_image scaledToSize: imgSize];
                        }];
#ifdef DEBUG
    NSLog(@"imageView:%@", imageView);
#endif
    [self addSubview:imageView];
    
    imageView.hidden = YES;
    
    float imageToFrameRatio = MAX(imageSize.width, imageSize.height) / self.frame.size.height;
    
    self.scrollEnabled = YES;
    self.minimumZoomScale = 0.5 / imageToFrameRatio;
    self.maximumZoomScale = imageToFrameRatio;
    
    CGRect startRect = [self zoomRectForScale:1.0 / imageToFrameRatio withCenter:CGPointMake(imgSize.width/2.0, imgSize.height/2.0)];
    
    [self zoomToRect:startRect animated: NO];
    
    self.originalFrame = self.frame;
    
    //CGSize newSize = imageView.frame.size;
    CGRect newRect = imageView.frame;
    self.frame = CGRectMake(x,y,tw,th);
    imageView.frame = CGRectMake(0,0,tw,th);
    
    //NSLog(@"content size == %f, %f, x== %f, y== %f self.contentScaleFactor == %f", self.contentSize.width, self.contentSize.height, self.contentOffset.x, self.contentOffset.y, self.contentScaleFactor);
    
    
    [UIView animateWithDuration: 0.3f
                     animations:^{
                         self.frame = self.originalFrame;
                         //self.contentSize = imgSize;
                         imageView.hidden = NO;
                         imageView.frame = newRect;
                         //imageView.frame = CGRectMake((1024 - newSize.width) / 2.0, (748 - newSize.height) / 2.0, newSize.width * imageToFrameRatio , newSize.height *imageToFrameRatio);
                     } completion:^(BOOL finished) {
                         
                     }];
    self.scrollEnabled = YES;
    //[self addGestureRecognizers];
}


- (void)showThumbImage:(NSString *) thumbUrl model:(NSDictionary *) imgModel{
    CGSize imgSize = CGSizeMake([[imgModel objectForKey:@"originalWidth"] floatValue], [[imgModel objectForKey:@"originalHeight"] floatValue]);
    CGSize imageSize = imgSize;
#ifdef DEBUG
    NSLog(@"imageSize:(%f,%f)", imageSize.width, imageSize.height);
#endif
    
    float x,y,tw,th;
    x = [[imgModel objectForKey:@"layoutX"] floatValue] - [[imgModel objectForKey:@"offsetX"] floatValue];
    y = [[imgModel objectForKey:@"layoutY"] floatValue];
    tw = [[imgModel objectForKey:@"width"] floatValue];
    th = [[imgModel objectForKey:@"height"] floatValue];
    
    //self.contentSize = imgSize;
    
    //NSLog(@"content size == %f, %f, x== %f, y== %f", self.contentSize.width, self.contentSize.height, self.contentOffset.x, self.contentOffset.y);
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //self.frame = CGRectMake(x,y,tw,th);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, imgSize.width, imgSize.height)];
    //UIImageView *imageView = [[UIImageView alloc] initWithFrame: self.frame];
    [imageView sd_setImageWithURL:[NSURL URLWithString:thumbUrl]
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            //imageView.image = [_image scaledToSize: imgSize];
    }];
    
#ifdef DEBUG
    NSLog(@"imageView:%@", imageView);
#endif
    [self addSubview:imageView];
    
    imageView.hidden = YES;
    
    [imageView release];
    
    
    float imageToFrameRatio = MAX(imageSize.width, imageSize.height) / self.frame.size.height;
    
    self.scrollEnabled = YES;
    self.minimumZoomScale = 0.5 / imageToFrameRatio;
    self.maximumZoomScale = imageToFrameRatio;
    
    CGRect startRect = [self zoomRectForScale:1.0 / imageToFrameRatio withCenter:CGPointMake(imgSize.width/2.0, imgSize.height/2.0)];
    
    [self zoomToRect:startRect animated: NO];
    
    self.originalFrame = self.frame;
    
    //CGSize newSize = imageView.frame.size;
    CGRect newRect = imageView.frame;
    self.frame = CGRectMake(x,y,tw,th);
    imageView.frame = CGRectMake(0,0,tw,th);
    
    //NSLog(@"content size == %f, %f, x== %f, y== %f self.contentScaleFactor == %f", self.contentSize.width, self.contentSize.height, self.contentOffset.x, self.contentOffset.y, self.contentScaleFactor);
    
    
    [UIView animateWithDuration: 0.3f
                     animations:^{
                         self.frame = self.originalFrame;
                         //self.contentSize = imgSize;
                         imageView.hidden = NO;
                         imageView.frame = newRect;
                         //imageView.frame = CGRectMake((1024 - newSize.width) / 2.0, (748 - newSize.height) / 2.0, newSize.width * imageToFrameRatio , newSize.height *imageToFrameRatio);
                     } completion:^(BOOL finished) {
                         
                     }];
    self.scrollEnabled = YES;
    [self addGestureRecognizers];
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = self.frame.size.height / scale;
    zoomRect.size.width  = self.frame.size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}

- (void)animateThumb:(UIImageView *)iv {
    [UIView animateWithDuration: 0.5f
                     animations:^{
                         self.frame = self.originalFrame;
                         iv.hidden = NO;
                         iv.frame = CGRectMake(0, 0, self.contentSize.width, self.contentSize.height);
                     } completion:^(BOOL finished) {
                         
                     }];
}



- (void)setImage:(UIImage*)image {
    CGSize imageSize = image.size;
#ifdef DEBUG
    NSLog(@"imageSize:(%f,%f)", imageSize.width, imageSize.height);
#endif
    
    self.contentSize = image.size;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    DAMImageView *imageView = [[DAMImageView alloc] initWithImage:image];
#ifdef DEBUG
    NSLog(@"imageView:%@", imageView);
#endif
    [self addSubview:imageView];
    [imageView release];

    //TODO - work out zoom min,max from image dimensions
//    self.minimumZoomScale = 0.15;
//    self.maximumZoomScale = 2.0;
    
    
    if ([[model objectForKey:@"mediaType"] isEqualToString:kMediaType_Samples]) {
        if ([[model objectForKey:@"HasCrop"] boolValue]) {
            //processing crop value
            /*[imageView zoomToRect:CGRectMake([[model objectForKey:@"CropX1"] intValue], [[model objectForKey:@"CropY1"] intValue], [[model objectForKey:@"CropX2"] intValue], [[model objectForKey:@"CropY2"] intValue]) animated:NO];
             */
            CGRect zoomRect;
            //= CGRectMake([[model objectForKey:@"CropX1"] intValue], [[model objectForKey:@"CropY1"] intValue], [[model objectForKey:@"CropX2"] intValue], [[model objectForKey:@"CropY2"] intValue]);
            zoomRect.origin.x = ([[model objectForKey:@"CropX2"] intValue] - [[model objectForKey:@"CropX2"] intValue]) / 2.0;
            zoomRect.origin.y = ([[model objectForKey:@"CropY2"] intValue] - [[model objectForKey:@"CropY2"] intValue]) / 2.0;
            zoomRect.size.width = [[model objectForKey:@"CropX2"] intValue] - [[model objectForKey:@"CropX1"] intValue];
            zoomRect.size.height = [[model objectForKey:@"CropY2"] intValue] - [[model objectForKey:@"CropY1"] intValue];
            
            
            //NSLog(@"zoom rect %d - %d - %d - %d", [[model objectForKey:@"CropX1"] intValue], [[model objectForKey:@"CropY1"] intValue], [[model objectForKey:@"CropX2"] intValue], [[model objectForKey:@"CropY2"] intValue]);
            //NSLog(@"zoom rect %d - %d - %d - %d",zoomRect.origin.x, zoomRect.origin.y, zoomRect.size.width, zoomRect.size.height);
            
            float imageToFrameRatio = MAX(imageSize.width, imageSize.height) / self.frame.size.height;
            
            self.scrollEnabled = YES;
            self.minimumZoomScale = 0.5 / imageToFrameRatio;
            self.maximumZoomScale = imageToFrameRatio * 2;
            
            //imageToFrameRatio = MAX(zoomRect.size.width, zoomRect.size.height) / self.frame.size.height;
            
            //CGRect startRect = [self zoomRectForScale: 1.0/imageToFrameRatio withCenter:CGPointMake(image.size.width/2.0, image.size.height/2.0)];
            
            [self zoomToRect: zoomRect animated:NO];
            /*if (imageToFrameRatio < 0.0) {
                [self zoomToRect: zoomRect animated:NO];
            }else{
                CGRect startRect = [self zoomRectForScale:1.0 / imageToFrameRatio withCenter:CGPointMake(image.size.width/2.0, image.size.height/2.0)];
                [self zoomToRect: startRect animated:NO];
            }*/
            
            
        }else{
            float imageToFrameRatio = MAX(imageSize.width, imageSize.height) / self.frame.size.height;
            
            self.scrollEnabled = YES;
            self.minimumZoomScale = 0.5 / imageToFrameRatio;
            self.maximumZoomScale = imageToFrameRatio;
            CGRect startRect = [self zoomRectForScale:1.0 withCenter:CGPointMake(image.size.width/2.0, image.size.height/2.0)];
            [self zoomToRect:startRect animated:NO];
        }
        
        
    }else{
        float imageToFrameRatio = MAX(imageSize.width, imageSize.height) / self.frame.size.height;
        
        self.scrollEnabled = YES;
        self.minimumZoomScale = 0.5 / imageToFrameRatio;
        self.maximumZoomScale = imageToFrameRatio;
        CGRect startRect = [self zoomRectForScale:1.0 / imageToFrameRatio withCenter:CGPointMake(image.size.width/2.0, image.size.height/2.0)];
        [self zoomToRect:startRect animated:NO];
    }

    [self addGestureRecognizers];
}

- (void)addGestureRecognizers {
    UIView *mapView = [self.subviews objectAtIndex:0];
    mapView.userInteractionEnabled = YES;
    
    // add gesture recognizers to the image view
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];
    
    [mapView addGestureRecognizer:singleTap];
    [mapView addGestureRecognizer:doubleTap];
    [mapView addGestureRecognizer:twoFingerTap];
    
    [singleTap release];
    [doubleTap release];
    [twoFingerTap release]; 
}

#pragma mark TapDetectingImageViewDelegate methods

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureDelegate) {
        [gestureDelegate imageSingleTapped];
    }
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // double tap zooms in
    float newScale = self.zoomScale * ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [self zoomToRect:zoomRect animated:YES];
}

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    
    //Bail out if we are already zoomed out to the MAX
    if (self.zoomScale == self.minimumZoomScale) {
        //        NSLog(@"ALREADY ZOOMED OUT, BAIL");
        return;
    }
    
    float newScale = self.zoomScale / ZOOM_STEP;
    if (newScale < self.minimumZoomScale) {
        newScale = self.minimumZoomScale;
    }
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [self zoomToRect:zoomRect animated:YES];
}

//- (void)viewDi
@end
