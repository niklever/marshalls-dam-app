//
//  DAMImageView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 12/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMImageView.h"
//#import "ASIHTTPRequest.h"
//#import "ASIDownloadCache.h"
//#import "ASINetworkQueue.h"
#import "DAMApplicationModel.h"
#import "ImageHelper.h"
#import "UIImageView+WebCache.h"


static int imageCount = 0;

@interface DAMImageView () {
    NSDictionary *model;

    UIImageView *defaultImage;
    UIImageView *selectedImage;
    
    //ASIHTTPRequest *request;
}
@property(nonatomic, retain) UIImageView *defaultImage;
@property(nonatomic, retain) UIImageView *selectedImage;
//@property(nonatomic, retain) ASIHTTPRequest *request;

@end

@implementation DAMImageView
@synthesize model;
@synthesize defaultImage;
@synthesize selectedImage;
//@synthesize request;

//static ASINetworkQueue *queue = nil;

+ (int)imageCount {
    return imageCount;
}

/*
+ (ASINetworkQueue*)networkQueue {
    if (!queue) {
        queue = [[ASINetworkQueue alloc] init];
        queue.shouldCancelAllRequestsOnFailure = NO;
        queue.delegate = self;
        //Throttle back so that this queue doesn't compete will the
        //realtime.
        queue.maxConcurrentOperationCount = 2;
//        queue.queueDidFinishSelector = @selector(queueFinished:);
//        [queue autorelease];
    }
    return queue;
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    self.clipsToBounds = YES;
    
    return self;
}

- (id)initWithUrl:(NSString*)url andFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //treat URLs beginning with forward slash as network URLS.
        if ([[url substringToIndex:1] compare:@"/"] == NSOrderedSame) {
            [self setUrl:url andPlaceholderIndex:2];
        }
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    imageCount++;
//    NSLog(@"DAMImageView.initWithFrame imageCount:%i", imageCount);
    
    return self;
}

- (void)dealloc {
    //request.delegate = nil;
    //[request cancel];
    //self.request = nil;
    self.model = nil;
    self.image = nil;
    self.defaultImage.image = nil;
    self.defaultImage = nil;
    self.selectedImage = nil;
    
    imageCount--;
//    NSLog(@"DAMImageView.dealloc imageCount:%i", imageCount);
    
    [super dealloc];
}

- (void)setUrl:(NSString*)uUrl andPlaceholderIndex:(int)placeholderIndex {
    self.image = nil;
    //put up a default image whilst we load

    NSArray *placeHolders = [NSArray arrayWithObjects:@"DAMImageView_Placeholder.png",
                             @"DAMImageView_Placeholder02.png", @"DAMImageView_Placeholder03_a80.png", @"DAMImageView_Photo_Placeholder.png", nil];

    NSString *placeHolder = nil;
    if (placeholderIndex != 3) {
        placeHolder = [placeHolders objectAtIndex:placeholderIndex];
    }
    
    [self setUrl:uUrl andPlaceholder:placeHolder];
}

- (void)setUrl:(NSString*)uUrl andPlaceholder:(NSString*)placeholder {
    self.image = nil;
    //put up a default image whilst we load
    
    self.defaultImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    if (placeholder) {
        defaultImage.image = [UIImage imageNamed:placeholder];
    } else {
        defaultImage.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.0];
    }
    //[self addSubview:defaultImage];
    
    //start the network request
    NSString *host = [[DAMApplicationModel model] apiHost];
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@", host, uUrl];
    
    //NSLog(@"imageUrl detail == %@", imageUrl);
    
    NSURL *url = [NSURL URLWithString:imageUrl];
    
    [self.defaultImage sd_setImageWithURL: url
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    if (!image) {
                                        return;
                                    }
                                    self.image = image;
                                    [UIView beginAnimations:@"fadePlaceholder" context:nil];
                                    [UIView setAnimationDuration:1.0];
                                    [UIView setAnimationDelegate:self];
                                    [UIView setAnimationDidStopSelector:@selector(imageLoaded)];
                                    self.defaultImage.alpha = 0.0;
                                    [UIView commitAnimations];
                                }];
    /*
    [self.defaultImage setImageWithURL: url success:^(UIImage *_image) {
        self.image = _image;
        [UIView beginAnimations:@"fadePlaceholder" context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(imageLoaded)];
        defaultImage.alpha = 0.0;
        [UIView commitAnimations];
        
    } failure:^(NSError *error) {
        NSLog(@"DAMImageView.requestFailed url:%@, error:%@", url, error);
    }];
    */
}

/*
- (void)requestFinished:(ASIHTTPRequest *)pRequest {
    NSData *imageData = [pRequest responseData];
    ASINetworkQueue *netQueue = [DAMImageView networkQueue];
    float delay = netQueue.operationCount * 0.2;
    [self performSelector:@selector(setImageData:) withObject:imageData afterDelay:delay];
}*/

- (void)setImageData:(NSData*)imageData {
    self.image = [UIImage imageWithData:imageData];
    //request.delegate = nil;
    //self.request = nil;
    
    [UIView beginAnimations:@"fadePlaceholder" context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(imageLoaded)];
    defaultImage.alpha = 0.0;
    [UIView commitAnimations];
}

/*
- (void)requestFailed:(ASIHTTPRequest *)pRequest {
    NSError *error = [pRequest error];
#ifdef DEBUG
    NSLog(@"DAMImageView.requestFailed url:%@, error:%@", [pRequest url], error);
#endif
    //request.delegate = nil;
    //self.request = nil;
}        
*/

- (void)imageLoaded {
    self.defaultImage = nil;
}

- (void)cancelSelection {
#ifdef DEBUG
    NSLog(@"DAMImageView.cancelSelection model.title:%@", [model objectForKey:@"title"]);
#endif
    [selectedImage removeFromSuperview];
    self.selectedImage = nil;
}

- (NSNumber*)isSelected {
    return selectedImage != nil ? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0];
}

- (void)toggleSelection {
    if (!selectedImage) {
        self.selectedImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selected.png"]];
        CGRect frame = selectedImage.frame;
        //put the image in the bottom right corner of the main image
        frame.origin.x = self.frame.size.width - frame.size.width;
        frame.origin.y = self.frame.size.height - frame.size.height;
        selectedImage.frame = frame;
        [self addSubview:selectedImage];
        [selectedImage release];
    } else {
        [selectedImage removeFromSuperview];
        self.selectedImage = nil;
    }
}



@end
