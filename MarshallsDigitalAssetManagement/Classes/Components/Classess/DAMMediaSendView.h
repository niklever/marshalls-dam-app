//
//  DAMMediaSendView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 11/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMModalViewBase.h"
#import "DAMEmailChooserView.h"

@protocol DAMMediaSendViewDelegate;

@interface DAMMediaSendView : DAMModalViewBase<UITextFieldDelegate, DAMEmailChooserViewDelegate, UIPopoverControllerDelegate> {
    
    id<DAMMediaSendViewDelegate> delegate;
    
//    UIView *overlayView;
//    UIView *contentPanel;
    
    UITextField *emailAddress;
    UITextField *subject;
    UITextView *message;
    
    UIButton *sendButton;
}
@property (nonatomic, assign) id<DAMMediaSendViewDelegate> delegate;

//@property (nonatomic, retain) IBOutlet UIView *overlayView;
//@property (nonatomic, retain) IBOutlet UIView *contentPanel;

@property (nonatomic, retain) IBOutlet UITextField *emailAddress;
@property (nonatomic, retain) IBOutlet UITextField *subject;
@property (nonatomic, retain) IBOutlet UITextView *message;

@property (nonatomic, retain) IBOutlet UIButton *sendButton;

@property (nonatomic, retain) IBOutlet UIButton *emailChoiceButton;

//- (void)animateOnStage;

- (IBAction)sendTapped:(id)sender;
- (IBAction)cancelTapped:(id)sender;

- (IBAction)clearEmailAdressTapped:(id)sender;
- (IBAction)clearSubjectTapped:(id)sender;
- (IBAction)clearMessageTapped:(id)sender;

- (IBAction)emailChoiceTapped:(id)sender;

@end

@protocol DAMMediaSendViewDelegate <NSObject>

- (void)sendMediaTapped;
- (void)cancelMediaSendTapped;

- (void)sendMediaTapped:(DAMMediaSendView *)sendView;
- (void)cancelMediaSendTapped:(DAMMediaSendView *)sendView;

@end
