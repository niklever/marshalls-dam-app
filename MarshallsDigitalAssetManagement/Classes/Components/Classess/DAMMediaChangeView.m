//
//  DAMMediaChangeView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 28/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMMediaChangeView.h"
#import "DAMIDriveTableViewCell.h"
#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"

@interface DAMMediaChangeView () {
    id<DAMMediaChangeViewDelegate> delegate;
    NSArray *model;
    
    UILabel *titleLabel;
    UITableView *table;
    UIButton *cancelButton;
    UIButton *okButton;
}

@end

@implementation DAMMediaChangeView
@synthesize delegate;
@synthesize model;
@synthesize titleLabel;
@synthesize table;
@synthesize cancelButton;
@synthesize okButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    titleLabel.textColor = [[DAMUIDefaults defaults] textColor];
    titleLabel.font = [[DAMUIDefaults defaults] modalTitleFont];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.titleLabel = nil;
    self.model = nil;
    self.table = nil;
    self.cancelButton = nil;
    self.okButton = nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return model.count;
}

- (CGFloat)cellHeight {
    return 74;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (void)verticallCentreView:(UIView*)view {
    CGRect frame = view.frame;
    frame.origin.y = (self.cellHeight - frame.size.height) / 2;
    view.frame = frame;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    int row = indexPath.row;

    NSDictionary *file = [model objectAtIndex:row];
    
    DAMIDriveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DAMMediaChangeView"];

    if (cell == NULL) {
        cell = [[DAMIDriveTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DAMMediaChangeView"];
        
        float pad = 10;
        cell.title.frame = CGRectMake(cell.thumbnail.frame.size.width + pad, 0, 500, self.cellHeight);
        
        cell.added.hidden = YES;
        cell.type.hidden = YES;
        cell.disclosure.hidden = YES;
    }
    NSString *folderPath = [file objectForKey:@"path"];
    NSMutableArray *pathComps = [NSMutableArray arrayWithArray:[folderPath componentsSeparatedByString:@"/"]];
    [pathComps removeLastObject];
    [pathComps removeObjectAtIndex:0];
    
    NSString *readableFolder = [pathComps componentsJoinedByString:@" > "];
    NSString *title = [file objectForKey:@"title"];
    NSString *removedPath = [NSString stringWithFormat:@"%@ > %@", readableFolder, title];
    cell.title.text = removedPath;
    
    NSString *imageUrl = [[DAMApplicationModel model] thumbnailForAsset:file inView:nil];
    [cell.thumbnail setUrl:imageUrl andPlaceholderIndex:2];
    
    [self verticallCentreView:cell.title];
    [self verticallCentreView:cell.thumbnail];
    
    return cell;
}

- (IBAction)cancelTapped:(id)sender {
    [self animateOffStage];
    if (delegate) {
        [delegate mediaChangeViewDidCancel];
    }
}

- (IBAction)okTapped:(id)sender {
    [self animateOffStage];
    if (delegate) {
        [delegate mediaChangeViewDidOK];
    }
}

@end
