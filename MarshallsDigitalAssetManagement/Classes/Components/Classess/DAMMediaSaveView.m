//
//  DAMMediaSaveView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMMediaSaveView.h"

@implementation DamEditableTableViewCell
@synthesize textField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, self.frame.size.height)];
        [self.textLabel addSubview:textField];
        [textField release];
    }
    return self;
}

- (void)dealloc {
    self.textField = nil;
    [super dealloc];
}

@end

@interface DAMMediaSaveView ()

@end

@implementation DAMMediaSaveView
@synthesize model;
@synthesize delegate;
@synthesize iDriveContainer;
@synthesize iDrive;

#define kMediaSaveViewTableCellId @"kMediaSaveViewTableCellId"
#define kMediaSaveViewTableCellAddFolderId @"kMediaSaveViewTableCellAddFolderId"

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.model = [NSMutableArray arrayWithCapacity:10];
        [(NSMutableArray*)model addObject:@"Clients"];
        [(NSMutableArray*)model addObject:@"Meetings"];
        [(NSMutableArray*)model addObject:@"Todo"];

//        self.selectedFolder = @"Test Folder";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.    
    
    self.iDrive = [[DAMIDriveNavigatorEmbedded alloc] initWithNibName:@"DAMIDriveNavigatorEmbedded" bundle:nil];
    iDrive.view.frame = CGRectMake(0, 0, iDriveContainer.frame.size.width, iDriveContainer.frame.size.height);
    [iDriveContainer addSubview:iDrive.view];
    [iDrive release];
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

    self.model = nil;
    self.iDriveContainer = nil;
    self.iDrive = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (IBAction)saveButtonPressed:(id)sender {
    if (delegate) {
        
        if([delegate respondsToSelector:@selector(saveActioned:ToFolder:)]){
            [self animateOffStage:^{
                [delegate saveActioned:self ToFolder:iDrive.breadcrumb.currentFolder];
            }];
        }
        else{
            [self animateOffStage];
            [delegate saveActionedToFolder:iDrive.breadcrumb.currentFolder];
        }
    }
}

- (IBAction)cancelButtonPressed:(id)sender {
    if (delegate) {
        if([delegate respondsToSelector:@selector(saveCancelled:)]){
            [self animateOffStage:^{
                [delegate saveCancelled:self];
            }];
        }
        else{
            [self animateOffStage];
            [delegate saveCancelled];
        }
    }
}

@end
