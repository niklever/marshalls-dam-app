//
//  DAMCheckBox.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMCheckBox.h"

@implementation DAMCheckBox
@synthesize tag;
@synthesize label;

const float checkBoxButtonXPad = 8;
const float checkBoxButtonYPad = 0;
const float checkBoxButtonWidth = 23;
const float checkBoxButtonHeight = 23;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

        label = [[UILabel alloc] initWithFrame:CGRectMake(checkBoxButtonWidth + 2*checkBoxButtonXPad, checkBoxButtonYPad, frame.size.width - checkBoxButtonWidth - 2*checkBoxButtonXPad , frame.size.height)];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(checkBoxButtonXPad, checkBoxButtonYPad, checkBoxButtonWidth, checkBoxButtonHeight)];
        button.tag = 0;
        [button setBackgroundImage:[UIImage imageNamed:@"DAM_CheckBox.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(checkboxTapped:) forControlEvents:UIControlEventTouchDown];
        
        [self addSubview:label];
        [self addSubview:button];
        
        [button release];
    }
    return self;
}

- (void)dealloc {
    [label release];
    [super dealloc];
}

//- (BOOL)isSelected {
//    UIButton *button = [self.subviews objectAtIndex:1];
//    return button.tag = 1;
//}

- (void)setSelected:(BOOL)newState {
//    UIButton *button = [self.subviews objectAtIndex:1];
//    button.tag = newState ? 1 : 0;
//    NSString *stateImage = button.tag ? @"DAM_CheckBoxSelected.png" : @"DAM_CheckBox.png";
//    [button setImage:[UIImage imageNamed:stateImage] forState:UIControlStateNormal];

    UIButton *button = [self.subviews objectAtIndex:1];
    NSString *stateImage = newState ? @"DAM_CheckBoxSelected.png" : @"DAM_CheckBox.png";
    [button setImage:[UIImage imageNamed:stateImage] forState:UIControlStateNormal];
    
    [super setSelected:newState];
}

//- (void)checkboxTapped:(id)sender {
//    UIButton *button = sender;
//    button.tag ^= 1;
//    NSString *stateImage = button.tag ? @"DAM_CheckBoxSelected.png" : @"DAM_CheckBox.png";
//    [button setImage:[UIImage imageNamed:stateImage] forState:UIControlStateNormal];
//    
//    [self sendActionsForControlEvents:UIControlEventTouchDown];
//}

- (void)checkboxTapped:(id)sender {
    UIButton *button = sender;
    self.selected = !self.selected;
    NSString *stateImage = self.selected ? @"DAM_CheckBoxSelected.png" : @"DAM_CheckBox.png";
    [button setImage:[UIImage imageNamed:stateImage] forState:UIControlStateNormal];
    
    [self sendActionsForControlEvents:UIControlEventTouchDown];
}

@end
