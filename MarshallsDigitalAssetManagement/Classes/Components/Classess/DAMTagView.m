//
//  DAMTagView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMTagView.h"
#import "DAMApplicationModel.h"
//#import "JSONKit.h"
#import "NSString+Utilities.h"
#import "DAMUIDefaults.h"

@interface DAMTagView () {
    
    id<DAMTagViewDelegate> delegate;
    
    NSMutableArray *selectedTags;
    NSMutableArray *indexPositions;
    
    DAMIndexStripView *indexStrip;
    UIScrollView *scrollView;
}

- (CGPoint)pointForIndex:(int)index;

@end

@implementation DAMTagView
@synthesize delegate;
@synthesize selectedTags;
@synthesize indexStrip;
@synthesize scrollView;
@synthesize activityView;

const int indexSize = 26;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andMediaType: (NSString *) mediaType
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        indexPositions = [NSMutableArray arrayWithCapacity:indexSize];
        [indexPositions retain];
        for (int i = 0; i < indexSize; i++) {
            [indexPositions addObject:[NSNumber numberWithFloat:-1]];
        }
        selectedTags = [NSMutableArray arrayWithCapacity:20];
        [selectedTags retain];
        
        self.mediaType = mediaType;
        
//        NSArray *businessAreas = [NSArray arrayWithObjects:@"domestic", @"commercial", nil];
//        NSString *containers = [[[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas] componentsJoinedByString:@"|"];
//        NSString *url = [NSString stringWithFormat:@"/api/tags?containers=%@", [containers encodeURL]];
//        ASIHTTPRequest *request = [[DAMApplicationModel model] requestForPath:url withDelegate:self alwaysUseCache:NO];
//        NSLog(@"DAMTagView.initWithNibName url:%@", request.url);

        //[[DAMApplicationModel model] photoGetTags: self];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    indexStrip.numItems = indexSize;
    indexStrip.delegate = self;
    self.activityView.hidden = NO;
    self.scrollView.hidden = YES;
    [self performSelectorInBackground:@selector(loadTags:) withObject: nil];
}

- (void) loadTags:(id)sender {
    [[DAMApplicationModel model] photoGetTags: self mediaType: self.mediaType];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [selectedTags release];
    selectedTags = nil;
    self.indexStrip = nil;
    self.scrollView = nil;
    
    [indexPositions release];
    indexPositions = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[[DAMApplicationModel model] registerCommand:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[[DAMApplicationModel model] unregisterCommand:self];
}

- (void)selectTags:(NSArray*)tags {
    [selectedTags addObjectsFromArray:tags];
}

- (void)tagsAvailable:(NSArray*)dict{
    self.activityView.hidden = YES;
    self.scrollView.hidden = NO;
    NSArray *tags = [self modelFromTags:(NSArray *)dict];
    [self setupWithModel:tags];
    [self preselectTags:selectedTags];
}

//TODO - handle requestFailed in DAMApplicationModel somehow ?
/*
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSLog(@"DAMTagView.requestFailed request.url:%@", request.url);
}*/
/*
- (void)requestFinished:(ASIHTTPRequest *)request {
    NSArray *tags = [self modelFromRequest:request];
    [self setupWithModel:tags];
    [self preselectTags:selectedTags];
}*/

/*
- (NSArray*)modelFromRequest:(ASIHTTPRequest*)request {
    NSMutableArray *tags = [[request responseData] objectFromJSONData];
    return [self modelFromTags:tags];
}*/

- (NSArray*)modelFromTags:(NSArray*)tags {
    NSMutableArray *cleanTags = [NSMutableArray arrayWithCapacity:tags.count];
    for (NSString *tag in tags) {
        NSString *lowerCaseTag = [[tag lowercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        unichar firstChar = [lowerCaseTag characterAtIndex:0];
        if (firstChar >= 'a' && firstChar <= 'z') {
            [cleanTags addObject:lowerCaseTag];
        } else {
#ifdef DEBUG
            NSLog(@"DAMTagView.modelFromRequest.WARN_TAG_IGNORED tag:%@", tag);
#endif
        }
    }
    [cleanTags sortUsingSelector:@selector(caseInsensitiveCompare:)];
    return cleanTags;
}

- (IBAction)cancelTapped:(id)sender {
    [self animateOffStage];
    if (delegate) {
        [delegate tagViewDidCancel:self];
    }
}

- (IBAction)goTapped:(id)sender {
    [self animateOffStage];
    if (delegate) {
        [delegate tagViewDidFinish:self withTags:selectedTags];
    }    
}

- (void)indexStrip:(DAMIndexStripView *)pIndexStrip indexChangedTo:(int)index {
    NSLog(@"indexStrip:%@ indexChangedTo:%i", indexStrip, index);
    
    scrollView.contentOffset = [self pointForIndex:index];
}

- (NSArray*)testModel {
    int numTags = 200;
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:numTags];
    for (int i = 0; i < numTags; i++) {
        int asciiChar = 65 + (i % indexSize);
        NSString *testString = [NSString stringWithFormat:@"%c TEST TAG %3i", asciiChar, i];
        [result addObject:testString];
    }
    [result sortUsingSelector:@selector(caseInsensitiveCompare:)];
    return result;
}

- (void)setupWithModel:(NSArray*)tags {
    int numRows = 4;
    int numColumns = 6;
    
    int rowIndex = 0;
    int colIndex = 0;
    
    int numTags = tags.count;
    
    float labelWidth = 160;
    float blockHeight = scrollView.frame.size.height / numRows;
    //NSLog(@"scrollView.frame.size.height == %f", scrollView.frame.size.height);
    for (int i = 0; i < numTags; i++) {
        NSString *tagString = [tags objectAtIndex:i];
        
        float yShift = colIndex % 2 == 0 ? 0 : blockHeight / 2;
        
        float xPos = colIndex * (scrollView.frame.size.width / numColumns);
        float yPos = rowIndex * blockHeight + yShift;

        [self addTagToIndex:tagString forPos:xPos];
        //labelWidth = [tagString textWidthWithFont: [UIFont fontWithName:@"Helvetica" size:24]] + 50;
        labelWidth = [tagString textWidthWithFont: [DAMUIDefaults defaults].tagLabelFont] + 50;
        
        //NSLog(@"added tag == %@ ; xpos == %f ; ypos == %f ; labelWidth == %f ; blockHeight == %f", tagString, xPos, yPos, labelWidth, blockHeight);
        
        DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(xPos, yPos, labelWidth, 40)];
//        tagLabel.textAlignment = UITextAlignmentCenter;
//        tagLabel.text = tagString;
        tagLabel.delegate = self;
        tagLabel.label.text = tagString;
        
        
        //DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(isCol1 ? x1Pos : x2Pos, yPos, labelWidth, 40)];
        //        tagLabel.textAlignment = UITextAlignmentCenter;
        //        tagLabel.text = tagString;
        
        [scrollView addSubview:tagLabel];
        [tagLabel release];
        
        rowIndex++;
        if (rowIndex == numRows) {
            rowIndex = 0;
            colIndex++;
        }
    }
    float contentWidth = (numTags / (numRows * numColumns) + 1) * scrollView.frame.size.width;
    scrollView.contentSize = CGSizeMake(contentWidth, scrollView.frame.size.height);
}

- (void)addTagToIndex:(NSString*)tagString forPos:(float)pos {
    
    unichar firstChar = [tagString characterAtIndex:0];
    int indexPos = firstChar - 'a';
    
    if ([[indexPositions objectAtIndex:indexPos] floatValue] < 0) {
        [indexPositions insertObject:[NSNumber numberWithFloat:pos] atIndex:indexPos];
    }
}

- (CGPoint)pointForIndex:(int)index {
    NSNumber *pos = nil;
    int candidateIndex = index;
    while (!pos && candidateIndex < indexPositions.count) {
        if ([indexPositions objectAtIndex:candidateIndex]) {
            pos = [indexPositions objectAtIndex:candidateIndex];
        }
        candidateIndex++;
    }
    return pos ? CGPointMake([pos floatValue], 0) : CGPointMake(0, 0);
}

- (void)preselectTags:(NSArray*)selected {
    NSMutableArray *selectedLabels = [NSMutableArray arrayWithCapacity:selected.count];
    for (UIView *view in scrollView.subviews) {
        if ([view isKindOfClass:[DAMTagLabel class]]) {
            DAMTagLabel *tagLabel = (DAMTagLabel *)view;
            if ([selected indexOfObject:tagLabel.label.text] != NSNotFound) {
                [selectedLabels addObject:tagLabel];
            }
        } else {
            
#ifdef DEBUG
            NSLog(@"DAMTagView.preselectTags.UNEXPECTED_VIEW view:%@", view);
#endif
        }
    }
    [selectedLabels makeObjectsPerformSelector:@selector(setSelected)];
}

- (void)tagSelected:(NSString*)tag {
    [selectedTags addObject:tag];
}

- (void)tagDeselected:(NSString*)tag {
    [selectedTags removeObject:tag];
}

@end
