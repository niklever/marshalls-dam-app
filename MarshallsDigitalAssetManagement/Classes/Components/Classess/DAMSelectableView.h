//
//  DAMSelectableView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 28/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMSelectableView : UIView<UIGestureRecognizerDelegate>

@property (assign, setter = setSelecting:) BOOL isSelecting;
@property (nonatomic, retain) NSArray *selectedAssets;

- (void)sharedInit;

- (void)addAssetGestureRecognizersToView:(UIView*)view;

- (void)toggleSelection:(UIView*)view;

//Subviews MUST implement this method;
- (UIView*)viewContainingSelectableItems;

@end
