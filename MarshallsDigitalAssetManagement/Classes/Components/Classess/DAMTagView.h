//
//  DAMTagView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMModalViewBase.h"
#import "DAMIndexStripView.h"
//#import "ASIHTTPRequest.h"
#import "DAMTagLabel.h"

@class DAMTagView;

@protocol DAMTagViewDelegate <NSObject>

- (void)tagViewDidCancel:(DAMTagView*)tagView;
- (void)tagViewDidFinish:(DAMTagView*)tagView withTags:(NSArray*)tags;
- (void)offstageAnimationDidFinish;

@end

@interface DAMTagView : DAMModalViewBase<DAMIndexStripViewDelegate, DAMTagLabelDelegate>
@property (nonatomic, strong) NSString *mediaType;
@property (nonatomic, assign) id<DAMTagViewDelegate> delegate;

@property (nonatomic, readonly) NSArray *selectedTags;

@property (nonatomic, retain) IBOutlet DAMIndexStripView *indexStrip;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityView;
- (IBAction)cancelTapped:(id)sender;
- (IBAction)goTapped:(id)sender;

- (void)selectTags:(NSArray*)tags;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andMediaType: (NSString *) mediaType;

@end
