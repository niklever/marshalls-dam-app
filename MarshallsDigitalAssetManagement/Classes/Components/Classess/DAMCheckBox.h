//
//  DAMCheckBox.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMCheckBox : UIControl {
    int tag;
    UILabel *label;
}
@property int tag;
@property (nonatomic, retain) UILabel *label;
//@property(getter = isSelected, setter = setSelected:) BOOL selected;

@end
