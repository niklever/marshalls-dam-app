//
//  DAMSearchView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 11/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMSearchBarView.h"

@interface DAMSearchBarView ()

@end

@implementation DAMSearchBarView
@synthesize delegate;
@synthesize searchField;
@synthesize cancelButton;
@synthesize businessAreaButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.businessAreaButton.hidden = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.searchField = nil;
    self.cancelButton = nil;
    self.businessAreaButton = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (IBAction)cancelButtonTapped:(id)sender {
    if (delegate) {
        [delegate searchBarCancelTapped];
    }
}

- (IBAction)businessAreaButtonTapped:(id)sender {
    if (delegate) {
        [delegate searchBarBusinessAreaTapped];
    }
}

@end
