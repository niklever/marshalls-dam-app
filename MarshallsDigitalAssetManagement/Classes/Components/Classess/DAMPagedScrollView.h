//
//  DAMPagedScrollView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMSelectableView.h"

@class DAMPagedScrollView;

@protocol DAMPagedScrollViewDelegate <NSObject>

- (void)requestModelForPage:(int)pageNum onCarousel:(NSString*)carousel;
- (void)assetOpened:(NSDictionary*)asset atIndex:(int)index fromPagedScrollView:(DAMPagedScrollView*)pageScrollView; 

@end

@protocol DAMPagedScrollViewLayoutDelegate <NSObject>

- (void)layoutModel:(NSDictionary*)model onView:(DAMPagedScrollView*)view withPage:(int)page;

@end

@interface DAMPagedScrollView : DAMSelectableView<UIScrollViewDelegate, UIGestureRecognizerDelegate> {
    //NSDictionary *model;
    //NSString *mediaType;
    //id<DAMPagedScrollViewDelegate> delegate;
    //id<DAMPagedScrollViewLayoutDelegate> layoutDelegate;
    
    //identifier of carousel on which this view sits
    //NSString *carousel;
    //BOOL pageControlEnabled;

    //If the view is in selection mode, isSelecting will be YES
//    BOOL isSelecting;
//    NSArray *selectedAssets;

    //UIScrollView *scrollView;
    //UIImageView *pageControlBackground;
    
    //NSDictionary *loadedPages;
}
@property (nonatomic, retain) NSDictionary *model;
@property (nonatomic, retain) NSString *mediaType;

@property (nonatomic, assign) IBOutlet id<DAMPagedScrollViewDelegate> delegate;
@property (nonatomic, assign) id<DAMPagedScrollViewLayoutDelegate> layoutDelegate;

@property (nonatomic, retain) NSString *carousel;
@property BOOL pageControlEnabled;

//@property (assign, setter = setSelecting:) BOOL isSelecting;
//@property (nonatomic, retain) NSArray *selectedAssets;

@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIImageView *pageControlBackground;

@property (nonatomic, retain) NSDictionary *loadedPages;

- (void)clearContent;

- (void)setupWithModel:(NSDictionary*)model forPage:(int)page;

//- (void)addAssetGestureRecognizersToView:(UIView*)view;

@end
