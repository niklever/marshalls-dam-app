//
//  DAMEmailChooserView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMEmailChooserViewDelegate <NSObject>

- (void)contactChosen:(NSDictionary*)contactModel;

@end

@interface DAMEmailChooserView : UIViewController<UITableViewDataSource>

@property (nonatomic, assign) id<DAMEmailChooserViewDelegate> delegate;
@property (nonatomic, retain) IBOutlet UITableView *table;

@end
