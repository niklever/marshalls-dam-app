//
//  DAMNavigatorViewController.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMNavigatorViewController.h"
#import "AVCamViewController.h"
#import "DAMUIDefaults.h"
#import "DAMApplicationModel.h"
#import "DAMMAPViewController.h"
#import "DAMAppDelegate.h"

@interface NSArray(Extensions)
- (id)pop;
@end

@implementation NSArray(Extensions)
- (id)pop {
    id result = nil;
    if ([self isKindOfClass:[NSMutableArray class]] && self.count > 0) {
        NSMutableArray *mutableSelf = (NSMutableArray *)self;
        result = [mutableSelf objectAtIndex:0];
        [mutableSelf removeObjectAtIndex:0];
    }
    return result;
}
@end

const int kDefaultNavigationStackSize = 5;

@interface DAMNavigatorViewController ()

- (void)configureWithModel:(NSDictionary*)navigationModel;

@end

@implementation DAMNavigatorViewController
@synthesize delegate;
@synthesize viewMap;
@synthesize navigationStack;

@synthesize mainBar;
@synthesize customButtonView;
@synthesize searchBar;

@synthesize titleImage;
@synthesize titleLabel;

@synthesize leftButton;
@synthesize rightButton1;
@synthesize rightButton2;
@synthesize rightButton3;
@synthesize rightButton4;
@synthesize rightButton5;

@synthesize greenButton;
@synthesize animationTimer;

@synthesize rolloutView;
@synthesize rolloutContentBackground;
@synthesize rolloutContentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        navigationStack = [NSMutableArray arrayWithCapacity:kDefaultNavigationStackSize];
        [navigationStack retain];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = YES;
    self.titleLabel.font = [[DAMUIDefaults defaults] navigatorTitleFont];
    self.titleLabel.textColor = [UIColor whiteColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

    [viewMap release];
    viewMap = nil;
    
    [navigationStack release];
    navigationStack = nil;
    
    self.mainBar = nil;
    self.customButtonView = nil;
    self.searchBar = nil;
    
    self.titleImage = nil;
    self.titleLabel = nil;
    
    self.leftButton = nil;
    self.rightButton1 = nil;
    self.rightButton2 = nil;
    self.rightButton3 = nil;
    self.rightButton4 = nil;
    
    self.greenButton = nil;
    [animationTimer invalidate];
    self.animationTimer = nil;
    
    self.rolloutView = nil;
    self.rolloutContentBackground = nil;
    self.rolloutContentView = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [self animateRightButtons];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (void)setModel:(NSString*)modelResourceName {
#ifdef DEBUG
    NSLog(@"modelResourceName = %@", modelResourceName);
#endif
    
    NSString *path = [[NSBundle mainBundle] pathForResource:modelResourceName ofType:@"json"];

#ifdef DEBUG
    NSLog(@"path = %@", modelResourceName);
#endif
    //NSLog(@"path = %@", path);
    
    NSString *modelString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *parsedModel = [modelString objectFromJSONString];
#ifdef DEBUG
    //NSLog(@"parsedModel: %@", parsedModel);
#endif
    if (!parsedModel) {
#ifdef DEBUG
        NSLog(@"FATAL_NO_MODEL");
#endif
        exit(-1);
    }
    NSMutableDictionary *tmpMap = [NSMutableDictionary dictionaryWithCapacity:parsedModel.count];
    for (NSDictionary *view in parsedModel) {
        NSString *viewName = [view objectForKey:@"name"];
        NSLog(@"Added view, viewName:%@", viewName);
        [tmpMap setObject:view forKey:viewName];
    }
    viewMap = [tmpMap copy];
    [viewMap retain];
}

- (void)clearSearch{
    NSMutableDictionary *currentTopNavigation = [navigationStack lastObject];
    [currentTopNavigation removeObjectForKey:@"search"];
    [currentTopNavigation removeObjectForKey:@"searchDelegate"];
}

//TODO - animations of DAMNavigatorViewController

- (void)pushNavigation:(NSString*)navigationName {
//    NSMutableDictionary *navigationModel = [NSMutableDictionary dictionaryWithDictionary:[viewMap objectForKey:navigationName]];
    
    NSLog(@"DAMNavigatorViewController - navigationName = %@", navigationName);
    
    if ([navigationName isEqualToString:kMediaType_SalesPhotos] || [navigationName isEqualToString:kMediaType_Photos] || [navigationName isEqualToString:kMediaType_Samples] || [navigationName isEqualToString:kMediaType_Videos] || [navigationName isEqualToString:kMediaType_CaseStudies] || [navigationName isEqualToString:kMediaType_Brochures] || [navigationName isEqualToString:kMediaType_InThePress]) {
        self.searchBar.businessAreaButton.frame = CGRectMake(340, self.searchBar.businessAreaButton.frame.origin.y , self.searchBar.businessAreaButton.frame.size.width, self.searchBar.businessAreaButton.frame.size.height);
    }

    if ([viewMap objectForKey:navigationName]) {
        NSMutableDictionary *navigationModel = [NSMutableDictionary dictionaryWithDictionary:[viewMap objectForKey:navigationName]];
        if (searchBar && searchBar.searchField.delegate && searchBar.searchField.text) { //added searchBar.searchField.text
            NSMutableDictionary *currentTopNavigation = [navigationStack lastObject];
            [currentTopNavigation setObject:searchBar.searchField.text forKey:@"search"];
            [currentTopNavigation setObject:searchBar.searchField.delegate forKey:@"searchDelegate"];
            //NSLog(@"DAMNavigatorVC pushNavigation %@ %@", navigationName, searchBar.searchField.text);
        }
        
        [self clearCustomButtonView];
        
#ifdef DEBUG
        NSLog(@"navigationModel added == %@", navigationModel);
#endif
        
        [(NSMutableArray*)navigationStack addObject:navigationModel];
        if (navigationStack.count > 1) {
            [self expand];
        } else {
            [self compress];
        }
        [self configureWithModel:navigationModel];
    } else {
#ifdef DEBUG
        NSLog(@"DAMNavigatorViewController.pushNavigation.NAME_NOT_FOUND navigationName:%@", navigationName);
#endif
    }
//    if ([navigationName isEqualToString:kMediaType_Samples]) [self clearSearch];
}

- (void)popNavigation {
    //ensure at least one object remains on the stack
    
    if(_isNewCase){
        _isNewCase  =   NO;
        [self.view removeFromSuperview];
    }
    
    [self clearCustomButtonView];
    
    [self dismissRollout];
    
    //NSLog(@"navigationStack count = %d", navigationStack.count);
    
    //NSLog(@"navigationStack = %@", navigationStack);
    BOOL isUserGuide = NO;
    NSDictionary *lastNavObject = [navigationStack lastObject];
    if ([[lastNavObject objectForKey:@"name"] isEqualToString:@"HowToGuide-View"]) {
        isUserGuide = YES;
    }
#ifdef DEBUG
    NSLog(@"navigationStack before removed= %@", navigationStack);
#endif
    
    if (navigationStack.count > 1) {
        [(NSMutableArray*)navigationStack removeLastObject];
    }
#ifdef DEBUG
    NSLog(@"navigationStack after removed= %@", navigationStack);
#endif
    
    //NSLog(@"navigationStack after removed= %@", navigationStack);
    
    if (navigationStack.count == 1) {
        [self compress];
        [self clearCustomButtonView];
    }
    
    NSDictionary *newConfiguration = [navigationStack lastObject];
    
    
    
    [self configureWithModel:newConfiguration];
    
    
    
    NSString *searchString = [newConfiguration objectForKey:@"search"];
    NSString *name = [newConfiguration objectForKey:@"name"];
//    if ([name isEqualToString:kMediaType_Samples]) searchString = nil;
    //TODO - need to remember the search delegate
    if (searchString) {
        id searchDelegate = [newConfiguration objectForKey:@"searchDelegate"];
        if (searchDelegate) {
            [self displaySearchBarWithDelegate:searchDelegate];
            searchBar.searchField.text = searchString;
        } else {
#ifdef DEBUG
            NSLog(@"DAMNavigatorViewController.popNavigation.WARN_CANNOT_FIND_SEARCH_DELEGATE");
#endif
        }
    }
    
    if (name!=nil &&([name isEqualToString:@"Home"])){
        searchBar.view.hidden = YES;
    }

    NSLog(@"popNavigation %@", name);
    
    if (name!=nil && ([name isEqualToString:kMediaType_SalesPhotos] || [name isEqualToString:kMediaType_Photos] || [name isEqualToString:@"Global-Search"] || [name isEqualToString:kMediaType_Samples] || [name isEqualToString:kMediaType_Videos] || [name isEqualToString:kMediaType_CaseStudies] || [name isEqualToString:kMediaType_Brochures] || [name isEqualToString:kMediaType_InThePress])){
        NSLog(@"Hide cancelButton");
        self.searchBar.cancelButton.hidden = YES;
        //self.searchBar.businessAreaButton.hidden = YES;
        //if([name isEqualToString:@"Global-Search"]) {
        self.searchBar.businessAreaButton.hidden = NO;
        //need to update position again
        self.searchBar.businessAreaButton.frame = CGRectMake(340, self.searchBar.businessAreaButton.frame.origin.y , self.searchBar.businessAreaButton.frame.size.width, self.searchBar.businessAreaButton.frame.size.height);
        //}
    }else{
        self.searchBar.businessAreaButton.frame = CGRectMake(380, self.searchBar.businessAreaButton.frame.origin.y , self.searchBar.businessAreaButton.frame.size.width, self.searchBar.businessAreaButton.frame.size.height);
    }
    //enable button again
    leftButton.enabled = YES;
    
    /*iOS7 added*/
    lastNavObject = [navigationStack lastObject];
    if (![[lastNavObject objectForKey:@"showsSearchBar"] isEqual: [NSNull null]]) {
        if ([[lastNavObject objectForKey:@"showsSearchBar"] boolValue] == YES) {
            //post notification to show search bar
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"k_%@_showsSearchBar", [lastNavObject objectForKey:@"name"]] object: nil];
        }
    }
}

- (void)clearCustomButtonView {
    self.searchBar.searchField.delegate = nil;
    [customButtonView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.searchBar = nil;
}

- (UIButton *)rightButtonFromIndex:(int)index {
    UIButton *result = nil;
    //TODO clunky - use a container
    switch (index) {
        case 0:
            result = rightButton1;
        break;
        case 1:
            result = rightButton2;
        break;
        case 2:
            result = rightButton3;
        break;
        case 3:
            result = rightButton4;
        break;
    }
    return result;
}
const float rolloutButtonWidth = 90;
const float rolloutButtonHeight = 35;
const float rolloutButtonPadding = 10;
const float rolloutCornerWidth = 9;
const float rolloutAnimationDuration = 0.5;
const float rightButtonWidth = 54;


- (void)addRolloutButtons:(NSArray *)buttonSpecs forRightButtonIndex:(int)index withTarget:(id)target {
#ifdef DEBUG
    NSLog(@"DAMNavigatorViewController.addRolloutButtons");
#endif
    NSMutableArray *mutableSpec = [NSMutableArray arrayWithArray:buttonSpecs];

    UIButton *replacedButton = [self rightButtonFromIndex:index];

    float rolloutContentWidth = buttonSpecs.count / 2 * (rolloutButtonWidth + rolloutButtonPadding) + rolloutButtonPadding;
    float xPos = rolloutContentWidth - rolloutButtonPadding - rolloutButtonWidth;
    float yPos = (self.view.frame.size.height - rolloutButtonHeight) / 2;

    CGRect frame = rolloutContentView.frame;
    frame.size.width = rolloutContentWidth;
    rolloutContentView.frame = frame;
    rolloutContentBackground.frame = frame;
#ifdef DEBUG
    NSLog(@"rolloutContentView.frame: (%f,%f,%f,%f)", frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
#endif
    frame = rolloutView.frame;
    frame.size.width = rolloutCornerWidth + rolloutContentWidth;
    frame.origin.x = replacedButton.frame.origin.x + rightButtonWidth - frame.size.width;
    rolloutView.frame = frame;
    rolloutView.transform = CGAffineTransformMakeTranslation(1024, 0);
#ifdef DEBUG
    NSLog(@"rolloutView.frame: (%f,%f,%f,%f)", frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
#endif
    [rolloutView removeFromSuperview];
    [self.view insertSubview:rolloutView atIndex:2];

    rolloutTriggerButton = replacedButton;
    
    [rolloutContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    /*[rolloutContentView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [(UIView *)obj removeFromSuperview];
    }];*/
    
    while (mutableSpec.count > 0) {
        NSString *icon = [mutableSpec pop];
        SEL selector = NSSelectorFromString([mutableSpec pop]);

        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(xPos, yPos, rolloutButtonWidth, rolloutButtonHeight)];
        [cancelButton setBackgroundImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
        [cancelButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        [rolloutContentView addSubview:cancelButton];

        xPos -= rolloutButtonWidth + rolloutButtonPadding;
    }
    
    replacedButton.userInteractionEnabled = NO;
    
    [UIView beginAnimations:@"rollout" context:nil];
    [UIView setAnimationDuration:rolloutAnimationDuration];
    
    rolloutView.transform = CGAffineTransformIdentity;
    replacedButton.alpha = 0.0f;

    [UIView commitAnimations];
}

- (void)dismissRollout {
#ifdef DEBUG
    NSLog(@"DAMNavigatorViewController.dismissRollout");
#endif
    [UIView beginAnimations:@"rollout" context:nil];
    [UIView setAnimationDuration:rolloutAnimationDuration];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(rolloutDidStop)];

    rolloutView.transform = CGAffineTransformMakeTranslation(1024, 0);
    rolloutTriggerButton.alpha = 1.0f;
    

    [UIView commitAnimations];
}

- (void)rolloutDidStop {
    rolloutTriggerButton.userInteractionEnabled = YES;
    [rolloutView removeFromSuperview];
    rolloutView.transform = CGAffineTransformIdentity;
}

//TODO Animate the search bar into view
- (DAMSearchBarView*)displaySearchBarWithDelegate:(id<UITextFieldDelegate,DAMSearchBarViewDelegate>)aDelegate {
    //[self clearCustomButtonView];
    if (self.searchBar != nil) {
        return self.searchBar;
    }
    
    self.searchBar = [[DAMSearchBarView alloc] initWithNibName:@"DAMSearchBarView" bundle:nil];
    self.searchBar.view.transform = CGAffineTransformMakeTranslation(0, -searchBar.view.frame.size.height);
    [customButtonView addSubview: self.searchBar.view];
    [searchBar release];
    
    self.searchBar.searchField.delegate = aDelegate;
    self.searchBar.delegate = aDelegate;

    [UIView beginAnimations:@"searchBarBecameVisible" context:nil];
    [UIView setAnimationDuration:0.5];
    searchBar.view.transform = CGAffineTransformMakeTranslation(0, (self.view.frame.size.height - searchBar.view.frame.size.height) / 2);
    [UIView commitAnimations];
    
    return searchBar;
}

- (NSString*)searchText {
    return searchBar.searchField.text;
}

- (void)displayTags:(NSArray*)tags withDelegate:(id<DAMTagLabelDelegate>)aDelegate {
    [self clearCustomButtonView];
    
    float xPos = 0;
    float tagWidth = 60;
    float tagPadding = 10;
    float tagHeight = 40;
    float yPos = (self.view.frame.size.height - tagHeight) / 2;
    for (NSString *tag in tags) {
        DAMTagLabel *tagLabel = [[DAMTagLabel alloc] initWithFrame:CGRectMake(xPos, yPos, tagWidth, tagHeight)];
        tagLabel.label.text = tag;
        [tagLabel setSelected];
        tagLabel.delegate = aDelegate;
        [tagLabel layout];
        [customButtonView addSubview:tagLabel];
        [tagLabel release];
        xPos += tagWidth + tagPadding;
    }
    [self layoutCustomButtons];
}

- (void)removeTag:(NSString*)tag {
    DAMTagLabel *label = nil;
    int index = 0;
    while (label == nil && index < customButtonView.subviews.count) {
        DAMTagLabel *candidate = (DAMTagLabel *)[customButtonView.subviews objectAtIndex:index];
        if ([candidate.label.text compare:tag] == NSOrderedSame) {
            label = candidate;
        }
        index++;
    }
    if (label) {
        [label removeFromSuperview];
        [self layoutCustomButtons];
    }
}

- (void)layoutCustomButtons {
    float padding = 10;
    float xPos = 0;
    
    for (UIView *view in customButtonView.subviews) {
        CGRect viewFrame = view.frame;
        viewFrame.origin.x = xPos;
        view.frame = viewFrame;
        xPos += padding + viewFrame.size.width;
    }
}

- (void)layoutCustomButtonsRightToLeft {
    float padding = 10;
    float xPos = customButtonView.frame.origin.x + customButtonView.frame.size.width;
    
    for (UIView *view in customButtonView.subviews) {
        CGRect viewFrame = view.frame;
        xPos -= padding + viewFrame.size.width;
        viewFrame.origin.x = xPos;
        view.frame = viewFrame;
    }
}


- (void)setTitleImageFile:(NSString*)fileName {
    titleImage.image = [UIImage imageNamed:fileName];
}

- (void)setTitleImageForView:(NSString*)viewName {
    NSDictionary *dict = [viewMap objectForKey:viewName];
    if (dict) {
        NSString *titleImageFile = [dict objectForKey:@"titleImage"];
        titleImage.image = [UIImage imageNamed:titleImageFile];
    } else {
        NSLog(@"DAMNavigatorViewController.setTitleImageOfView.ERROR_BAD_VIEW_NAME viewName:%@", viewName);
    }
}

- (void)configureWithModel:(NSDictionary*)navigationModel {
    NSLog(@"configureWithModel = %@", navigationModel);

    NSString *name = [navigationModel objectForKey:@"name"];
    
    NSLog(@"DAMNavigatorViewController.configureWithModel navigationModel.name:%@", name);
    
    NSString *title = [navigationModel objectForKey:@"title"];
    titleLabel.text = title;
    
    NSString *titleImageFile = [navigationModel objectForKey:@"titleImage"];
    titleImage.image = [UIImage imageNamed:titleImageFile];
        
    [self setLeftButtonImage:[navigationModel objectForKey:@"leftButton"]];
    NSArray *rightButtons = [navigationModel objectForKey:@"rightButtons"];
    [self setRightButtonImages:rightButtons];
}

//Ensure compress is called after setRightButtons, so that the view knows
//the correct sizing of the compressed view
- (void)compress {
#ifdef DEBUG
    NSLog(@"DAMNavigatorViewController.compress");
#endif
    leftButton.enabled = NO;
    
    int numVisibleButtons = ((NSArray*)[[navigationStack lastObject] objectForKey:@"rightButtons"]).count;
    float leftPos = mainBar.frame.size.width - numVisibleButtons * rightButtonWidth;

    [UIView beginAnimations:@"compress" context:nil];
    [UIView setAnimationDuration:0.5];

    mainBar.transform = CGAffineTransformMakeTranslation(leftPos, 0);
    leftButton.alpha = 0;
    titleLabel.alpha = 0;
    titleImage.alpha = 0;

    [UIView commitAnimations];
    
    //TODO compression position/sizing
}

- (void)expand {
#ifdef DEBUG
    NSLog(@"DAMNavigatorViewController.expand");
#endif
    leftButton.enabled = YES;
//    titleLabel.hidden = NO;
//    titleImage.hidden = NO;

//    return;
    
    [UIView beginAnimations:@"expand" context:nil];
    [UIView setAnimationDuration:0.5];

    mainBar.transform = CGAffineTransformIdentity;
    leftButton.alpha = 1;
    titleLabel.alpha = 1;
    titleImage.alpha = 1;

    [UIView commitAnimations];
}

- (void)setLeftButtonImage:(NSString*)imageName {
    [leftButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}

- (void)setRightButtonImages:(NSArray *)buttonImages {
    NSArray *buttons = [NSArray arrayWithObjects:rightButton1,
        rightButton2, rightButton3, rightButton4, rightButton5,
    nil];
    
    int index = 0;
    [self.greenButton removeFromSuperview];
    self.greenButton = nil;
    iDriveButton = nil;
    [animationTimer invalidate];
    self.animationTimer = nil;
    
    for (NSString *buttonImage in buttonImages) {
        NSLog(@"DAMNavigatorViewController.setRightButtonImages buttonImage:%@", buttonImage);

        if (index < buttons.count) {
            UIButton *button = [buttons objectAtIndex:index];
            UIImage *image = [UIImage imageNamed:buttonImage];
            [button setBackgroundImage:image forState:UIControlStateNormal];
            button.hidden = NO;
            
            //TODO - massive hack, time is tight - should specify in config JSON
            if ([buttonImage compare:@"nav-idrive.png"] == NSOrderedSame) {
                iDriveButton = button;
                [self animateIDriveButton];
            }
        } else {
            NSLog(@"IGNORING_EXTRA_BUTTON buttonImage:%@", buttonImage);
        }
        index++;
    }
    //Hide any unused buttons
    for (int unusedIndex = index; unusedIndex < buttons.count; unusedIndex++) {
        UIButton *button = [buttons objectAtIndex:unusedIndex];
        button.hidden = YES;
    }
//    [self animateRightButtons];
    //[self compress];
}

- (void)animateRightButtons {
    [UIView beginAnimations:@"animateRightButtons" context:nil];
    [UIView setAnimationDuration:0.5];
    
    NSArray *buttons = [NSArray arrayWithObjects:rightButton1,
                        rightButton2, rightButton3, rightButton4, rightButton5,
                        nil];
    
    //setup button on-screen positions
    
//    int index = 1;
    float buttonX = 1024;
    for (UIButton *button in buttons) {
        CGRect buttonFrame = button.frame;
        buttonX -= buttonFrame.size.width;
        buttonFrame.origin.x = buttonX;
        button.frame = buttonFrame;
    }
    
    [UIView commitAnimations];
}

- (void)animateIDriveButton {
    if (iDriveButton) {
        //[[DAMApplicationModel model] reportCMSDeletionsToPlugin:@"DAMNavigatorViewController" andMethod:@"deletionsDetected"];
        [[DAMApplicationModel model] reportCMSDeletionsToPlugin: self andMethod:@selector(deletionsDetected:)];
    }
}

- (void)deletionsDetected:(NSMutableArray*)deletions {
    
    if (deletions.count > 0) {
        //if model has deleted assets
        iDriveAnimationState = 0;
        CGRect frame = CGRectMake(iDriveButton.frame.origin.x, iDriveButton.frame.origin.y, iDriveButton.frame.size.width, iDriveButton.frame.size.height);
        self.greenButton = [[UIButton alloc] initWithFrame:frame];
        [greenButton setBackgroundImage:[UIImage imageNamed:@"nav-idrive-notification.png"] forState:UIControlStateNormal];
        greenButton.alpha = 0;
        greenButton.tag = iDriveButton.tag;
        [greenButton addTarget:self action:@selector(rightButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:greenButton];
        [greenButton release];
        
        self.animationTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    } else {
        [self clearIDriveAnimation];
    }
}

- (void)clearIDriveAnimation {
    [animationTimer invalidate];
    self.animationTimer = nil;
    [greenButton removeFromSuperview];
    self.greenButton = nil;
    iDriveButton.alpha = 1;
}

- (void)timerFired:(id)sender {
    [UIView beginAnimations:@"iDrivePulseAnimation" context:nil];
    
//    [UIView setAnimationDelay:1.0];
    [UIView setAnimationDuration:1.0];
    
    if (iDriveAnimationState == 0) {
        iDriveButton.alpha = 0;
        greenButton.alpha = 1;
    } else {
        iDriveButton.alpha = 1;
        greenButton.alpha = 0;
    }
    iDriveAnimationState = (iDriveAnimationState + 1) % 2;
    [UIView commitAnimations];
}

- (IBAction)leftButtonTapped:(id)sender {
#ifdef DEBUG
    NSLog(@"DAMNavigatorViewController.leftButtonTapped");
#endif
    [JSMain sharedInstance].opsSuspended = FALSE;
    
    leftButton.enabled = NO;
    BOOL shouldPopView = FALSE;

    if (delegate && [delegate respondsToSelector:@selector(leftButtonTapped:)]) {
        shouldPopView = [delegate leftButtonTapped:self];
    }
    if (shouldPopView) {
        [self popNavigation];
    }
}

- (IBAction)rightButtonTapped:(id)sender {
#ifdef DEBUG
    NSLog(@"DAMNavigatorViewController.rightButtonTapped");
#endif
    UIButton *button = sender;
    int index = button.tag;

    NSString *nextViewName = nil;
    NSString *currentViewName = @"";
    
    NSDictionary *topView = [navigationStack lastObject];
    NSArray *actionArray = [topView objectForKey:@"rightButtonActions"];
    if (actionArray) {
        if (index >= 0 && index < actionArray.count) {
            nextViewName = [actionArray objectAtIndex:index];
        }
    }
    
    NSLog(@"DAMNavigatorViewController.rightButtonTapped delegate:%@, currentViewName:%@, nextViewName:%@", delegate, currentViewName, nextViewName);
    
    //FIX CAMERA AND LOCATION
    if ([nextViewName isEqualToString: @"SalesPhotos_LaunchCameraView"]) {
        AVCamViewController *controller =   [[AVCamViewController alloc] initWithNibName:@"AVCamViewController" bundle:nil];
        DAMAppDelegate *appDelegate =   (DAMAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate.navigationController pushViewController:controller animated:YES];
        [controller release];
    }else if ([nextViewName isEqualToString: @"SalesPhotos_LaunchGPSLocationsView"]) {
        
        /*
        DAMMAPViewController *controller    =   [[DAMMAPViewController alloc] initWithNibName:@"DAMMAPViewController" bundle:nil];
        
        DAMAppDelegate *appDelegate =   (DAMAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate.navigationController pushViewController:controller animated:YES];
        [controller release];
        */
        
        if (delegate) {
            UIButton *button = sender;
            [delegate rightButtonTapped:self withIndex:button.tag];
        } else {
#ifdef DEBUG
            NSLog(@"DAMNavigatorViewController.rightButtonTapped.DELEGATE_DOESNT_RESPOND delegate:%@", delegate);
#endif
        }
        
        
    }else{
        [self pushNavigation:nextViewName];
        //TODO why does this not get called for sub-classes like DAMBrochureView (respondsToSelector fails)??
        //    if (delegate && [delegate respondsToSelector:@selector(rightButtonTapped::)]) {
        if (delegate) {
            UIButton *button = sender;
            [delegate rightButtonTapped:self withIndex:button.tag];
        } else {
#ifdef DEBUG
            NSLog(@"DAMNavigatorViewController.rightButtonTapped.DELEGATE_DOESNT_RESPOND delegate:%@", delegate);
#endif
        }
    }
}


- (void)dealloc {
    [rightButton5 release];
    [super dealloc];
}
@end
