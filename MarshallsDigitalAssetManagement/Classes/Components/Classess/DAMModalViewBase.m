//
//  DAMModalViewBase.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 02/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMModalViewBase.h"
#import "DAMUIDefaults.h"

@interface DAMModalViewBase () {
 
    UILabel *modalTitle;
    UIView *overlayView;
    UIView *contentPanel;
}

@end

@implementation DAMModalViewBase
@synthesize modalTitle;
@synthesize overlayView;
@synthesize contentPanel;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    modalTitle.font = [[DAMUIDefaults defaults] modalTitleFont];
    overlayView.alpha = 0;
    contentPanel.transform = CGAffineTransformMakeTranslation(0, contentPanel.frame.size.height);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.

    self.modalTitle = nil;
    self.overlayView = nil;
    self.contentPanel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (void)animateOnStage {
    [UIView beginAnimations:@"animateOnStage" context:nil];
    [UIView setAnimationDuration:0.5];
    overlayView.alpha = 1;
    contentPanel.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}

- (void)animateOffStage {
    [UIView beginAnimations:@"animateOnStage" context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    SEL selector = @selector(animateOffStageDidStop:finished:context:);
    [UIView setAnimationDidStopSelector:selector];
    overlayView.alpha = 0;
    contentPanel.transform = CGAffineTransformMakeTranslation(0, contentPanel.frame.size.height);;
    [UIView commitAnimations];
}

- (void)animateOffStage:(void (^)(void))finishBlock{
    [UIView animateWithDuration:0.5
                     animations:^{
                         overlayView.alpha = 0;
                         contentPanel.transform = CGAffineTransformMakeTranslation(0, contentPanel.frame.size.height);
                     } completion:^(BOOL finished) {
                         if(finishBlock){
                             finishBlock();
                         }
                     }];
}


- (void)animateOffStageDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context {
    id delegate = [self performSelector:@selector(delegate)];
    if (delegate && [delegate respondsToSelector:@selector(modalViewOffstageAnimationDidFinish:)]) {
        [delegate performSelector:@selector(modalViewOffstageAnimationDidFinish:) withObject:self];
    }
}

@end
