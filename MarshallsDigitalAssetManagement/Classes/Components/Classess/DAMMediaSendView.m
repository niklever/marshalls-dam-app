//
//  DAMMediaSendView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 11/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMMediaSendView.h"
#import "DAMUIDefaults.h"
#import "JSMain.h"

@interface DAMMediaSendView () {

    UIButton *emailChoiceButton;
    UIPopoverController *emailChoicePopover;
}
@property (nonatomic, retain) UIPopoverController *emailChoicePopover;

@end

@implementation DAMMediaSendView
@synthesize delegate;
@synthesize emailAddress;
@synthesize subject;
@synthesize message;
@synthesize sendButton;
@synthesize emailChoiceButton;
@synthesize emailChoicePopover;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    emailAddress.font = [[DAMUIDefaults defaults] mediaSendFont];
    subject.font = [[DAMUIDefaults defaults] mediaSendFont];
    message.font = [[DAMUIDefaults defaults] mediaSendFont];
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

    self.emailAddress = nil;
    self.subject = nil;
    self.message = nil;
    self.sendButton = nil;
    self.emailChoiceButton = nil;
    self.emailChoicePopover = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [UIView beginAnimations:@"shouldChangeCharactersInRange" context:nil];
    [UIView setAnimationDuration:[[DAMUIDefaults defaults] textPlaceholderFadeTime]];
    if (newText.length > 0) {
        textField.backgroundColor = [UIColor whiteColor];
    } else {
        textField.backgroundColor = [UIColor clearColor];
    }
    [UIView commitAnimations];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    [UIView beginAnimations:@"shouldChangeCharactersInRange" context:nil];
    [UIView setAnimationDuration:[[DAMUIDefaults defaults] textPlaceholderFadeTime]];
    if (newText.length > 0) {
        textView.backgroundColor = [UIColor whiteColor];
    } else {
        textView.backgroundColor = [UIColor clearColor];
    }
    [UIView commitAnimations];
    return YES;
}

- (IBAction)sendTapped:(id)sender {
    if (delegate) {
        if([delegate respondsToSelector:@selector(sendMediaTapped:)]){
            [self animateOffStage:^{
                [delegate sendMediaTapped:self];
            }];
        }
        else{
            [self animateOffStage];
            [delegate sendMediaTapped];
        }
    }
}

- (IBAction)cancelTapped:(id)sender {
    if (delegate) {
        if([delegate respondsToSelector:@selector(cancelMediaSendTapped:)]){
            [self animateOffStage:^{
                [delegate cancelMediaSendTapped:self];
            }];
            
        }
        else{
            [self animateOffStage];
            [delegate cancelMediaSendTapped];
        }
    }
}

- (IBAction)clearEmailAdressTapped:(id)sender {
    emailAddress.text = @"";
}

- (IBAction)clearSubjectTapped:(id)sender {
    subject.text = @"";
}

- (IBAction)clearMessageTapped:(id)sender {
    message.text = @"";
}

- (IBAction)emailChoiceTapped:(id)sender {
    DAMEmailChooserView *chooserView = [[DAMEmailChooserView alloc] initWithNibName:@"DAMEmailChooserView" bundle:nil];
    chooserView.delegate = self;
    self.emailChoicePopover = [[UIPopoverController alloc] initWithContentViewController:chooserView];
    emailChoicePopover.delegate = self;
    CGRect presentRect = CGRectMake(0, emailChoiceButton.frame.size.height / 2, 0, 0);
    [emailChoicePopover presentPopoverFromRect:presentRect inView:emailChoiceButton permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    [chooserView release];
    [emailChoicePopover release];
}

- (void)contactChosen:(NSDictionary*)contactModel {
    NSString *email = [contactModel objectForKey:@"email"];
    emailAddress.text = email;
    [emailChoicePopover dismissPopoverAnimated:YES];
    [UIView beginAnimations:@"contactChosen" context:nil];
    [UIView setAnimationDuration:[[DAMUIDefaults defaults] textPlaceholderFadeTime]];
    emailAddress.backgroundColor = [UIColor whiteColor];
    [UIView commitAnimations];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.emailChoicePopover = nil;
}

@end
