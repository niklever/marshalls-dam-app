//
//  DAMBusinessAreaFiltersView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 1/14/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "DAMModalViewBase.h"
#import "DAMTagLabel.h"

@protocol DAMBusinessAreaFiltersViewDelegate;

@interface DAMBusinessAreaFiltersView : DAMModalViewBase<DAMTagLabelDelegate>

@property (nonatomic, assign) id<DAMBusinessAreaFiltersViewDelegate> delegate;

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *commercialLabel;
@property (nonatomic, retain) IBOutlet UILabel *landscapesLabel;
@property (nonatomic, retain) IBOutlet UIView *containerPlacerView;

@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *okButton;
@property (nonatomic, retain)NSMutableArray *selectedContainers;
@property (nonatomic, retain) IBOutlet UIScrollView *gridContents;
@property (nonatomic, retain) IBOutlet UIImageView *devider1;
@property (nonatomic, retain) IBOutlet UIImageView *devider2;
@property (nonatomic, retain) IBOutlet UIImageView *deviderGroup;


- (void) removeSelectedTag:(NSString *)tagString;
- (void) selectTagsFollowSelecteTags:(NSMutableArray *)arrTags;

- (IBAction)cancelTapped:(id)sender;
- (IBAction)okTapped:(id)sender;


@end

@protocol DAMBusinessAreaFiltersViewDelegate <NSObject>

- (void)businessAreaFiltersViewDidCancel;
- (void)businessAreaFiltersViewDidOK;

- (void) businessAreaFiltersViewDidCancel:(DAMBusinessAreaFiltersView *)businessAreaView;
- (void)businessAreaFiltersViewDidOK:(DAMBusinessAreaFiltersView *)businessAreaView;

@end