//
//  DAMNearbyLocationsView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 29/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMNearbyLocationsViewDelegate

- (void)nearbyCaseStudyTapped:(NSDictionary*)caseStudy;
- (void)nearbyLocationsCancelled;

@end

@interface DAMNearbyLocationsView : UIView<UITableViewDataSource, UITableViewDelegate, UITabBarControllerDelegate>

@property(nonatomic, assign) id<DAMNearbyLocationsViewDelegate> delegate;

- (void)setNearbyLocations:(NSArray*)locations;

@end
