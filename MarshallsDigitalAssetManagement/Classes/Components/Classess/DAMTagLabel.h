//
//  DAMTagLabel.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 04/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMTagLabelDelegate;

@interface DAMTagLabel : UIView

@property (nonatomic, assign) id<DAMTagLabelDelegate> delegate;
@property (nonatomic, retain) UILabel *label;
@property (nonatomic, assign) BOOL disableTapOnLabel;

- (void)setSelected;
- (BOOL)isSelected;
- (void)setUnSelected;
- (void)layout;

@end

@protocol DAMTagLabelDelegate <NSObject>

- (void)tagSelected:(NSString*)tag;
- (void)tagDeselected:(NSString*)tag;

- (void) removeTags:(DAMTagLabel *)tag;

@end