//
//  DAMNearbyLocationsView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 29/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DAMNearbyLocationsView.h"

@interface DAMNearbyLocationsView() {
    
    id<DAMNearbyLocationsViewDelegate> delegate;
    NSArray *nearbyLocs;
    
    UITableView *table;
}
@property (nonatomic, retain) NSArray *nearbyLocs;
@property (nonatomic, retain) UITableView *table;
@end

@implementation DAMNearbyLocationsView
@synthesize nearbyLocs;
@synthesize table;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createRoundedCorner];
    }
    return self;
}

- (void)dealloc {
    self.nearbyLocs = nil;
    self.table = nil;
    [super dealloc];
}

- (void)setNearbyLocations:(NSArray*)locations {
    self.nearbyLocs = locations;
    
    float labelHeight = 40;
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, labelHeight)];
    header.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.8];
    header.textColor = [UIColor whiteColor];
    header.text = @"Nearby locations";
    header.textAlignment = UITextAlignmentCenter;
    [self addSubview:header];
    [header release];
    self.table = [[UITableView alloc] initWithFrame:CGRectMake(0, labelHeight, self.frame.size.width, self.frame.size.height - labelHeight) style:UITableViewStylePlain];
    table.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    table.dataSource = self;
    table.delegate = self;
    [self addSubview:table];
    [table release];
    
    CGRect cancelButtonFrame = CGRectMake(self.frame.size.width - 30, 0, 30, 30);
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:cancelButtonFrame];
    cancelButton.transform = CGAffineTransformMakeRotation(M_PI_4);
    [cancelButton setImage:[UIImage imageNamed:@"email-add.png"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelButton];
    [cancelButton release];
}

- (void)createRoundedCorner {
    float cornerRadius = 8;
    // Create the path (with only the top-left corner rounded)
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds 
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the image view's layer
    self.layer.mask = maskLayer;      
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return nearbyLocs ? nearbyLocs.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *location = [nearbyLocs objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nearbyLocations"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"nearbyLocations"];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.textColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0];
    cell.textLabel.text = [location objectForKey:@"title"];
    cell.detailTextLabel.text = [location objectForKey:@"location"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (delegate) {
        NSDictionary *caseStudy = [nearbyLocs objectAtIndex:indexPath.row];
        [delegate nearbyCaseStudyTapped:caseStudy];
    }
}

- (void)cancelButtonTapped:(id)sender {
    if (delegate) {
        [delegate nearbyLocationsCancelled];
    }
}

@end
