//
//  DAMModalViewBase.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 02/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMModalViewBase : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *modalTitle;
@property (nonatomic, retain) IBOutlet UIView *overlayView;
@property (nonatomic, retain) IBOutlet UIView *contentPanel;

- (void)animateOnStage;
- (void)animateOffStage;
- (void)animateOffStage:(void (^)(void))finishBlock;

@end
