//
//  DAMMediaChangeView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 28/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMModalViewBase.h"

@protocol DAMMediaChangeViewDelegate <NSObject>

- (void)mediaChangeViewDidCancel;
- (void)mediaChangeViewDidOK;

@end

@interface DAMMediaChangeView : DAMModalViewBase<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) id<DAMMediaChangeViewDelegate> delegate;
@property (nonatomic, retain) NSArray *model;

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UITableView *table;

@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *okButton;

- (IBAction)cancelTapped:(id)sender;
- (IBAction)okTapped:(id)sender;

@end
