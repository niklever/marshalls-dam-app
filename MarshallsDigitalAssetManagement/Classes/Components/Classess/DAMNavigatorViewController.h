//
//  DAMNavigatorViewController.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMSearchBarView.h"
#import "DAMTagLabel.h"

//#define navigationOptionCancel      0;
//#define navigationOptionHome        1;
//#define navigationOptionIDrive      2;
//#define navigationOptionSearch      3;
//#define navigationOptionInfo        4;
//#define navigationOptionSettings    5;
//#define navigationOptionShare       6;

@protocol DAMNavigatorDelegate <NSObject>

- (BOOL)leftButtonTapped:(id)sender;
- (void)rightButtonTapped:(id)sender withIndex:(int)index;

@end

@interface DAMNavigatorViewController : UIViewController {
    
    id<DAMNavigatorDelegate> delegate;

    NSDictionary *viewMap;
    
    //Stack of NSDictionary objects, containing keys:
    //leftButton: imageFilename
    //right
    NSArray *navigationStack;
    
    UIView *mainBar;
    UIView *customButtonView;
    DAMSearchBarView *searchBar;
    
    UIImageView *titleImage;
    UILabel *titleLabel;

    UIButton *leftButton;
    
    UIButton *rightButton1;
    UIButton *rightButton2;
    UIButton *rightButton3;
    UIButton *rightButton4;
    UIButton *rightButton5;

    UIButton *rolloutTriggerButton;
    
    UIButton *iDriveButton;
    UIButton *greenButton;
    NSTimer *iDriveNotificationTimer;
    int iDriveAnimationState;
    NSTimer *animationTimer;
    
    UIView *rolloutView;
    UIImageView *rolloutContentBackground;
    UIView *rolloutContentView;
}
@property (nonatomic, assign) id<DAMNavigatorDelegate> delegate;

@property (nonatomic, readonly) NSDictionary *viewMap;

@property (nonatomic, readonly) NSArray *navigationStack;

@property (nonatomic, retain) IBOutlet UIView *mainBar;
@property (nonatomic, retain) IBOutlet UIView *customButtonView;
@property (nonatomic, retain) DAMSearchBarView *searchBar;

@property (nonatomic, retain) IBOutlet UIImageView *titleImage;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, retain) IBOutlet UIButton *leftButton;

@property (nonatomic, retain) IBOutlet UIButton *rightButton1;
@property (nonatomic, retain) IBOutlet UIButton *rightButton2;
@property (nonatomic, retain) IBOutlet UIButton *rightButton3;
@property (nonatomic, retain) IBOutlet UIButton *rightButton4;
@property (retain, nonatomic) IBOutlet UIButton *rightButton5;

@property (nonatomic, retain) UIButton *greenButton;
@property (nonatomic, retain) NSTimer *animationTimer;

@property (nonatomic, retain) IBOutlet UIView *rolloutView;
@property (nonatomic, retain) IBOutlet UIImageView *rolloutContentBackground;
@property (nonatomic, retain) IBOutlet UIView *rolloutContentView;

@property (nonatomic, assign) BOOL isNewCase;

- (void)setModel:(NSString*)modelResourceName;

- (void)pushNavigation:(NSString*)navigationName;
- (void)popNavigation;

//TODO - should clearCustomButtonView should be deprecated and removed?
- (void)clearCustomButtonView;
- (void)layoutCustomButtons;
- (void)layoutCustomButtonsRightToLeft;

- (void)addRolloutButtons:(NSArray *)buttonSpecs forRightButtonIndex:(int)index withTarget:(id)target;
- (void)dismissRollout;

- (DAMSearchBarView*)displaySearchBarWithDelegate:(id<UITextFieldDelegate>)delegate;
- (void)displayTags:(NSArray*)tags withDelegate:(id<DAMTagLabelDelegate>)aDelegate;
- (void)removeTag:(NSString*)tag;
- (NSString*)searchText;
- (void)setTitleImageForView:(NSString*)viewName;
- (void)setTitleImageFile:(NSString*)fileName;

- (void)compress;
- (void)expand;
- (void)setLeftButtonImage:(NSString*)imageName;
- (void)setRightButtonImages:(NSArray *)buttonImages;

- (IBAction)leftButtonTapped:(id)sender;
- (IBAction)rightButtonTapped:(id)sender;

- (void)clearIDriveAnimation;
- (void)clearSearch;

@end
