//
//  DAMMediaSaveView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 10/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMIDriveNavigatorEmbedded.h"
#import "DAMModalViewBase.h"

@interface DamEditableTableViewCell : UITableViewCell {
    UITextField *textField;
}
@property(nonatomic, retain) UITextField *textField;

@end

@protocol DAMMediaSaveViewDelegate;

@interface DAMMediaSaveView : DAMModalViewBase {

    NSArray *model;
    id<DAMMediaSaveViewDelegate> delegate;
    
    UIView *iDriveContainer;
    DAMIDriveNavigatorEmbedded *iDrive;
}

@property (nonatomic, retain) NSArray *model;
@property (nonatomic, assign) id<DAMMediaSaveViewDelegate> delegate;

@property (nonatomic, retain) IBOutlet UIView *iDriveContainer;
@property (nonatomic, retain) DAMIDriveNavigatorEmbedded *iDrive;

- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;

@end


@protocol DAMMediaSaveViewDelegate <NSObject>

- (void)saveActionedToFolder:(NSString*)folder;
- (void)saveCancelled;

- (void)saveActioned:(DAMMediaSaveView *)saveView ToFolder:(NSString*)folder;
- (void)saveCancelled:(DAMMediaSaveView *)saveView;

@end
