//
//  DAMAssetView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "DAMAssetView.h"
#import "AFDownloadRequestOperation.h"
#import "ImageHelper.h"
#import <QuartzCore/QuartzCore.h>

#import "GkximPDFReaderView.h"

@interface DAMAssetView () {
    
    MPMoviePlayerController *moviePlayer;
    DAMImageScrollingView *imageView;
    DAMImageScrollingView *thumbImageView;
    
    CGRect oldFrame;
    CGRect oldViewFrame;
    
    BOOL navigatorHidden;
    BOOL alreadyLoad;
}

@property (nonatomic, retain) MPMoviePlayerController *moviePlayer;
@property (nonatomic, retain) DAMImageScrollingView *imageView;
@property (nonatomic, retain) DAMImageScrollingView *thumbImageView;
@property (nonatomic, retain) AFDownloadRequestOperation *downloadAssetRequestOperation;

- (BOOL)useImageViewer;
- (void)playVideo:(NSURL*)url;
//- (void)showImage:(ASIHTTPRequest*)url;

- (void)showImage:(NSURL*)url;

/*  ========================================================================== */
/*  ================================DUY LE==================================== */

/* handle zoom fullscreen video */
- (void) addObserver;
- (void) removeObserver;
- (void) videoGoesFullscreen;
- (void) videoExitFullscreen;


/* brochure */
- (void) showBrochure:(NSString *)filePath;

/*  ========================================================================== */

@end

@implementation DAMAssetView
@synthesize delegate;
@synthesize model;
@synthesize mediaType;
@synthesize webView;
@synthesize viewContainer;
@synthesize moviePlayer;
@synthesize imageView;
@synthesize progressView;
@synthesize fileSizeInfo;
//@synthesize downloadAssetRequest;
@synthesize thumbImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([mediaType compare:kMediaType_Videos] == NSOrderedSame) {
//        self.moviePlayer = [[MPMoviePlayerController alloc] init];
//        moviePlayer.view.frame = viewContainer.frame;
//        [viewContainer addSubview:moviePlayer.view];
//        [moviePlayer prepareToPlay];
//        [moviePlayer release];
    } else if ([mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos]) {
        webView.frame = CGRectMake(0, 0, 1024, 748);
    }
    
    
    webView.scalesPageToFit = YES;
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(assetSwiped:)];
    swipeLeft.delegate = self;
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [webView addGestureRecognizer:swipeLeft];

    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(assetSwiped:)];
    swipeRight.delegate = self;
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [webView addGestureRecognizer:swipeRight];
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    if([[JSMain sharedInstance] isIOS7]){
        [self addObserver];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.model = nil;
    self.mediaType = nil;
    self.webView = nil;
    self.viewContainer = nil;
    if (moviePlayer) {
        [moviePlayer stop];
        self.moviePlayer = nil;
    }
    self.imageView = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [damNavigator setDelegate:self];
    
    if(alreadyLoad == NO){
        alreadyLoad =   YES;
        if (model) {
            
            webView.hidden = self.useImageViewer;
            
            self.progressView.hidden = YES;
            self.fileSizeInfo.hidden = YES;
            
            if ([mediaType compare:kMediaType_Videos] == NSOrderedSame) {
                viewContainer.userInteractionEnabled = YES;
                self.moviePlayer = [[MPMoviePlayerController alloc] init];
                self.moviePlayer.view.frame = viewContainer.bounds;
                //            [self.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
                [viewContainer addSubview:self.moviePlayer.view];
                [self.moviePlayer prepareToPlay];
                [moviePlayer release];
            } else {
                viewContainer.hidden = YES;
            }
            
            NSString *title;
            if (delegate) {
                title = [delegate titleForAsset:model];
            } else {
                title = [model objectForKey:@"title"];
            }
            self.damNavigator.titleLabel.text = title;
            if ([mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos]) {
                [damNavigator setTitleImageFile:@"icon-med-photos.png"];
            } else {
                [damNavigator setTitleImageForView:mediaType];
            }
            
            //if this is sample we need to update color of fontsize
            if ([mediaType isEqualToString:kMediaType_Samples] || [mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos] || [mediaType isEqualToString:kMediaType_Videos]) {
                self.fileSizeInfo.textColor = [UIColor whiteColor];
            }
            
            
            NSString *url = [model objectForKey:@"url"];
            
            NSLog(@"loading url details == %@", url);
            
            //NSLog(@"loading url details == %@ == model %@", url, model);
            
            if ([mediaType compare:@"UserGuide"] == NSOrderedSame) {
                
                [self loadAssetFromExternal:url];
                
            }else{
                if ([[url substringToIndex:1] compare:@"/"] == NSOrderedSame) {
                    [self loadAssetFromExternal:url];
                } else {
                    NSString *resourcePath = [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] resourcePath], url];
                    NSURL *nsUrl = [NSURL fileURLWithPath:resourcePath];
                    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:nsUrl];
                    [webView loadRequest:urlRequest];
                }
            }
        }
    }
}

- (void)loadAssetFromExternal:(NSString*)url {
    
    NSString *downloadDestinationPath = [NSString stringWithFormat:@"%@/%@", [self basePathToThumbMedia], [JSMain getRealMediaFileName: url]];
    
    NSLog(@"....downloadDestinationPath = %@", downloadDestinationPath);
    
   //NSLog(@"....downloadDestinationPath = %@ or url == %@", downloadDestinationPath, url);
    
    if ([[NSFileManager defaultManager] fileExistsAtPath: downloadDestinationPath]) {
        
        if ([mediaType compare:kMediaType_Videos] == NSOrderedSame) {
            
            [self playVideo:[NSURL fileURLWithPath:downloadDestinationPath]];
            
        }else if ([mediaType compare:@"UserGuide"] == NSOrderedSame) {
            
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:downloadDestinationPath]];
            [webView loadRequest:urlRequest];
            
        }else if ([self useImageViewer]) {
            
            [self showImageWithFile:downloadDestinationPath];
            
        } else {
            /* 
                DUY LE
                loading the pdf brochure file
             */
            [self showBrochure:downloadDestinationPath];
//            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:downloadDestinationPath]];
//            [webView loadRequest:urlRequest];
        }
        
        return;
    }
    
    //TODO add ASINetworkQueue - extend to priority q - live(1), near live(4), background/sync(n)
    //TODO - gather host spec into shared/singleton model


    if ([[model objectForKey:@"mediaType"] isEqualToString:kMediaType_Samples]) {
        //if samples we will add UseOriginal=true
        if ([url length] > 0) {
            if ([[url componentsSeparatedByString:@"?"] count] > 1) {
                //has ? then we just add &
                url = [url stringByAppendingString:@"&UseOriginal=true"];
            }else{
                url = [url stringByAppendingString:@"?UseOriginal=true"];
            }
        }
    }
    
    self.progressView.hidden = NO;
    self.fileSizeInfo.hidden = NO;
    
    [self.view bringSubviewToFront: self.progressView];
    [self.view bringSubviewToFront: self.fileSizeInfo];
    
    NSString *host = [[DAMApplicationModel model] apiHost];
    NSString *resourceUrl = [NSString stringWithFormat:@"%@%@", host, url];
    
    NSLog(@"DAMAssetView.loadAssetFromExternal resourceUrl:%@", resourceUrl);
    
    //NSLog(@"DAMAssetView.loadAssetFromExternal resourceUrl:%@", resourceUrl);
    
    if ([mediaType compare:@"UserGuide"] == NSOrderedSame) {
        resourceUrl = url;
    }
    
    if ([mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos]) {
        
        NSString *thumbImageUrl = [NSString stringWithFormat:@"%@%@", host, [model objectForKey:@"thumbnail"]];
        //if this is Photos then we need to load thumb
        [self performSelector:@selector(showThumbImageWithFile:) withObject:thumbImageUrl afterDelay:0.1f];
        //[self showThumbImageWithFile: thumbImageUrl];
    }else if ([mediaType compare:kMediaType_Samples] == NSOrderedSame) {
        
        NSString *thumbImageUrl = [NSString stringWithFormat:@"%@%@", host, [model objectForKey:@"thumbnail"]];
        
        //NSLog(@"thumbImageUrl %@", thumbImageUrl);
        
        //if this is Photos then we need to load thumb
        [self performSelector:@selector(showThumbImageWithFileForSamples:) withObject:thumbImageUrl afterDelay:0.1f];
        //[self showThumbImageWithFile: thumbImageUrl];
    }
    
    
    NSURL *nsUrl = [NSURL URLWithString:resourceUrl];
    
    NSURLRequest *request = [NSURLRequest requestWithURL: nsUrl];
    
    self.downloadAssetRequestOperation = [[AFDownloadRequestOperation alloc] initWithRequest: request targetPath: downloadDestinationPath shouldResume: NO];
    //op.outputStream = [NSOutputStream outputStreamToFileAtPath:targetPath append:NO];
    //op.userInfo = item;
    self.progressView.progress = 0.0f;
    
    
    self.fileSizeInfo.hidden = NO;
    self.progressView.hidden = NO;
    
    [self.downloadAssetRequestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"DownloadPlugin.requestFinished url:%@", operation);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.progressView.hidden = YES;
                self.fileSizeInfo.hidden = YES;
            });
            //NSString *downloadDestinationPath = downloadDestinationPath;
            NSLog(@"....downloadDestinationPath = %@", downloadDestinationPath);
            if ([[NSFileManager defaultManager] fileExistsAtPath:downloadDestinationPath]) {
                if ([mediaType compare:kMediaType_Videos] == NSOrderedSame) {
                    [self playVideo:[NSURL fileURLWithPath:downloadDestinationPath]];
                }else if ([mediaType compare:@"UserGuide"] == NSOrderedSame) {
                    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:downloadDestinationPath]];
                    [webView loadRequest:urlRequest];
                }else if (self.useImageViewer) {
                    [self showImageWithFile:downloadDestinationPath];
                } else {
                    /* brochures */
                    [self showBrochure:downloadDestinationPath];
                 }
            }
    }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  
                                  NSLog(@"download error %@", error);
                                  self.progressView.hidden = YES;
                                  self.fileSizeInfo.hidden = YES;
                                  
                                  if ([operation isCancelled]) {
                                      //that doesn't work.
                                      NSLog(@"Canceled");
                                  }
                                  
                              }];
    
    [self.downloadAssetRequestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        if (totalBytesExpectedToRead > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.aiCheckingResourceActivities.hidden = YES;
                self.progressView.progress = (float)totalBytesRead / totalBytesExpectedToRead;
                self.fileSizeInfo.text = [NSString stringWithFormat:@"%@ / %@", [self stringFromFileSize: totalBytesRead], [self stringFromFileSize: totalBytesExpectedToRead]];
            });
        }
    }];
    
    [self.downloadAssetRequestOperation start];
    
    /*
    self.downloadAssetRequestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    self.downloadAssetRequestOperation.outputStream = [NSOutputStream outputStreamToFileAtPath: downloadDestinationPath append:NO];
    
    // Set a download progress block for the operation
    [self.downloadAssetRequestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        totalBytes = totalBytesExpectedToRead;
        if (totalBytesRead > 0) {
            self.aiCheckingResourceActivities.hidden = YES;
            bytesReceived += totalBytesRead;
            self.fileSizeInfo.text = [NSString stringWithFormat:@"%@ / %@", [self stringFromFileSize: bytesReceived], [self stringFromFileSize: totalBytes]];
            self.progressView.progress = (1.0f * bytesReceived) / (totalBytes * 1.0f);
        }
        
    }];
    
    // Set a completion block for the operation
    [self.downloadAssetRequestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        self.progressView.hidden = YES;
        self.fileSizeInfo.hidden = YES;
        //NSString *downloadDestinationPath = downloadDestinationPath;
        NSLog(@"....downloadDestinationPath = %@", downloadDestinationPath);
        if ([[NSFileManager defaultManager] fileExistsAtPath:downloadDestinationPath]) {
            if ([mediaType compare:kMediaType_Videos] == NSOrderedSame) {
                [self playVideo:[NSURL fileURLWithPath:downloadDestinationPath]];
            }else if ([mediaType compare:@"UserGuide"] == NSOrderedSame) {
                NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:downloadDestinationPath]];
                [webView loadRequest:urlRequest];
            }else if (self.useImageViewer) {
                [self showImageWithFile:downloadDestinationPath];
            } else {
                /* brochures * /
                 [self showBrochure:downloadDestinationPath];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.progressView.hidden = YES;
        self.fileSizeInfo.hidden = YES;
        
        if ([operation isCancelled]) {
            //that doesn't work.
            NSLog(@"Canceled");
        }
    }];
    
    // Start the image download operation
    [self.downloadAssetRequestOperation start];
    */
    
    //TODO - tie together the ASI stuff/download caches etc
    /*
    self.downloadAssetRequest = [ASIHTTPRequest requestWithURL:nsUrl];
    self.downloadAssetRequest.downloadCache = [ASIDownloadCache sharedCache];
    self.downloadAssetRequest.didReceiveResponseHeadersSelector = @selector(didRequestReceivedResponseHeaders:);
    [self.downloadAssetRequest setShouldWaitToInflateCompressedResponses: NO];
    self.downloadAssetRequest.showAccurateProgress = YES;
    self.downloadAssetRequest.downloadProgressDelegate = self;//self.progressView;
    self.downloadAssetRequest.timeOutSeconds = 60 * 10;
    [self.downloadAssetRequest setDownloadDestinationPath: downloadDestinationPath];
    [self.downloadAssetRequest setAllowCompressedResponse: YES];
    [self.downloadAssetRequest setCacheStoragePolicy: ASICachePermanentlyCacheStoragePolicy];
    [self.downloadAssetRequest setCachePolicy: ASIAskServerIfModifiedWhenStaleCachePolicy];
     
     
    totalBytes = 0;
    
    [self.downloadAssetRequest setDelegate:self];
    [self.downloadAssetRequest startAsynchronous];
    */
}

- (NSString *)stringFromFileSize:(long)theSize
{
	float floatSize = theSize;
	
    if (theSize<1023)
		return([NSString stringWithFormat:@"%lu bytes",theSize]);
	floatSize = floatSize / 1024;
	if (floatSize<1023)
		return([NSString stringWithFormat:@"%1.2f KB",floatSize]);
	floatSize = floatSize / 1024;
	
    if (floatSize<1023)
		return([NSString stringWithFormat:@"%1.2f MB",floatSize]);
	floatSize = floatSize / 1024;
	
	return([NSString stringWithFormat:@"%1.2f GB",floatSize]);
}

/*
- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes {
    if (totalBytes > 0) {
        self.aiCheckingResourceActivities.hidden = YES;
        bytesReceived += bytes;
        self.fileSizeInfo.text = [NSString stringWithFormat:@"%@ / %@", [self stringFromFileSize: bytesReceived], [self stringFromFileSize: totalBytes]];
        self.progressView.progress = (1.0f * bytesReceived) / (totalBytes * 1.0f);
    }
}

- (void)didRequestReceivedResponseHeaders:(ASIHTTPRequest *)request
{
    NSDictionary *rheaders = [request responseHeaders];
    NSLog(@"%@",[request responseHeaders]);
    //NSLog(@"request==%@, responseHeaders == %@",request.url,[request responseHeaders]);
    
    if (![[rheaders objectForKey:@"Connection"] isEqualToString:@"Close"]) {
        self.fileSizeInfo.hidden = NO;
        self.fileSizeInfo.text = @"";
        NSString *cl = [rheaders objectForKey:@"Content-Length"];
        totalBytes = [cl intValue];
        bytesReceived = 0;
        self.progressView.progress = 0;
        NSLog(@"file size %lu", totalBytes);
    }
}
*/


- (NSString *)basePathToThumbMedia {
    
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory=[paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"ThumbMedia"];
    
    /*
    return [NSString stringWithFormat:@"%@/Caches/ThumbMedia/",[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0]];
    */
}


/*Get real file name from medial url
 url: url of media to download. Sample: http://www.marshalls.co.uk/dam-svc/AssetStore/145b377d-248f-4104-9fe5-69c76e594f43.jpg?MaxHeight=243.000000
 return: file name of media with out ?. We will return 145b377d-248f-4104-9fe5-69c76e594f43.jpg from above input
 
 sample video
 },
 {
 id = 2592;
 mediaRef = "fafb17b0-6584-4500-a7bd-1bdccaa40354";
 mediaType = Videos;
 url = "http://www.marshalls.co.uk/dam-svc/Thumbnails/fafb17b0-6584-4500-a7bd-1bdccaa40354.png";
 },
 {
 id = 2593;
 mediaRef = "fafb17b0-6584-4500-a7bd-1bdccaa40354";
 mediaType = Videos;
 url = "http://www.marshalls.co.uk/dam-svc/AssetStore/fafb17b0-6584-4500-a7bd-1bdccaa40354.mp4";
 },
 */
/*
- (NSString *) getRealMediaFileName: (NSString *)url {
    NSString *filename = [url lastPathComponent];
    NSRange range = [filename rangeOfString:@"?"];
    if (range.location != NSNotFound) {
        return [[filename componentsSeparatedByString:@"?"] objectAtIndex: 0];
    }
    if ([url rangeOfString:@"Thumbnails"].location != NSNotFound) {
        //this is thumbnail url
        filename = [NSString stringWithFormat:@"t-%@", filename];
    }else if ([url rangeOfString:@"AssetStore"].location != NSNotFound) {
        filename = [NSString stringWithFormat:@"as-%@", filename];
    }
    
    return filename;
}*/

- (BOOL)useImageViewer {
    return [mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_Samples] || [mediaType isEqualToString:kMediaType_SalesPhotos];
}

/*
- (void)requestFinished:(ASIHTTPRequest *)request {
    
    self.progressView.hidden = YES;
    self.fileSizeInfo.hidden = YES;

    
    NSString *downloadDestinationPath = request.downloadDestinationPath;
    NSLog(@"....downloadDestinationPath = %@", downloadDestinationPath);
    
    //NSLog(@"....downloadDestinationPath = %@", downloadDestinationPath);
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:downloadDestinationPath]) {
        if ([mediaType compare:kMediaType_Videos] == NSOrderedSame) {
            
            [self playVideo:[NSURL fileURLWithPath:downloadDestinationPath]];
            
        }else if ([mediaType compare:@"UserGuide"] == NSOrderedSame) {
            
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:downloadDestinationPath]];
            [webView loadRequest:urlRequest];
            
        }else if (self.useImageViewer) {
            
            [self showImageWithFile:downloadDestinationPath];
            
        } else {
            /* brochures * /
            [self showBrochure:downloadDestinationPath];
//            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:downloadDestinationPath]];
//            [webView loadRequest:urlRequest];
        }
        
        return;
    }
}
*/

/*
- (void)requestFailed:(ASIHTTPRequest *)request {
    
    self.progressView.hidden = YES;
    self.fileSizeInfo.hidden = YES;
}*/

- (void)playVideo:(NSURL*)url {
    NSLog(@"DAMAssetView.playVideo url:%@", url);
    moviePlayer.contentURL = url;
    
    //TODO - movie natural size not available until MPMovieNaturalSizeAvailableNotification
//    CGSize movieSize = moviePlayer.naturalSize;
//    NSLog(@"playVideo movieSize:(%f,%f)", movieSize.width, movieSize.height);
    
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = self.view.bounds.size.height / scale;
    zoomRect.size.width  = self.view.bounds.size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}


- (void)showImageWithFile:(NSString *)file{
    
    if (([mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos])&& self.thumbImageView != NULL) {
        [self.thumbImageView.layer removeAllAnimations];
        [self.thumbImageView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.thumbImageView removeFromSuperview];
    }
    
    if ([mediaType isEqualToString:kMediaType_Samples] && self.thumbImageView != NULL) {
        [self.thumbImageView.layer removeAllAnimations];
        [self.thumbImageView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.thumbImageView removeFromSuperview];
    }
    
    self.imageView = [[DAMImageScrollingView alloc] initWithFrame:self.view.bounds];
    imageView.model = self.model;
    
    /*
    if ([mediaType isEqualToString:kMediaType_Samples] && [[model objectForKey:@"HasCrop"] boolValue]) {
        //need to scale up
        float imageToFrameRatio = MAX([[model objectForKey:@"width"] floatValue], [[model objectForKey:@"height"] floatValue])  / self.view.bounds.size.height;
        if (imageToFrameRatio < 1.0) {
            //orginal image is too smal, then scale up to x.22
            imageView.image = [[UIImage imageWithContentsOfFile:file] scaledToSize: CGSizeMake(self.view.bounds.size.width * 1.25, self.view.bounds.size.height * 1.25)];
        }else{
            imageView.image = [UIImage imageWithContentsOfFile:file];
        }
    }else{
        imageView.image = [UIImage imageWithContentsOfFile:file];
    }*/
    
    imageView.gestureDelegate = self;
    
    if ([mediaType isEqualToString:kMediaType_Samples]) {
        if ([[model objectForKey:@"HasCrop"] boolValue]) {
            //processing crop value
        }
#ifdef DEBUG
        NSLog(@"asset with image file == %@", model);
#endif
       // NSLog(@"asset with image file == %@ == model == %@",file, model);
        //float cropWidth = [[model objectForKey:@"CropX2"] intValue] - [[model objectForKey:@"CropX1"] intValue];
        //float cropHeight = [[model objectForKey:@"CropY2"] intValue] - [[model objectForKey:@"CropY1"] intValue];
        
        /*float imageToFrameRatio = MAX(imageView.image.size.width, imageView.image.size.height)  / self.view.bounds.size.height;
        if (imageToFrameRatio > 1) {
           NSLog(@"zoom for larger asset");
           [imageView zoomToRect:CGRectMake([[model objectForKey:@"CropX1"] intValue], [[model objectForKey:@"CropY1"] intValue], [[model objectForKey:@"CropX2"] intValue], [[model objectForKey:@"CropY2"] intValue]) animated:NO];
        }else{
            //too small asset
            //CGRect startRect = [self zoomRectForScale:1.0 / imageToFrameRatio withCenter:CGPointMake(cropWidth/2.0, cropHeight/2.0)];
        }*/
        
        /*[imageView zoomToRect:CGRectMake([[model objectForKey:@"CropX1"] intValue], [[model objectForKey:@"CropY1"] intValue], [[model objectForKey:@"CropX2"] intValue], [[model objectForKey:@"CropY2"] intValue]) animated:NO];
        */
        imageView.image = [UIImage imageWithContentsOfFile:file];
    }else{
        imageView.image = [UIImage imageWithContentsOfFile:file];
    }
    
    [self.view insertSubview:imageView atIndex:1];
    [imageView release];
    
    self.fileSizeInfo.hidden = YES;
    self.aiCheckingResourceActivities.hidden = YES;
}

- (void)showThumbImageWithFile:(NSString *)thumbUrl{
    self.thumbImageView = [[DAMImageScrollingView alloc] initWithFrame:self.view.bounds];
    
    [thumbImageView showThumbImage: thumbUrl model: model];
    
    [self.view insertSubview: thumbImageView atIndex: 1];
    [thumbImageView release];
}

- (void)showThumbImageWithFileForSamples:(NSString *)thumbUrl{
    self.thumbImageView = [[DAMImageScrollingView alloc] initWithFrame:self.view.bounds];
    
    [thumbImageView showThumbImageForSamples:thumbUrl model: model];
    
    [self.view insertSubview: thumbImageView atIndex: 1];
}


- (void)imageSingleTapped {
    navigatorHidden = !navigatorHidden;
    
    [UIView beginAnimations:@"hideNavigator" context:nil];
    [UIView setAnimationDuration:0.5];
    if (navigatorHidden) {
        damNavigator.view.transform = CGAffineTransformMakeTranslation(0, -damNavigator.view.frame.size.height);
        damNavigator.view.alpha = 0.0;
    } else {
        damNavigator.view.transform = CGAffineTransformIdentity;
        damNavigator.view.alpha = 1.0;
    }
    [UIView commitAnimations];
}

- (void)assetSwiped:(UISwipeGestureRecognizer*)gestureRecognizer {
#ifdef DEBUG
    NSLog(@"assetSwiped gestureRecognizer:%@", gestureRecognizer);
#endif
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


- (BOOL)leftButtonTapped:(id)sender {
    if (self.downloadAssetRequestOperation != NULL && self.downloadAssetRequestOperation.isExecuting) {
        [self.downloadAssetRequestOperation cancel];
        self.downloadAssetRequestOperation = nil;
    }
    if (self.thumbImageView) {
        [self.thumbImageView.layer removeAllAnimations];
    }
    
    
    /* destroy movie player */
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    self.moviePlayer = nil;
    
    [self removeObserver];
    
    //remove all subview
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    return [super leftButtonTapped: sender];
}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMAssetView.rightButtonTapped self:%@", self);
#endif
    switch (index) {
        case 0:
            [self addSendSaveButtons];
            break;
    }
}

- (void)addSendSaveButtons {
    NSArray *buttonSpec = [NSArray arrayWithObjects:@"btn-cancel.png", @"cancelButtonTapped:", @"btn-send.png", @"shareButtonTapped:",
                           @"btn-save.png", @"saveButtonTapped:", nil];
    [damNavigator addRolloutButtons:buttonSpec forRightButtonIndex:0 withTarget:self];
}

- (NSArray*)selectedMedia {
    return [NSArray arrayWithObject:model];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	if ([mediaType compare:kMediaType_CaseStudies] == NSOrderedSame || [mediaType compare:kMediaType_Brochures] == NSOrderedSame) {
        return YES;
    } else {
        return [super shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
    }
}


#pragma mark -  DUY LE - NEW METHOD

/* Videos  */
- (void) addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoGoesFullscreen) name:MPMoviePlayerWillEnterFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoExitFullscreen) name:MPMoviePlayerDidExitFullscreenNotification object:nil];
}

- (void) removeObserver{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillEnterFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:nil];
}

- (void) videoGoesFullscreen{
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    oldFrame        =   appDelegate.window.bounds;
    oldViewFrame    =   self.view.bounds;
    
    CGRect viewBounds = [appDelegate.window bounds];
    appDelegate.window.frame    =   viewBounds;
    [appDelegate.window setClipsToBounds:YES];
}

- (void) videoExitFullscreen{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kAppInitDone" object:nil];
}

/* Brochure */
- (void) showBrochure:(NSString *)filePath{
    [self.webView setHidden:YES];
    [self.viewContainer setHidden:NO];
    GkximPDFReaderView *pdfView =   [GkximPDFReaderView new];
    [pdfView setFrame:self.viewContainer.bounds];
    [pdfView loadPDFFileWithFileName:filePath];
    [self.viewContainer addSubview:pdfView];
}

@end
