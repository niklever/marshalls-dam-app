//
//  DAMPredictiveViewController.h
//  MarshallsDigitalAssetManagement
//
//  Created by Hoa Truong Huu on 1/16/13.
//
//

#import <UIKit/UIKit.h>

@class  DAMPredictiveViewController;

@protocol DAMPredictiveViewControllerDelegate <NSObject>

- (void)predictiveViewController:(DAMPredictiveViewController *)predictiveViewController didChooseItem:(NSDictionary *)item;

@end

@interface DAMPredictiveViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *contentTableView;
@property (nonatomic, assign) IBOutlet id<DAMPredictiveViewControllerDelegate> delegate;
@property(nonatomic, retain) UITextField *searchField;
- (void)resetData;
- (void)reloadData;
- (void)reloadDataWithItems:(NSArray *)items;
- (void)requestDataWithKey:(NSString *)_key mediaType:(NSString *)_mediaType containers:(NSArray *)_containers;
- (void)hideAfterTime:(float)time;
@end
