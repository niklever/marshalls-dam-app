//
//  DAMAssetView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreVideo/CoreVideo.h>
#import "DAMShareableView.h"
#import "DAMImageScrollingView.h"


@protocol DAMAssetViewDelegate <NSObject>

- (NSString*)titleForAsset:(NSDictionary*)asset;

@end

@interface DAMAssetView : DAMShareableView<UIGestureRecognizerDelegate, DAMImageScrollingViewDelegate> {
    
    id<DAMAssetViewDelegate> delegate;
    
    NSDictionary *model;
    NSString *mediaType;
    
    UIWebView *webView;
    UIView *viewContainer;
    long totalBytes;
    long bytesReceived;
    
}
@property (nonatomic, assign) id<DAMAssetViewDelegate> delegate;

@property (nonatomic, retain) NSDictionary *model;
@property (nonatomic, retain) NSString *mediaType;

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *aiCheckingResourceActivities;
@property (nonatomic, retain) IBOutlet UIView *viewContainer;
@property (nonatomic, retain) IBOutlet UIProgressView *progressView;
@property (nonatomic, retain) IBOutlet UILabel *fileSizeInfo;
//overriden from DAMShareableView
- (NSArray*)selectedMedia;

@end
