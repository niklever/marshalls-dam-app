//
//  DAMTagLabel.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 04/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/CALayer.h>
#import "DAMTagLabel.h"
#import "DAMUIDefaults.h"

@interface DAMTagLabel() {
    
    id<DAMTagLabelDelegate> delegate;
    
    UILabel *label;
    
    BOOL selected;
}

//@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UIButton *cancelButton;

@end

@implementation DAMTagLabel
@synthesize delegate;
@synthesize label;
@synthesize cancelButton;

const float buttonAreaWidth = 30;
const float buttonWidth = 15;
const float buttonHeight = 15;
const float labelPadding = 10;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
//        float pad = 10;
        float labelWidth = self.frame.size.width - buttonAreaWidth;
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelWidth, self.frame.size.height)];
        label.textColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];
        label.backgroundColor = [UIColor clearColor];
        label.font = [DAMUIDefaults defaults].tagLabelFont;//[UIFont fontWithName:@"Helvetica" size:24];
        label.textAlignment = UITextAlignmentCenter;//UITextAlignmentRight;

        UITapGestureRecognizer *tapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
        self.userInteractionEnabled = YES;
        [self addGestureRecognizer:tapRecog];
        [tapRecog release];
        
        float buttonX = labelWidth + (buttonAreaWidth-buttonWidth) / 2;
        float buttonY = (self.frame.size.height - buttonHeight) / 2;
        self.cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight)];
//        [cancelButton setTitle:@"X" forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchDown];
        [cancelButton setBackgroundImage:[UIImage imageNamed:@"btn-clearx.png"] forState:UIControlStateNormal];

        cancelButton.alpha = 0.0;
        cancelButton.enabled = NO;
        
        [self addSubview:label];
        [self addSubview:cancelButton];
        
        [label release];
        [cancelButton release];
        
        selected = NO;
    }
    return self;
}

- (void)dealloc {
    self.label = nil;
    self.cancelButton = nil;
    [super dealloc];
}

- (void)layout {
    float textWidth = [label.text sizeWithFont:label.font].width;
    float labelWidth = 2 * labelPadding + textWidth;
    CGRect frame = label.frame;
    frame.size.width = labelWidth;
    label.frame = frame;
    frame = cancelButton.frame;
    frame.origin.x = labelWidth;
    cancelButton.frame = frame;
    frame = self.frame;
    frame.size.width = labelWidth + buttonAreaWidth;
    self.frame = frame;    
}

- (void)labelTapped:(id)sender {
    
    if(_disableTapOnLabel){
        return;
    }
    
    selected = !selected;
    if (selected) {
        [self setSelected];
        if (delegate) {
            [delegate tagSelected:label.text];
        }
    } else {
        self.layer.cornerRadius = 0;
        self.backgroundColor = [UIColor clearColor];
        cancelButton.alpha = 0.0;
        cancelButton.enabled = NO;
        if (delegate && [delegate respondsToSelector:@selector(tagDeselected:)]) {
            [delegate tagDeselected:label.text];
        }
        else if(delegate && [delegate respondsToSelector:@selector(removeTags:)]){
            [delegate removeTags:self];
        }
    }
}

- (BOOL)isSelected {
    return selected;
}

- (void)setSelected {
    selected = YES;
    self.layer.cornerRadius = 8;
    self.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:1.0];
    cancelButton.alpha = 1.0;
    cancelButton.enabled = YES;
}

- (void)setUnSelected {
    selected = NO;
    self.layer.cornerRadius = 0;
    self.backgroundColor = [UIColor clearColor];
    cancelButton.alpha = 0.0;
    cancelButton.enabled = NO;
}

- (void)cancelButtonTapped:(id)sender {
    selected = !selected;
    if (selected) {
        [self setSelected];
        if (delegate) {
            [delegate tagSelected:label.text];
        }
    } else {
        self.layer.cornerRadius = 0;
        self.backgroundColor = [UIColor clearColor];
        cancelButton.alpha = 0.0;
        cancelButton.enabled = NO;
        if (delegate && [delegate respondsToSelector:@selector(tagDeselected:)]) {
            [delegate tagDeselected:label.text];
        }
        else if(delegate && [delegate respondsToSelector:@selector(removeTags:)]){
            [delegate removeTags:self];
        }
    }
//    self.layer.cornerRadius = 0;
//    self.backgroundColor = [UIColor clearColor];
//    cancelButton.alpha = 0.0;
//    cancelButton.enabled = NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
