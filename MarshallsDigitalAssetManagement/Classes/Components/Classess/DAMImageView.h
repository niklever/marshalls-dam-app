//
//  DAMImageView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 12/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMImageView : UIImageView

@property (nonatomic, retain) NSDictionary *model;

- (id)initWithUrl:(NSString*)url andFrame:(CGRect)frame;

- (void)setUrl:(NSString*)uUrl andPlaceholderIndex:(int)placeholderIndex;

- (void)setUrl:(NSString*)uUrl andPlaceholder:(NSString*)placeholder;

- (void)cancelSelection;
- (NSNumber*)isSelected;
- (void)toggleSelection;

@end
