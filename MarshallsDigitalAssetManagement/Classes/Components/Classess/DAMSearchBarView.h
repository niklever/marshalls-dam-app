//
//  DAMSearchBarView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 11/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DAMSearchBarViewDelegate <NSObject>

- (void)searchBarCancelTapped;
- (void)searchBarBusinessAreaTapped;

@end

@interface DAMSearchBarView : UIViewController {
//    id<DAMSearchBarViewDelegate> delegate;
    
    UITextField *searchField;
    UIButton *cancelButton;
    UIButton *businessAreaButton;
}
@property (nonatomic, assign) id<DAMSearchBarViewDelegate> delegate;

@property (nonatomic, retain) IBOutlet UITextField *searchField;
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *businessAreaButton;

- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)businessAreaButtonTapped:(id)sender;

@end
