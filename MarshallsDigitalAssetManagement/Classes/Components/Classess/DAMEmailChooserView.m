//
//  DAMEmailChooserView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import "DAMEmailChooserView.h"
#include <dlfcn.h>

@interface DAMEmailChooserView () {
    
    id<DAMEmailChooserViewDelegate> delegate;
    
    NSMutableArray *model;
    UITableView *table;
}
@property (nonatomic, retain) NSMutableArray *model;
- (void)lookupContacts;

@end

@implementation DAMEmailChooserView
@synthesize delegate;
@synthesize model;
@synthesize table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self lookupContacts];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.model = nil;
    self.table = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (void)lookupContacts {
    self.model = [NSMutableArray arrayWithCapacity:100];

    ABAddressBookRef addressBook = ABAddressBookCreate();
    
    __block BOOL accessGranted = NO;
    
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        dispatch_release(sema);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    //accessGranted = YES;
    
    if (accessGranted) {
       
        CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
#ifdef DEBUG
        NSLog(@"....people count  = %ld", CFArrayGetCount(people));
#endif
        for (CFIndex i = 0; i < CFArrayGetCount(people); i++) {
            ABRecordRef person = CFArrayGetValueAtIndex(people, i);
            
            if (!person) {
                continue;
            }

            
            NSString *lastName = (NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            NSString *firstName = (NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            
            NSString *email = nil;
            
            ABMultiValueRef emails = ABRecordCopyValue(person, kABPersonEmailProperty);
            
            if (ABMultiValueGetCount(emails) > 0) {
                email = (NSString*)ABMultiValueCopyValueAtIndex(emails, 0);
            }
            CFRelease(emails);
            
            if (lastName == nil)
                lastName = @"";
            if (firstName == nil)
                firstName = @"";
            if (email == nil) {
                email = @"";
            }
            
            NSDictionary *modelPerson = [NSDictionary dictionaryWithObjectsAndKeys:lastName, @"lastName", firstName, @"firstName", email, @"email", nil];
            [model addObject:modelPerson];
            
        }
    
        CFRelease(people);
        
    }
    
    if(addressBook){
        CFRelease(addressBook);
    }
    
    //try to sort the array here
    [model sortUsingDescriptors:[NSArray arrayWithObjects:
                                 [[[NSSortDescriptor alloc]
                                   initWithKey:@"lastName"
                                   ascending: YES]
                                  autorelease],
                                 [[[NSSortDescriptor alloc]
                                   initWithKey:@"firstName"
                                   ascending: YES]
                                  autorelease],
                                 nil]];
    
    /*
    for (id person in people) {
        //        NSLog(@"person:%@", person);
        ABMultiValueRef mvRef = ABRecordCopyValue(person, kABPersonEmailProperty);
        int numValues = ABMultiValueGetCount(mvRef);
        if (numValues > 0) {
            for (int i = 0; i < numValues; i++) {
                NSString *lastName = ABRecordCopyValue(person, kABPersonCompositeNameFormatLastNameFirst);
                NSString *firstName = ABRecordCopyValue(person, kABPersonCompositeNameFormatFirstNameFirst);
                NSString *email = ABMultiValueCopyValueAtIndex(mvRef, i);
                NSLog(@"name:%@ %@, email:%@", firstName, [lastName uppercaseString], email);
                
                if (lastName == nil)
                    lastName = @"";
                if (firstName == nil)
                    firstName = @"";
                NSDictionary *modelPerson = [NSDictionary dictionaryWithObjectsAndKeys:lastName, @"lastName", firstName,
                                             @"firstName", email, @"email", nil];
                [model addObject:modelPerson];
            }
        }
    }
    
     */
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return model.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *result;
    
    result = [tableView dequeueReusableCellWithIdentifier:@"emailChooser"];
    if (!result) {
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"emailChooser"];
    }
    NSDictionary *person = [model objectAtIndex:indexPath.row];
    NSString *firstName = [person objectForKey:@"firstName"];
    NSString *lastName = [person objectForKey:@"lastName"];
    NSString *email = [person objectForKey:@"email"];
    if (!firstName) firstName = @"";
    if (!lastName) lastName = @"";
    if (!email) email = @"";
    result.textLabel.text = [NSString stringWithFormat:@"%@ %@", firstName, [lastName uppercaseString]];
    result.detailTextLabel.text = email;
    
    return result;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (delegate) {
        NSDictionary *contact = [model objectAtIndex:indexPath.row];
        [delegate contactChosen:contact];
    }
}

@end
