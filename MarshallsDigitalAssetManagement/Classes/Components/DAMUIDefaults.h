//
//  DAMUIDefaults.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DAMUIDefaults : NSObject {
    UIFont *textFont;
    UIFont *iDriveFont;
    UIFont *iDriveFreespaceFont;
    UIFont *navigatorTitleFont;
    UIFont *carouselLabelFont;
    UIFont *thumbnailTitleFont;
    UIFont *thumbnailSubtitleFont;
    UIFont *modalTitleFont;
    UIFont *mediaSendFont;

    UIColor *textColor;
    
    UIFont *busAreasFont;
    UIFont *tagLabelFont;
}

@property (nonatomic, readonly) UIFont *tagLabelFont;
@property (nonatomic, readonly) UIFont *busAreasFont;
@property (nonatomic, readonly) UIFont *textFont;
@property (nonatomic, readonly) UIFont *iDriveFont;
@property (nonatomic, readonly) UIFont *iDriveFreespaceFont;
@property (nonatomic, readonly) UIFont *navigatorTitleFont;
@property (nonatomic, readonly) UIFont *carouselLabelFont;
@property (nonatomic, readonly) UIFont *thumbnailTitleFont;
@property (nonatomic, readonly) UIFont *thumbnailSubtitleFont;
@property (nonatomic, readonly) UIFont *modalTitleFont;
@property (nonatomic, readonly) UIFont *mediaSendFont;

@property (nonatomic, readonly) UIColor *textColor;

+ (DAMUIDefaults*)defaults;

- (float)textPlaceholderFadeTime;

@end
