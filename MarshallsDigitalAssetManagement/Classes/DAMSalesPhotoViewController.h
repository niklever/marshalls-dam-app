//
//  DAMSalesPhotoViewController.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/28/14.
//
//

#import <UIKit/UIKit.h>

#import "DAMPhotoScrollView.h"
#import "DAMNavigatorViewController.h"

@interface DAMSalesPhotoViewController : UIViewController<DAMPhotoScrollViewDelegate>

@property (nonatomic, assign)DAMNavigatorViewController *damNavigation;

@end
