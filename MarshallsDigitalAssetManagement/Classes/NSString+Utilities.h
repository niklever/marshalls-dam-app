//
//  NSString+Utilities.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 14/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Utilities)

- (NSString*)encodeURL;
- (float) textWidthWithFont:(UIFont *)font;

@end
