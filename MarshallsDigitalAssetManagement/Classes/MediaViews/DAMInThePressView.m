//
//  DAMInThePressView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 21/8/14.
//
//

#import "DAMInThePressView.h"
#import "DAMAssetView.h"

@interface DAMInThePressView ()

@end

@implementation DAMInThePressView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.mediaType = kMediaType_InThePress;
        self.pageSize = 6;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.topScrollView.scrollView.pagingEnabled = NO;
    self.topScrollView.pageControlEnabled = YES;
    
    if([[JSMain sharedInstance] isIOS7])
    {
        self.topScrollView.frame = CGRectMake(self.topScrollView.frame.origin.x, self.topScrollView.frame.origin.y, self.topScrollView.frame.size.width, self.topScrollView.frame.size.height);
    }else{
        self.topScrollView.frame = CGRectMake(self.topScrollView.frame.origin.x, self.topScrollView.frame.origin.y, self.topScrollView.frame.size.width, self.topScrollView.frame.size.height + 40);
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"In The Press";
    
    [damNavigator displaySearchBarWithDelegate:self];
    [damNavigator setTitleImageForView:kMediaType_InThePress];
    damNavigator.searchBar.cancelButton.hidden = YES;
    //damNavigator.searchBar.businessAreaButton.hidden = YES;
    damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);
}

- (void) mediaViewBaseInit {
    /*if ([[JSMain sharedInstance] isIOS7]) {
     [self hideUnusedCarousels];
     }*/
}


- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMSampleBookView.rightButtonTapped index:%i", index);
#endif
    DAMIDriveNavigatorViewController *iDrive;
    switch (index) {
        case 0:
            [self lauchInfo];
            break;
        case 1:
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            break;
        case 2:
            [self addSendSaveButtons];
            break;
        case 3:
            
            break;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
