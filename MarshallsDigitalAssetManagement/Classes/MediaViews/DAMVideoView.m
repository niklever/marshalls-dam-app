//
//  DAMVideoView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMVideoView.h"
#import "DAMIDriveNavigatorViewController.h"

@interface DAMVideoView ()

@end

@implementation DAMVideoView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.mediaType = kMediaType_Videos;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.topScrollView.pageControlEnabled = YES;
    self.bottomScrollView.pageControlEnabled = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Videos";
    
    [damNavigator displaySearchBarWithDelegate:self];
    [damNavigator setTitleImageForView:kMediaType_Videos];
    damNavigator.searchBar.cancelButton.hidden = YES;
    //damNavigator.searchBar.businessAreaButton.hidden = YES;
    damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMVideoView.rightButtonTapped index:%i", index);
#endif
    DAMIDriveNavigatorViewController *iDrive;
    switch (index) {
        case 0:
            [self lauchInfo];
            break;
        case 1:
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            break;
        case 2:
            [self addSendSaveButtons];
            break;
        case 3:
            
            break;
    }
}



@end
