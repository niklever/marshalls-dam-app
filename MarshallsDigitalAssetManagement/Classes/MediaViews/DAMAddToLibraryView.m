//
//  DAMAddToLibraryView.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/31/14.
//
//

#import "DAMAddToLibraryView.h"
#import "DAMBusinessAreaFiltersView.h"
#import "MBProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#import "DAMTagLabel.h"
#import "UIImage+fixOrientation.h"
#import <ImageIO/ImageIO.h>

#define MAX_DESC    140

@interface DAMAddToLibraryView() <DAMBusinessAreaFiltersViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate, DAMTagLabelDelegate>{
    
    __weak IBOutlet UIImageView *imvThumbnail;
    __weak IBOutlet UIScrollView *scrContentView;
    __weak IBOutlet UILabel *lblPhotoTitles;
    __weak IBOutlet UIView *vPhotoTitle;
    __weak IBOutlet UITextField *txtPhotoTitles;
    __weak IBOutlet UILabel *lblLocation;
    __weak IBOutlet UIView *vLocations;
    __weak IBOutlet UITextField *txtLocation;
    __weak IBOutlet UILabel *lblDescriptions;
    __weak IBOutlet UIView *vDescriptions;
    __weak IBOutlet UITextView *txvDescriptions;
    __weak IBOutlet UILabel *lblDescriptionsPlaceHolder;
    __weak IBOutlet UILabel *lblBusinessArea;
    __weak IBOutlet UILabel *lblProducts;
    __weak IBOutlet UIView *vProducts;
    __weak IBOutlet UITextView *txtProducts;
    __weak IBOutlet UILabel *lblProductPlaceHolder;
    __weak IBOutlet UIView *vBusinessArea;
    __weak IBOutlet UIButton *btnBussinessArea;
    
    __weak IBOutlet UIButton *btnClearPhoto;
    __weak IBOutlet UIButton *btnClearLocation;
    __weak IBOutlet UIButton *btnClearDescription;
    __weak IBOutlet UIButton *btnClearProduct;
    
    __weak IBOutlet UIScrollView *scrBusiness;
    
    DAMBusinessAreaFiltersView *businessAreaFiltersView;
}

@property (nonatomic, strong) NSMutableArray *arrBusiness;
@property (nonatomic, assign) CLLocation *currentLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;

- (IBAction)didClickCancel:(id)sender;
- (IBAction)didClickSave:(id)sender;
- (IBAction)didClickX:(id)sender;
- (IBAction)didClickBussinessArea:(id)sender;

@end

@implementation DAMAddToLibraryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) awakeFromNib{
    [super awakeFromNib];
    
    [self configFontsize];
    [self configPlaceHolder];
    [self borderHolderView];
    
    self.locationManager =   [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [self.locationManager startUpdatingLocation];
    
    
    [self hiddenAllClearButton];
}

- (void)dealloc {
    [imvThumbnail release];
    [scrContentView release];
    [lblPhotoTitles release];
    [vPhotoTitle release];
    [txtPhotoTitles release];
    [lblLocation release];
    [vLocations release];
    [txtLocation release];
    [lblDescriptions release];
    [vDescriptions release];
    [txvDescriptions release];
    [lblDescriptionsPlaceHolder release];
    [lblBusinessArea release];
    [lblProducts release];
    [vProducts release];
    [txtProducts release];
    [lblProductPlaceHolder release];
    [vBusinessArea release];
    [_arrBusiness release];
    [_imageTitle release];
    [super dealloc];
}

#pragma mark - private method
- (void) hiddenAllClearButton{
    [btnClearPhoto setHidden:YES];
    [btnClearLocation setHidden:YES];
    [btnClearDescription setHidden:YES];
    [btnClearProduct setHidden:YES];
}

static CGRect swapWidthAndHeight(CGRect rect)
{
    CGFloat  swap = rect.size.width;
    
    rect.size.width  = rect.size.height;
    rect.size.height = swap;
    
    return rect;
}

-(UIImage*)rotateImage:(UIImage *)image andOrientation:(UIImageOrientation)orient
{
    CGRect             bnds = CGRectZero;
    UIImage*           copy = nil;
    CGContextRef       ctxt = nil;
    CGImageRef         imag = image.CGImage;
    CGRect             rect = CGRectZero;
    CGAffineTransform  tran = CGAffineTransformIdentity;
    
    rect.size.width  = CGImageGetWidth(imag);
    rect.size.height = CGImageGetHeight(imag);
    
    bnds = rect;
    
    switch (orient)
    {
        case UIImageOrientationUp:
            // would get you an exact copy of the original
            assert(false);
            return nil;
            
        case UIImageOrientationUpMirrored:
            tran = CGAffineTransformMakeTranslation(rect.size.width, 0.0);
            tran = CGAffineTransformScale(tran, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown:
            tran = CGAffineTransformMakeTranslation(rect.size.width,
                                                    rect.size.height);
            tran = CGAffineTransformRotate(tran, M_PI);
            break;
            
        case UIImageOrientationDownMirrored:
            tran = CGAffineTransformMakeTranslation(0.0, rect.size.height);
            tran = CGAffineTransformScale(tran, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeft:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(0.0, rect.size.width);
            tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeftMirrored:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(rect.size.height,
                                                    rect.size.width);
            tran = CGAffineTransformScale(tran, -1.0, 1.0);
            tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRight:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(rect.size.height, 0.0);
            tran = CGAffineTransformRotate(tran, M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeScale(-1.0, 1.0);
            tran = CGAffineTransformRotate(tran, M_PI / 2.0);
            break;
            
        default:
            // orientation value supplied is invalid
            assert(false);
            return nil;
    }
    
    UIGraphicsBeginImageContext(bnds.size);
    ctxt = UIGraphicsGetCurrentContext();
    
    switch (orient)
    {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextScaleCTM(ctxt, -1.0, 1.0);
            CGContextTranslateCTM(ctxt, -rect.size.height, 0.0);
            break;
            
        default:
            CGContextScaleCTM(ctxt, 1.0, -1.0);
            CGContextTranslateCTM(ctxt, 0.0, -rect.size.height);
            break;
    }
    
    CGContextConcatCTM(ctxt, tran);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), rect, imag);
    
    copy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return copy;
}

#pragma mark - CLLocation Manager Delegates

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // Last object contains the most recent location
    CLLocation *newLocation = [locations lastObject];
    
    // If the location is more than 5 minutes old, ignore it
    if([newLocation.timestamp timeIntervalSinceNow] > 300)
        return;
    
    [self.locationManager stopUpdatingLocation];
    self.currentLocation = newLocation;
}

#pragma mark - IBACTION
- (IBAction)didClickCancel:(id)sender {
    
    [self removeObserver];
    [self.locationManager stopUpdatingLocation];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self setFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height)];
                     } completion:^(BOOL finished) {
                         [self setHidden:YES];
                         //[self removeFromSuperview];
                         if (sender) {
                             self.cancelBlock();
                         }else{
                             self.saveBlock();
                         }
                     }];
    
}

- (void) createCancelBlock:(CancelBlock)block {
    self.cancelBlock = block;
}
- (void) createSaveBlock:(SaveBlock)block {
    self.saveBlock = block;
}

void exportCGImageToJPEGFileWithDestination( CGImageRef image, CFURLRef url,float desResolution)
{
    CFTypeRef keys[2];
    CFTypeRef values[2];
    CFDictionaryRef options = NULL;
    
    // Create image destination to go into URL, using JPEG
    CGImageDestinationRef imageDestination = CGImageDestinationCreateWithURL( url, kUTTypeJPEG, 1, NULL);
    
    if ( imageDestination == NULL )
    {
        fprintf( stderr, "Error creating image destination\n");
        return;
    }
    
    // Set the keys to be the X and Y resolution of the image
    keys[0] = kCGImagePropertyDPIWidth;
    keys[1] = kCGImagePropertyDPIHeight;
    
    // Create a number for the DPI value for the image
    values[0] = CFNumberCreate( NULL, kCFNumberFloatType, &desResolution );
    values[1] = values[0];
    
    // Options dictionary for output
    options = CFDictionaryCreate(NULL,
                                 (const void **)keys,
                                 (const void **)values,
                                 2,
                                 &kCFTypeDictionaryKeyCallBacks,
                                 &kCFTypeDictionaryValueCallBacks);
    
    CFRelease(values[0]);
    
    // Adding the image to the destination
    CGImageDestinationAddImage( imageDestination, image, options );
    CFRelease( options );
    
    // Finalizing writes out the image to the destination
    CGImageDestinationFinalize( imageDestination );
    
    CFRelease( imageDestination );
}


- (IBAction)didClickSave:(id)sender {
    
    if([self isValidInfo]){
        
        if([[[JSMain sharedInstance] userEmail] length] == 0){
            
            UIAlertView *alvInform    =   [[UIAlertView alloc] initWithTitle:nil
                                                                     message:@"Please enter a valid email in Settings to upload your photo to the server."
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
            [alvInform show];
            alvInform.tag = 1001;
            [alvInform release];
            return;
        }
        
        [MBProgressHUD showHUDAddedTo:self animated:YES];
        [[APICommon sharedInstance] doRenewAuthToken:^(id resp) {
            
            [_arrBusiness enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if(idx == 0){
                    [btnBussinessArea setTitle:obj forState:UIControlStateNormal];
                }
                else{
                    [btnBussinessArea setTitle:[NSString stringWithFormat:@"%@,%@", btnBussinessArea.titleLabel.text, obj]
                                      forState:UIControlStateNormal];
                }
            }];
            
            
            NSDictionary *params    =   [NSDictionary dictionaryWithObjectsAndKeys:
                                         [[JSMain sharedInstance] userEmail],@"AppUserEmail",
                                         txtPhotoTitles.text, @"Name",
                                         ([txtLocation.text length] == 0) ? @"" : txtLocation.text, @"Location",
                                         [txvDescriptions.text length] == 0 ? @"" : txvDescriptions.text, @"Description",
                                         [btnBussinessArea.titleLabel.text isEqualToString:@"Please choose"] ? @"" : btnBussinessArea.titleLabel.text, @"Specialities",
                                         ([txtProducts.text length] == 0) ? @"" : txtProducts.text, @"Products",
                                         [NSString stringWithFormat:@"%f", _locationManager.location.coordinate.latitude],@"Latitude",
                                         [NSString stringWithFormat:@"%f", _locationManager.location.coordinate.longitude],@"Longitude",
                                         nil];
            
//            NSData *imageToPost = !([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft) ? UIImagePNGRepresentation(imvThumbnail.image) : UIImagePNGRepresentation([self rotateImage:imvThumbnail.image andOrientation:UIImageOrientationDown]) ;
            
            NSData *imageToPost = nil;//  UIImagePNGRepresentation([imvThumbnail.image fixOrientation]);
            // start remove me now
            
            NSString *outputPath = [NSString stringWithFormat:@"%@tmp_%@.jpg", NSTemporaryDirectory(), @"1"];
            NSURL *outputURL = [NSURL fileURLWithPath:outputPath];

            exportCGImageToJPEGFileWithDestination([imvThumbnail.image fixOrientation].CGImage, (__bridge CFURLRef)outputURL,72);
            UIImage * jpegImg = [UIImage imageWithContentsOfFile:outputPath];
            imageToPost =   UIImageJPEGRepresentation(jpegImg, (CGFloat)1.0);
            // end remove me now
            [[APICommon sharedInstance] doPostMedia:params mediaData:imageToPost
                                    successCallback:^(id resp) {
                
                [MBProgressHUD hideAllHUDsForView:self animated:YES];
                
                UIAlertView *alvInform    =   [[UIAlertView alloc] initWithTitle:nil
                                                                         message:kAddSalesMediaSuccess
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                [alvInform show];
                alvInform.tag = 1001;
                [alvInform release];
                [self didClickCancel:nil];
            } andFailCallback:^(id msg) {
                [MBProgressHUD hideAllHUDsForView:self animated:YES];
                NSLog(@"fail");
                UIAlertView *alvInform    =   [[UIAlertView alloc] initWithTitle:nil
                                                                         message:kAddSalesMediaFailed
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                [alvInform show];
                alvInform.tag = 1001;
                [alvInform release];
            }];
            
            [btnBussinessArea setTitle:@"Please choose" forState:UIControlStateNormal];

        } andFailCallback:^(id msg) {
            [MBProgressHUD hideAllHUDsForView:self animated:YES];
            
            UIAlertView *alvInform    =   [[UIAlertView alloc] initWithTitle:nil
                                                                     message: kFailedToRenewLoginToken
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
            [alvInform show];
            alvInform.tag = 1001;
            [alvInform release];
             NSLog(@"%@", msg);
        }];
        
    }
    else{
        UIAlertView *alvInform  =   [[UIAlertView alloc] initWithTitle:nil
                                                               message: kInputMissingMessage
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
        [alvInform show];
        [alvInform release];
    }
}

- (IBAction)didClickX:(id)sender {
    
    UIButton *btnSender =   (UIButton *)sender;
    switch (btnSender.tag) {
        case 0:{
            /* photo */
            [txtPhotoTitles setText:@""];
            break;
        }
            
        case 1:{
            /* location */
            [txtLocation setText:@""];
            break;
        }
            
        case 2:{
            /* description */
            [txvDescriptions setText:@""];
            break;
        }
            
        case 3:{
            /* product area */
            [txtProducts setText:@""];
            break;
        }
            
        default:
            break;
    }
    
    
}

- (IBAction)didClickBussinessArea:(id)sender {
    [self animateFrameToNormal];
    
    if(!businessAreaFiltersView){
        businessAreaFiltersView = [[DAMBusinessAreaFiltersView alloc] initWithNibName:@"DAMBusinessAreaFiltersView" bundle:nil];
        businessAreaFiltersView.delegate = self;
        [self addSubview:businessAreaFiltersView.view];
    }
    [businessAreaFiltersView.view setHidden:NO];
    [businessAreaFiltersView selectTagsFollowSelecteTags:_arrBusiness];
    [businessAreaFiltersView animateOnStage];
}

#pragma mark - private method
- (void) configFontsize{
    
    [lblPhotoTitles setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:22]];
    [lblLocation setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:22]];
    [lblDescriptions setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:22]];
    [lblBusinessArea setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:22]];
    [lblProducts setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:22]];
}

- (void) configPlaceHolder{
    
    UIColor *placeHolderColor   =   [UIColor colorWithRed:162.0/255.0 green:87.0/255.0 blue:107.0/255.0 alpha:1.0];
    
    if ([txtPhotoTitles respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        txtPhotoTitles.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtPhotoTitles.placeholder attributes:@{NSForegroundColorAttributeName: placeHolderColor, NSFontAttributeName: txtPhotoTitles.font}];
        txtLocation.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtLocation.placeholder attributes:@{NSForegroundColorAttributeName: placeHolderColor, NSFontAttributeName: txtLocation.font}];
        
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    
}

- (BOOL)isValidInfo{
    
    if([txtPhotoTitles.text length] == 0){
        //return NO;
        txtPhotoTitles.text = _imageTitle;
    }
    
//    if([txtLocation.text length] == 0){
//        return NO;
//    }
//    
//    if([txvDescriptions.text length] == 0){
//        return NO;
//    }
//    
//    if([_arrBusiness count] == 0){
//        return NO;
//    }
//    
//    if([txtProducts.text length] == 0){
//        return NO;
//    }
    
    return YES;
}

- (void)borderHolderView{
    
    float cornerRadius  =   5.0;
    
    [vPhotoTitle.layer setCornerRadius:cornerRadius];
    [vPhotoTitle setClipsToBounds:YES];
    
    [vLocations.layer setCornerRadius:cornerRadius];
    [vLocations setClipsToBounds:YES];
    
    [vDescriptions.layer setCornerRadius:cornerRadius];
    [vDescriptions setClipsToBounds:YES];
    
    [vBusinessArea.layer setCornerRadius:cornerRadius];
    [vBusinessArea setClipsToBounds:YES];
    
    [vProducts.layer setCornerRadius:cornerRadius];
    [vProducts setClipsToBounds:YES];
    
    
}

- (void) setThumbnailImage:(UIImage *)image{
    [imvThumbnail setImage:image];
}

- (void) animationShowUp{
    
    [self removeObserver];
    [self addObserver];
    //[txtPhotoTitles setText:_imageTitle];
    [self setHidden:NO];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void) animateViewToFrame:(CGRect)frameToScroll{
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.frame =   frameToScroll;
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void) animateFrameToNormal{
    [self hiddenAllClearButton];
    [txtLocation resignFirstResponder];
    [txtPhotoTitles resignFirstResponder];
    [txtProducts resignFirstResponder];
    [txvDescriptions resignFirstResponder];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.frame =   CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);;
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void) addObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animateFrameToNormal) name:UIKeyboardWillHideNotification object:nil];
}

- (void) removeObserver{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void) layoutSelectedTags{
    
    [scrBusiness.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if([obj isKindOfClass:[DAMTagLabel class]]){
            [obj removeFromSuperview];
        }
    }];
    
    __block int originX =   0;
    __block int contentSize =   0;
    [_arrBusiness enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        UILabel *labelTemp  =   [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
        [labelTemp setText:obj];
        [labelTemp sizeToFit];
        
        DAMTagLabel *tags   =   [[DAMTagLabel alloc] initWithFrame:CGRectMake(originX, 0, labelTemp.frame.size.width + 10 + 30, scrBusiness.frame.size.height)];
        [tags setDelegate:self];
        [tags setDisableTapOnLabel:YES];
        [tags.label setText:obj];
        [tags setSelected];
        
        originX += tags.frame.size.width + 5;
        contentSize += tags.frame.size.width + 5;
        [scrBusiness addSubview:tags];
        [tags release];
        [labelTemp release];
    }];
    
    if([_arrBusiness count] > 0){
        [btnBussinessArea setHidden:YES];
        [scrBusiness setContentSize:CGSizeMake(contentSize, scrBusiness.frame.size.height)];
    }
    else{
        [btnBussinessArea setHidden:NO];
        [scrBusiness setContentSize:CGSizeMake(scrBusiness.frame.size.width, scrBusiness.frame.size.height)];
    }
}

#pragma mark - uitextview delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self hiddenAllClearButton];
    if(textView == txvDescriptions){
        [btnClearDescription setHidden:NO];
        [self animateViewToFrame:CGRectMake(0, -200, self.frame.size.width, self.frame.size.height)];
    }
    else{
        [btnClearProduct setHidden:NO];
        [self animateViewToFrame:CGRectMake(0, -280, self.frame.size.width, self.frame.size.height)];
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    NSString *newString =   [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(textView == txvDescriptions){
        
        if([newString length] == 0){
            [lblDescriptionsPlaceHolder setHidden:NO];
        }
        else{
            [lblDescriptionsPlaceHolder setHidden:YES];
        }
        
        if([newString length] > MAX_DESC){
            return NO;
        }
        
        return YES;
        
    }
    
    if([newString length] == 0){
        [lblProductPlaceHolder setHidden:NO];
    }
    else{
        [lblProductPlaceHolder setHidden:YES];
    }
    
    return YES;
}

#pragma mark - uitextfield delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [self hiddenAllClearButton];
    
    if(txtPhotoTitles == textField){
        [btnClearPhoto setHidden:NO];
    }
    if(textField == txtLocation){
        [btnClearLocation setHidden:NO];
        [self animateViewToFrame:CGRectMake(0, -100, self.frame.size.width, self.frame.size.height)];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self hiddenAllClearButton];
    [self animateFrameToNormal];
    return  [textField resignFirstResponder];
}

#pragma mark - DAMBusinessAreaFiltersViewDelegate

- (void) businessAreaFiltersViewDidCancel:(DAMBusinessAreaFiltersView *)businessAreaView{
    [businessAreaView.view setHidden:YES];
}

- (void)businessAreaFiltersViewDidOK:(DAMBusinessAreaFiltersView *)businessAreaView{
    
    [_arrBusiness removeAllObjects];
    if(!_arrBusiness){
        _arrBusiness    =   [[NSMutableArray alloc] init];
    }
    [_arrBusiness addObjectsFromArray:businessAreaFiltersView.selectedContainers];
    
    [self layoutSelectedTags];
    
    [businessAreaView.view setHidden:YES];
}

#pragma mark - tag label delegate
- (void) removeTags:(DAMTagLabel *)tag{
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         [tag setAlpha:0.0];
                     } completion:^(BOOL finished) {
                         [_arrBusiness removeObjectAtIndex:tag.tag];
                         [businessAreaFiltersView removeSelectedTag:tag.label.text];
                         
                         [self layoutSelectedTags];
                     }];
}

@end
