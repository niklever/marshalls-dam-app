//
//  DAMCaseStudiesView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMMediaViewBase.h"
#import "DAMMapAnnotationView.h"
#import "DAMMapScrollView.h"
#import "DAMNearbyLocationsView.h"

@interface DAMCaseStudiesView : DAMMediaViewBase<UIScrollViewDelegate, DAMNearbyLocationsViewDelegate> {
    DAMMapScrollView *scrollView;
    DAMMapAnnotationView *annotation;
}
@property(nonatomic,retain) IBOutlet DAMMapScrollView *scrollView;
@property(nonatomic,retain) DAMMapAnnotationView *annotation;

@end
