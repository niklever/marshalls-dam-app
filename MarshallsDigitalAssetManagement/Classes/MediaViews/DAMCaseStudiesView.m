//
//  DAMCaseStudiesView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <math.h>
#import "DAMCaseStudiesView.h"
//#import "JSONKit.h"
//#import "ASIHTTPRequest.h"

#define ZOOM_STEP 1.5

float distance(float lat1, float lon1, float lat2, float lon2) {
    float dlat = lat2 - lat1;
    float dlon = lon2 - lon1;
    return sqrt(dlat * dlat + dlon * dlon);
}

@interface DAMCaseStudiesView () {
    NSDictionary *testData;
    UIButton *selectedPin;
    NSDictionary *selectedCaseStudy;
    
    DAMNearbyLocationsView *nearbyLocationsView;
}
@property(nonatomic,retain) NSDictionary *testData;
@property(nonatomic,retain) NSDictionary *selectedCaseStudy;

@property(nonatomic,retain) DAMNearbyLocationsView *nearbyLocationsView;

- (NSArray*)nearbyLocations;

@end

@implementation DAMCaseStudiesView
@synthesize scrollView;
@synthesize annotation;
@synthesize testData;
@synthesize selectedCaseStudy;
@synthesize nearbyLocationsView;
@synthesize queryString = _queryString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        selectedPin = nil;
        //Case Studies are not paged (yet), so set a mentally high page size to
        //get all results from the API call
        self.mediaType = kMediaType_CaseStudies;
        self.pageSize = 100000000;
    }
    return self;
}

const float mapWidth = 2000;
const float mapHeight = 2762;

const float mapPinWidth = 78;
const float mapPinHeight = 110;

const float mapPinTipX = 78 / 2;
const float mapPinTipY = 198 / 2;

const float mapPinAnnotationX = 39;
const float mapPinAnnotationY = 0;

const float mapAnnotationTipX = 168;
const float mapAnnotationTipY = 106;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    scrollView.contentSize = CGSizeMake(mapWidth, mapHeight);
    CGRect startRect = [self zoomRectForScale:0.33 withCenter:CGPointMake(mapWidth/2, mapHeight/2)];
    [scrollView zoomToRect:startRect animated:NO];
    
    UIView *mapView = [scrollView.subviews objectAtIndex:0];
    mapView.userInteractionEnabled = YES;
        
    [self addGestureRecognizers];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Case Studies";
    
    [damNavigator displaySearchBarWithDelegate:self];
    [damNavigator setTitleImageForView:kMediaType_CaseStudies];
    damNavigator.searchBar.cancelButton.hidden = YES;
    //damNavigator.searchBar.businessAreaButton.hidden = YES;
    damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);
    
}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMVideoView.rightButtonTapped index:%i", index);
#endif
    DAMIDriveNavigatorViewController *iDrive;
    switch (index) {
        case 0:
            [self lauchInfo];
            break;
        case 1:
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            break;
        case 2:
//            [self addSendSaveButtons];
            break;
        case 3:
            
            break;
    }
}

- (void)addGestureRecognizers {
    UIView *mapView = [scrollView.subviews objectAtIndex:0];
    mapView.userInteractionEnabled = YES;
    
    // add gesture recognizers to the image view
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];

//TODO single tap gesture interferes with pin buttons
//    [mapView addGestureRecognizer:singleTap];
    [mapView addGestureRecognizer:doubleTap];
    [mapView addGestureRecognizer:twoFingerTap];
    
    [singleTap release];
    [doubleTap release];
    [twoFingerTap release]; 
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.scrollView = nil;
    self.annotation = nil;
    self.testData = nil;
    self.selectedCaseStudy = nil;
    self.nearbyLocationsView = nil;
}

- (void)setQueryString:(NSString *)pQueryString {
    if (pQueryString!= nil){
        _queryString = [pQueryString copy];
        [pQueryString retain];
    }else{
        _queryString = @"";
    }
    
    [self requestModelForPage:0 onCarousel:nil];
}

//TODO - remove soon, using local database now. 
//- (void)requestFinished:(ASIHTTPRequest *)request {
//
//    NSString *responseString = [request responseString];
//    NSDictionary *searchResults = [responseString objectFromJSONString];
//    [self setupWithModel:searchResults];
//}

- (void)localDataGotResult:(NSMutableDictionary*)options {
#ifdef DEBUG
    NSLog(@"DAMCaseStudiesView.localDataGotResult");
#endif
    NSDictionary *result = options;
    [self setupWithModel:result];
}

- (void)setupWithModel:(NSDictionary*)model {
    int index = 0;
    
    UIView *mapView = [scrollView.subviews objectAtIndex:0];
    [mapView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.assets = [model objectForKey:@"assets"];
    
#ifdef DEBUG
    NSLog(@"case studies count == %d", self.assets.count);
#endif
    
    for (NSDictionary *caseStudy in assets) {
        if ([caseStudy objectForKey:@"latitude"] && [caseStudy objectForKey:@"longitude"]) {
            float latitude = [[caseStudy objectForKey:@"latitude"] floatValue];
            float longitude = [[caseStudy objectForKey:@"longitude"] floatValue];
            
            CGPoint pinPoint = [self pointForGPSLatitude:latitude longitude:longitude];
            UIButton *mapPinButton = [[UIButton alloc] initWithFrame:CGRectMake(pinPoint.x, pinPoint.y, mapPinWidth, mapPinHeight)];
#ifdef DEBUG
            NSLog(@"caseStudy == %@ pinPoint: (%f,%f)",caseStudy, pinPoint.x, pinPoint.y);
#endif
            
            mapPinButton.tag = index;
            [mapPinButton setBackgroundImage:[UIImage imageNamed:@"DAM_CaseStudiesMapPin.png"] forState:UIControlStateNormal];
            [mapPinButton addTarget:self action:@selector(mapPinTapped:) forControlEvents:UIControlEventTouchUpInside];
            //        [scrollView addSubview:mapPinButton];
            [mapView addSubview:mapPinButton];
            [mapPinButton release];
        
        } else {
#ifdef DEBUG
            NSLog(@"DAMCaseStudiesView.setupWithModel.ERROR_NO_GPS caseStudy. %@", caseStudy);
#endif
        }
        index++;
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)pScrollView {
//    NSLog(@"scrollViewDidScroll");

    [self clampMapObjectsForScale:pScrollView.zoomScale];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)pScrollView { 
    UIView *viewForZoom = [pScrollView.subviews objectAtIndex:0];
    return viewForZoom;
}    

- (void)scrollViewDidEndZooming:(UIScrollView *)pScrollView withView:(UIView *)view atScale:(float)scale {
#ifdef DEBUG
    NSLog(@"scrollViewDidEndZooming scale:%f", scale);
#endif
    [self clampMapObjectsForScale:scale];
}

- (void)clampMapObjectsForScale:(float)scale {
    float pinScale = 0.33 / scale;
    
    UIView *mapView = [scrollView.subviews objectAtIndex:0];
    for (UIView *pin in mapView.subviews) {
        if (![pin isKindOfClass:[DAMMapAnnotationView class]]) {
            pin.transform = CGAffineTransformMakeScale(pinScale, pinScale);
        } else {
#ifdef DEBUG
            NSLog(@"view was DAMMapAnnotationView");
#endif
        }
    }
    if (annotation) {
        float annotationScale = 1.5 / scale;
        annotation.view.transform = CGAffineTransformMakeScale(annotationScale, annotationScale);
    }
}

#pragma mark TapDetectingImageViewDelegate methods

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    if (selectedPin) {
        selectedPin = nil;
    }
    if (annotation) {
        [annotation.view removeFromSuperview];
        self.annotation = nil;
    }
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // double tap zooms in
    float newScale = [scrollView zoomScale] * ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [scrollView zoomToRect:zoomRect animated:YES];
}

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out

    //Bail out if we are already zoomed out to the MAX
    if (scrollView.zoomScale == scrollView.minimumZoomScale) {
//        NSLog(@"ALREADY ZOOMED OUT, BAIL");
        return;
    }
    
    float newScale = [scrollView zoomScale] / ZOOM_STEP;
    if (newScale < scrollView.minimumZoomScale) {
        newScale = scrollView.minimumZoomScale;
    }
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [scrollView zoomToRect:zoomRect animated:YES];
}

- (void)disclosureButtonTapped:(id)sender {
    if (selectedPin) {
        int assetIndex = selectedPin.tag;
        NSDictionary *asset = [assets objectAtIndex:assetIndex];
#ifdef DEBUG
        NSLog(@"DAMCaseStudiesView.disclosureButtonTapped %@", [self stringForAsset:asset]);
#endif
        [self assetOpened:asset atIndex:assetIndex fromPagedScrollView:nil];
    } else {
#ifdef DEBUG
        NSLog(@"disclosureButtonTapped.NO_SELECTED_PIN");
#endif
    }
}

- (void)cancelButtonTapped:(id)sender {
    if (annotation) {
        [annotation.view removeFromSuperview];
        self.annotation = nil;
    }
    if (nearbyLocationsView) {
        [nearbyLocationsView removeFromSuperview];
        self.nearbyLocationsView = nil;
    }
    selectedPin = nil;
    self.selectedCaseStudy = nil;
}

- (void)nearbyLocationsCancelled {
    [self cancelButtonTapped:nil];
}


- (void)mapPinTapped:(UIButton*)button {
    int tag = button.tag;
    
    selectedPin = button;
    self.selectedCaseStudy = [assets objectAtIndex:tag];
#ifdef DEBUG
    NSLog(@"DAMCaseStudiesView.mapPinTapped tag:%i, %@", tag, [self stringForAsset:selectedCaseStudy]);
#endif
    //bring the tapped button to the front
    UIView *mapView = [scrollView.subviews objectAtIndex:0];    
    [mapView insertSubview:button atIndex:mapView.subviews.count];
     
    if (annotation) {
        [annotation.view removeFromSuperview];
        self.annotation = nil;
    }
    self.annotation = [[DAMMapAnnotationView alloc] initWithNibName:@"DAMMapAnnotationView" bundle:nil];

    float xPos = button.frame.origin.x + mapPinAnnotationX - mapAnnotationTipX - 30;
    float yPos = button.frame.origin.y + mapPinAnnotationY - mapAnnotationTipY;
    annotation.view.frame = CGRectMake(xPos, yPos, annotation.view.frame.size.width, annotation.view.frame.size.height);
    [mapView addSubview:annotation.view];

    annotation.titleLabel.text = [selectedCaseStudy objectForKey:@"title"];
    annotation.locationLabel.text = [selectedCaseStudy objectForKey:@"location"];
    [annotation.image setUrl:[selectedCaseStudy objectForKey:@"thumbnail"] andPlaceholder:@"placeholder-case-study.png"];
    [annotation.disclosureButton addTarget:self action:@selector(disclosureButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    if (nearbyLocationsView) {
        [nearbyLocationsView removeFromSuperview];
        self.nearbyLocationsView = nil;
    }
    NSArray *nearbyLocations = [self nearbyLocations];
    if (nearbyLocations.count > 0) {
        self.nearbyLocationsView = [[DAMNearbyLocationsView alloc] initWithFrame:CGRectMake(1024 - 310, 60, 300, 220)];
        [nearbyLocationsView setNearbyLocations:[self nearbyLocations]];
        nearbyLocationsView.delegate = self;
        [self.view addSubview:nearbyLocationsView];
        [nearbyLocationsView release];
    }
    float zooomTo = 1.5;

    float latitude = [[selectedCaseStudy objectForKey:@"latitude"] floatValue];
    float longitude = [[selectedCaseStudy objectForKey:@"longitude"] floatValue];
    
    CGPoint pinPoint = [self pointForGPSLatitude:latitude longitude:longitude];
    
    CGRect zoomRect = [self zoomRectForScale:zooomTo withCenter:CGPointMake(pinPoint.x, pinPoint.y)];
    [scrollView zoomToRect:zoomRect animated:YES];
}

- (NSArray*)nearbyLocations {
    NSMutableArray *mutableAssets = [NSMutableArray arrayWithCapacity:assets.count];
    for (NSDictionary *asset in assets) {
        NSMutableDictionary *mutableAsset = [NSMutableDictionary dictionaryWithDictionary:asset];
        [mutableAssets addObject:mutableAsset];
    }
    NSArray *sortedAssets = [mutableAssets sortedArrayUsingComparator:^(id obj1, id obj2) {
        NSMutableDictionary *asset1 = obj1;
        NSMutableDictionary *asset2 = obj2;
        float latitude1 = [[asset1 objectForKey:@"latitude"] floatValue];
        float longitude1 = [[asset1 objectForKey:@"longitude"] floatValue];
        float latitude2 = [[asset2 objectForKey:@"latitude"] floatValue];
        float longitude2 = [[asset2 objectForKey:@"longitude"] floatValue];
        
        float pinLatitude = [[selectedCaseStudy objectForKey:@"latitude"] floatValue];
        float pinLongitude = [[selectedCaseStudy objectForKey:@"longitude"] floatValue];
        
        float d1 = distance(pinLatitude, pinLongitude, latitude1, longitude1);
        float d2 = distance(pinLatitude, pinLongitude, latitude2, longitude2);
        
        [asset1 setObject:[NSNumber numberWithFloat:d1] forKey:@"dist"];
        [asset2 setObject:[NSNumber numberWithFloat:d2] forKey:@"dist"];
        
        if (d1 > d2) {
            return NSOrderedDescending;
        } else if (d1 < d2) {
            return NSOrderedAscending;
        } else  {
            return NSOrderedSame;
        }
    }];
    
//Show all locations
    return sortedAssets;

//    NSMutableArray *nearbyLocations = [NSMutableArray arrayWithCapacity:10];
//    int index = 0;
//    for (NSDictionary *asset in sortedAssets) {
//        if (index > 0) {
//            float dist = [[asset objectForKey:@"dist"] floatValue];
//            if (dist < 0.5) {
//                [nearbyLocations addObject:asset];
//            }
//        }
//        index++;
//    }
//    return nearbyLocations;
}

- (void)nearbyCaseStudyTapped:(NSDictionary*)caseStudy {
    NSString *mediaRef = [caseStudy objectForKey:@"mediaRef"];
    BOOL found = NO;
    int index = 0;
    while (!found && index < assets.count) {
        NSString *testMediaRef = [[assets objectAtIndex:index] objectForKey:@"mediaRef"];
        if ([testMediaRef compare:mediaRef] == NSOrderedSame) {
            found = YES;
        } else {
            index++;
        }
    }
    if (found) {
        UIButton *button = nil;
        int viewIndex = 0;
        found = NO;
        UIView *mapView = [scrollView.subviews objectAtIndex:0];
        while (!found && viewIndex < mapView.subviews.count) {
            UIView *view = [mapView.subviews objectAtIndex:viewIndex];
            if ([view isKindOfClass:[UIButton class]]) {
                button = (UIButton *)view;
                if (button.tag == index) {
                    found = YES;
                }
            }
            viewIndex++;
        }
        if (found) {
            [self mapPinTapped:button];
        } else {
#ifdef DEBUG
            NSLog(@"DAMCaseStudiesView.nearbyCaseStudyTapped.ERROR_CANNOT_FIND_BUTTON mediaRef:%@", mediaRef);
#endif
        }
    } else {
#ifdef DEBUG
        NSLog(@"DAMCaseStudiesView.nearbyCaseStudyTapped.ERROR_CANNOT_FIND_CASE_STUDY mediaRef:%@", mediaRef);
#endif
    }
}

- (NSArray*)selectedMedia {
    NSMutableArray *result;
    
    result = selectedCaseStudy ? [NSArray arrayWithObject:selectedCaseStudy] : [NSArray arrayWithObjects:nil];

    return result;
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates. 
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [scrollView frame].size.height / scale;
    zoomRect.size.width  = [scrollView frame].size.width  / scale;
        
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}

//http://wiki.openstreetmap.org/wiki/Mercator#Spherical_Mercator

double y2lat(double a) { return 180/M_PI * (2 * atan(exp(a*M_PI/180)) - M_PI/2); }
double lat2y(double a) { return 180/M_PI * log(tan(M_PI/4+a*(M_PI/180)/2)); }

//Ballyferrier (west most, least long) 52.141074, -10.475464, 405, 2947
//Nr Margate (east most, greatest long) 51.382924, 1.444702, 3485, 3242
//Landewednack (southerly, least lat) 49.959614, -5.203056, 1752, 3898
//Port of ness (northerly, greatest lat) 58.51719, -6.26049, 1601, 281

- (CGPoint)pointForGPSLatitude:(float)latitude longitude:(float)longitude {
//    CGPoint result;
    
    float translationFudgeX = -62 + 34 - 18 + 6;
    float translationFudgeY = -98 + 50 - 24 + 10;
    
//Marshalls UK coverage (Mercator coords)
    float minx = -1268235.78657497;
    float miny = 6284921.78021954;
    float maxx = 195677.712923691;
    float maxy = 8306516.54061641;   
    
    CGPoint point = [self mercatorForLattitude:latitude longitude:longitude];
    
    //normalise on (0,1)
    point.x = (point.x - minx) / (maxx - minx);
    point.y = 1.0 - (point.y - miny) / (maxy - miny);
    
    //scale to map pixel dimensions
    point.x *= mapWidth;
    point.y *= mapHeight;
    
    point.x += translationFudgeX;
    point.y += translationFudgeY;
    
    return point;
}

//based on the Ruby function:

//def deg_to_rad(deg) Math::PI * deg / 180.0 end
//
//def ll_to_m(lat, lon) r=6378100.0; [ r * deg_to_rad(lon), r * Math.log((1 + Math.sin(deg_to_rad(lat))) / Math.cos(deg_to_rad(lat))) ] end

#define DEG_TO_RAD(deg) (M_PI * deg / 180.0)

- (CGPoint)mercatorForLattitude:(float) lattitude longitude:(float)longitude {
    CGPoint result;
    
    float earthR = 6378100.0;
    
    result.x = earthR * DEG_TO_RAD(longitude);
    result.y = earthR * log((1.0 + sin(DEG_TO_RAD(lattitude))) / cos(DEG_TO_RAD(lattitude)));
    
    return result;
}

@end
