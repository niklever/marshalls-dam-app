//
//  DAMMediaViewBase.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 11/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMShareableView.h"
//#import "DAMViewControllerBase.h"
#import "DAMPagedScrollView.h"
#import "DAMMediaSaveView.h"
#import "DAMMediaSendView.h"
#import "DAMIDriveNavigatorViewController.h"
#import "DAMAssetView.h"
#import "DAMPredictiveViewController.h"
#import "DAMBusinessAreaFiltersView.h"
#import "DAMSearchBarView.h"
//#import "JSPhotoLayout.h"

@interface DAMMediaViewBase : DAMShareableView<DAMPagedScrollViewDelegate, DAMIDriveNavigatorViewDelegate, UITextFieldDelegate, DAMAssetViewDelegate, DAMSearchBarViewDelegate, DAMPredictiveViewControllerDelegate, DAMBusinessAreaFiltersViewDelegate> {

    NSString *mediaSourceMethod;
    int pageSize;

    NSArray *assets;
    
    UITextField *activeTextField;
    
    bool needDividePageSize;
    
}

@property (nonatomic, assign)CGRect originalTopScrollViewFrame;
@property (nonatomic, assign)CGRect originalBottomScrollViewFrame;
@property (nonatomic, retain) DAMBusinessAreaFiltersView *businessAreaFiltersView;
@property(nonatomic, retain) NSString *mediaSourceMethod; //@"search_tag", @"search", @"carousel"
@property(nonatomic, retain) NSString *mediaType;
@property(nonatomic, retain) NSString *queryString;
@property int pageSize;
@property bool needDividePageSize;

@property(nonatomic, retain) NSArray *assets;

@property(nonatomic, retain) IBOutlet DAMPagedScrollView *topScrollView;
@property(nonatomic, retain) IBOutlet DAMPagedScrollView *bottomScrollView;

@property(nonatomic, retain) IBOutlet UILabel *homeOwnerLabel;
@property(nonatomic, retain) IBOutlet UILabel *commercialLabel;


@property (nonatomic, retain) DAMPredictiveViewController *predictiveViewController;

- (void)lauchInfo;

- (void)sourceFromCarousel;
- (void)sourceFromQuery:(NSString*)query;
- (void)setMediaSourceMethod:(NSString *)pMediaSourceMethod andQuery:(NSString*)query;

- (void)cancelButtonTapped:(UIButton*)sender;
- (void)addSendSaveButtons;
- (void)saveActionedToFolder:(NSString*)folder;

- (void)cancelMediaSendTapped;
- (void)sendMediaTapped;

- (void)setSingleCarouselWithFrame:(CGRect)frame;

- (NSString*)stringForAsset:(NSDictionary*)asset;

- (void) mediaViewBaseInit;

@end
