//
//  DAMMarkerPhotoView.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 8/27/14.
//
//

#import "DAMMarkerPhotoView.h"
#import "ImageHelper.h"

@interface DAMMarkerPhotoView(){
    
    __weak IBOutlet UIImageView *imvPhoto;
    __weak IBOutlet UIActivityIndicatorView *indicator;
    
    void (^dismissBlock)(void);
}

@end

@implementation DAMMarkerPhotoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - selector
- (void) showImage:(NSString *)imageURL{
    
    [indicator startAnimating];
    
    [imvPhoto sd_setImageWithURL:[NSURL URLWithString:imageURL]
                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                           [indicator stopAnimating];
//                           if (image) {
//                               if (image.size.height > imvPhoto.frame.size.height) {
//                                   float hratio = image.size.height / imvPhoto.frame.size.height;
//                                   float newwidth = image.size.width / hratio;
//                                   if (newwidth < imvPhoto.frame.size.width) {
//                                       //then we need to crop image to bottom part
//                                       image = [image crop: CGRectMake(0, image.size.height - imvPhoto.frame.size.height, imvPhoto.frame.size.width, imvPhoto.frame.size.height)];
//                                   }else{
//                                       image = [image scaledToSize: CGSizeMake(newwidth, imvPhoto.frame.size.height)];
//                                   }
//                               }
//                               imvPhoto.opaque  =   YES;
//                               imvPhoto.alpha   =   1.0;
//                               imvPhoto.image   =   image;
//                           }
                           
//                           [imvPhoto setImage:image];
                           
                           int width    =   image.size.width;
                           int height   =   image.size.height;
                           
                           if(width > 1024){
                               width    =   width   -   1024;
                           }
                           
                           if(height > 768){
                               height = height - 768;
                           }
                           
                           int x    =   abs((width - 1024)) / 2;
                           int y    =   abs((height - 768)) / 2;
                           [imvPhoto setFrame:CGRectMake(x, y, width, height)];
                           [imvPhoto setImage:image];
                           
                       }];
    
}

- (void) createDissmissBlock:(void (^)(void))block{
    dismissBlock    =   block;
}

#pragma mark - ibaction
- (IBAction)didClickDismiss:(id)sender {
    if(dismissBlock){
        dismissBlock();
    }
}


@end
