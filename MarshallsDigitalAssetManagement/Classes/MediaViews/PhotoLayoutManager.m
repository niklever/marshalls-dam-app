//
//  LayoutManager.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoLayoutManager.h"
//#import "ASIDownloadCache.h"
#import "DAMImageView.h"
#import "DAMApplicationModel.h"
#import "UIImageView+WebCache.h"
#import "ImageHelper.h"
#import <UIKit/UIKit.h>
#import "DAMUIDefaults.h"

@interface ItemMetadata : NSObject {
    int sequenceNumber;
    int pageNumber;
    float x, y, width, height;
    float backgroundLeft, backgroundTop;
    NSString *imageUrl;
}

@property int sequenceNumber;
@property int pageNumber;
@property float x;
@property float y;
@property float width;
@property float height;
@property float backgroundLeft;
@property float backgroundTop;
@property int priority;
@property (nonatomic, copy) NSString *imageUrl;

@end

@implementation ItemMetadata
@synthesize priority;
@synthesize sequenceNumber;
@synthesize pageNumber;
@synthesize x;
@synthesize y;
@synthesize width;
@synthesize height;
@synthesize imageUrl;
@synthesize backgroundLeft;
@synthesize backgroundTop;

- (void)dealloc {
    self.imageUrl = nil;
    [super dealloc];
}

@end

@interface UIPhotoPageView() {
    
    int pageNumber;
}

@end

@implementation UIPhotoPageView

@synthesize pageNumber;

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    NSEnumerator *reverseE = [self.subviews reverseObjectEnumerator];
    UIView *iSubView;
    
    while ((iSubView = [reverseE nextObject])) {    
        UIView *viewWasHit = [iSubView hitTest:[self convertPoint:point toView:iSubView] withEvent:event];
        if(viewWasHit) {
            return viewWasHit;
        }    
    }
    return [super hitTest:point withEvent:event];
}
@end


@interface PhotoLayoutManager() {
    
    id<PhotoLayoutManagerDelegate> delegate;
    
    float maxX;
    
    
}
@property (nonatomic, retain)NSMutableSet *visiblePages;
@property (nonatomic, retain)NSMutableSet *recycledPages;
@property (nonatomic, retain) NSMutableArray *pageMetadata;

@end

@implementation PhotoLayoutManager
@synthesize delegate;
@synthesize maxX;
@synthesize pageMetadata;
@synthesize visiblePages;
@synthesize recycledPages;

- (id)init {
    if ((self = [super init])) {
        self.visiblePages = [[NSMutableSet alloc] init];
        self.recycledPages = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)dealloc {
    self.pageMetadata = nil;
    [visiblePages release];
    [recycledPages release];
    [super dealloc];
}

- (int)numPages {
    return self.pageMetadata.count;
}

- (float)floatForKey:(NSString*)key inDict:(NSDictionary*)dict withDefault:(float)defaultValue {
    float result = defaultValue;
    id obj = [dict objectForKey:key];
    if (obj && obj != [NSNull null]) {
        result = [obj floatValue];
    } else {
        
#ifdef DEBUG
        NSLog(@"KEY_NOT_FOUND key:%@ item:%@", key, dict);
#endif
        
    }
    return result;
}

- (void)reset {
    
    /*for (UIPhotoPageView *page in self.visiblePages) {
        [[page subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[DAMImageView class]]) {
                [(DAMImageView *) obj cancelCurrentImageLoad];
                //[obj release];
            }
        }];
        [page.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [page removeFromSuperview];
    }*/
    
    [self.visiblePages removeAllObjects];
    [self.recycledPages removeAllObjects];
    //[self.visiblePages release];
    //[self.recycledPages release];
    //self.visiblePages = nil;
    //self.recycledPages = nil;
    if (self.pageMetadata != nil && self.pageMetadata.count > 0) {
        [self.pageMetadata removeAllObjects];
        self.pageMetadata = nil;
    }
}

- (void)layoutItems:(NSArray*)items {
    
    
#ifdef DEBUG
    NSLog(@"PhotoLayoutManager.layoutItems items.count:%i", items.count);
#endif
    
    int numPages = 0;
    
    maxX = 0;
    for (NSDictionary *item in items) {
        id idLayoutX = [item objectForKey:@"layoutX"];
        id idLayoutY = [item objectForKey:@"layoutY"];
        id idLayoutW = [item objectForKey:@"layoutW"];
        id idLayoutH = [item objectForKey:@"layoutH"];
        if (idLayoutX != [NSNull null] && idLayoutY != [NSNull null] && idLayoutW != [NSNull null] && idLayoutH != [NSNull null]) {
            float layoutX = [idLayoutX floatValue];
            //NSLog(@"************** layoutX:%f", layoutX);
            float layoutW = [idLayoutW floatValue];
            maxX = MAX(maxX, layoutX + layoutW);
            int page = floorf(layoutX / 1024);
            numPages = MAX(numPages, page);
        } else {
            
#ifdef DEBUG
            NSLog(@"PhotoLayoutManager.layoutItems.ERROR_UNEXPECTED_ITEM item:%@", item);
#endif
            
        }
    }
    numPages++;
    
    if (self.pageMetadata) {
        int necessaryExpansion = numPages - self.pageMetadata.count;

#ifdef DEBUG
        NSLog(@"layoutItems.EXPANSION_RUN necessaryExpansion:%i", necessaryExpansion);
#endif
        
        for (int i = 0; i < necessaryExpansion; i++) {
            NSMutableArray *newPage = [NSMutableArray arrayWithCapacity:8];
            [pageMetadata addObject:newPage];
        }
        //        [delegate experiencedContentExpansion];
    } else {

#ifdef DEBUG
        NSLog(@"layoutItems.FIRST_RUN numPages:%i", numPages);
#endif
        
        //NSLog(@"layoutItems.FIRST_RUN numPages:%i", numPages);
        
        self.pageMetadata = [NSMutableArray arrayWithCapacity:numPages];
        for (int i = 0; i < numPages; i++) {
            //NSMutableArray *newPage = [NSMutableArray arrayWithCapacity:8];
            //NSMutableArray *newPage = [NSMutableArray array];
            [self.pageMetadata addObject: [NSMutableArray array]];
        }
    }
    
    for (NSDictionary *item in items) {
        NSString *url = [item objectForKey:@"thumbnail"];
        
        float layoutX = [self floatForKey:@"layoutX" inDict:item withDefault:0.0];
        float layoutY = [self floatForKey:@"layoutY" inDict:item withDefault:0.0];
        float thumbWidth = [self floatForKey:@"layoutW" inDict:item withDefault:0.0];
        float thumbHeight = [self floatForKey:@"layoutH" inDict:item withDefault:0.0];
        //        float backgroundLeft = [[item objectForKey:@"backgroundLeft"] floatValue];
        //        float backgroundTop = [[item objectForKey:@"backgroundTop"] floatValue];  
        int sequenceNumber = [[item objectForKey:@"sequenceNumber"] intValue];
        
        int page = floorf(layoutX / 1024);
        
        //NSString *mediaRef = [item objectForKey:@"mediaRef"];
        int priority = [[item objectForKey:@"priority"] intValue];
        
        //NSMutableArray *currentPage = [self.pageMetadata objectAtIndex:page];
        ItemMetadata *meta = [[ItemMetadata alloc] init];
        meta.sequenceNumber = sequenceNumber;
        meta.pageNumber = page;
        meta.x = layoutX;
        meta.y = layoutY;
        meta.width = thumbWidth;
        meta.height = thumbHeight;
        meta.imageUrl = url;
        meta.priority = priority;
        
        [(NSMutableArray *)[self.pageMetadata objectAtIndex:page] addObject: [meta retain]];
        [meta release];
    }
    
#ifdef DEBUG
    NSLog(@"PhotoLayoutManager.layoutItems.DONE items.count:%i, pages:%i", items.count, numPages);
#endif
    
}

- (void)configurePage:(UIPhotoPageView*)page forIndex:(int)index callback:(void (^)(BOOL isValid))successCalback{
    
    //NSLog(@"PhotoLayoutManager.configurePage index:%i imageCount:%i", index, [DAMImageView imageCount]);
    
    page.userInteractionEnabled = YES;
    page.clipsToBounds = NO;
    
    if (index < 0 || index >= self.pageMetadata.count) {
        //NSLog(@"configurePage.BAD_INDEX index:%i", index);
        if (successCalback) {
            successCalback(NO);
        }
        return;
    }
    //clear out any existing photos
    [page.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass: [DAMImageView class]]) {
            [(DAMImageView *) obj cancelCurrentImageLoad];
            //[obj release];
        }
    }];
    [page.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    NSMutableArray *pageMeta = [NSMutableArray arrayWithArray:[self.pageMetadata objectAtIndex:index]];
    
    float minLayoutX = MAXFLOAT;
    float maxLayoutX = 0;
    for (int i = 0 ; i < pageMeta.count ; i++) {
        ItemMetadata *meta = (ItemMetadata *)[pageMeta objectAtIndex:i];
            minLayoutX = MIN(minLayoutX, meta.x);
            maxLayoutX = MAX(maxLayoutX, meta.x + meta.width);
    }
    
    //NSLog(@"configurePage index:%i imageCount:%i", index, pageMeta.count);
    
    //    float pageLeft = 1024 * index;
    
    float pageRight = maxLayoutX;//1024 * (index + 1);
    float pageLeft = minLayoutX;
    
    //    NSLog(@"pageLeft:%f pageRight:%f", pageLeft, pageRight);
    //    CGRect frame = CGRectMake(pageLeft, 0, pageRight - pageLeft, 748);
    
    pageLeft = 1024 * (index + 0.5);
    pageRight = pageLeft + 1;
    CGRect frame = CGRectMake(pageLeft, 0, pageRight - pageLeft, 748);
    page.frame = frame;
    page.pageNumber = index;
    
#ifdef DEBUG_PRIORITY
        //NSArray *pageCols = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], [UIColor blueColor], nil];
        //page.backgroundColor = [pageCols objectAtIndex:(index % 3)];
#endif
    
    int createImages = 0;
    ItemMetadata *meta = nil;
    for (int i = 0 ; i < pageMeta.count ; i++) {
        meta = (ItemMetadata *)[pageMeta objectAtIndex:i];
        /*
        if ([obj isKindOfClass:[ItemMetadata class]]) {
            meta = obj;
        }else{
            NSLog(@"meta obj %@", obj);
            continue;
        }*/
        
        
        float itemLeft = meta.x - pageLeft;
        //NSLog(@"page == %d , itemLeft == %f", index, itemLeft);
        DAMImageView *image = [[DAMImageView alloc] initWithFrame:CGRectMake(itemLeft, meta.y, meta.width, meta.height)];
        
        //image.bounds = CGRectMake(-meta.backgroundLeft, -meta.backgroundTop, meta.width, meta.height);
        
        image.contentMode = UIViewContentModeTopLeft;
        image.clipsToBounds = YES;
        image.model = [delegate modelForItemSequenceNumber:meta.sequenceNumber];
        
        image.backgroundColor = [UIColor colorWithRed:1 green:0.5 blue:0.5 alpha:0.25];
        image.tag = meta.sequenceNumber;
        
        [page addSubview:image];
        
        NSString *apiHost = [[DAMApplicationModel model] apiHost];
        NSString *fullUrl = [NSString stringWithFormat:@"%@%@", apiHost, meta.imageUrl];
        
        //NSLog(@"load image url == %@", fullUrl);
        [image sd_setImageWithURL: [NSURL URLWithString: fullUrl]
                        completed:^(UIImage *img, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            if (img) {
                                if (img.size.height > meta.height) {
                                    float hratio = image.image.size.height / meta.height;
                                    float newwidth = image.image.size.width / hratio;
                                    if (newwidth < meta.width) {
                                        //then we need to crop image to bottom part
                                        image.image = [img crop: CGRectMake(0, image.image.size.height - meta.height, meta.width, meta.height)];
                                    }else{
                                        image.image = [img scaledToSize: CGSizeMake(newwidth, meta.height)];
                                    }
                                }
                                image.opaque = YES;
                                image.alpha = 1.0;
                            }
                        }];
        
        /*
        [image setImageWithURL: [NSURL URLWithString: fullUrl] success:^(UIImage *img) {
            //trying to scale image to fit the box here
            
            if (image.image.size.height > meta.height) {
                float hratio = image.image.size.height / meta.height;
                float newwidth = image.image.size.width / hratio;
                if (newwidth < meta.width) {
                    //then we need to crop image to bottom part
                       image.image = [img crop: CGRectMake(0, image.image.size.height - meta.height, meta.width, meta.height)];
                }else{
                    image.image = [img scaledToSize: CGSizeMake(newwidth, meta.height)];
                }
            }
            image.opaque = YES;
            image.alpha = 1.0;
        } failure:^(NSError *error) {

#ifdef DEBUG
            NSLog(@"PhotoLayoutManager.requestFailed request.url:%@ error:%@", fullUrl, error);
#endif
        }];
         */
        
        if (image.image) {
            if (image.image.size.height > meta.height) {
                float hratio = image.image.size.height / meta.height;
                float newwidth = image.image.size.width / hratio;
                if (newwidth < meta.width) {
                    //then we need to crop image to bottom part
                    image.image = [image.image crop: CGRectMake(0, image.image.size.height - meta.height, meta.width, meta.height)];
                }else{
                    image.image = [image.image scaledToSize: CGSizeMake(newwidth, meta.height)];
                }
            }
            //image.image = [image.image scaledToSize: CGSizeMake(meta.width, meta.height)];
            image.opaque = YES;
            image.alpha = 1.0;
        }
        
#ifdef DEBUG_PRIORITY
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, meta.width, meta.height)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = [DAMUIDefaults defaults].mediaSendFont;
        lbl.textColor = [UIColor redColor];
        lbl.text = [NSString stringWithFormat:@"%d", meta.priority];
        [image addSubview: lbl];
        [lbl release];
#endif
        
        
        [image release];
        createImages++;
    }
    if (delegate) {
        [delegate postPageConfiguration:page];
    }
    //NSLog(@"PhotoLayoutManager.configurePage.DONE index:%i imageCount:%i, createImages:%i", index, [DAMImageView imageCount], createImages);
    if (successCalback) {
        successCalback(YES);
    }
    return;
}


- (void)tilePagesOnView:(UIView*)view {
    //Calculate which pages should now be visible
    
    CGRect visibleBounds = view.bounds;
    int firstNeededPage = floor(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
    int lastNeededPage = floor((CGRectGetMaxX(visibleBounds) - 1.0) / CGRectGetWidth(visibleBounds));
    firstNeededPage = MAX(0, firstNeededPage - 1);
    lastNeededPage = MIN(lastNeededPage + 1, self.numPages);
    
    //NSLog(@"Need pages %i -> %i self.numPages == %d ; self.visiblePages == %d, self.recycledPages == %d", firstNeededPage, lastNeededPage, self.numPages, self.visiblePages.count, self.recycledPages.count);
    
    //Recycle no-longer needed pages
    for (UIPhotoPageView *page in self.visiblePages) {
        if (page.pageNumber < firstNeededPage || page.pageNumber > lastNeededPage) {
            if (![self.recycledPages containsObject: page]) {
                [self.recycledPages addObject:page];
            }
            
            [[page subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if ([obj isKindOfClass:[DAMImageView class]]) {
                    [(DAMImageView *) obj cancelCurrentImageLoad];
                    //[obj release];
                }
            }];
            [page.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [page removeFromSuperview];
        }
    }
    
    [self.visiblePages minusSet: self.recycledPages];
    
    //[(NSMutableArray *)self.visiblePages removeObjectsInArray: self.recycledPages];
    
    //add missing pages
    
    for (int index = firstNeededPage; index <= lastNeededPage; index++) {
        if (![self isDisplayingPageForIndex:index]) {
            UIPhotoPageView *page = [self dequeueRecycledPage];
            if (!page) {
                //                page = [lm contentForPage:index];
                page = [[[UIPhotoPageView alloc] init] autorelease];
            }
            //[self configurePage:page forIndex:index];
            [self configurePage:page forIndex:index callback:^(BOOL isValid) {
                if (isValid) {
                    [view addSubview:page];
                    //NSLog(@"new page to visible == %d", page.pageNumber);
                    [self.visiblePages addObject:page];
                }
            }];
        }
    }
    //NSLog(@"self.visiblePages count == %d", self.visiblePages.count);
}

- (void)tilePagesOnViewV2:(UIView*)view {
    //Calculate which pages should now be visible
    
    CGRect visibleBounds = view.bounds;
    int firstNeededPage = floor(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
    int lastNeededPage = floor((CGRectGetMaxX(visibleBounds) - 1.0) / CGRectGetWidth(visibleBounds));
    
    firstNeededPage = MAX(0, firstNeededPage - 1);
    lastNeededPage = MIN(lastNeededPage + 1, self.numPages);
    
    
    //NSLog(@"Need pages %i -> %i == visiblePages count == %d", firstNeededPage, lastNeededPage, visiblePages.count);
    
    //Recycle no-longer needed pages
    
    /*for (UIPhotoPageView *page in visiblePages) {
        if (page.pageNumber < firstNeededPage || page.pageNumber > lastNeededPage) {
            NSLog(@"remove page num == %d", page.pageNumber);
            [recycledPages addObject:page];
            [page removeFromSuperview];
            //[(NSMutableArray *)visiblePages removeObject: page];
        }
    }*/
    for (int pageNumber = 0 ; pageNumber < self.numPages ; pageNumber++) {
        if (pageNumber < firstNeededPage || pageNumber > lastNeededPage) {
            //need to remove
            [[view viewWithTag: 500 + pageNumber] removeFromSuperview];
        }
    }
    
    //[visiblePages minusSet:recycledPages];
    
    //add missing pages
    
    for (int index = firstNeededPage; index <= lastNeededPage; index++) {
        /*if ([view viewWithTag: 500 + index] == NULL) {
            UIPhotoPageView *page = [[[UIPhotoPageView alloc] init] autorelease];
            page.tag = 500 + index;
            [self configurePage:page forIndex:index];
            [view addSubview:page];
            //[visiblePages addObject:page];
        }*/
        
        /*
        if (![self isDisplayingPageForIndex:index]) {
            UIPhotoPageView *page = [self dequeueRecycledPage];
            if (!page) {
                //NSLog(@"create new page");
                //page = [lm contentForPage:index];
                page = [[[UIPhotoPageView alloc] init] autorelease];
            }
            [self configurePage:page forIndex:index];
            [view addSubview:page];
            [self.visiblePages addObject:page];
        }*/
    }
}

- (BOOL)isDisplayingPageForIndex:(int)index {
    __block BOOL result = NO;
    [self.visiblePages enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        UIPhotoPageView *page = obj;
        result = (page.pageNumber == index);
        *stop = result;
    }];
    return result;
}

- (UIPhotoPageView*)dequeueRecycledPage {
    //UIPhotoPageView *page = [self.recycledPages anyObject];
    UIPhotoPageView *page = [[self.recycledPages allObjects] lastObject];
    if (page) {
        [[page retain] autorelease];
        [self.recycledPages removeObject:page];
    }
    return page;
}
/*
- (void)requestFinished:(ASIHTTPRequest *)request {
    [self performSelectorInBackground:@selector(bgImageUpdate:) withObject:request];
}


- (void)bgImageUpdate:(ASIHTTPRequest*)request {
    UIImageView *imageView = [request.userInfo objectForKey:@"image"];
    UIImage *image = [UIImage imageWithData:request.responseData];
    imageView.image = image;
}
*/
/*
- (void)requestFailed:(ASIHTTPRequest *)request {
#ifdef DEBUG
    NSLog(@"PhotoLayoutManager.requestFailed request.url:%@ error:%@", request.url, [request error]);
#endif
    
}
*/

@end
