//
//  DAMPhotoView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMMediaViewBase.h"
#import "DAMPagedScrollView.h"
#import "DAMTagView.h"
#import "DAMTagLabel.h"
#import "DAMPhotoScrollView.h"

@interface DAMPhotoView : DAMMediaViewBase<DAMTagViewDelegate, DAMTagLabelDelegate, DAMPhotoScrollViewDelegate> {
    NSMutableDictionary *layoutQueue;
    
    //TODO - mediaMap may be useful in the superclass?
    NSDictionary *mediaMap;
}
@property (nonatomic, retain) NSMutableDictionary *layoutQueue;
@property (nonatomic, retain) NSDictionary *mediaMap;

@property (nonatomic, retain) IBOutlet DAMPhotoScrollView *photoScrollView;
@property (nonatomic, retain) IBOutlet UILabel *statusLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, retain) UIImageView *noResultsView;

@end
