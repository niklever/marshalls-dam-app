//
//  DAMMarkerPhotoView.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 8/27/14.
//
//

#import <UIKit/UIKit.h>

@interface DAMMarkerPhotoView : UIView

- (void) showImage:(NSString *)imageURL;
- (void) createDissmissBlock:(void (^)(void))block;

@end
