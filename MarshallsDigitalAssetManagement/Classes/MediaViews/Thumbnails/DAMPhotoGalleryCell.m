//
//  DAMPhotoGalleryCell.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/30/14.
//
//

#import "DAMPhotoGalleryCell.h"

@implementation DAMPhotoGalleryCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    [imvThumbnail release];
    [super dealloc];
}

#pragma mark - selector
- (void) loadData:(UIImage *)image{
    
    [imvThumbnail setImage:image];
    
}

@end
