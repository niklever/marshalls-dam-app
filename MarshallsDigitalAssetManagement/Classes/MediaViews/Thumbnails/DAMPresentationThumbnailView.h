//
//  DAMPresentationThumbnailView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 03/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMImageView.h"
#import "DAMThumbnailBaseView.h"

@interface DAMPresentationThumbnailView : DAMThumbnailBaseView

@property (nonatomic, retain) IBOutlet DAMImageView *thumbnailImage;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *subTitleLabel;

@end
