//
//  DAMView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 01/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMView : UIView

@property (nonatomic, retain) NSDictionary *model;

- (void)cancelSelection;

@end
