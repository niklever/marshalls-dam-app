//
//  DAMInThePressThumbnailView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 22/8/14.
//
//

#import "DAMInThePressThumbnailView.h"
#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"

@interface DAMInThePressThumbnailView () {
    
}

@end

@implementation DAMInThePressThumbnailView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.titleLabel.font = [[DAMUIDefaults defaults] thumbnailTitleFont];
    self.subTitleLabel.font = [[DAMUIDefaults defaults] thumbnailSubtitleFont];
}

- (void)updateDisplay {
    
    //    float textRightPad = 8;
    self.titleLabel.text = [self.model objectForKey:@"title"];
    self.subTitleLabel.text = [self.model nonNullValueForKey:@"description"];
    
    
    CGSize textSize =  DAM_MULTILINE_TEXTSIZE([self.model objectForKey:@"title"], self.titleLabel.font, self.titleLabel.frame.size, NSLineBreakByWordWrapping);
    //float titleLabelWidth = MIN(textSize.width, self.view.frame.size.width);
    float titleLabelWidth = MIN(textSize.width + 50.0, self.titleLabel.frame.size.width);
    CGRect labelFrame = self.titleLabel.frame;
    labelFrame.size.width = titleLabelWidth;
    labelFrame.size.height = textSize.height;
    
    self.titleLabel.frame = labelFrame;
    
    if ([self.model nonNullValueForKey:@"description"].length > 0) {
        textSize = DAM_MULTILINE_TEXTSIZE([self.model objectForKey:@"description"], self.subTitleLabel.font, self.subTitleLabel.frame.size, NSLineBreakByWordWrapping);
        titleLabelWidth = MIN(textSize.width + 50.0, self.subTitleLabel.frame.size.width);
        CGRect slabelFrame = self.subTitleLabel.frame;
        slabelFrame.size.width = titleLabelWidth;
        slabelFrame.origin.y = self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height;
        
        self.subTitleLabel.frame = slabelFrame;
        self.subTitleLabel.numberOfLines = 5;
    }else{
        self.subTitleLabel.hidden = YES;
    }
    
    //    NSString *thumbnail = [self.model objectForKey:@"thumbnail"];
    //    NSString *imageUrl;
    //    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1)         {
    //        imageUrl = thumbnail;
    //    } else {
    //        imageUrl = [NSString stringWithFormat:@"%@?MaxWidth=%i", thumbnail, (int)thumbnailImage.frame.size.width];
    //    }
    NSString *imageUrl = [[DAMApplicationModel model] thumbnailForAsset:self.model inView: self.thumbnailImage];
    
    
    [self.thumbnailImage setUrl:imageUrl andPlaceholder:@"placeholder-brochure.png"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    [self createRoundedCorner];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.thumbnailImage = nil;
    self.titleLabel= nil;
    self.subTitleLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

@end

