//
//  DAMInThePressThumbnailView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 22/8/14.
//
//

#import <Foundation/Foundation.h>
#import "DAMImageView.h"
#import "DAMThumbnailBaseView.h"

@interface DAMInThePressThumbnailView : DAMThumbnailBaseView

@property (nonatomic, retain) IBOutlet DAMImageView *thumbnailImage;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *subTitleLabel;

@end
