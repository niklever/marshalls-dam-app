//
//  DAMView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 01/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMView.h"

@interface DAMView() {
    NSDictionary *model;
}

@end

@implementation DAMView
@synthesize model;

- (void)dealloc {
    [super dealloc];
    if (model) {
        self.model = nil;    
    }   
}

- (void)cancelSelection {
    for (UIView *view in self.subviews) {
        if ([view respondsToSelector:@selector(cancelSelection)]) {
            [view performSelector:@selector(cancelSelection)];
        }
    }
}

- (NSNumber*)isSelected {
    NSNumber *result = [NSNumber numberWithInt:0];
    for (UIView *view in self.subviews) {
        if ([view respondsToSelector:@selector(isSelected)]) {
            result = [view performSelector:@selector(isSelected)];
            if ([result boolValue]) {
                return result;
            }
        }
    }
    return result;
}

- (void)toggleSelection {
    for (UIView *view in self.subviews) {
        if ([view respondsToSelector:@selector(toggleSelection)]) {
            [view performSelector:@selector(toggleSelection)];
        }
    }
}

@end
