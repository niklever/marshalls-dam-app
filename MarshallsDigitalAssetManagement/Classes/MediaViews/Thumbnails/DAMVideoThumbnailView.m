//
//  DAMVideoThumbnailView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 30/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMVideoThumbnailView.h"
#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"

@interface DAMVideoThumbnailView () {
    
    DAMImageView *thumbnailImage;
    UILabel *titleLabel;
    UILabel *subTitleLabel;
}

@end

@implementation DAMVideoThumbnailView

@synthesize thumbnailImage;
@synthesize titleLabel;
@synthesize subTitleLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    titleLabel.font = [[DAMUIDefaults defaults] thumbnailTitleFont];
    subTitleLabel.font = [[DAMUIDefaults defaults] thumbnailSubtitleFont];
}

- (void)updateDisplay {
    titleLabel.text = [self.model objectForKey:@"title"];
    subTitleLabel.text = [self.model objectForKey:@"location"];
    
    CGSize textSize = [titleLabel.text sizeWithFont:titleLabel.font];
    //float titleLabelWidth = MIN(textSize.width, self.view.frame.size.width);
    float titleLabelWidth = MIN(textSize.width + 50.0, titleLabel.frame.size.width);
    CGRect labelFrame = titleLabel.frame;
    labelFrame.size.width = titleLabelWidth;
    titleLabel.frame = labelFrame;
    
    //    NSString *thumbnail = [self.model objectForKey:@"thumbnail"];
    //    NSString *imageUrl;
    //    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1)         {
    //        imageUrl = thumbnail;
    //    } else {
    //        imageUrl = [NSString stringWithFormat:@"%@?MaxWidth=%i", thumbnail, (int)thumbnailImage.frame.size.width];
    //    }
    NSString *imageUrl = [[DAMApplicationModel model] thumbnailForAsset:self.model inView:thumbnailImage];
    
    [thumbnailImage setUrl:imageUrl andPlaceholder:@"placeholder-video.png"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self createRoundedCorner];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.thumbnailImage = nil;
    self.titleLabel= nil;
    self.subTitleLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

@end
