//
//  DAMThumbnailBaseView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 30/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMThumbnailBaseView.h"
#import "DAMView.h"

@interface DAMThumbnailBaseView () {

//    NSDictionary *model;
}
@end

@implementation DAMThumbnailBaseView
//@synthesize model;
//@synthesize view;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//	// Do any additional setup after loading the view.
//}
//
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
//    self.model = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)updateDisplay {
    
}

- (NSDictionary*)model {
    NSDictionary *result = nil;
    if ([self.view isKindOfClass:[DAMView class]]) {
        result = ((DAMView*)self.view).model;
    } else {
#ifdef DEBUG
        NSLog(@"DAMThumbnailBaseView.model.ERROR_VIEW_WRONG_CLASS self.view:%@", self.view);
#endif
    }
    return result;
}

- (void)setModel:(NSDictionary*)newModel {
    if ([self.view isKindOfClass:[DAMView class]]) {
        ((DAMView*)self.view).model = newModel;
    } else {
#ifdef DEBUG
        NSLog(@"DAMThumbnailBaseView.setModel.ERROR_VIEW_WRONG_CLASS self.view:%@", self.view);
#endif
    }
}

@end
