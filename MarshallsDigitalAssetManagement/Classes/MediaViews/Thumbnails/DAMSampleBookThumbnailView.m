//
//  DAMSampleBookThumbnailView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DAMSampleBookThumbnailView.h"
#import "DAMApplicationModel.h"
#import "DAMUIDefaults.h"

@interface DAMSampleBookThumbnailView () {
    
    DAMImageView *thumbnailImage;
    UIView *labelBackground;
    UILabel *titleLabel;
    UILabel *subTitleLabel;
}
@property (nonatomic, retain) IBOutlet UIView *labelBackground;

@end

@implementation DAMSampleBookThumbnailView

@synthesize thumbnailImage;
@synthesize labelBackground;
@synthesize titleLabel;
@synthesize subTitleLabel;

const float cornerRadius = 8;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    titleLabel.font = [[DAMUIDefaults defaults] thumbnailTitleFont];
    subTitleLabel.font = [[DAMUIDefaults defaults] thumbnailSubtitleFont];
    self.view.backgroundColor = [UIColor colorWithRed:1 green:0.5 blue:0.5 alpha:0.25];
}

- (void)updateDisplay {
    if (self.model) {
        float textRightPad = 8;
        titleLabel.text = [self.model objectForKey:@"title"];
        subTitleLabel.text = [self.model objectForKey:@"colour"];
        
        CGSize textSize = [titleLabel.text sizeWithFont:titleLabel.font];
        //float titleLabelWidth = MIN(textSize.width, self.view.frame.size.width);
        float titleLabelWidth = MIN(textSize.width + 50.0, titleLabel.frame.size.width);
        CGRect labelFrame = titleLabel.frame;
        labelFrame.size.width = titleLabelWidth;
        titleLabel.frame = labelFrame;
        
        textSize = [subTitleLabel.text sizeWithFont:subTitleLabel.font];
        float subTitleLabelWidth = MIN(textSize.width, self.view.frame.size.width - textRightPad);
        labelFrame = subTitleLabel.frame;
        labelFrame.size.width = subTitleLabelWidth;
        subTitleLabel.frame = labelFrame;
        
        float widestLabel = MAX(titleLabelWidth, subTitleLabelWidth - textRightPad);
        labelFrame = labelBackground.frame;
        labelFrame.size.width = widestLabel + textRightPad + titleLabel.frame.origin.x + cornerRadius;
        labelBackground.frame = labelFrame;
        
        NSString *imageUrl = [[DAMApplicationModel model] thumbnailForAsset:self.model inView:self.view];
        
        //NSLog(@"imageUrl %@", imageUrl);
        
        [thumbnailImage setUrl:imageUrl andPlaceholderIndex:3];
    }else{
        titleLabel.hidden = YES;
        subTitleLabel.hidden = YES;
        labelBackground.hidden = YES;
        thumbnailImage.hidden = YES;
    }
    [self createRoundedCorner];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.thumbnailImage = nil;
    self.labelBackground = nil;
    self.titleLabel= nil;
    self.subTitleLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (void)createRoundedCorner {
    // Create the path (with only the top-left corner rounded)
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:labelBackground.bounds 
                                                   byRoundingCorners:UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = labelBackground.bounds;
    maskLayer.path = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the image view's layer
    labelBackground.layer.mask = maskLayer;      
}

@end
