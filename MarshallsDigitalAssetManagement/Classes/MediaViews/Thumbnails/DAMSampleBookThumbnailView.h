//
//  DAMSampleBookThumbnailView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMImageView.h"
#import "DAMThumbnailBaseView.h"

@interface DAMSampleBookThumbnailView : DAMThumbnailBaseView

@property (nonatomic, retain) IBOutlet DAMImageView *thumbnailImage;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *subTitleLabel;

@end
