//
//  DAMPhotoGalleryCell.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/30/14.
//
//

#import <UIKit/UIKit.h>

@interface DAMPhotoGalleryCell : UICollectionViewCell{
    
    IBOutlet UIImageView *imvThumbnail;
}

- (void) loadData:(UIImage *)image;

@end
