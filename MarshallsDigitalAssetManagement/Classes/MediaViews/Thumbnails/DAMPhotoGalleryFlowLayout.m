//
//  DAMPhotoGalleryFlowLayout.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/30/14.
//
//

#import "DAMPhotoGalleryFlowLayout.h"
#import "DAMAppDelegate.h"

@implementation DAMPhotoGalleryFlowLayout

- (CGSize)collectionViewContentSize
{
    // Only support single section for now.
    // Only support Horizontal scroll
    //NSUInteger count = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0];
    
    CGSize canvasSize = self.collectionView.frame.size;
    CGSize contentSize = canvasSize;
    
    int pages = kAppDelegate.photoGalleryCount / photoItemPerPage;
    if (kAppDelegate.photoGalleryCount % photoItemPerPage  > 0) {
        pages = pages + 1;
    }
    
    contentSize.width = pages * canvasSize.width;
    
    return contentSize;
}


- (CGSize) sizeForIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(200, 200);
}

- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize canvasSize = self.collectionView.frame.size;
    CGSize customItemSize  =   [self sizeForIndexPath:indexPath];
    
    NSUInteger rowCount = (canvasSize.height - customItemSize.height) / (customItemSize.height + self.minimumInteritemSpacing) + 1;
    NSUInteger columnCount = (canvasSize.width - customItemSize.width) / (customItemSize.width + self.minimumLineSpacing) + 1;
    
    CGFloat pageMarginX = (canvasSize.width - columnCount * customItemSize.width - (columnCount > 1 ? (columnCount - 1) * self.minimumLineSpacing : 0)) / 2.0f;
    CGFloat pageMarginY = (canvasSize.height - rowCount * customItemSize.height - (rowCount > 1 ? (rowCount - 1) * self.minimumInteritemSpacing : 0)) / 2.0f;
    
    if (rowCount * columnCount == 0) {
        return CGRectZero;
    }
    
    NSUInteger page = indexPath.row / (rowCount * columnCount);
    NSUInteger remainder = indexPath.row - page * (rowCount * columnCount);
    NSUInteger row = remainder / columnCount;
    NSUInteger column = remainder - row * columnCount;
    
    CGRect cellFrame = CGRectZero;
    cellFrame.origin.x = pageMarginX + column * (customItemSize.width + self.minimumLineSpacing);
    cellFrame.origin.y = pageMarginY + row * (customItemSize.height + self.minimumInteritemSpacing);
    cellFrame.size.width = customItemSize.width;
    cellFrame.size.height = customItemSize.height;
    

    cellFrame.origin.x += page * canvasSize.width;
    
    return cellFrame;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes * attr = [super layoutAttributesForItemAtIndexPath:indexPath];
    attr.frame = [self frameForItemAtIndexPath:indexPath];
    return attr;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSUInteger count = [self.collectionView.dataSource collectionView:self.collectionView
                                               numberOfItemsInSection:0];
    
    NSMutableArray * attrs = [NSMutableArray array];
    
    for (NSUInteger idx = 0; idx < count; ++idx)
    {
        UICollectionViewLayoutAttributes * attr = nil;
        NSIndexPath * idxPath = [NSIndexPath indexPathForRow:idx inSection:0];
        CGRect itemFrame = [self frameForItemAtIndexPath:idxPath];
        if (CGRectIntersectsRect(itemFrame, rect))
        {
            attr = [self layoutAttributesForItemAtIndexPath:idxPath];
            [attrs addObject:attr];
        }
    }
    
    return attrs;
}
@end
