//
//  DAMThumbnailBaseView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 30/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMThumbnailBaseView : UIViewController

//@property (nonatomic, retain) NSDictionary *model;

//@property (nonatomic, retain) IBOutlet DAMView *view;

- (NSDictionary*)model;
- (void)setModel:(NSDictionary*)newModel;
- (void)updateDisplay;

@end
