//
//  ALAsset+ALAsset_Date.m
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 8/28/14.
//
//

#import "ALAsset+ALAsset_Date.h"

@implementation ALAsset (ALAsset_Date)

- (NSDate *) date
{
    return [self valueForProperty:ALAssetPropertyDate];
}

@end
