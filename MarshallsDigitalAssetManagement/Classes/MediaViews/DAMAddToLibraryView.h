//
//  DAMAddToLibraryView.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 7/31/14.
//
//

#import <UIKit/UIKit.h>

typedef void (^CancelBlock)(void);
typedef void (^SaveBlock)(void);

@interface DAMAddToLibraryView : UIView {
    
    //void (^cancelBlock)(void);
    //void (^saveBlock)(void);
}
//@property (nonatomic, assign) ALAsset *asset;
@property (nonatomic, retain)NSString *imageTitle;
@property (nonatomic, strong)CancelBlock cancelBlock;
@property (nonatomic, strong)SaveBlock saveBlock;

- (void) createCancelBlock:(CancelBlock)block;
- (void) createSaveBlock:(SaveBlock)block;

- (void) setThumbnailImage:(UIImage *)image;
- (void) animationShowUp;

@end
