//
//  LayoutManager.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DAMView.h"
#import <UIKit/UIKit.h>

@interface UIPhotoPageView : DAMView 

@property int pageNumber;

@end

@protocol PhotoLayoutManagerDelegate <NSObject>

- (void)postPageConfiguration:(UIPhotoPageView*)page;
- (void)experiencedContentExpansion;

@required
- (NSDictionary*)modelForItemSequenceNumber:(int)sequenceNumber;

@end

@interface PhotoLayoutManager : NSObject

@property (nonatomic, retain) NSString *photoType;
@property (nonatomic, assign) id<PhotoLayoutManagerDelegate> delegate;
@property float maxX;


- (int)numPages;

- (void)reset;
//- (void)testLayoutItems:(NSArray*)items;
- (void)layoutItems:(NSArray*)items;
- (void)tilePagesOnView:(UIView*)view;

@end
