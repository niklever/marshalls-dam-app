//
//  ALAsset+ALAsset_Date.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 8/28/14.
//
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAsset (ALAsset_Date)

- (NSDate *) date;

@end
