//
//  DAMMediaViewBase.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 11/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMMediaViewBase.h"
#import "DAMApplicationModel.h"
#import "NSString+Utilities.h"
#import "DAMUIDefaults.h"
#import "JSMain.h"

@interface DAMMediaViewBase () {
    
    UILabel *homeOwnerLabel;
    UILabel *commercialLabel;
}

- (void)setSelectionMode:(BOOL)mode;        //TRUE to enable asset selection
- (void)clearCarousels;

//- (ASIHTTPRequest*)createRequestForPage:(int)pageNum onCarousel:(NSString*)carousel;

- (void)getModelFromLocalDataForPage:(int)page onCarousel:(NSString*)carousel;

@end

@implementation DAMMediaViewBase
@synthesize businessAreaFiltersView;

@synthesize mediaSourceMethod;
@synthesize mediaType;
@synthesize queryString;
@synthesize pageSize;

@synthesize assets;

@synthesize topScrollView;
@synthesize bottomScrollView;

@synthesize homeOwnerLabel;
@synthesize commercialLabel;

@synthesize predictiveViewController;
@synthesize needDividePageSize;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.pageSize = 3;
        self.needDividePageSize = NO;
        //[[DAMApplicationModel model] registerCommand:self forKey:@"OfflineData"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    homeOwnerLabel.font = [[DAMUIDefaults defaults] carouselLabelFont];
    commercialLabel.font = [[DAMUIDefaults defaults] carouselLabelFont];
    
    self.topScrollView.carousel = @"top";
    self.bottomScrollView.carousel = @"bottom";
    
    self.originalBottomScrollViewFrame = self.bottomScrollView.frame;
    self.originalTopScrollViewFrame = self.topScrollView.frame;
    
	// Do any additional setup after loading the view.
    
    self.predictiveViewController = [[[DAMPredictiveViewController alloc] init] autorelease];
    self.predictiveViewController.view.frame = CGRectMake(129, 45, 337, 246);
    self.predictiveViewController.view.hidden = YES;
    self.predictiveViewController.delegate = self;
    [self.view addSubview:self.predictiveViewController.view];
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
    
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self hideUnusedCarousels];
}

- (void) mediaViewBaseInit {
    //[self hideUnusedCarousels];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[[DAMApplicationModel model] unregisterCommandForKey:@"OfflineData"];
}

- (void)setSingleCarouselWithFrame:(CGRect)frame {
    [self.bottomScrollView removeFromSuperview];
    self.bottomScrollView = nil;
    
    self.topScrollView.frame = frame;
}

- (void)hideUnusedCarousels {
    
    if ([mediaType isEqualToString: kMediaType_InThePress]) {
        needDividePageSize = YES;
        return;
    }
    
    if (![self singleCarousel]) {
        //NSArray *activeAreas = [[DAMApplicationModel model] settingsActiveBusinessAreas];
        [[DAMApplicationModel model] settingsActiveBusinessAreas:^(NSArray *activeAreas) {
            NSLog(@"pageSize == %d activeAreas %@", pageSize, activeAreas);
            if (activeAreas.count == 1) {
                NSString *area = [activeAreas objectAtIndex:0];
                //pageSize *= 2;
                self.pageSize = 6;
                
                float delta = 0;
                if ([[JSMain sharedInstance] isIOS7]) {
                    delta = -20;
                }
                
                
                needDividePageSize = YES;
                commercialLabel.hidden = YES;
                self.topScrollView.frame = self.originalTopScrollViewFrame;
                self.bottomScrollView.frame = self.originalBottomScrollViewFrame;
                if ([area compare:@"domestic"] == NSOrderedSame) {
                    self.bottomScrollView.hidden = YES;
                    self.topScrollView.hidden = NO;
                    homeOwnerLabel.text = @"HOMEOWNER";
                    self.topScrollView.frame = CGRectMake(self.originalTopScrollViewFrame.origin.x, self.originalTopScrollViewFrame.origin.y + delta, self.originalTopScrollViewFrame.size.width, self.originalTopScrollViewFrame.size.height + self.originalBottomScrollViewFrame.size.height);
                } else {
                    homeOwnerLabel.text = @"COMMERCIAL";
                    self.topScrollView.hidden = YES;
                    self.bottomScrollView.hidden = NO;
                    //bottomScrollView.frame = CGRectMake(topScrollView.frame.origin.x, topScrollView.frame.origin.y, topScrollView.frame.size.width, topScrollView.frame.size.height + bottomScrollView.frame.size.height);
                    self.bottomScrollView.frame = CGRectMake(self.originalTopScrollViewFrame.origin.x, self.originalTopScrollViewFrame.origin.y +delta, self.originalTopScrollViewFrame.size.width, self.originalTopScrollViewFrame.size.height + self.originalBottomScrollViewFrame.size.height);
                    
                    NSLog(@"self.bottomScrollView.frame %@", NSStringFromCGRect(self.bottomScrollView.frame));
                    
                }
            }else{
                //show 2 areas
                if (needDividePageSize) {
                    pageSize = self.pageSize / 2;
                    needDividePageSize = NO;
                }
                
                homeOwnerLabel.hidden = NO;
                commercialLabel.hidden = NO;
                bottomScrollView.hidden = NO;
                homeOwnerLabel.text = @"HOMEOWNER";
                self.topScrollView.hidden = NO;
                self.topScrollView.frame = self.originalTopScrollViewFrame;
                self.bottomScrollView.frame = self.originalBottomScrollViewFrame;
            }
        }];
    }
}

- (void)sourceFromCarousel {
    [self setMediaSourceMethod:@"carousel" andQuery:nil];
}

- (void)sourceFromQuery:(NSString*)query {
    //activeTextField.text = query;
    damNavigator.searchBar.searchField.text = query;
    [self setMediaSourceMethod:@"search" andQuery:query];
}


- (void)setMediaSourceMethod:(NSString *)pMediaSourceMethod andQuery:(NSString*)query {
    [self clearCarousels];
    self.mediaSourceMethod = pMediaSourceMethod;
    self.queryString = query;
}

- (void)setQueryString:(NSString *)pQueryString {
#ifdef DEBUG
    NSLog(@"DAMMediaViewBase.setQueryString queryString:[%@]", pQueryString);
#endif
    queryString = [pQueryString copy];
    [pQueryString retain];

    [self refreshCarousels];
}

- (void)refreshCarousels {
    //Only send requests for carousels that are visible.
    if (self.topScrollView) {
        self.topScrollView.mediaType = mediaType;
        [self requestModelForPage:0 onCarousel:@"top"];
    }
    if (self.bottomScrollView) {
        self.bottomScrollView.mediaType = mediaType;
        [self requestModelForPage:0 onCarousel:@"bottom"];
    }
}

- (void)clearCarousels {
    if (self.topScrollView != NULL) {
        [self.topScrollView clearContent];
    }
    if (self.bottomScrollView != NULL) {
        [self.bottomScrollView clearContent];
    }
}


#pragma DAMPagedScrollViewDelegate -- requestModelForPage

//Page numbers are zero based
- (void)requestModelForPage:(int)pageNum onCarousel:(NSString*)carousel {

    [self getModelFromLocalDataForPage:pageNum onCarousel:carousel];
}

- (BOOL)singleCarousel {
    
    return [mediaType isEqualToString: kMediaType_SalesPhotos] || [mediaType isEqualToString: kMediaType_Photos] || [mediaType isEqualToString: kMediaType_Samples] || [mediaType isEqualToString: kMediaType_CaseStudies] || [mediaType isEqualToString: kMediaType_InThePress];
    
    //return [mediaType compare:kMediaType_Photos] == NSOrderedSame || [mediaType compare:kMediaType_Samples] == NSOrderedSame || [mediaType compare:kMediaType_CaseStudies] == NSOrderedSame || [mediaType compare:kMediaType_InThePress] == NSOrderedSame;
}

- (void)getModelFromLocalDataForPage:(int)page onCarousel:(NSString*)carousel {
    NSLog(@"DAMMediaViewBase.getModelFromLocalData carousel == %@ ; mediaType == %@", carousel, mediaType);
    NSArray *businessAreas = nil;
    if (carousel == nil || [self singleCarousel]) {
        businessAreas = [NSArray arrayWithObjects:@"domestic", @"commercial", @"group", nil];
    } else if ([carousel compare:@"top"] == NSOrderedSame) {
        businessAreas = [NSArray arrayWithObjects:@"domestic", nil];
    } else {
        businessAreas = [NSArray arrayWithObjects:@"commercial", nil];
    }
    
    //NSArray *activeContainers = [[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas];
    
    [[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas callback:^(NSArray *activeContainers) {
        NSArray *filterAreas = [[JSMain sharedInstance] getBusinessAreaFilters];
        if (filterAreas != NULL && [filterAreas count] > 0) {
#ifdef DEBUG
            NSLog(@"Apply business filter area %@", filterAreas);
#endif
            activeContainers = filterAreas;
        }
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:page], @"page",
                                  [NSString stringWithFormat:@"%@", carousel], @"carousel", nil];
        
        if ([mediaSourceMethod compare:@"carousel"] == NSOrderedSame) {
            [[DAMApplicationModel model] offlineDataGetCarouselForMediaType:mediaType inContainers:activeContainers withPageSize:pageSize andPageNum:page withUserInfo:userInfo plugin:self method:@selector(localDataGotResult:)];
        } else if ([mediaSourceMethod compare:@"search"] == NSOrderedSame) {
            [[DAMApplicationModel model] offlineDataGetCarouselForSearchTerm:queryString ofMediaType:mediaType inContainers: activeContainers withPageSize:pageSize andPageNum:page withUserInfo:userInfo plugin:self method:@selector(localDataGotResult:)];
        } else {
            NSLog(@"getModelFromLocalDataForPage.ERROR_UNKNOWN_MEDIA_SOURCE_METHOD mediaSourceMethod:%@", mediaSourceMethod);
        }
    }];
    
    
}

//- (void)localDataGotResult:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options {
- (void)localDataGotResult:(NSMutableDictionary*)options {
    NSDictionary *result = options;
    NSDictionary *userInfo = [result objectForKey:@"userInfo"];
    
    NSNumber *pageNumber = [userInfo objectForKey:@"page"];
    NSString *carousel = [userInfo objectForKey:@"carousel"];
#ifdef DEBUG
    NSLog(@"DAMMediaViewBase.localDataGotResult carousel:%@", carousel);
#endif
    if ([carousel compare:@"top"] == NSOrderedSame) {
        [self.topScrollView setupWithModel:result forPage:[pageNumber intValue]];
    } else {
        [self.bottomScrollView setupWithModel:result forPage:[pageNumber intValue]];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.mediaSourceMethod = nil;
    self.mediaType = nil;
    self.queryString = nil;

    self.assets = nil;
    
    self.topScrollView = nil;
    self.bottomScrollView = nil;
    
    self.mediaType = nil;

    self.homeOwnerLabel = nil;
    self.commercialLabel = nil;
}

- (void)assetOpened:(NSDictionary*)asset atIndex:(int)index fromPagedScrollView:(DAMPagedScrollView *)pageScrollView {
    DAMAssetView *assetView = [[DAMAssetView alloc] initWithNibName:@"DAMAssetView" bundle:nil];
    assetView.model = asset;
    assetView.mediaType = self.mediaType;
    assetView.delegate = self;
    [self pushView:assetView withNavigation:@"Brochure-View"]; 
}


#pragma mark RIGHT BUTTON PRESSED

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMMediaViewBase.rightButtonTapped index:%i", index);
#endif
    DAMIDriveNavigatorViewController *iDrive;
    DAMSearchBarView *searchBar;
    switch (index) {
        case 0:
            searchBar = [damNavigator displaySearchBarWithDelegate:self];
            searchBar.delegate = self;
            searchBar.searchField.text = self.queryString;
            break;
        case 1:
            [self lauchInfo];
            break;
        case 2:
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            break;
        case 3:
            [self addSendSaveButtons];
            break;
    }
}

- (void)lauchInfo{
    DAMAssetView *assetView = [[DAMAssetView alloc] initWithNibName:@"DAMAssetView" bundle:nil];
    assetView.model = [NSDictionary dictionaryWithObjectsAndKeys: [JSMain getURLOfUserGuide] , @"url", @"How To Guide", @"title", nil];
    assetView.mediaType = @"UserGuide";
    //[self pushView:assetView withNavigation:@"HowToGuide-View"];
    [self pushView:assetView];
    [assetView release];
}

- (void)searchBarCancelTapped {
#ifdef DEBUG
    NSLog(@"DAMMediaViewBase.searchBarCancelTapped");
#endif
    if ([mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos]) {
        damNavigator.searchBar.searchField.delegate = nil;
        [damNavigator clearCustomButtonView];
        [self sourceFromCarousel];
    }
}

- (NSString*)titleForAsset:(NSDictionary*)asset {
    NSString *result;
    NSString *title = [asset objectForKey:@"title"];
    if ([mediaType compare:kMediaType_Samples] == NSOrderedSame) {
        NSString *colour = [asset objectForKey:@"colour"];
        result = colour ? [NSString stringWithFormat:@"%@ %@", title, colour] : title;
    } else {
        result = title;
    }
    return result;
}

- (void)addSendSaveButtons {

    NSArray *buttonSpec = [NSArray arrayWithObjects:@"btn-cancel.png", @"cancelButtonTapped:", @"btn-send.png", @"shareButtonTapped:",
                           @"btn-save.png", @"saveButtonTapped:", nil];
    [damNavigator addRolloutButtons:buttonSpec forRightButtonIndex:2 withTarget:self];
    
    [self setSelectionMode:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    NSString *newText = textField.text;
    if (newText.length >= 3) {
        //[self clearCarousels];
        [JSPhotoLayoutV2 sharedInstance].searchReset = TRUE;
        [self setMediaSourceMethod:@"search" andQuery:newText];
    } else if (newText.length == 0){
        //need to reset data
        [self clearCarousels];
        self.queryString = @"";
    }
    [self.predictiveViewController hideAfterTime: 0];
    return NO;
}


- (void)predictiveViewController:(DAMPredictiveViewController *)predictiveViewController didChooseItem:(NSDictionary *)item{
#ifdef DEBUG
    NSLog(@"predictiveViewController.didChooseItem = %@", item);
#endif
    NSString * searchText = [item objectForKey:@"text"];
    activeTextField.text = searchText;
    [self.predictiveViewController hideAfterTime: 0];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    activeTextField = textField;
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (newText.length >= 3) {
        NSArray *businessAreas = [NSArray arrayWithObjects:@"domestic", @"commercial", @"group", nil];
        //NSArray *activeContainers = [[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas];
        
        [[DAMApplicationModel model] settingsGetActiveContainersForBusinessAreas:businessAreas callback:^(NSArray *activeContainers) {
            NSArray *filterAreas = [[JSMain sharedInstance] getBusinessAreaFilters];
            if (filterAreas != NULL && [filterAreas count] > 0) {
#ifdef DEBUG
                NSLog(@"Apply business filter area %@", filterAreas);
#endif
                activeContainers = filterAreas;
            }
            
#ifdef DEBUG
            NSLog(@"self.mediaType == %@", self.mediaType);
#endif
            [self.predictiveViewController requestDataWithKey:newText mediaType: self.mediaType == NULL ? kMediaType_Photos : self.mediaType containers: activeContainers];
        }];
    } else {
        [self.predictiveViewController resetData];
        self.predictiveViewController.view.hidden = YES;
    }
    return YES;
}



- (void)setSelectionMode:(BOOL)mode {
    self.topScrollView.isSelecting = mode;
    self.bottomScrollView.isSelecting = mode;
}

- (NSArray*)selectedMedia {
    NSMutableArray *result = [NSMutableArray arrayWithArray: self.topScrollView.selectedAssets];
    [result addObjectsFromArray: self.bottomScrollView.selectedAssets];
    return result;
}

- (NSString*)stringForAsset:(NSDictionary*)asset {
    NSString *result = nil;
    NSString *assetMediaType = [asset objectForKey:@"mediaType"];
    if ([assetMediaType compare:kMediaType_CaseStudies] == NSOrderedSame) {
        float latitude = [[asset objectForKey:@"latitude"] floatValue];
        float longitude = [[asset objectForKey:@"longitude"] floatValue];
        result = [NSString stringWithFormat:@"mediaRef:%@, title:%@, location:%@, gps:(%f,%f)", [asset objectForKey:@"mediaRef"],
                  [asset objectForKey:@"title"], [asset objectForKey:@"location"], latitude, longitude];
    }
    return result;
}

- (void)assetSelected:(NSDictionary*)asset {
    [self openAsset:asset];
}

- (void) searchBarBusinessAreaTapped {
#ifdef DEBUG
    NSLog(@"show business area");
#endif
    self.businessAreaFiltersView = [[DAMBusinessAreaFiltersView alloc] initWithNibName:@"DAMBusinessAreaFiltersView" bundle:nil];
    businessAreaFiltersView.delegate = self;
    [self.view addSubview:businessAreaFiltersView.view];
    [businessAreaFiltersView animateOnStage];
    [businessAreaFiltersView release];
}

- (void)businessAreaFiltersViewDidCancel {
    [businessAreaFiltersView.view removeFromSuperview];
    self.businessAreaFiltersView = nil;
    //    [self setInitialSwitchPositions];
}

- (void)businessAreaFiltersViewDidOK {
    [businessAreaFiltersView.view removeFromSuperview];
    self.businessAreaFiltersView = nil;
    //[self actionChanges];
    
    [self clearCarousels];
    
    [self.topScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.bottomScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [self.topScrollView clearPageControl];
    [self.bottomScrollView clearPageControl];
    
    
    
    [self hideUnusedCarousels];
    
    [self.topScrollView sharedInit];
    [self.bottomScrollView sharedInit];
    
    [JSPhotoLayoutV2 sharedInstance].searchReset = TRUE;
    
    if (activeTextField && activeTextField.text && activeTextField.text.length >= 3) {
        //[self clearCarousels];
        [JSPhotoLayoutV2 sharedInstance].searchReset = TRUE;
        [self setMediaSourceMethod:@"search" andQuery: activeTextField.text];
    } else {
        //need to reset data
        //[self clearCarousels];
        self.queryString = @"";
    }
}

@end
