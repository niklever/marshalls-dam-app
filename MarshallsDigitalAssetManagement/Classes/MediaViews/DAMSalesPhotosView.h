//
//  DAMSalesPhotosView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 22/8/14.
//
//

#import <Foundation/Foundation.h>

#import "DAMMediaViewBase.h"
#import "DAMPagedScrollView.h"
#import "DAMTagView.h"
#import "DAMTagLabel.h"
#import "DAMPhotoScrollView.h"

@interface DAMSalesPhotosView : DAMMediaViewBase<DAMTagViewDelegate, DAMTagLabelDelegate, DAMPhotoScrollViewDelegate> {
    NSMutableDictionary *layoutQueue;
    
    //TODO - mediaMap may be useful in the superclass?
    NSDictionary *mediaMap;
}
@property (nonatomic, retain) NSMutableDictionary *layoutQueue;
@property (nonatomic, retain) NSDictionary *mediaMap;

@property (nonatomic, retain) IBOutlet DAMPhotoScrollView *photoScrollView;
@property (nonatomic, retain) IBOutlet UILabel *statusLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, retain) UIImageView *noResultsView;
@property (nonatomic, retain) NSArray *photoResults;
@end
