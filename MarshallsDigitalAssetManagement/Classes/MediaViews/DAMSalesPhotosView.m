//
//  DAMSalesPhotosView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 22/8/14.
//
//

#import "DAMSalesPhotosView.h"
#import "DAMApplicationModel.h"
#import "DAMImageView.h"
#import "DAMPagedScrollView.h"
#import "DAMIDriveNavigatorViewController.h"
#import "DAMView.h"
#import "DAMMAPViewController.h"

@interface DAMSalesPhotosView () {
    
    NSMutableArray *selectedTags;
    UILabel *statusLabel;
    UIActivityIndicatorView *activityView;
}
@property (nonatomic, retain) NSMutableArray *selectedTags;
@end

@implementation DAMSalesPhotosView
@synthesize layoutQueue;
@synthesize mediaMap;
@synthesize selectedTags;
@synthesize statusLabel;
@synthesize activityView;
@synthesize noResultsView;

@synthesize photoScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //        self.mediaType = kMediaType_Photos;
        //        self.pageSize = 15;//32000;
        //[[DAMApplicationModel model] registerCommand:self];
        //self.mediaType = kMediaType_SalesPhotos;
        self.mediaType = kMediaType_SalesPhotos;
        self.layoutQueue = [NSMutableDictionary dictionaryWithCapacity:20];
        self.mediaMap = [NSMutableDictionary dictionaryWithCapacity:20];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.layoutQueue = nil;
    self.mediaMap = nil;
    self.selectedTags = nil;
    self.statusLabel = nil;
    self.activityView = nil;
}

- (void) mediaViewBaseInit {
    activityView.hidden = YES;
    [activityView stopAnimating];
    self.mediaType = kMediaType_SalesPhotos;
    if (selectedTags.count > 0) {
        [damNavigator displayTags:selectedTags withDelegate:self];
        [damNavigator setTitleImageForView:@"Sales Photos-Tags"];
    } else {
        [damNavigator displaySearchBarWithDelegate:self];
        [damNavigator setTitleImageForView:kMediaType_SalesPhotos];
        damNavigator.searchBar.cancelButton.hidden = YES;
        //damNavigator.searchBar.businessAreaButton.hidden = YES;
        damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);
    }
    if (self.view.subviews.count > 0 && [[self.view.subviews objectAtIndex:0] isKindOfClass:[DAMPhotoScrollView class]]) {
        self.photoScrollView.pauseLayout = NO;
    }else{
        self.photoScrollView = [[DAMPhotoScrollView alloc] initWithFrame:CGRectMake(0, 0, 1024, 748)];
        [self.view insertSubview: self.photoScrollView atIndex:0];
        self.photoScrollView.backgroundColor = [UIColor blackColor];
        self.photoScrollView.delegate = self;
        self.photoScrollView.photoType = kMediaType_SalesPhotos;
        //[photoScrollView release];
        //[[DAMApplicationModel model] suspendOps];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.screenName = @"Sales Photos";
    
    /*FIX ME ios 7*/
    [self mediaViewBaseInit];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (BOOL)leftButtonTapped:(id)sender {
    [damNavigator clearCustomButtonView];
    [self clearCarousels];
    
    [self.photoScrollView resetData];
    //[self.photoScrollView release];
    
    self.photoScrollView = nil;
    return [super leftButtonTapped: sender];
}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
    NSLog(@"DAMSalesPhotosView.rightButtonTapped index:%i", index);
    
    DAMIDriveNavigatorViewController *iDrive;
    switch (index) {
        case 1:
            [self launchGPSView];
            break;
        case 2:
            self.photoScrollView.pauseLayout = YES;
            [self addTagView];
            break;
        case 3:
            self.photoScrollView.pauseLayout = YES;
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            break;
        case 4:
            //share
            [self addSendSaveButtons];
            break;
        case 5:
            break;
    }
}


/*
 
 self.photoResults is list of object
 
 {
 blockIndex = 0;
 filename = "http://newsite.marshalls-etest.co.uk/dam-svc//AssetStore/a8efcdf9-d982-4bc1-a5ff-c1089b4c6aae.png?MaxHeight=368";
 height = 368;
 imageIndex = 0;
 latitude = "51.503647";
 layoutH = 368;
 layoutW = 490;
 layoutX = 3;
 layoutY = 3;
 left = 0;
 longitude = "-0.091299";
 mediaRef = "a8efcdf9-d982-4bc1-a5ff-c1089b4c6aae";
 mediaType = "Sales Photos";
 originalHeight = 1536;
 originalWidth = 2048;
 priority = 0;
 sequenceNumber = 0;
 thumbHeight = 368;
 thumbWidth = "490.666687";
 thumbnail = "/AssetStore/a8efcdf9-d982-4bc1-a5ff-c1089b4c6aae.png?MaxHeight=368";
 title = "iOS Simulator Screen shot 21 Aug, 2014 11.42.17 am";
 top = 0;
 url = "/AssetStore/a8efcdf9-d982-4bc1-a5ff-c1089b4c6aae.png";
 used = 0;
 width = 490;
 }
 
 */
- (void) launchGPSView {
    
    DAMMAPViewController *controller    =   [[DAMMAPViewController alloc] initWithNibName:@"DAMMAPViewController" bundle:nil];
    [controller setArrLocation:_photoResults];
    DAMAppDelegate *appDelegate =   (DAMAppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)addSendSaveButtons {
    
    NSArray *buttonSpec = [NSArray arrayWithObjects:@"btn-cancel.png", @"cancelButtonTapped:", @"btn-send.png", @"shareButtonTapped:",
                           @"btn-save.png", @"saveButtonTapped:", nil];
    [damNavigator addRolloutButtons:buttonSpec forRightButtonIndex:3 withTarget:self];
    
    [self setSelectionMode:YES];
}

- (void)addTagView {
    DAMTagView *tagView = [[DAMTagView alloc] initWithNibName:@"DAMTagView" bundle:nil andMediaType: self.mediaType];
    if (selectedTags.count > 0) {
        [tagView selectTags:selectedTags];
    }
    tagView.delegate = self;
    [self.view addSubview:tagView.view];
    [tagView animateOnStage];
}

//Tag view delegate

- (void)tagViewDidCancel:(DAMTagView*)tagView {
}

- (void)tagViewDidFinish:(DAMTagView*)tagView withTags:(NSArray*)tags {
    
    self.selectedTags = [NSMutableArray arrayWithArray:tags];
    [self setMediaSourceMethod:@"search_tag" andQuery:[tags componentsJoinedByString:@"|"]];
    [damNavigator displayTags:tags withDelegate:self];
    [damNavigator setTitleImageForView:@"Sales Photos-Tags"];
}

- (void)modalViewOffstageAnimationDidFinish:(DAMModalViewBase*)modalView {
    [modalView.view removeFromSuperview];
}

- (void)tagSelected:(NSString*)tag {
    //not actually needed
}

- (void)tagDeselected:(NSString*)tag {
    [selectedTags removeObject:tag];
    if (selectedTags.count > 0) {
        [self setMediaSourceMethod:@"search_tag" andQuery:[selectedTags componentsJoinedByString:@"|"]];
        [damNavigator removeTag:tag];
        [damNavigator setTitleImageForView:@"Sales Photos-Tags"];
    } else {
        [damNavigator displaySearchBarWithDelegate:self];
        damNavigator.searchBar.cancelButton.hidden = true;
        //damNavigator.searchBar.businessAreaButton.hidden = true;
        damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);
        [self sourceFromCarousel];
        [damNavigator setTitleImageForView:kMediaType_SalesPhotos];
    }
}

- (void)clearCarousels {
#ifdef DEBUG
    NSLog(@"DAMPhotoView.clearCarousels");
#endif
    [self.photoScrollView clearContent];
}

//TODO mock out old dependencies for now (MediaViewBase.h)
- (void)refreshCarousels {
    NSLog(@"DAMSalesPhotosView.refreshCarousels - OVERRIDDEN method:%@ queryString:%@", self.mediaSourceMethod, self.queryString);
    
    NSLog(@"photoScrollView = %@", photoScrollView);
    [self clearNoResultsView];
    statusLabel.hidden = YES;
    activityView.hidden = NO;
    [activityView startAnimating];
    statusLabel.text = @"Please wait, retrieving images.";
    //self.photoScrollView.hidden = YES;
    if ([self.mediaSourceMethod compare:@"search_tag"] == NSOrderedSame) {
        [self performSelectorInBackground:@selector(initLoadImageMetaDataFromTags:) withObject: self.queryString];
        //[photoScrollView loadImageMetaDataFromTags:queryString];
    } else {
        //[photoScrollView loadImageMetaData:queryString];
        [self performSelectorInBackground:@selector(initLoadImageMetaData:) withObject: self.queryString];
        //[self performSelectorOnMainThread:@selector(initLoadImageMetaData:) withObject: queryString waitUntilDone: NO];
    }
    [super refreshCarousels];
    
    //    [DAMApplicationModel model];
    //    [self addGestureRecognizers];
}

- (void) initLoadImageMetaDataFromTags:(NSString *)query {
    [self clearNoResultsView];
    [self.photoScrollView loadImageMetaDataFromTags:query];
}

- (void) initLoadImageMetaData:(NSString *)query {
    [self clearNoResultsView];
    
    [self.photoScrollView loadImageMetaData:query];
}



- (void)layoutFinished: (NSArray *)results {
    
    if (results) {
        self.photoResults = results;
    }
    
    statusLabel.hidden = YES;
    activityView.hidden = YES;
    [activityView stopAnimating];
}

- (void)noResults:(NSDictionary *) result {
    activityView.hidden = YES;
    [activityView stopAnimating];
    //statusLabel.text = @"No results found";
    statusLabel.hidden = YES;
    [self displayNoResults];
}


- (NSString*)getNoResultsImageFile {
    NSString *result = @"search-noresults-white.png";
    NSDictionary *map = [NSDictionary dictionaryWithObjectsAndKeys:@"search-noresults-black.png", kMediaType_Photos,@"search-noresults-black.png", kMediaType_SalesPhotos,
                                @"search-noresults-black.png", kMediaType_Samples, nil];
    if ([map objectForKey: self.mediaType]) {
        result = [map objectForKey: self.mediaType];
    }
    return result;
}

- (void)displayNoResults {
    
    if (!noResultsView) {
        NSString *image = [self getNoResultsImageFile];
        self.noResultsView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:image]];
        noResultsView.center = self.view.center;
        [self.view addSubview:noResultsView];
        [noResultsView release];
    }
}

- (void)clearNoResultsView {
    if (noResultsView) {
        [noResultsView removeFromSuperview];
        self.noResultsView = nil;
    }
}

- (void)imageTapped:(NSDictionary*)image {
    
    //NSLog(@"photoScrollView content offset == %f", self.photoScrollView.scrollView.contentOffset.x);
    
    self.photoScrollView.pauseLayout = YES;
    
    //add offset x
    [(NSMutableDictionary *)image setObject: [NSString stringWithFormat:@"%f", self.photoScrollView.scrollView.contentOffset.x] forKey:@"offsetX"];
    
    [self openAsset:image];
}

- (void)setSelectionMode:(BOOL)mode {
    [photoScrollView setSelecting:mode];
}

- (NSArray*)selectedMedia {
    NSMutableArray *result = [NSMutableArray arrayWithArray:photoScrollView.selectedAssets];
    [result addObjectsFromArray: self.bottomScrollView.selectedAssets];
    return result;
}

#pragma bridge API to DAMPhotoScrollView
- (void)layoutPartialResult:(NSDictionary*)dict {
    [self clearNoResultsView];
    activityView.hidden = YES;
    
    [self.photoScrollView layoutPartialResult:dict];
}

@end
