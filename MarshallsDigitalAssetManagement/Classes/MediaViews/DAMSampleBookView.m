//
//  DAMSampleBookView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMSampleBookView.h"

@interface DAMSampleBookView ()

@end

@implementation DAMSampleBookView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.mediaType = kMediaType_Samples;
        self.pageSize = 9;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.topScrollView.scrollView.pagingEnabled = NO;
    
    if([[JSMain sharedInstance] isIOS7])
    {
        self.topScrollView.frame = CGRectMake(0, 0, self.topScrollView.frame.size.width, self.topScrollView.frame.size.height);
    }else{
        self.topScrollView.frame = CGRectMake(0, 0, self.topScrollView.frame.size.width, self.topScrollView.frame.size.height + 40);
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Samples";
    
    [damNavigator displaySearchBarWithDelegate:self];
    [damNavigator setTitleImageForView:kMediaType_Samples];
    damNavigator.searchBar.cancelButton.hidden = YES;
    //damNavigator.searchBar.businessAreaButton.hidden = YES;
    damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//	return YES;
//}

- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMSampleBookView.rightButtonTapped index:%i", index);
#endif
    DAMIDriveNavigatorViewController *iDrive;
    switch (index) {
        case 0:
            [self lauchInfo];
            break;
        case 1:
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            break;
        case 2:
            [self addSendSaveButtons];
            break;
        case 3:
            
            break;
    }
}

@end
