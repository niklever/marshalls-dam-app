//
//  DAMBrochureView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMBrochureView.h"
//#import "JSONKit.h"
#import "DAMAssetView.h"

@interface DAMBrochureView ()

@end

@implementation DAMBrochureView
@synthesize testData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.mediaType = kMediaType_Brochures;
        //self.originalBottomScrollViewFrame = CGRectMake(0, 423, 1024, 346);
        //self.originalTopScrollViewFrame = CGRectMake(0, 77, 1024, 346);
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.screenName = @"Brochures";
    
    [damNavigator displaySearchBarWithDelegate:self];
    [damNavigator setTitleImageForView:kMediaType_Brochures];
    damNavigator.searchBar.cancelButton.hidden = YES;
    //damNavigator.searchBar.businessAreaButton.hidden = YES;
    damNavigator.searchBar.businessAreaButton.frame = CGRectMake(340, damNavigator.searchBar.businessAreaButton.frame.origin.y , damNavigator.searchBar.businessAreaButton.frame.size.width, damNavigator.searchBar.businessAreaButton.frame.size.height);
}

- (void) mediaViewBaseInit {
    /*if ([[JSMain sharedInstance] isIOS7]) {
        [self hideUnusedCarousels];
    }*/
}


- (void)rightButtonTapped:(id)sender withIndex:(int)index {
#ifdef DEBUG
    NSLog(@"DAMSampleBookView.rightButtonTapped index:%i", index);
#endif
    DAMIDriveNavigatorViewController *iDrive;
    switch (index) {
        case 0:
            [self lauchInfo];
            break;
        case 1:
            iDrive = (DAMIDriveNavigatorViewController*)[self launch:@"DAMIDriveNavigatorViewController" withNavigation:@"iDrive"];
            iDrive.delegate = self;
            break;
        case 2:
            [self addSendSaveButtons];
            break;
        case 3:
            
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.topScrollView.pageControlEnabled = YES;
    self.bottomScrollView.pageControlEnabled = YES;
    
    //TODO - remove testdata from DAMBrochureView
    /*
    NSError *error = nil;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Brochure_Test" ofType:@"json"];
    NSString *dataString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    self.testData = [dataString objectFromJSONString];
    if (!testData) {
#ifdef DEBUG
        NSLog(@"DAMBrochureView.viewDidLoad.FATAL_NO_MODEL");
#endif
        exit(-1);
    }*/
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    self.testData = nil;
}

@end
