//
//  DAMPhotoScrollView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 28/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMPhotoScrollView.h"
//#import "JSONKit.h"
#import "DAMAssetView.h"

@interface DAMPhotoScrollView() {

    id<DAMPhotoScrollViewDelegate> delegate;
    
    PhotoLayoutManager *lm;
    
    NSArray *photosToLayout;
    
    
    NSString *query;
}    
@property (nonatomic, retain) PhotoLayoutManager *lm;
@property (nonatomic, retain) NSArray *photosToLayout;
@property (nonatomic, retain) NSString *query;

@end

@implementation DAMPhotoScrollView
@synthesize delegate;
@synthesize lm;
@synthesize photosToLayout;
@synthesize scrollView;
@synthesize query;
@synthesize pauseLayout;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self sharedInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self sharedInit];
    }
    return self;
}

- (void)sharedInit {
    [super sharedInit];
    self.pauseLayout = NO;
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.scrollView.delegate = self;
    [self addSubview: self.scrollView];
    
    self.lm = [[PhotoLayoutManager alloc] init];
    self.lm.delegate = self;
    self.lm.photoType = self.photoType;
}

- (void)dealloc {
    
    //[[DAMApplicationModel model] unregisterCommand:self];
    self.lm = nil;
    self.photosToLayout = nil;
    self.scrollView = nil;
    self.query = nil;
    [super dealloc];
}

-(void) resetData{
    [self.scrollView release];
    self.scrollView = nil;
    [self.lm release];
    self.lm= nil;
}

- (UIView*)viewContainingSelectableItems { return scrollView; }

- (void)clearContent {
    self.scrollView.contentOffset = CGPointMake(0, 0);
#ifdef DEBUG
    NSLog(@"DAMPhotoScrollView.clearContent scrollView.subviews.count:%i", scrollView.subviews.count);
#endif
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.lm reset];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fetchNextResultBatch) object:nil];
}

- (void)loadImageMetaData:(NSString*)queryString {
    
    self.query = queryString;
    self.photosToLayout = nil;
    [[DAMApplicationModel model] photoInitLayoutForPlugin: self withQuery:queryString mediaType: self.photoType];
}
        
- (void)loadImageMetaDataFromTags:(NSString*)tagSpec {
    self.photosToLayout = nil;
    [[DAMApplicationModel model] photoInitLayoutForPlugin: self withTags:tagSpec mediaType: self.photoType];
}


- (void)layoutComplete:(NSArray*)args withDict:(NSDictionary*)dict {
    self.photosToLayout = (NSArray*)dict;//[dict objectForKey:@"assets"];
    NSLog(@"DAMPhotoScrollView.layoutComplete photosToLayout.count:%i", photosToLayout.count);
    [delegate layoutFinished];
    [self.lm layoutItems: self.photosToLayout];
//    NSLog(@"lm.maxX:%f", lm.maxX);
    scrollView.contentSize = CGSizeMake(lm.maxX, scrollView.frame.size.height);
    [lm tilePagesOnView:scrollView];
    
//    [self performSelectorInBackground:@selector(bgLayout) withObject:nil];
}

- (void)layoutPartialResult:(NSDictionary*)dict {
    if(self.delegate && [self.delegate respondsToSelector:@selector(clearNoResultsView)]){
        [self.delegate clearNoResultsView];
    }
    
    NSDictionary *result = (NSDictionary*)dict;
    BOOL more = [[result objectForKey:@"more"] boolValue];
    NSArray *newPhotos = [result objectForKey:kMediaType_Photos];
    if (self.photosToLayout != nil && self.photosToLayout.count > 0) {
        [(NSMutableArray *)self.photosToLayout removeAllObjects];
        self.photosToLayout = nil;
    }
    
    self.photosToLayout = [NSMutableArray arrayWithArray:newPhotos];
    
#ifdef DEBUG
    NSLog(@"DAMPhotoScrollView.layoutPartialResult photosToLayout.count:%i", photosToLayout.count);
#endif
    
    if ([delegate respondsToSelector: @selector(layoutFinished:)]) {
        [delegate layoutFinished: newPhotos];
    }else{
        [delegate layoutFinished];
    }
    
    [lm layoutItems: newPhotos];
    
#ifdef DEBUG
    NSLog(@"lm.maxX:%f", lm.maxX);
#endif
    
    scrollView.contentSize = CGSizeMake(lm.maxX, scrollView.frame.size.height);
    
    [lm tilePagesOnView:scrollView];
    
    if (more && !self.pauseLayout) {
        [self performSelector:@selector(fetchNextResultBatch) withObject:nil afterDelay: 5];//2 is original
    } else {
        if (self.pauseLayout) {
#ifdef DEBUG
            NSLog(@"DAMPhotoScrollView.layoutPartialResult.PAUSED LAYOUT");
#endif
        }else{
#ifdef DEBUG
        NSLog(@"DAMPhotoScrollView.layoutPartialResult.FINISHED");
#endif
        }
    }
}

- (void)fetchNextResultBatch {
    //[[DAMApplicationModel model] photoLayoutIteratorForPlugin:@"DAMPhotoScrollView"];
    //[[DAMApplicationModel model] photoLayoutIteratorForPlugin:@"DAMPhotoView"];
    [[DAMApplicationModel model] photoLayoutIteratorForPlugin: self];
}

- (void)noLayoutResults: (NSDictionary *) result{
    
    [delegate noResults: result];
}


- (void)scrollViewDidScroll:(UIScrollView *)pScrollView {
    [lm tilePagesOnView:pScrollView];
    
    /*IOS7 fix*/
    [pScrollView setContentOffset:CGPointMake(pScrollView.contentOffset.x, 0)];
}

- (NSArray*)modelForItemSequenceNumber:(int)sequenceNumber {
//    NSLog(@"sequenceNumber:%i", sequenceNumber);
    if (sequenceNumber >=0 && sequenceNumber < photosToLayout.count)
        return [photosToLayout objectAtIndex:sequenceNumber];
    else {
#ifdef DEBUG
        NSLog(@"DAMPhotoScrollView.modelForItemSequenceNumber.ERROR_BAD_INDEX sequenceNumber:%i", sequenceNumber);
#endif
        return nil;
    }
}

- (void)postPageConfiguration:(UIPhotoPageView*)page {
    [self addAssetGestureRecognizersToView:page];
}

- (void)fireAssetOpened:(UIView*)view{
    int modelSequenceNumber = view.tag;
    NSDictionary *image = [photosToLayout objectAtIndex:modelSequenceNumber];
    if (delegate) {
        [delegate imageTapped:image];
    }
}

@end
