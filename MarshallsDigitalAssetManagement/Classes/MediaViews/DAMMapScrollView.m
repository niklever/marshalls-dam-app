//
//  DAMMapScrollView.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 25/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMMapScrollView.h"

@implementation DAMMapScrollView

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    NSLog(@"DAMMapScrollView.layoutSubviews");
    
    CGSize boundsSize = self.bounds.size;
    UIView *mapView = [self.subviews objectAtIndex:0];
    CGRect frameToCentre = mapView.frame;
    
    //centre horizontally
    if (frameToCentre.size.width < boundsSize.width)
        frameToCentre.origin.x = (boundsSize.width - frameToCentre.size.width) / 2;
    else
        frameToCentre.origin.x = 0;
    
    //centre vertically
    if (frameToCentre.size.height < boundsSize.height)
        frameToCentre.origin.y = (boundsSize.height - frameToCentre.size.height) / 2;
    else
        frameToCentre.origin.y = 0;
    
    mapView.frame = frameToCentre;
}

@end
