//
//  DAMPhotoScrollView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 28/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMSelectableView.h"
#import "PhotoLayoutManager.h"

@protocol DAMPhotoScrollViewDelegate <NSObject>

- (void)imageTapped:(NSDictionary*)image;
- (void)layoutFinished;
- (void)layoutFinished: (NSArray *)results;
- (void)noResults:(NSDictionary *) result;

@end

@interface DAMPhotoScrollView : DAMSelectableView<UIScrollViewDelegate, PhotoLayoutManagerDelegate> {
    UIScrollView *scrollView;
}

@property (nonatomic, retain) NSString *photoType;
@property (nonatomic, retain) id<DAMPhotoScrollViewDelegate> delegate;
@property (nonatomic) BOOL pauseLayout;
@property (nonatomic, retain) UIScrollView *scrollView;


- (void)clearContent;
- (void)loadImageMetaData:(NSString*)queryString;
- (void)loadImageMetaDataFromTags:(NSString*)tagSpec;
@end
