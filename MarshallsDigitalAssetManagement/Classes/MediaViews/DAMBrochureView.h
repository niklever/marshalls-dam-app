//
//  DAMBrochureView.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 27/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAMMediaViewBase.h"

@interface DAMBrochureView : DAMMediaViewBase<DAMNavigatorDelegate> {
    NSDictionary *testData;
}
@property (nonatomic, retain) NSDictionary *testData;

@end
