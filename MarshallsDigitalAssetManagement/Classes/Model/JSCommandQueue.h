//
//  JSCommandQueue.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSCommandQueue : NSObject {
}

@property (nonatomic, assign) BOOL inProgress;
@property (nonatomic, retain) NSString *idInProgress;

/*public static function that we mirrage from PG commandQueue.js file*/
/*add: function(id, command)*/
+ (void) add: (NSString *)id command:(NSString *) cmd;
/*finished:*/
+ (void)finished;

@end
