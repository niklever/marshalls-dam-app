//
//  JSMain.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSMain.h"
#import "JSSync.h"
#import "DAMHomeView.h"

@implementation JSMain
@synthesize opsSuspended;
@synthesize networkReachable;
@synthesize isOnWifi;

static JSMain *sharedInstance = nil;

+ (BOOL)validateEmail:(NSString *)email{
	if (!email) {
		return NO;
	}
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL result = [emailTest evaluateWithObject:email];
#ifdef DEBUG
    NSLog(@"result %d", result);
#endif
    return result;
}

+ (void)showAlertWithMessage:(NSString *)message{
    [self showAlertWithTitle:nil message:message];
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}

+ (BOOL)isIPad3 {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1);
}

+ (NSString *) getURLOfUserGuide {
    return BASE_USER_GUIDE_URL;
    /*
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *server = [prefs stringForKey:@"userguide"];
    return server;
    */
}

+ (NSString *) stringify: (NSDictionary *) object {
    
    /*SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
    
    NSString *jsonString = [jsonWriter stringWithObject: object];  
    
    [jsonWriter release];
    return jsonString;
    */
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        return @"{}";
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
}

+ (NSString*)apiHost {
    /*
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *server = [prefs stringForKey:@"server"];
    return server;
    */
    return BASE_URL;
}


+ (JSMain *)sharedInstance{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)init{
    self = [super init];
    if (self) {
        self.opsSuspended = FALSE;
    }
    return self;
}


- (void) pgFire:(NSString *)viewClass selector:(SEL) func withData:(id) data {
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSArray *controllers= appDelegate.navigationController.viewControllers;
    for (UIViewController *vc in controllers) {
        NSString *vcName = NSStringFromClass([vc class]);
#ifdef DEBUG
        NSLog(@"checking class name %@ vs viewClass == %@", vcName, viewClass);
#endif
        if ([vcName isEqualToString: viewClass]) {
            if ([vc respondsToSelector:func]) {
                [vc performSelector:func withObject: data];    
                return;
            }
        }else if ([[vc childViewControllers] count] > 0) {
            for (UIViewController *svc in [vc childViewControllers]) {
                NSString *vcName = NSStringFromClass([svc class]);
#ifdef DEBUG
                NSLog(@"checking child class name %@ vs viewClass == %@", vcName, viewClass);
#endif
                if ([vcName isEqualToString: viewClass]) {
                    if ([svc respondsToSelector:func]) {
                        [svc performSelector:func withObject: data];    
                        return;
                    }
                }
            } 
        }
        //end 
    }
}
          

- (void)goOffline{
    //if (self.networkReachable) {
#ifdef DEBUG
        NSLog(@"main.goOffline");
#endif
    self.isOnWifi = false;
    self.networkReachable = false;
    [kAppDelegate.damViewController.homeView notificationTimerFired:nil];
    //}
}

+ (NSDictionary*) deviceProperties
{
    UIDevice *device = [UIDevice currentDevice];
    NSMutableDictionary *devProps = [NSMutableDictionary dictionaryWithCapacity:4];
    [devProps setObject:[device model] forKey:@"platform"];
    [devProps setObject:[device systemVersion] forKey:@"version"];
    [devProps setObject:[[NSUUID UUID] UUIDString] forKey:@"uuid"];
    [devProps setObject:[device name] forKey:@"name"];
    
    NSDictionary *devReturn = [NSDictionary dictionaryWithDictionary:devProps];
    return devReturn;
}


+ (NSString *) getDate{
    
    /*NSString *myString;
	NSDate *now = [NSDate date];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
	myString = [dateFormatter stringFromDate:now];
	[dateFormatter release];
    return myString;
    */
    //get current date in GMT+0 time
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];
    
    return timeStamp;
}

-(void) goOnline{
    if (!self.networkReachable) {
        NSLog(@"main.goOnline");
        self.networkReachable = true;
        //TODO
        //sync.run();
        [[JSSync sharedInstance] run:^{
            NSLog(@"Done goOnline");
            [kAppDelegate.damViewController.homeView notificationTimerFired:nil];
            //[self performSelector:@selector(doneGoOnline) withObject: nil afterDelay:0.5];
        }];
    }
}

/*
- (void) doneGoOnline {
    
    [kAppDelegate.damViewController.homeView notificationTimerFired:nil];
    
    //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(notificationTimerFired:) withData: NULL];
}*/

+ (NSString *) getRealMediaFileName: (NSString *)url{
    
    NSLog(@"url == %@", url);
    
    NSString *filename = [url lastPathComponent];
    
    NSRange range = [filename rangeOfString:@"?"];
    if (range.location != NSNotFound) {
        return [[filename componentsSeparatedByString:@"?"] objectAtIndex: 0];
    }
    /*fix problem for videos*/
    if ([url rangeOfString:@"Thumbnails"].location != NSNotFound || [url rangeOfString:@"MaxHeight"].location != NSNotFound) {
        //this is thumbnail url
        if (filename == nil || filename.length == 0) {
            NSLog(@"empty file name %@", url);
            return nil;
        }
        
        filename = [NSString stringWithFormat:@"t-%@", filename];
    }else if ([url rangeOfString:@"AssetStore"].location != NSNotFound) {
        filename = [NSString stringWithFormat:@"as-%@", filename];
    }
    
    return filename;
}


- (void) updateDownloadAllVideos:(BOOL)state {
    [[JSLocalStorage sharedInstance] setItem: @"main.settings_video" value: state ? @"yes" : @"no"];
}

- (void) updateDownloadAllSamples:(BOOL)state {
    [[JSLocalStorage sharedInstance] setItem: @"main.settings_samples" value: state ? @"yes" : @"no"];
}

- (void) updateDownloadAllBrochures:(BOOL)state {
    [[JSLocalStorage sharedInstance] setItem: @"main.settings_brochures" value: state ? @"yes" : @"no"];
}


- (BOOL) isDownloadAllVideos {
    NSString *value = [[JSLocalStorage sharedInstance] getItem: @"main.settings_video"];
    if (value && [value isEqualToString:@"yes"]) {
        return TRUE;
    }
    return NO;
}

- (BOOL) isDownloadAllSamples {
    NSString *value = [[JSLocalStorage sharedInstance] getItem: @"main.settings_samples"];
    if (value && [value isEqualToString:@"yes"]) {
        return TRUE;
    }
    return NO;
}

- (BOOL) isDownloadAllBrochures {
    NSString *value = [[JSLocalStorage sharedInstance] getItem: @"main.settings_brochures"];
    if (value && [value isEqualToString:@"yes"]) {
        return TRUE;
    }
    return NO;
}

- (void) goOnWifi {
    self.isOnWifi = YES;
}
- (void) goOffWifi {
    self.isOnWifi = NO;
}

- (BOOL)onWifi{
    return self.isOnWifi;
}

- (BOOL)online{
    return self.networkReachable;
}

- (NSString *) userEmail{
    return [[JSLocalStorage sharedInstance] getItem: @"main.emailKey"];
}

- (void)setUserEmail:(NSString *)email{
    //TODO
    //iDrive.login(email, "");
    [[JSLocalStorage sharedInstance] setItem: @"main.emailKey" value:email];
}

- (BOOL) isIOS7
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        return YES;
    }
    return NO;
}

- (NSString *) validUserEmail{
    return [[JSLocalStorage sharedInstance] getItem: @"main.validUserEmailKey"];
}

- (void)setValidUserEmail:(NSString *)email{
    [[JSLocalStorage sharedInstance] setItem: @"main.validUserEmailKey" value:email];
}


- (void) setBusinessAreaFilters:(NSArray *) filters {
#ifdef DEBUG
    NSLog(@"set businessArea filters == %@", [filters JSONString]);
#endif
    [[JSLocalStorage sharedInstance] setItem: @"businessAreaFilters" value: [filters JSONString]];
}

- (NSArray *) getBusinessAreaFilters{
    NSString *configs = [[JSLocalStorage sharedInstance] getItem: @"businessAreaFilters"];
    NSArray *_settings = [configs objectFromJSONString];
    //NSLog(@"sync._getSettings %@", configs);
    if (_settings == nil || [_settings count] == 0) {
#ifdef DEBUG
        NSLog(@"businessArea filters NULL");
#endif
        return NULL;
    }
    return _settings;
}

- (NSString *) extract2WordsFromTitle:(NSString *)title withKey:(NSString *)key{
    NSArray *temp = [title componentsSeparatedByString:@" "];
    NSArray *keytemp = [key componentsSeparatedByString:@" "];
    
    int startPos = 0;
    int index = 0;
    int nextPos = 0;
    for (NSString *str in temp) {
        if ([str rangeOfString: [keytemp objectAtIndex:nextPos] options:NSCaseInsensitiveSearch].location!=NSNotFound) {
            //remember this spos need to check next key temp index if keytemp > 1
            if (nextPos == 0)
                startPos = index;
            nextPos = index + 1;
            if ([keytemp count] <= nextPos) {
                break;
            }
        }else{
            nextPos = 0;
        }
        index++;
        
    }
    
    NSString *result = @"";
    if (nextPos == 0) {
        return title;
    }
    for (int i = startPos ; i<= nextPos && i < [temp count]; i++) {
        result = [NSString stringWithFormat:@"%@ %@", result, [temp objectAtIndex: i]];
    }
    return result;
}

-(void)_globalSearch:(NSString *)mediaType queryString:(NSString *)query containers:(NSArray *)_containers callback:(void (^)(int result))fcallback{
    NSString * placeholders = [JSDBV2 placeholdersForArray: _containers];
    NSString *sql = @"";
    if (query == NULL || query.length == 0) {
        sql = [NSString stringWithFormat:@"select distinct mediaRef from media where mediaType = '%@' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", mediaType, placeholders];
    }else{
        sql = [NSString stringWithFormat:@"select distinct mediaRef from media where (title like '%%%@%%' or tags like '%%%@%%' or products like '%%%@%%' or location like '%%%@%%' or meta like '%%%@%%') AND mediaType = '%@' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", allTrim(query),allTrim(query),allTrim(query),allTrim(query),allTrim(query), mediaType, placeholders];
    }
#ifdef DEBUG
    NSLog(@"_globalSearch == %@", sql);
#endif
    
    //NSArray *rows = [[JSDBV2 sharedInstance] sql: sql params: _containers];
    [[JSDBV2 sharedInstance] executeQuery: sql params: _containers successCallback:^(NSMutableArray *rows) {
        fcallback(rows ? rows.count : 0);
    }];
    //fcallback(rows ? rows.count : 0);
}

/*
dictionary return in format {"Video":2,"Presentation":0,"Photo":124,"Brochure":0,"Sample":12,"CaseStudy":15}
 */
- (void) globalSearch:(NSString *)queryString containers:(NSArray *)_containers callback:(void (^)(NSDictionary *result))successCalback{
    __block int photos = 0, samples = 0, videos = 0, brochures = 0, cs = 0, pre = 0;
    [self _globalSearch:kMediaType_Photos queryString:queryString containers:_containers callback:^(int result) {
        photos = result;
        [self _globalSearch:kMediaType_Samples queryString:queryString containers:_containers callback:^(int result) {
            samples = result;
            [self _globalSearch:kMediaType_Videos queryString:queryString containers:_containers callback:^(int result) {
                videos = result;
                [self _globalSearch:kMediaType_Brochures queryString:queryString containers:_containers callback:^(int result) {
                    brochures = result;
                    [self _globalSearch:kMediaType_CaseStudies queryString:queryString containers:_containers callback:^(int result) {
                        cs = result;
                        [self _globalSearch:kMediaType_Presentations queryString:queryString containers:_containers callback:^(int result) {
                            pre = result;
                            if (successCalback) {
                                successCalback([NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt: photos], @"Photo",[NSNumber numberWithInt:samples],@"Sample", [NSNumber numberWithInt:videos], @"Video", [NSNumber numberWithInt:brochures], @"Brochure", [NSNumber numberWithInt:cs], @"CaseStudy", [NSNumber numberWithInt:pre], @"Presentation",nil]);
                            }
                        }];
                    }];
                }];
            }];
        }];
        
    }];
    /*
    int photos = [self _globalSearch:kMediaType_Photos queryString:queryString containers:_containers];
    int samples = [self _globalSearch:kMediaType_Samples queryString:queryString containers:_containers];
    int videos = [self _globalSearch:kMediaType_Videos queryString:queryString containers:_containers];
    int brochures = [self _globalSearch:kMediaType_Brochures queryString:queryString containers:_containers];
    int cs = [self _globalSearch:kMediaType_CaseStudies queryString:queryString containers:_containers];
    int pre = [self _globalSearch:kMediaType_Presentations queryString:queryString containers:_containers];
    */
    
}

/*
 
 The svc/search_summary uses this code.
 
 var searchResults = db.MediaItems.Where(x => (x.Name.ToLower().Contains(searchText)
 || x.Location.ToLower().Contains(searchText)
 || x.MediaItemTags.Any(y => y.Name.ToLower().Contains(searchText))
 || x.MediaItemProducts.Any(y => y.Product.Name.ToLower().Contains(searchText)))
 && x.DeletedDate == null);
 */
- (void) predictiveKeysWithKey:(NSString *)key mediaType:(NSString *)_mediaType containers:(NSArray *)_containers callback:(void (^)(NSArray *results))successCalback {
    NSString * placeholders = [JSDBV2 placeholdersForArray: _containers];
    /*
    if ([_mediaType isEqualToString:kMediaType_Photos]) {
        sql = [NSString stringWithFormat:@"select distinct title from media where title like '%@%%' AND mt=1    order by priority desc, updatedDate limit 10", allTrim(key)];
        _containers = NULL;
    }else{
        sql = [NSString stringWithFormat:@"select distinct title from media where title like '%@%%' AND mediaType = '%@' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) ) order by priority desc, updatedDate limit 10", allTrim(key), _mediaType, placeholders];
    }*/
    NSString *sql  = @"";
    if (_mediaType) {
        sql = [NSString stringWithFormat:@"select distinct title from media where (title like '%%%@%%' or tags like '%%%@%%' or products like '%%%@%%' or location like '%%%@%%') AND mediaType = '%@' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) ) order by title", allTrim(key),allTrim(key),allTrim(key),allTrim(key), _mediaType, placeholders];
    }else{
        sql = [NSString stringWithFormat:@"select distinct title from media where (title like '%%%@%%' or tags like '%%%@%%' or products like '%%%@%%' or location like '%%%@%%') AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) ) order by title", allTrim(key),allTrim(key),allTrim(key),allTrim(key), placeholders];
    }
    
    //NSArray *rows = [[JSDBV2 sharedInstance] sql: sql params: _containers];
    [[JSDBV2 sharedInstance] executeQuery: sql params: _containers successCallback:^(NSMutableArray *rows) {
        NSLog(@"predictiveKeysWithKey == %@ containers == [%@] ==> results length == %d", sql,[_containers componentsJoinedByString:@","], [rows count]);
        NSMutableArray *result = [NSMutableArray array];
        NSMutableArray *checkExist = [NSMutableArray array];
        if (rows && [rows count] > 0) {
            for (NSDictionary *row in rows) {
                if ([allTrim([row objectForKey:@"title"]) length] > 0) {
                    NSString *title = allTrim([self extract2WordsFromTitle:allTrim([row objectForKey:@"title"]) withKey: allTrim(key)]);
                    if (![checkExist containsObject: title]) {
                        [result addObject: [NSDictionary dictionaryWithObjectsAndKeys: title, @"text", nil]];
                        [checkExist addObject: title];
                    }
                }
            }
        }
        if(successCalback) {
            /*
             NSArray *result = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:@"Tegula suggesstion 1", @"text", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Tegula suggesstion 2", @"text", nil] ,[NSDictionary dictionaryWithObjectsAndKeys:@"Tegula suggesstion 3", @"text", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Tegula suggesstion 4", @"text", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Tegula suggesstion 5", @"text", nil],nil];
             */
            successCalback(result);
        }
    }];

}


- (void) checkForCMSDeletions:(id)plugin method:(SEL)_medthod {
    
    /*iDrive.filesDeletedByCMS(function(files){
        main.pgFire(plugin, method, files);
    });
    */
    [[JSiDrive sharedInstance] filesDeletedByCMS:^(NSMutableArray *files) {
        [plugin performSelector:_medthod withObject: files];
    }];
    //TODO
    //main.pgFire(plugin, method, files);
}

- (void)search: (NSString *)term mediaType:(NSString *)_mediaType containers:(NSArray *)_containers pageSize:(int)_pageSize pageNum:(int)_pageNum userInfo:(id)_userInfo plugin:(id)delegate method: (SEL) _method {
#ifdef DEBUG
    NSLog(@"main.search term: %@ mediaType: %@, pageSize: %d, pageNum: %d", term,_mediaType, _pageSize,_pageNum);
#endif
    NSString * placeholders = [JSDBV2 placeholdersForArray: _containers];
    //NSString * sql = [NSString stringWithFormat:@"SELECT distinct meta FROM media WHERE mediaType = ? AND meta LIKE ? AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", placeholders];
    //replace meta by title
    NSString *sql = @"";
    if (term == NULL || term.length == 0) {
        sql = [NSString stringWithFormat:@"select distinct meta from media where mediaType = '%@' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", _mediaType, placeholders];
    }else{
        sql = [NSString stringWithFormat:@"select distinct meta from media where (title like '%%%@%%' or tags like '%%%@%%' or products like '%%%@%%' or location like '%%%@%%' or meta like '%%%@%%') AND mediaType = '%@' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", allTrim(term),allTrim(term),allTrim(term),allTrim(term),allTrim(term), _mediaType, placeholders];
    }
    
    NSLog(@"mainSearch == %@", sql);
    
    /*NSString * sql = [NSString stringWithFormat:@"SELECT distinct meta FROM media WHERE mediaType = ? AND meta LIKE ? AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", placeholders];
    */
    //client request: sort videos alphabetically, rather than last modded
    //Samples do not use star rating in ordering. Alphabetical only. Star rating ONLY refers to Photos.
    //final client want order by priority for all
    sql = [NSString stringWithFormat:@"%@ ORDER BY priority desc, title asc", sql];
    
    [self carouselGetResults:sql params: _containers pageSize:_pageSize pageNum:_pageNum userInfo:_userInfo plugin:delegate method:_method];
}

//carousel: function(mediaType, containers, pageSize, pageNum, userInfo, plugin, method) {
- (void)carousel:(NSString *) mediaType containers: (NSArray *) _containers pageSize:(int)_pageSize pageNum: (int) _pageNum userInfo:(id)_userInfo plugin:(id)delegate method: (SEL) _method{
#ifdef DEBUG
    NSLog(@"main.carousel mediaType: %@, pageSize: %d, pageNum: %d _containers: %@",mediaType, _pageSize, _pageNum, _containers);
#endif
    if (mediaType == NULL) {
        mediaType = @"";
    }
    NSString * placeholders = [JSDBV2 placeholdersForArray: _containers];
    NSString *sql = [NSString stringWithFormat:@"SELECT distinct meta FROM media WHERE mediaType = ? AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN (%@) )", placeholders];
    
    //client request: sort videos alphabetically, rather than last modded, clients request to order by title for Samples in phase2
    //final client want order by priority for all
    sql = [NSString stringWithFormat:@"%@ ORDER BY priority desc, title asc", sql];
    
    /*
    if ([mediaType isEqualToString:kMediaType_Photos]) {
        sql = [NSString stringWithFormat:@"%@ ORDER BY priority desc", sql];
    }else {
        sql = [NSString stringWithFormat:@"%@ ORDER BY title", sql];
    }*/
    /*
    if ([mediaType isEqualToString:kMediaType_Videos] || [mediaType isEqualToString:kMediaType_Samples]) {
        sql = [NSString stringWithFormat:@"%@ ORDER BY priority desc,title", sql];
    }else {
        //phase 2 need to order by Priority
        sql = [NSString stringWithFormat:@"%@ ORDER BY priority desc", sql];
    }*/
    NSMutableArray *params = [NSMutableArray arrayWithArray: _containers];
    [params insertObject: mediaType atIndex:0];
    
    [self carouselGetResults: sql params: params pageSize:_pageSize pageNum:_pageNum userInfo:_userInfo plugin:delegate method:_method];
}


//- (void) carouselGetResults: function(sql, params, pageSize, pageNum, userInfo, plugin, method) {
- (void) carouselGetResults: (NSString *) sql params: (NSArray *) _params pageSize: (int)_pageSize pageNum: (int) _pageNum userInfo: (id) _userInfo plugin:(id)delegate method: (SEL) _method{
    NSLog(@"******* CAROUSEL: sql: %@, params: %@", sql, _params);

	NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    [result setObject:[NSNumber numberWithInt: 0] forKey:@"pageCount"];
    [result setObject:_userInfo forKey:@"userInfo"];
    [result setObject:[NSMutableArray array] forKey:@"assets"];
    /*
    var result = {
    pageCount: 1,
    userInfo: userInfo,
    assets: []
    };
    */
    NSLog(@"main.carousel sql: %@, params: %@ , _pageNum == %d, _pageSize == %d", sql, _params, _pageNum, _pageSize);
    
    //NSArray * res = [[JSDBV2 sharedInstance] map: sql params:_params];
    [[JSDBV2 sharedInstance] executeQueryMap:sql params:_params successCallback:^(NSMutableArray *res) {
        if (res) {
            int start = _pageNum * _pageSize;
            int end = start + _pageSize;
            for (int i = start ; i < end && i < [res count]; i++) {
                [(NSMutableArray *)[result objectForKey:@"assets"] addObject: [res objectAtIndex: i]];
            }
            
            int pageCount = ceil(([res count] * 1.0) / (_pageSize * 1.0));
            
            [result setObject: [NSNumber numberWithInt: pageCount] forKey:@"pageCount"];
            
            //result.assets = res.slice(start, end);
            //result.pageCount = Math.ceil(res.length / pageSize);
        }
        if (delegate && [delegate respondsToSelector:_method]) {
            //need to fire clear content first
            [delegate performSelector:_method withObject: result];
        }else{
            [[JSMain sharedInstance] pgFire:@"OfflineData" selector:@selector(localDataGotResult:) withData:result];
        }
        [result release];
    }];
    
    //return result;
    /*
    db.map(sql, params, function(res){
        console.log(res);
        var start = pageNum * pageSize;
        var end = start + pageSize;
        result.assets = res.slice(start, end);
        result.pageCount = Math.ceil(res.length / pageSize);
        if (plugin && method)
            main.pgFire(plugin, method, result);
        else
            main.pgFire("OfflineData", "localDataGotResult", result);
    });*/
}

@end
