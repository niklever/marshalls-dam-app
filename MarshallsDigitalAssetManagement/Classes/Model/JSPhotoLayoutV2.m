//
//  JSPhotoLayoutV2.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 3/5/13.
//
//

#import "JSPhotoLayoutV2.h"

@implementation JSPhotoLayoutV2

static JSPhotoLayoutV2 *sharedInstance = nil;


+ (JSPhotoLayoutV2 *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}


- (void)photos2: (NSString *)query mediaType: (NSString *)mediaType callback:(void (^)(NSArray *results))successCalback{
    
    NSArray *businessAreas = [NSArray arrayWithObjects:@"domestic", @"commercial", @"group", nil];
    
    //NSArray *activeContainers = [[[JSSync sharedInstance] getActiveContainers: [businessAreas JSONString]] objectFromJSONString];
    [[JSSync sharedInstance] getActiveContainers:[businessAreas JSONString] callback:^(NSString * js) {
        NSArray *activeContainers = [js objectFromJSONString];
        NSArray *filterAreas = [[JSMain sharedInstance] getBusinessAreaFilters];
        if (filterAreas != NULL && [filterAreas count] > 0) {
            NSLog(@"Apply business filter area %@", filterAreas);
            activeContainers = filterAreas;
        }
        NSString * placeholders = [JSDBV2 placeholdersForArray: activeContainers];
        //var sql = "SELECT meta FROM media WHERE mt = 1 ORDER BY thumbHeight ASC, thumbWidth ASC";
        NSString * q = @"";
        
        if (query && [allTrim(query) length] > 0) {
            /*
             q = " AND meta LIKE ?";
             params.push("%" + query + "%");
             */
            NSLog(@"photos2 query: %@ length == %d", query, [allTrim(query) length]);
            
            q = [NSString stringWithFormat:@" AND (title like '%%%@%%' or tags like '%%%@%%' or products like '%%%@%%' or location like '%%%@%%' or meta like '%%%@%%') AND mediaType = '%@' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", allTrim(query),allTrim(query),allTrim(query),allTrim(query),allTrim(query), mediaType, placeholders];
            
        }else{
            q = [NSString stringWithFormat:@" AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )",placeholders];
        }
        
        //NSString *sql = [NSString stringWithFormat:@"SELECT distinct mediaRef, mediaType, title, thumbnail, url, thumbWidth, thumbHeight,width,height FROM media WHERE mt = 1 %@ and priority = %d ORDER BY thumbHeight ASC, thumbWidth ASC", q, activePriority];
        
        NSString *sql = [NSString stringWithFormat:@"SELECT distinct mediaRef, mediaType, title, thumbnail, url, thumbWidth, thumbHeight,width,height,priority,latitude,longitude FROM media WHERE mt = 1 and mediaType='%@' %@ ORDER BY priority DESC, thumbHeight ASC, thumbWidth ASC ", mediaType, q];//priority DESC, , thumbHeight ASC, thumbWidth ASC
        
        //sql += " LIMIT 1000";
        
        NSLog(@"photos2 sql: %@",sql);
        
        [[JSDBV2 sharedInstance] executeQueryMap:sql params:activeContainers successCallback:^(NSMutableArray *photos) {
            successCalback(photos);
        }];
        /*
        if (successCalback) {
            successCalback([[JSDB sharedInstance] map: sql params: activeContainers success:nil fail:nil]);
        }
         */
    }];
    
}


- (void)photosWithTags: (NSString *)tags mediaType: (NSString *)mediaType callback:(void (^)(NSArray *media))successCalback {
    //        var photos = [];
    //quotedTags = $(tags.split("|")).map(function(index, tag){ return "'" + tag.toUpperCase() + "'"});
    NSArray *temp = [tags componentsSeparatedByString:@"|"];
    NSMutableArray *upperTags = [NSMutableArray array];
    for (NSString *str in temp) {
        [upperTags addObject:[NSString stringWithFormat:@"'%@'", [str uppercaseString]]];
    }
    NSString *tagStr = [upperTags componentsJoinedByString:@","];//$.makeArray(quotedTags).join(",");
    NSString *sql = [NSString stringWithFormat:@"SELECT distinct m.mediaRef, mediaType, title, thumbnail, url, thumbWidth, thumbHeight,width,height,priority FROM media m INNER JOIN media_tags mt ON m.mediaRef = mt.mediaRef WHERE upper(tag) in (%@)  AND mt = 1 and AND mediaType = '%@' ORDER BY priority DESC, thumbHeight ASC, thumbWidth ASC", tagStr, mediaType];
    
#ifdef DEBUG
    NSLog(@"photosWithTags == %@", sql);
#endif
    
    //NSArray *rows = [[JSDBV2 sharedInstance] sql:sql params:nil];
    [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray * rows) {
        successCalback(rows);
    }];
    
    /*if (successCalback) {
        successCalback(rows);
    }*/
    
}

- (void)photoTags:(id) delegate mediaType: (NSString *)mediaType{
    //NSArray *rows = [[JSDBV2 sharedInstance] sql:@"SELECT DISTINCT tag FROM media_tags" params:nil];
    [[JSDBV2 sharedInstance] executeQuery:@"SELECT DISTINCT tag FROM media_tags" params:nil successCallback:^(NSMutableArray *rows) {
        if (rows && [rows count] > 0) {
            NSMutableArray *tagsArray = [[NSMutableArray alloc] init];
            for (NSDictionary *row in rows) {
                [tagsArray addObject: [row objectForKey:@"tag"]];
            }
            
#ifdef DEBUG
            NSLog(@"tagsArray %@", tagsArray);
#endif
            
            [delegate performSelector:@selector(tagsAvailable:) withObject: tagsArray];
            [tagsArray release];
        }
    }];
    
}

- (void) layoutInitWithTags: (id)plugin  tagSpec: (NSString *)_tagSpec mediaType: (NSString *)mediaType{

    [self photosWithTags: _tagSpec mediaType: mediaType callback:^(NSArray *photos) {
        //self.currentPhotos = [NSMutableArray arrayWithArray:photos];
        //photoCount = 0;
        
#ifdef DEBUG
        NSLog(@"photoLayout.layoutInit photos.length: %d", photos ? [photos count] : 0);
#endif
        
        //NSLog(@"photoLayout.layoutInit photos.length: %d", photos ? [photos count] : 0);
        
        if ([photos count] == 0) {
            //[[JSMain sharedInstance] pgFire:plugin selector:@selector(noLayoutResults:) withData: nil];
            [plugin performSelector:@selector(noLayoutResults:) withObject: nil];
            return;
        }
        
        NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //NSString *temp = @"";
        for (NSDictionary *photo in photos) {
            
            NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
            [tmp setValuesForKeysWithDictionary: photo];
            
            //[tmp setObject: [photo objectForKey:@"mediaRef"] forKey:@"filename"];
            [tmp setObject: [photo objectForKey:@"mediaRef"] forKey:@"mediaRef"];
            [tmp setObject: [NSString stringWithFormat:@"%@/%@", [JSMain apiHost], [photo objectForKey:@"thumbnail"]] forKey: @"filename"];
            [tmp setObject: [photo objectForKey:@"thumbnail"] forKey: @"thumbnail"];
            
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbWidth"] intValue]] forKey:@"width"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbHeight"] intValue]] forKey:@"height"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"width"] intValue]] forKey:@"originalWidth"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"height"] intValue]] forKey:@"originalHeight"];
            //priority
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"priority"] intValue]] forKey:@"priority"];
            [tmp setObject: [NSNumber numberWithBool: FALSE] forKey:@"used"];
            
            //adding lat and long
            if ([photo objectForKey:@"latitude"] && [photo objectForKey:@"longitude"]) {
                [(NSMutableDictionary *)tmp setObject:[photo objectForKey:@"latitude"] forKey:@"latitude"];
                [(NSMutableDictionary *)tmp setObject:[photo objectForKey:@"longitude"] forKey:@"longitude"];
            }
            
            
            [searchResults addObject: [NSMutableDictionary dictionaryWithDictionary:tmp]];
            
            [tmp release];
            
            
            //[tmp release];
        }
        
#ifdef DEBUG
        NSLog(@"AFTER initTiling() searchResults length == %d", [self.searchResults count]);
#endif
        NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
        BOOL more = NO;
        more = !true;
        
        //JSTiling * titling = [[JSTiling alloc] init];
        
        [result setObject: [NSNumber numberWithBool: more] forKey:@"more"];
        //[result setObject: [[JSTiling sharedInstance] initTiling: self.searchResults] forKey:kMediaType_Photos];
        //[titling release];
        //titling = nil;
        JSTiling *tile = [[JSTiling alloc] init];
        NSMutableArray *tilingResults = [NSArray arrayWithArray: [tile getTiling: searchResults]];
        [result setObject: tilingResults forKey:kMediaType_Photos];
        [tile release];
        
        //[result setObject: [tile initTiling: searchResults] forKey:kMediaType_Photos];
        //[tile release];
        //[self.searchResults release];
        
        [plugin performSelector:@selector(layoutPartialResult:) withObject: result];
        
        [searchResults release];
        [result release];
        
    }];
    
}


- (void) layoutInit:(id) plugin query:(NSString *)strQuery mediaType: (NSString *)mediaType{
    
#ifdef DEBUG
    NSLog(@"photoLayout.layoutInit plugin: %@",plugin);
#endif
    //[self initTiling];
    //self.layoutQueryString = strQuery;
    
    [self photos2: strQuery mediaType: mediaType callback:^(NSArray *photos) {
        
        //self.currentPhotos = [NSMutableArray arrayWithArray:photos];
        //photoCount = 0;
        
#ifdef DEBUG
        NSLog(@"photoLayout.layoutInit photos.length: %d", photos ? [photos count] : 0);
#endif
        
        //NSLog(@"photoLayout.layoutInit photos.length: %d", photos ? [photos count] : 0);
        
        if ([photos count] == 0) {
            //[[JSMain sharedInstance] pgFire:plugin selector:@selector(noLayoutResults:) withData: nil];
            [plugin performSelector:@selector(noLayoutResults:) withObject: nil];
            return;
        }
        
        NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //NSString *temp = @"";
        for (NSDictionary *photo in photos) {
            NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
            [tmp setValuesForKeysWithDictionary: photo];
            //NSMutableDictionary *tmp = [NSMutableDictionary dictionaryWithDictionary: photo];
            
            //[tmp setObject: [photo objectForKey:@"mediaRef"] forKey:@"filename"];
            [tmp setObject: [photo objectForKey:@"mediaRef"] forKey:@"mediaRef"];
            [tmp setObject: [NSString stringWithFormat:@"%@/%@", [JSMain apiHost], [photo objectForKey:@"thumbnail"]] forKey: @"filename"];
            [tmp setObject: [photo objectForKey:@"thumbnail"] forKey: @"thumbnail"];
            
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbWidth"] intValue]] forKey:@"width"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbHeight"] intValue]] forKey:@"height"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"width"] intValue]] forKey:@"originalWidth"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"height"] intValue]] forKey:@"originalHeight"];
            [tmp setObject: [NSNumber numberWithBool: FALSE] forKey:@"used"];
            //priority
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"priority"] intValue]] forKey:@"priority"];
            
            //adding lat and long
            if ([photo objectForKey:@"latitude"] && [photo objectForKey:@"longitude"]) {
                [(NSMutableDictionary *)tmp setObject:[photo objectForKey:@"latitude"] forKey:@"latitude"];
                [(NSMutableDictionary *)tmp setObject:[photo objectForKey:@"longitude"] forKey:@"longitude"];
            }
            
            [searchResults addObject: [NSMutableDictionary dictionaryWithDictionary:tmp]];
            
            [tmp release];
            
            /*temp = [temp stringByAppendingString: [JSMain stringify: tmp]];
            temp = [temp stringByAppendingString: @","];
            */
            //[tmp release];
        }
        
        //NSLog(@"AFTER initTiling() searchResults length == %d", [self.searchResults count]);
        
        //NSLog(@"self.searchResults == %@", temp);
        
        //NSMutableDictionary *result = [NSMutableDictionary dictionary];
        NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
        BOOL more = NO;
        more = !true;
        [result setObject: [NSNumber numberWithBool: more] forKey:@"more"];
        
        //[result setObject: [[JSTiling sharedInstance] initTiling: self.searchResults] forKey:kMediaType_Photos];
        JSTiling *tile = [[JSTiling alloc] init];
        NSMutableArray *tilingResults = [NSArray arrayWithArray: [tile getTiling: searchResults]];
        [result setObject: tilingResults forKey:kMediaType_Photos];
        [tile release];
        

        [plugin performSelector:@selector(layoutPartialResult:) withObject: [NSMutableDictionary dictionaryWithDictionary: result]];
        
        [searchResults release];
        [result release];
    }];
}

@end
