//
//  JSDBV2.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 8/11/13.
//
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "FMResultSet.h"

@interface JSDBV3 : NSObject {
    //FMDatabase *db;
}

+ (id)sharedInstance;

- (void) sql: (NSString *)sqlStatement params: (NSArray *) parameters success:(void (^)(NSMutableArray *))callbackFunc;
- (void) map: (NSString *)sqlStatement params: (NSArray *) parameters success:(void (^)(NSMutableArray *))callbackFunc;

/*batchInsert: function(batch, success, fail) {*/
- (void) batchInsert: (NSArray *) batch;
- (bool)sqlExec:(NSDictionary *)batch;
- (bool)sqlExecWithSQLStatement:(NSString *)sql;
- (bool)sqlExecWithSQLStatement:(NSString *)sql params: (NSArray *) parameters;

+ (NSString *) placeholdersForArray: (NSArray *) array;

//-(bool)openDB;
//-(void)closeDB;
-(bool) initialiseDatabase;

@end
