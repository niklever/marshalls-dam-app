//
//  JSiDrive.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSiDrive.h"
#import "JSDBV2.h"
#import "JSMain.h"
#import "JSSync.h"
#import "JSPath.h"
#import "JSLocalStorage.h"

@implementation JSiDrive
//@synthesize iNodetable;
@synthesize iDrivePushInterval;
//@synthesize iNodeCols;
//@synthesize schemaCols;
//@synthesize journalTable;
@synthesize user;
@synthesize pushMethod;
@synthesize pullMethod;

static JSiDrive *sharedInstance = nil;

+ (JSiDrive *)sharedInstance{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (void) setDelegate:(id)aDelegate {
}

- (id)init{
    self = [super init];
    if (self) {
        self.iNodetable = @"idrive";
        self.journalTable = @"idrive_journal";
        self.user = @""; //NULL we use empty string instead of NULL value
        
        
        self.iDrivePushInterval = 10;//10* 1000;
    /*
    lsDateKey: "iDrive.lastUpdated",
    user: null,
     */
    self.pushMethod = @"/api/iDrive.svc/json/syncCall";
    self.pullMethod = @"/api/iDrive";
        
        /*
        self.iNodeCols = [NSMutableArray array];
        [self.iNodeCols addObject:@"id INTEGER PRIMARY KEY"];
        [self.iNodeCols addObject:@"ctime DATE"];
        [self.iNodeCols addObject:@"user TEXT"];
        [self.iNodeCols addObject:@"type INT"];//0=file, 1 = folder
        [self.iNodeCols addObject:@"fdep INT"];         //depth of inode
                //        "parent TEXT",      //parent folder
                //        "folder TEXT",
        [self.iNodeCols addObject:@"path TEXT"];
                //cached meta-data from carousel and search APIs (empty if folder)
        [self.iNodeCols addObject:@"mediaRef TEXT"];
        [self.iNodeCols addObject:@"mediaType TEXT"];
        [self.iNodeCols addObject:@"title TEXT"];
        [self.iNodeCols addObject:@"thumbnail TEXT"];
        [self.iNodeCols addObject:@"url TEXT"];
        
        self.journalTable = @"idrive_journal";
        
        self.schemaCols = [NSMutableArray array];
        [self.schemaCols addObject:@"id INTEGER PRIMARY KEY"];
        [self.schemaCols addObject:@"direction INT"];            //0 == outgoing, 1 == incoming
        [self.schemaCols addObject:@"user TEXT"];
        [self.schemaCols addObject:@"op TEXT"];
        [self.schemaCols addObject:@"params TEXT"];
        [self.schemaCols addObject:@"mtime DATE"];
        
        NSString *iDriveInitialised = [[JSLocalStorage sharedInstance] getItem: @"iDrive.initialised"];
#ifdef DEBUG
        NSLog(@"iDrive.init iDriveInitialised: %@",iDriveInitialised);
#endif
        if (!iDriveInitialised || ![iDriveInitialised isEqualToString:@"true"]) {
            //TODO
            //commandQueue.add("iDrive.createSchema", iDrive.createSchema);
            [self createSchema];
            [[JSLocalStorage sharedInstance] setItem:@"iDrive.initialised" value:@"true"];
        }
        */
        //commandQueue.finished();
        //[self performSelector:@selector(pushMedia) withObject: nil afterDelay:iDrivePushInterval];
        /*setInterval(function(){
            //TODO - synchronisation disabled. Not explicitly in scope per client request.
            //However, push ops up to server so that state is recorded.
            //            iDrive.pullMedia();
            //            iDrive.pullMediaFromQueue();
            iDrive.pushMedia();
        }, iDrive.iDrivePushInterval);
       */  
    }
    return self;
}

- (void) pushMedia {
    if ([JSMain sharedInstance].opsSuspended)
        return;
    if (![[JSMain sharedInstance] online]) {
#ifdef DEBUG
        NSLog(@"iDrive.pushMedia.WARN_OFFLINE");
#endif
        return;
    }
    if (self.user == NULL || [self.user length] == 0) {
#ifdef DEBUG
        NSLog(@"iDrive.pushMedia.WARN_NO_USER");
#endif
        return;
    }
    //iDrive.readJournal(true, function(result) {
    NSMutableDictionary *result = [self readJournal: true];
    if (result != nil) {
        //TODO
        //            console.log("iDrive.pushMedia json:"+JSON.stringify(result));
#ifdef DEBUG
        NSLog(@"iDrive.pushMedia json: %@", result);
#endif
        if ([result objectForKey:@"ops"] != NULL && [(NSMutableArray *)[result objectForKey:@"ops"] count] > 0) {
            NSMutableDictionary *post = [NSMutableDictionary dictionary];
            [post setObject: self.user forKey: @"userEmail"];
            [post setObject: [[JSMain deviceProperties] objectForKey:@"uuid"] forKey:@"iPadID"];
            [post setObject: [JSMain getDate] forKey:@"since"];
            [post setObject: [result objectForKey:@"ops"] forKey:@"ops"];
            //NSString *jsonPost = [JSMain stringify: post];
#ifdef DEBUG
            //NSLog(@"iDrive.pushMedia post: %@",jsonPost);
#endif
            
            [[APICommon sharedInstance] iDrivePushMethod: post
                                         successCallback:^(id resp) {
                                             NSLog(@"response body %@", resp);
                                             //NSDictionary *result = request.userInfo;
                                             //NSString *response = [request responseString];
                                             [self clearJournal:0 ids: [result objectForKey:@"ops"]];
                                         } andFailCallback:^(id msg) {
                                             
                                         }];
            
            /*
            NSMutableDictionary *rheader = [NSMutableDictionary dictionary];
            [rheader setObject:@"text/json; charset=utf-8" forKey: @"Content-Type"];
            AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
            [operationManager POST: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], self.pushMethod]
                        parameters:post
                           success:^(AFHTTPRequestOperation *operation, id responseObject){
                               // Enter what happens here if successsful.
                               NSLog(@"response body %@", responseObject);
                               //NSDictionary *result = request.userInfo;
                               //NSString *response = [request responseString];
                               [self clearJournal:0 ids: [result objectForKey:@"ops"]];
                           }
                           failure:^(AFHTTPRequestOperation *operation, NSError *error){
                               
                               // Enter what happens here if failure happens
                           }];
            */
            /*
            ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [JSMain apiHost], self.pushMethod]]];
            [request appendPostData:[jsonPost dataUsingEncoding:NSUTF8StringEncoding]];
            [request setRequestMethod:@"POST"];
            [request setRequestHeaders: rheader];
            [request setDelegate:self];
            [request setDidFinishSelector:@selector(requestDone:)];
            [request setDidFailSelector:@selector(requestWentWrong:)];
            request.userInfo = result;
            [request start];
             */
        }
        /*
        if (result.ops.length > 0) {
            post = {
            userEmail: iDrive.user,
            iPadID: DeviceInfo.uuid, //"1234567890123456789012356789012345678901", //
            since: (new Date()).toISOString(),
            ops: result.ops
            };
            //                console.log("iDrive.pushMedia post:" + JSON.stringify(post));
            $.ajax({
            type: "POST",
            url: main.apiHost + iDrive.pushMethod,
            data: JSON.stringify(post),
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Content-Type", "text/json; charset=utf-8");
            }
            }).done(function(response){
                if (response.d.success) {
                    iDrive.clearJournal(0, result.ids);
                } else {
                    console.log("iDrive.pushMedia.ERROR message:" + response.d.message);
                }
            });
        }*/
    }
}

/*
- (void)requestDone:(ASIHTTPRequest *)request
{
    NSDictionary *result = request.userInfo;
    //NSString *response = [request responseString];
    [self clearJournal:0 ids: [result objectForKey:@"ids"]];
}

- (void)requestWentWrong:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
#ifdef DEBUG
    NSLog(@"iDrive.pushMedia.ERROR message: %@" , error);
#endif
}
*/


 -(void)clearJournal:(int) direction ids:(NSMutableArray *) idsData {
#ifdef DEBUG
     NSLog(@"iDrive.clearJournal");
#endif
     NSString *where = [NSString stringWithFormat:@" WHERE direction = %d",direction];
     if (idsData && [idsData count] > 0) {
         //where += " AND id in (" + ids.join(",") + ")";
         where = [NSString stringWithFormat:@"%@ AND id in (%@)", where, [idsData componentsJoinedByString:@","]];
     }
     [[JSDBV2 sharedInstance] sqlExecWithSQLStatement: [NSString stringWithFormat:@"DELETE FROM %@ %@", self.journalTable, where] successCallback:^(BOOL resp) {
         
     }];
     
     //[[JSDBV2 sharedInstance] sqlExecWithSQLStatement: [NSString stringWithFormat:@"DELETE FROM %@ %@", self.journalTable, where]];
     //db.sql("DELETE FROM " + iDrive.journalTable + where);
 }
     
- (void) createSchema{
/*
#ifdef DEBUG
    NSLog(@"iDrive.createSchema iDrive.schemaCols: %@",schemaCols);
#endif
    if ([[JSDBV2 sharedInstance] createTable: self.iNodetable withFields: self.iNodeCols]) {
        if ([[JSDBV2 sharedInstance] createTable: self.journalTable withFields: self.schemaCols]) {
            [self mkdir: @"/Videos"];
        }
    }
*/
    /*db.createTable(iDrive.iNodetable, iDrive.iNodeCols, function() {
        db.createTable(iDrive.journalTable, iDrive.schemaCols, function() {
            //TODO - take this test code out.
            //                iDriveTest.unit01();
            //Create folder to store videos
            iDrive.mkdir("/Videos");
            
            commandQueue.finished();
        });
    });*/
}

- (void)mkdir:(NSString *)dir {
    //iDrive.mkdirInternal((new Date()).toISOString(), dir, true);
    [self mkdirInternal: [JSMain getDate] folder:dir journal:true];
}

- (void)writeJournal:(NSString *)op params:(NSString *)_params {
#ifdef DEBUG
    NSLog(@"iDrive.writeJournal op: %@, params:%@", op, _params);
#endif
    NSString *mtime = [JSMain getDate];//(new Date()).toISOString();
    NSMutableArray *params = [[NSMutableArray alloc] init];
    [params addObject: self.user];
    [params addObject: op];
    [params addObject: _params];
    [params addObject: mtime];
    //db.sql("INSERT INTO " + iDrive.journalTable + " (direction, user, op, params, mtime) VALUES (0,?,?,?,?)", [iDrive.user, op, params, mtime]);
    NSString *sql = [NSString stringWithFormat: @"INSERT INTO %@ (direction, user, op, params, mtime) VALUES (0,?,?,?,?)", self.journalTable];
    NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
    [singleBatch setObject: sql forKey:@"sql"];
    [singleBatch setObject: params forKey:@"params"];
    //[[JSDB sharedInstance] sqlExec: singleBatch];
    [[JSDBV2 sharedInstance] batchInsert: [NSArray arrayWithObject: singleBatch] successCallback:^(BOOL resp) {
        
    }];
    [params release];
    [singleBatch release];
}


- (void)mkdirInternal: (NSString *)date folder:(NSString *)fname journal:(BOOL)_journal {
    JSPath *path = [[JSPath alloc] initWithPath:fname];
    NSMutableArray *params = [[NSMutableArray alloc] init];
    [params addObject: self.user];
    [params addObject: date];
    [params addObject: @"1"];
    [params addObject: [NSString stringWithFormat:@"%d",[path length]]];
    [params addObject: [path path]];
    
    NSString *placeholders = [JSDBV2 placeholdersForArray: params];
    // params = [iDrive.user, date, 1, path.length(), path.path];
    //placeholders = db.placeholdersForArray(params);
    NSString *sql = [NSString stringWithFormat: @"INSERT INTO %@ (user,ctime,type,fdep,path) VALUES (%@)", self.iNodetable, placeholders];
#ifdef DEBUG
    NSLog(@"iDrive.mkdirInternal sql: %@, params: %@",sql, params);
#endif
    NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
    [singleBatch setObject: sql forKey:@"sql"];
    [singleBatch setObject: params forKey:@"params"];
    //[[JSDB sharedInstance] sqlExec: singleBatch];
    [[JSDBV2 sharedInstance] batchInsert: [NSArray arrayWithObject: singleBatch] successCallback:^(BOOL resp) {
        
    }];
    //TODO: put release here cause BAD_EXEC exception
    [params release];
    [singleBatch release];
    [path release];
    //db.sql(sql, params);
    if (_journal) {
        //iDrive.writeJournal("mkdir", folder);
        [self writeJournal: @"mkdir" params: fname];
    }
}

//array of mediaRefs
- (void) filesDeletedByCMS:(void (^)(NSMutableArray *result))successCalback{
    NSString *inner_sql = [NSString stringWithFormat:@"SELECT DISTINCT mediaRef FROM %@",[JSSync sharedInstance].deleteTable];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE mediaRef IN (%@)", self.iNodetable, inner_sql];
    //NSArray *rows = [[JSDB sharedInstance] sql:sql params:nil success:nil fail:nil];
    //NSMutableArray *rows = [[JSDBV2 sharedInstance] sql:sql params:nil];
    [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray *rows) {
        NSLog(@"iDrive.filesDeletedByCMS result: %@", rows);
        if (successCalback) {
            successCalback(rows);
        }
    }];
    
    
    /*
    if (rows && [rows count] > 0) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        for (NSDictionary *file in rows) {
            [result addObject: file];
        }
        
        //return result;
    }*/

    /*
    db.sql(sql, [], function(rows){
        result = [];
        for (i = 0; i < rows.length; i++) {
            file = rows.item(i);
            result.push(file);
        }
        console.log("iDrive.filesDeletedByCMS result:" + JSON.stringify(result));
        callback && callback(result);
    });
    */ 
}


- (void) readJournal:(BOOL) outgoing success:(void (^)(NSMutableDictionary *))callbackFunc{
    int dir = outgoing ? 0 : 1;
    NSString *sql = [NSString stringWithFormat:@"SELECT id, user, op, params, mtime FROM %@ WHERE direction = %d", self.journalTable,dir];
    
    //NSMutableArray *rows = [[JSDBV2 sharedInstance] sql: sql params:nil];
    [[JSDBV2 sharedInstance] executeQuery: sql params:nil successCallback:^(NSMutableArray *rows) {
        if (rows) {
#ifdef DEBUG
            NSLog(@"iDrive.readJournal rows.length: %d", [rows count]);
#endif
            if ([rows count] > 0) {
                NSMutableDictionary * result = [NSMutableDictionary dictionary];
                [result setObject:[NSMutableArray array] forKey:@"ops"];
                [result setObject:[NSMutableArray array] forKey:@"ids"];
                
                for (int i = 0; i < [rows count] ; i++) {
                    NSDictionary *row = [rows objectAtIndex:i];
                    NSMutableDictionary * api = [NSMutableDictionary dictionary];
                    [(NSMutableArray *)[result objectForKey:@"ids"] addObject: [row objectForKey:@"id"]];
                    //result.ids.push(row.id);
                    //switch(row.op) {
                    NSString *op = [row objectForKey:@"op"];
                    if ([op isEqualToString:@"mkdir"]) {
                        [api setObject:@"createFolder" forKey:@"op"];
                        [api setObject: [row objectForKey: @"params"] forKey:@"folder"];
                        
                        //                        case "cp":
                        //                            api.op = "download";
                        //                            api.mediaRef = row.params.split("|")[0];
                        //                            api.folder = row.params.split("|")[1];
                        //                            break;
                    }else if ([op isEqualToString:@"cpMedia"]) {
                        //mediaType, mediaRef, url, dst
                        [api setObject:@"download" forKey:@"op"];
                        NSArray *params = [[row objectForKey:@"params"] componentsSeparatedByString:@"|"];
                        //                            api.mediaType = row.params.split("|")[0];
                        [api setObject: [params objectAtIndex:0] forKey:@"mediaRef"];
                        [api setObject: [params objectAtIndex:1] forKey:@"folder"];
                        //api.folder = row.params.split("|")[1];
                    }else if ([op isEqualToString:@"rmAsset"]) {
                        [api setObject:@"deleteAsset" forKey:@"op"];
                        NSArray *params = [[row objectForKey:@"params"] componentsSeparatedByString:@"|"];
                        [api setObject: [params objectAtIndex:1] forKey:@"mediaRef"];
                        [api setObject: [params objectAtIndex:0] forKey:@"folder"];
                        //api.op = "deleteAsset";
                        //api.folder = row.params.split("|")[0];
                        //api.mediaRef = row.params.split("|")[1];
                    }else if ([op isEqualToString:@"rmFolder"]) {
                        [api setObject:@"deleteFolder" forKey:@"op"];
                        [api setObject: [row objectForKey:@"params"] forKey:@"folder"];
                        //api.op = "deleteFolder";
                        //api.folder = row.params;
                    }else if ([op isEqualToString:@"mvAsset"]) {
                        [api setObject:@"move" forKey:@"op"];
                        NSArray *params = [[row objectForKey:@"params"] componentsSeparatedByString:@"|"];
                        [api setObject: [params objectAtIndex:0] forKey:@"mediaRef"];
                        [api setObject: [params objectAtIndex:1] forKey:@"folder"];
                        [api setObject: [params objectAtIndex:2] forKey:@"to"];
                        //api.op = "move"
                        //api.mediaRef = row.params.split("|")[0];
                        //api.folder = row.params.split("|")[1];
                        //api.to = row.params.split("|")[2];
                    }else if ([op isEqualToString:@"mvFolder"]) {
                        [api setObject:@"move" forKey:@"op"];
                        NSArray *params = [[row objectForKey:@"params"] componentsSeparatedByString:@"|"];
                        [api setObject: [params objectAtIndex:0] forKey:@"folder"];
                        [api setObject: [params objectAtIndex:1] forKey:@"to"];
                        //api.op = "move"
                        //api.folder = row.params.split("|")[0];
                        //api.to = row.params.split("|")[1];
                    }
                    [(NSMutableArray *)[result objectForKey:@"ops"] addObject: api];
                    //result.ops.push(api);
                }
                //console.log("iDrive.readJournal json:"+JSON.stringify(result));
#ifdef DEBUG
                NSLog(@"iDrive.readJournal json: %@",[JSMain stringify:result]);
#endif
                if (callbackFunc) {
                    callbackFunc(result);
                }
                return;
                /*if (callback) {
                 callback(result);
                 }*/
            }
        }else{
            if (callbackFunc) {
                callbackFunc(nil);
            }
        }
    }];
}

- (void)rmAssetInternal: (NSString *)mediaRef folder:(NSString *)folderName {
    
    //        path = folder == "/" ? "/" + mediaRef : folder + "/" + mediaRef;
    JSPath *path = [[JSPath alloc] initWithPath:folderName];
    [path navDown:mediaRef];
#ifdef DEBUG
    NSLog(@"iDrive.rmAssetInternal mediaRef: %@, folder: %@ ; path removed : %@", mediaRef, folderName, [path path]);
#endif
    //NSMutableDictionary *singleBatch = [NSMutableDictionary dictionary];
    //[singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE path = ?", self.iNodetable] forKey:@"sql"];
    /*TODO using [path.path] cause wrong where param, here is sample logs:
    2013-01-09 00:20:47.055 Media Library[3254:12203] iDrive.rmAssetInternal mediaRef: 14bcdc36-d40e-4149-8a6b-c8545c4b1ab9, folder: /Videos/14bcdc36-d40e-4149-8a6b-c8545c4b1ab9/ ; path removed : /Videos/14bcdc36-d40e-4149-8a6b-c8545c4b1ab9//14bcdc36-d40e-4149-8a6b-c8545c4b1ab9/
     */
    //[singleBatch setObject: [NSMutableArray arrayWithObject:folderName] forKey:@"params"];
    [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:[NSString stringWithFormat:@"DELETE FROM %@ WHERE path = '%@'", self.iNodetable, folderName] successCallback:^(BOOL resp) {
        
    }];
    [path release];
    //params = [path.path];
    //db.sql("DELETE FROM " + iDrive.iNodetable + " WHERE path = ?", params);
}

- (void)rmDirInternal: (NSString *)folder {
    int folderLen = [folder length];
#ifdef DEBUG
    NSLog(@"iDrive.rmDirInternal folder: %@, folderLen: %d", folder,folderLen);
#endif
    //params = [folderLen, folder];
    //NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
    //[singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE substr(path, 1, ?) = ?", self.iNodetable] forKey:@"sql"];
    //[singleBatch setObject: [NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%d", folderLen],folder, nil] forKey:@"params"];
    [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:[NSString stringWithFormat:@"DELETE FROM %@ WHERE substr(path, 1, %d) = '%@'", self.iNodetable, folderLen, folder] successCallback:^(BOOL resp) {
        
    }];
    //[singleBatch release];
    //db.sql("DELETE FROM " + iDrive.iNodetable + " WHERE substr(path, 1, ?) = ?", params);
}


- (void)rm: (NSDictionary *)item {
    if ([[item objectForKey:@"type"] intValue] == 1) {
        [self rmDirInternal: [item objectForKey:@"path"]];
        [self writeJournal:@"rmFolder" params: [item objectForKey:@"path"]];
        /*iDrive.rmDirInternal(item.path);
        p = item.path;
        iDrive.writeJournal("rmFolder", p);
        */ 
    } else {
        JSPath *path = [[JSPath alloc] initWithPath:[item objectForKey:@"path"]];
        [self rmAssetInternal: [item objectForKey:@"mediaRef"] folder: [path parent]];
        //NSString p = item.folder + "|" + item.mediaRef;
        [self writeJournal:@"rmAsset" params:[NSString stringWithFormat:@"%@|%@", [item objectForKey:@"folder"], [item objectForKey:@"mediaRef"]]];
        [path release];
        //[self remove
        /*path = new Path(item.path);
        iDrive.rmAssetInternal(item.mediaRef, path.parent());
        p = item.folder + "|" + item.mediaRef;
        iDrive.writeJournal("rmAsset", p);
        */ 
    }
}


- (void) rmMultiple:(NSArray *)items {
#ifdef DEBUG
    NSLog(@"iDrive.rmMultiple items: %@",items);
#endif
    for (NSDictionary *item in items) {
        [self rm: item];
    }
    /*$(items).each(function(index, item){
        iDrive.rm(item);
    });*/
}


- (void)mvInternal: (NSString *) mediaRef fromFolder:(NSString *)_fromFolder toFolder: (NSString *)_toFolder {
#ifdef DEBUG
    NSLog(@"iDrive.mvInternal mediaRef: %@, fromFolder:%@, toFolder:%@", mediaRef, _fromFolder, _toFolder);
#endif
    if (mediaRef) {
        //asset move
        JSPath *oldPath = [[JSPath alloc] initWithPath:_fromFolder]; 
        [oldPath navDown:mediaRef];
        JSPath *newPath = [[JSPath alloc] initWithPath: _toFolder]; 
        [newPath navDown: mediaRef];
        int newDepth = [newPath length] - 1;
        NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET path = '%@', fdep = '%@' WHERE path = '%@'", self.iNodetable, [newPath path], [NSString stringWithFormat:@"%d",newDepth], _fromFolder];
        //NSMutableArray *params = [NSMutableArray arrayWithObjects: [newPath path], [NSString stringWithFormat:@"%d",newDepth], _fromFolder, nil];
        //console.log("iDrive.mvInternal sql:" + sql + ", params:" + params);
        //db.sql(sql, params);
        //[[JSDB sharedInstance] sql:sql params:params success:nil fail:nil];
        [[JSDBV2 sharedInstance] sqlExecWithSQLStatement: sql successCallback:^(BOOL resp) {
            
        }];
    } else {
        //folder move
        //            p = new Path(fromFolder);
        
        JSPath *fromPath = [[JSPath alloc] initWithPath:_fromFolder];
        JSPath *toPath = [[JSPath alloc] initWithPath: _toFolder]; 
        [toPath navDown: [fromPath top]];
        int pathDepthDiff = [toPath length] - [fromPath length];
        
        NSString *sql = [NSString stringWithFormat:@"UPDATE %@ SET path = replace(path, '%@', '%@'), fdep = fdep + '%@' WHERE substr(path, 1, '%@') = '%@'", self.iNodetable, [fromPath path], [toPath path], [NSString stringWithFormat:@"%d",pathDepthDiff], [NSString stringWithFormat:@"%d", [_fromFolder length]], _fromFolder];
        
        //NSMutableArray *params = [NSMutableArray arrayWithObjects:[fromPath path], [toPath path], [NSString stringWithFormat:@"%d",pathDepthDiff], [NSString stringWithFormat:@"%d", [_fromFolder length]], _fromFolder, nil];
        //params = [fromPath.path, toPath.path, pathDepthDiff, fromFolder.length, fromFolder];
        //console.log("iDrive.mvInternal sql:" + sql + ", params:" + params);
        //db.sql(sql, params);
        //[[JSDB sharedInstance] sql: sql params:params success:nil fail:nil];
        [[JSDBV2 sharedInstance] sqlExecWithSQLStatement: sql successCallback:^(BOOL resp) {
            
        }];
    }
}

- (void) mv:(NSDictionary *)item folder:(NSString *)folderName {
    
    if ([[item objectForKey:@"type"] intValue] == 1) {
        //folder
        [self mvInternal:NULL fromFolder: [item objectForKey:@"path"] toFolder: folderName];
        [self writeJournal:@"mvFolder" params:[NSString stringWithFormat:@"%@|%@", [item objectForKey:@"folder"], folderName]];
        /*iDrive.mvInternal(null, item.path, folder);
        p = [item.folder, folder].join("|");
        iDrive.writeJournal("mvFolder", p);
         */
    } else {
        JSPath *p = [[JSPath alloc] initWithPath: [item objectForKey:@"path"]];
        [self mvInternal: [item objectForKey:@"mediaRef"] fromFolder:[p parent] toFolder: folderName];
        [self writeJournal: @"mvAsset" params:[NSString stringWithFormat:@"%@|%@|%@", [item objectForKey:@"mediaRef"],[item objectForKey:@"folder"], folderName]];
        /*p = new Path(item.path);
        iDrive.mvInternal(item.mediaRef, p.parent(), folder);
        p = [item.mediaRef, item.folder, folder].join("|");
        iDrive.writeJournal("mvAsset", p);
        */ 
        [p release];
    }
}

-(void)mvMultiple: (NSArray *)items folder:(NSString *) folderName {
    //        console.log("iDrive.mvMultiple items:"+items+" folder:"+folder);
    /*$(items).each(function(index, item){
        iDrive.mv(item, folder);
    });
    */
    for (NSDictionary *item in items) {
        [self mv:item folder: folderName];
    }
}

- (void)cp:(NSDictionary*) asset folder:(NSString *)folderName {
    [self cpInternal:[JSMain getDate] asset:asset folder:folderName journal: true];
    //iDrive.cpInternal((new Date()).toISOString(), asset, folder, true);
}

- (void) cpMultiple: (NSArray *)assetArray folder:(NSString *)folderName {
#ifdef DEBUG
    NSLog(@"iDrive.cpMultiple assetArray: %@ folder: %@", assetArray, folderName);
#endif
    for (NSDictionary *asset in assetArray) {
        [self cp: asset folder: folderName];
    }
    /*$(assetArray).each(function(index, asset){
        iDrive.cp(asset, folder);
    });*/
}

- (void)cpInternal: (NSString *)date asset:(NSDictionary *)tempAsset folder:(NSString *)folderName journal:(BOOL)_journal {
    //NSDictionary *tempAsset = [[NSDictionary alloc] initWithDictionary: as];
    //        console.log("iDrive.cp folder:" + folder + ", asset:" + JSON.stringify(asset));
#ifdef DEBUG
    NSLog(@"iDrive.cp folder:%@, asset: %@", folderName, tempAsset);
#endif
    //        path = folder == "/" ? new Path("/" + asset.mediaRef) : new Path(folder + "/" + asset.mediaRef);
    JSPath *path = [[JSPath alloc] initWithPath: folderName];//new Path(folder); 
    [path navDown: [tempAsset objectForKey:@"mediaRef"]];
    int dep = [path length] - 1;
    
    NSString *mediaType = [tempAsset objectForKey:@"mediaType"];
    NSString *title = [tempAsset objectForKey:@"title"] ? [tempAsset objectForKey:@"title"] : @"Untitled";
    if ([mediaType isEqualToString:kMediaType_Samples]) {
        //TODO - location is re-used for colour for samples
        NSString *colour = [tempAsset objectForKey:@"colour"];
        if (colour  != NULL && [colour length] > 0) {
            title =  [NSString stringWithFormat:@"%@ %@", title, colour];// title + " " + asset.colour;
        }
    }
    
    //params = [iDrive.user, date, 0, dep, path.path, asset.mediaRef, mediaType, title, asset.thumbnail, asset.url];
    NSMutableArray *params = [[NSMutableArray alloc] init];
    [params addObject: self.user];
    [params addObject: date];
    [params addObject: @"0"];
    [params addObject: [NSString stringWithFormat:@"%d", dep]];
    [params addObject: [path path]];
    [params addObject: [tempAsset objectForKey:@"mediaRef"]];
    [params addObject: mediaType];
    [params addObject: title];
    [params addObject: [tempAsset objectForKey:@"thumbnail"]];
    [params addObject: [tempAsset objectForKey:@"url"]];
    
    NSString *placeholders = [JSDBV2 placeholdersForArray: params];
                         
    NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
    [singleBatch setObject:[NSString stringWithFormat:@"INSERT INTO %@ (user,ctime,type,fdep,path,mediaRef,mediaType,title,thumbnail,url) VALUES (%@)", self.iNodetable, placeholders] forKey:@"sql"];
    [singleBatch setObject: params forKey:@"params"];
    //[[JSDB sharedInstance] sqlExec: singleBatch];
    //[[JSDB sharedInstance] batchInsert:[NSArray arrayWithObject:singleBatch]];
    [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
        if (_journal) {
            NSString *p = [NSString stringWithFormat:@"%@|%@", [tempAsset objectForKey:@"mediaRef"], folderName];
            [self writeJournal:@"cpMedia" params: p];
            //iDrive.writeJournal("cpMedia", p);
        }
    }];
    [params release];
    [singleBatch release];
    [path release];
    //db.sql("INSERT INTO " + iDrive.iNodetable + " (user,ctime,type,fdep,path,mediaRef,mediaType,title,thumbnail,url) VALUES (" + placeholders + ")", params);
    
}

//array of mediaRefs
-(void)_removeMedia:(NSArray *)media {
    if ([media count] > 0) {
#ifdef DEBUG
        NSLog(@"iDrive._removeMedia media: %d", [media count]);
#endif
        NSString *placeholders = [NSString stringWithFormat:@"(%@)", [JSDBV2 placeholdersForArray: media]];
        NSMutableDictionary *singleBatch = [NSMutableDictionary dictionary];
        [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", self.iNodetable,placeholders] forKey:@"sql"];
        [singleBatch setObject: media forKey:@"params"];
        
        [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
            
        }];
        //[singleBatch release];
    }else{
        NSLog(@"iDrive._removeMedia.WARN_NO_FILES");
    }
    /*
    if (media.length > 0) {
        placeholders = "(" + db.placeholdersForArray(media) + ")";
        console.log("iDrive._removeMedia media:" + media);
        db.sql("DELETE FROM " + iDrive.iNodetable + " WHERE mediaRef IN " + placeholders, media);
    } else {
        console.log("iDrive._removeMedia.WARN_NO_FILES");
    }*/
}


- (void)filesAffectedByContainerRemoval: (NSArray *) containers plugin: (NSString *) _plugIn callback: (SEL) _callback {
    [self _filesAffectedByContainerRemoval:containers callback:^(NSMutableArray *files) {
        [[JSMain sharedInstance] pgFire:_plugIn selector:_callback withData: files];
    }];
    /*iDrive._filesAffectedByContainerRemoval(containers, function(files){
        main.pgFire(plugin, callback, files);
    });*/
}

- (void)removeFilesAffectedByContainerRemoval: (NSArray *) containers {
    [self _filesAffectedByContainerRemoval: containers callback:^(NSMutableArray *files) {
        if (files && [files count] > 0) {
            [self rmMultiple:files];
        }
    }];
    /*
    iDrive._filesAffectedByContainerRemoval(containers, function(files){
        if (files.length > 0) {
            iDrive.rmMultiple(files);
        }
    });*/
}

- (void)ls: (NSString *)path delegate:(id)aDel callback:(void (^)(NSMutableArray *result))successCalback{
    //iDrive.lsInternal(false, path, plugin, callback);
    [self lsInternal: false path:path delegate:aDel resultSelector: @selector(lsResults:) callback:successCalback];
}

//Only return a list of folders in a given path.
- (void) lsdir: (NSString *)path delegate:(id)aDel callback:(void (^)(NSMutableArray *result))successCalback{
    //iDrive.lsInternal(true, path, plugin, callback);
    [self lsInternal: true path:path delegate:aDel resultSelector: @selector(lsResults:) callback:successCalback];
}

- (void) lsInternal: (BOOL)onlydirs path:(NSString *)_path delegate:(id)aDel resultSelector:(SEL)lsresults callback:(void (^)(NSMutableArray *result))successCalback {
    int depth = 1;
    JSPath *p = [[JSPath alloc] initWithPath: _path];
    int fdep = [p length];
    int len = [_path length];
#ifdef DEBUG
    NSLog(@"len: %d",len);
#endif
    //[p release];
    NSString *where = @"";
    NSMutableArray *params = [[NSMutableArray alloc] init];
    if (onlydirs) {
        where = [NSString stringWithFormat:@" WHERE type = 1 AND substr(path, 1, %d) = ? AND fdep = ?", len];
        //params = [path, fdep + 1];
        [params addObject: _path];
        [params addObject: [NSString stringWithFormat:@"%d", fdep + 1]];
    } else {
        /*where = " WHERE (type = 1 AND substr(path, 1, " + len + ") = ? AND fdep = ?)";
        where += " OR (type = 0 AND substr(path, 1, " + len + ") = ? AND fdep = ?)"
        params = [path, fdep + 1, path, fdep];
        */
        where = [NSString stringWithFormat:@" WHERE (type = 1 AND substr(path, 1, %d) = ? AND fdep = ?)  OR (type = 0 AND substr(path, 1, %d) = ? AND fdep = ?)", len, len];
        [params addObject: _path];
        [params addObject: [NSString stringWithFormat:@"%d", fdep + 1]];
        [params addObject: _path];
        [params addObject: [NSString stringWithFormat:@"%d", fdep]];
    }
    NSString *order = @" ORDER BY type DESC, path";
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ %@ %@",self.iNodetable,where,order];
#ifdef DEBUG
    NSLog(@"iDrive.lsInternal sql: %@", sql);
#endif
    //NSMutableArray *rows = [[JSDBV2 sharedInstance] sql:sql params:params ];
    [[JSDBV2 sharedInstance] executeQuery:sql params:params successCallback:^(NSMutableArray *rows) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        if (rows && [rows count] > 0) {
            for (NSDictionary *row in rows) {
                [result addObject: row];
            }
            [aDel performSelector:lsresults withObject:result];
            //[[JSMain sharedInstance] pgFire:_plugin selector:lsresults withData: result];
        }else{
            [aDel performSelector:lsresults withObject:NULL];
            //[[JSMain sharedInstance] pgFire:_plugin selector:lsresults withData: NULL];
        }
        
        if (successCalback) {
            successCalback(result);
        }
        [result release];
   }];
    [params release];
    
    
    /*
    db.sql(sql, params, function(rows) {
        var result = [];
        if (rows.length > 0) {
            var result = [];
            for (i = 0; i < rows.length; i++) {
                row = rows.item(i);
                folder = row['folder'];
                dep = row['fdep'];
                type = row['type']
                title = row['title'];
                iDrive.dumpRow(row);
                result.push(row);
            }
        }
        main.pgFire(plugin, "lsResults", result);
        
        if (callback) {
            callback(result);
        }
    });
    */ 
}

- (void)_filesAffectedByContainerRemoval: (NSArray *)containers callback:(void (^)(NSMutableArray *result))successCalback {
    [[JSSync sharedInstance] mediaRemovedWhenRemovingContainers: containers callback:^(NSMutableArray *media) {
        NSString *mediaPlaceholders = [JSDBV2 placeholdersForArray:media];
        NSString *sql =[NSString stringWithFormat:@"SELECT * FROM %@ WHERE mediaRef IN ( %@ )", self.iNodetable, mediaPlaceholders];
#ifdef DEBUG
        NSLog(@"sql == %@", sql);
#endif
        //NSMutableArray *rows = [[JSDBV2 sharedInstance] sql:sql params:media ];
        [[JSDBV2 sharedInstance] executeQuery:sql params:media successCallback:^(NSMutableArray *rows) {
            if (rows && [rows count] > 0) {
                NSMutableArray * result = [NSMutableArray array];
                for (int i = 0; i < [rows count]; i++) {
                    NSMutableDictionary *file = [rows objectAtIndex:i];
                    [result addObject: file];
                }
#ifdef DEBUG
                NSLog(@"iDrive.filesAffectedByContainerRemoval result: %@", result);
#endif
                if (successCalback) {
                    successCalback(result);
                }
            }
        }];
    }];
    /*sync.mediaRemovedWhenRemovingContainers(containers, function(media) {
        mediaPlaceholders = db.placeholdersForArray(media);
        sql = "SELECT * FROM " + iDrive.iNodetable + " WHERE mediaRef IN (" + mediaPlaceholders + ")";
        db.sql(sql, media, function(rows){
            result = [];
            for (i = 0; i < rows.length; i++) {
                file = rows.item(i);
                result.push(file);
            }
            console.log("iDrive.filesAffectedByContainerRemoval result:" + JSON.stringify(result));
            callback && callback(result);
        });
    });*/
}
@end
