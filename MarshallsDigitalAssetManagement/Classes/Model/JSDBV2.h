//
//  JSDB.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <sqlite3.h>
#import "NSDictionary_JSONExtensions.h"

@interface JSDBV2 : NSObject {
    
}

+ (id)sharedInstance;


//- (NSMutableArray *) sql: (NSString *)sqlStatement params: (NSArray *) parameters;
- (void) executeQuery:(NSString *) sqlQuery params: (NSArray *) params successCallback:(void (^)(id resp))successCallback;
- (void) executeQueryMap: (NSString *) sqlQuery params: (NSArray *) parameters successCallback:(void (^)(id resp))successCalback;

- (void) batchInsert: (NSArray *) batch successCallback:(void (^)(BOOL resp))successCalback;
- (void) sqlExec:(NSDictionary *)batch successCallback:(void (^)(BOOL resp))successCalback;;
- (void) sqlExecWithSQLStatement:(NSString *)sql successCallback:(void (^)(BOOL resp))successCalback;;

+ (NSString *) placeholdersForArray: (NSArray *) array;

#pragma mark - Get Download Count
- (void) downloadCount:(NSArray *)mediaTypes successCallback:(void (^)(int count))successCalback;

- (void) clearJournal: (NSString *)where;


-(bool) initialiseDatabase;

@end
