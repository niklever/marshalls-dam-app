//
//  JSShare.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 1/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "JSShare.h"
#import "JSDBV2.h"
#import "JSMain.h"
#import "JSSync.h"
#import "JSPath.h"
#import "JSLocalStorage.h"

@implementation JSShare
@synthesize method;
@synthesize lsDateKey;
@synthesize emailPushInterval;
//@synthesize queueSendingEmails;

static JSShare *sharedInstance = nil;

+ (JSShare *)sharedInstance{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}


- (id)init{
    self = [super init];
    if (self) {
        self.emailPushInterval = 30;//30 * 1000,
        self.lsDateKey = @"share.lastUpdated";
        self.method = @"/api/iDrive.svc/json/sendEmail";
        
        NSString *shareInitialised = [[JSLocalStorage sharedInstance] getItem:@"share.initialised"];
        
#ifdef DEBUG
        NSLog(@"share.init shareInitialised : %@",shareInitialised );
#endif
        
        if (!shareInitialised || ![shareInitialised isEqualToString:@"true"]) {
            [[JSLocalStorage sharedInstance] setItem: @"share.initialised" value: @"true"];
        }
        
        [self performSelector:@selector(processEmailQueue) withObject:nil afterDelay: self.emailPushInterval];
        /*setInterval(function(){
            share.processEmailQueue();
        }, share.emailPushInterval);
        */
    }
    return self;
}

/*
-(void)initQueue {
#ifdef DEBUG
    NSLog(@"initQueue called..................");
#endif
    self.queueSendingEmails = [[ASINetworkQueue alloc] init];
    queueSendingEmails.shouldCancelAllRequestsOnFailure = NO;
    queueSendingEmails.showAccurateProgress = YES;
    queueSendingEmails.delegate = self;
    //Throttle back so that this queue doesn't compete will the
    //realtime.
    queueSendingEmails.maxConcurrentOperationCount = 1;
    queueSendingEmails.queueDidFinishSelector = @selector(queueSendingFinished:);
    //queue.requestDidStartSelector = @selector(queueRequestDidStart:);
    [queueSendingEmails release];
}*/

- (void)email: (NSDictionary *) params {
    //queue the email to local database
    NSMutableArray *sqlParams = [[NSMutableArray alloc] init];
    [sqlParams addObject: [params objectForKey:@"from"]];
    [sqlParams addObject: [params objectForKey:@"to"]];
    [sqlParams addObject: [params objectForKey:@"subject"]];
    [sqlParams addObject: [params objectForKey:@"body"]];
    [sqlParams addObject: [(NSArray *)[params objectForKey:@"mediaRefs"] componentsJoinedByString:@","]];
    
    //[[JSDBV2 sharedInstance] sqlExecWithSQLStatement:@"INSERT INTO email (sender, recipient, subject, body, assets) VALUES (?,?,?,?,?)" params: sqlParams];
    /*
    [[JSDBV2 sharedInstance] sql:@"INSERT INTO email (sender, recipient, subject, body, assets) VALUES (?,?,?,?,?)" params: sqlParams success:^(NSMutableArray *) {
        
    }];*/
    NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
    //[singleBatch setObject: [NSString stringWithFormat:@"INSERT INTO %@ (mediaRef, mt, mediaType, title, url, thumbnail, thumbWidth, thumbHeight,location,priority, updatedDate, meta) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", mediaTable] forKey:@"sql"];
    [singleBatch setObject: @"INSERT INTO email (sender, recipient, subject, body, assets) VALUES (?,?,?,?,?)" forKey:@"sql"];
    [singleBatch setObject:sqlParams forKey:@"params"];
    [sqlParams release];
    
    [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
        [self processEmailQueue];
    }];
    
    //sqlParams = [params.from, params.to, params.subject, params. body, params.mediaRefs.join(",")];
    //db.sql("INSERT INTO " + share.emailTable + " (sender, recipient, subject, body, assets) VALUES (?,?,?,?,?)", sqlParams);
    //do send email immediately
    //[self performSelector:@selector(processEmailQueue) withObject:nil afterDelay: 0];
    
}

- (void) processEmailQueue{
    //console.log("share.processEmailQueue");
    if ([[JSMain sharedInstance] opsSuspended]) {
        
#ifdef DEBUG
        NSLog(@"share.processEmailQueue.WARN_OPS_SUSPENDED");
#endif
        
        return;
    }
    
    if (![[JSMain sharedInstance] online]) {
        
#ifdef DEBUG
        NSLog(@"share.processEmailQueue.WARN_OFFLINE");
#endif
        
        return;
    }
    
    //console.log("share.processEmailQueue");
    //        sql = "SELECT id, sender, to, subject, body, assets FROM " + share.emailTable;
    //sql = "SELECT * FROM " + share.emailTable;
    //NSArray *rows = [[JSDBV2 sharedInstance] sql:@"select * from email" params:nil];
    [[JSDBV2 sharedInstance] executeQuery:@"select * from email" params:nil successCallback:^(NSMutableArray *rows) {
                             if (rows && [rows count] > 0) {
                                 
#ifdef DEBUG
                                 NSLog(@"share.processEmailQueue rows.length: %d", [rows count]);
#endif
                                 
                                 for (NSDictionary *row in rows) {
                                     NSMutableArray *mediaRefs = ([row objectForKey:@"assets"] && [[row objectForKey:@"assets"] length] > 0) ? (NSMutableArray *)[[row objectForKey:@"assets"] componentsSeparatedByString:@","] : [NSMutableArray array];
                                     NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
                                     [post setObject:[row objectForKey:@"sender"] forKey:@"from"];
                                     [post setObject:[row objectForKey:@"recipient"] forKey:@"to"];
                                     [post setObject:[row objectForKey:@"subject"] forKey:@"subject"];
                                     [post setObject:[row objectForKey:@"body"] forKey:@"body"];
                                     [post setObject:mediaRefs forKey:@"mediaRefs"];
                                     
#ifdef DEBUG
                                     NSLog(@"share.processEmailQueue id: %d", [[row objectForKey:@"id"] intValue]);
#endif                  
                                     [[APICommon sharedInstance] shareViaEmail: post
                                                               successCallback:^(id resp) {
                                                                   // Enter what happens here if successsful.
                                                                   NSLog(@"response body %@", resp);
                                                                   
                                                                   [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:[NSString stringWithFormat:@"DELETE FROM email WHERE id = %d", [[row objectForKey:@"id"] intValue]] successCallback:^(BOOL resp) {
                                                                       
                                                                   }];
                                     }
                                                               andFailCallback:^(id msg) {
                                         
                                     }];
                                     
                                     //NSString *jsonPost = [JSMain stringify: post];
                                     
                                     /*
                                     NSMutableDictionary *rheader = [NSMutableDictionary dictionary];
                                     [rheader setObject:@"text/json; charset=utf-8" forKey: @"Content-Type"];
                                     
                                     
                                     AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
                                     [operationManager POST: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], self.method]
                                            parameters:post
                                                    success:^(AFHTTPRequestOperation *operation, id responseObject){
                                                        // Enter what happens here if successsful.
                                                        NSLog(@"response body %@", responseObject);
                                                        
                                                        [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:[NSString stringWithFormat:@"DELETE FROM email WHERE id = %d", [[row objectForKey:@"id"] intValue]]];
                                         
                                     }
                                                    failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                         
                                         // Enter what happens here if failure happens
                                     }];*/
                                     
                                     /*
                                     ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [JSMain apiHost], self.method]]];
                                     
                                     NSLog(@"share.processEmailQueue post: %@ to host api == %@",jsonPost, request.url);
                                     request.userInfo = row;
                                     [request appendPostData:[jsonPost dataUsingEncoding:NSUTF8StringEncoding]];
                                     [request setRequestMethod:@"POST"];
                                     [request setRequestHeaders: rheader];
                                     [request setCompletionBlock:^{
                                         
                                         NSLog(@"response body %@", request.responseString);
                                         
                                         NSDictionary *row = (NSDictionary *)request.userInfo;
#ifdef DEBUG
                                         NSLog(@"share.processEmailQueue msg: %@", request);
                                         NSLog(@"share.processEmailQueue.DELETE_ROW row.id: %d", [[row objectForKey:@"id"] intValue]);
#endif
                                         //db.sql("DELETE FROM " + share.emailTable + " WHERE id = ?", [row.id]);
                                         [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:[NSString stringWithFormat:@"DELETE FROM email WHERE id = %d", [[row objectForKey:@"id"] intValue]]];
                                         
                                     }];
                                     
                                     [request setFailedBlock:^{
                                         NSError *error = [request error];
                                         NSLog(@"email.share.ERROR message: %@" , error);
                                         NSLog(@"email.share.ERROR message: %@" , [request error]);
                                         
                                     }];
                                     */
                                     /*
                                     [request setHeadersReceivedBlock:^(NSDictionary *responseHeaders) {
                                         NSLog(@"%@",responseHeaders);
                                     }];*/
                                     
                                     //[request setDelegate:self];
                                     //[request setDidFinishSelector:@selector(requestDone:)];
                                     //[request setDidFailSelector:@selector(requestWentWrong:)];
                                     
                                     //request.didReceiveResponseHeadersSelector = @selector(didRequestReceivedResponseHeaders:);
                                     
                                     //[request start];
                                     /*
                                      $.ajax({
                                      type: "POST",
                                      url: main.apiHost + share.method,
                                      data: JSON.stringify(post),
                                      dataType: "json",
                                      beforeSend: function(xhr) {
                                      xhr.setRequestHeader("Content-Type", "text/json; charset=utf-8");
                                      }
                                      }).done(function(msg){
                                      console.log("share.processEmailQueue msg:" + JSON.stringify(msg));
                                      console.log("share.processEmailQueue.DELETE_ROW row.id:" + row.id);
                                      db.sql("DELETE FROM " + share.emailTable + " WHERE id = ?", [row.id]);
                                      });
                                      */ 
                                     
                                 }
                             }
    }];
    
    /*db.sql(sql, [], function(rows){
        var result = [];
        if (rows.length > 0) {
            console.log("share.processEmailQueue rows.length:", rows.length);
            var result = [];
            for (i = 0; i < rows.length; i++) {
                row = rows.item(i);
                (function(row){
                    var mediaRefs = row.assets.length > 0 ? row.assets.split(",") : [];
                    var post = {
                    from: row.sender,
                    to: row.recipient,
                    subject: row.subject,
                    body: row.body,
                    mediaRefs: mediaRefs
                    };
                    console.log("share.processEmailQueue id:"+row.id+"post:" + JSON.stringify(post));
                    $.ajax({
                    type: "POST",
                    url: main.apiHost + share.method,
                    data: JSON.stringify(post),
                    dataType: "json",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Content-Type", "text/json; charset=utf-8");
                    }
                    }).done(function(msg){
                        console.log("share.processEmailQueue msg:" + JSON.stringify(msg));
                        console.log("share.processEmailQueue.DELETE_ROW row.id:" + row.id);
                        db.sql("DELETE FROM " + share.emailTable + " WHERE id = ?", [row.id]);
                    });
                })(row);
            }
            console.log("share.processEmailQueue result:"+JSON.stringify(result));
            //                if (callback) {
            //                    callback(result);
            //                }
        }
    });*/
}

@end
