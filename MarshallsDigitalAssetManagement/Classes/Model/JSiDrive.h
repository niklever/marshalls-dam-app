//
//  JSiDrive.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JSiDrive : NSObject {

}
@property (nonatomic, retain) NSString *iNodetable;
@property (nonatomic, assign) int iDrivePushInterval;
//@property (nonatomic, retain) NSMutableArray *iNodeCols;
//@property (nonatomic, retain) NSMutableArray *schemaCols;
@property (nonatomic, retain) NSString * journalTable;
@property (nonatomic, retain) NSString *user;
@property (nonatomic, retain) NSString *pushMethod;
@property (nonatomic, retain) NSString *pullMethod;

+ (JSiDrive *) sharedInstance;


- (void)mkdirInternal: (NSString *)date folder:(NSString *)fname journal:(BOOL)_journal; 
- (void) readJournal:(BOOL) outgoing success:(void (^)(NSMutableDictionary *))callbackFunc;
- (void) filesDeletedByCMS:(void (^)(NSMutableArray *result))successCalback;
- (void)removeFilesAffectedByContainerRemoval: (NSArray *) containers ;
- (void)filesAffectedByContainerRemoval: (NSArray *) containers plugin: (NSString *) _plugIn callback: (SEL) _callback;
- (void)cp:(NSDictionary*) asset folder:(NSString *)folderName;
-(void)_removeMedia:(NSArray *)media;
- (void)ls: (NSString *)path delegate:(id)aDel callback:(void (^)(NSMutableArray *result))successCalback;
- (void) lsdir: (NSString *)path delegate:(id)aDel callback:(void (^)(NSMutableArray *result))successCalback;
- (void)mkdir:(NSString *)dir;
- (void) cpMultiple: (NSArray *)assetArray folder:(NSString *)folderName;
- (void) rmMultiple:(NSArray *)items;
-(void)mvMultiple: (NSArray *)items folder:(NSString *) folderName;
@end
