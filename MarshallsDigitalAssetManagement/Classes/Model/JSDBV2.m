//
//  JSDB.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSDBV2.h"

@interface JSDBV2 ()

@property (nonatomic, retain) FMDatabase *db;

@end

@implementation JSDBV2
//@synthesize delegate;

//sqlite3 *db;
static JSDBV2 *sharedInstance = nil;

-(bool) initialiseDatabase{
    
    /*
    if ([self openDB]){
        if ([self createTables]){
            return true;
        }
    }
    return false;
    */
    
    return [self createTables];
}

+ (JSDBV2 *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)init{
    self = [super init];
    
    if (self) {
        // Work your initialising magic here as you normally would
        //[self initialiseDatabase];
        self.db = [FMDatabase databaseWithPath: [self databasePath]];
    }
    
    return self;
}

-(bool)createTables{
    if (![self openDB]) {
        return false;
    }
    NSString *sqlMaster = @"";
    
    
    NSMutableArray *fields = [[NSMutableArray alloc]init];
    
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"mediaType TEXT"];
    [fields addObject:@"priority int"];
    [fields addObject:@"url TEXT"];
    [fields addObject:@"original int"];/*added original field to indicate if this is original source or not*/
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS download_queue;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE download_queue (%@);",[fields componentsJoinedByString:@","]]];

    /*if (![self createTable:@"download_queue" withFields:fields]) {
        [fields release];
        return false;
    }*/
    
    [fields removeAllObjects];
    
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"mediaType TEXT"];
    [fields addObject:@"priority int"];
    [fields addObject:@"url TEXT"];
    [fields addObject:@"original int"];/*added original field to indicate if this is original source or not*/
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS download_queue_original;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE download_queue_original (%@);",[fields componentsJoinedByString:@","]]];
    
    /*if (![self createTable:@"download_queue_original" withFields:fields]) {
        [fields release];
        return false;
    }*/
    
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"mediaType TEXT"];
    [fields addObject:@"mt INTEGER"];
    [fields addObject:@"title TEXT"];
    [fields addObject:@"url TEXT"];
    [fields addObject:@"thumbnail TEXT"];
    [fields addObject:@"thumbWidth TEXT"];
    [fields addObject:@"thumbHeight TEXT"];
    [fields addObject:@"width TEXT"];
    [fields addObject:@"height TEXT"];
    [fields addObject:@"location TEXT"];
    [fields addObject:@"latitude TEXT"];
    [fields addObject:@"longitude TEXT"];
    [fields addObject:@"tags TEXT"];
    [fields addObject:@"products TEXT"];
    [fields addObject:@"priority int"];
    [fields addObject:@"updatedDate date"];
    [fields addObject:@"hasCrop INTEGER"];
    [fields addObject:@"cropRect TEXT"];//in format X1,Y1,X2,Y2
    [fields addObject:@"mediaDesc TEXT"];//media short desc
    [fields addObject:@"meta TEXT"];
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS media;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE media (%@);",[fields componentsJoinedByString:@","]]];
    
    /*if (![self createTable:@"media" withFields:fields]) {
        [fields release];
        return false;
    }*/

    sqlMaster = [sqlMaster stringByAppendingString:@"CREATE INDEX idx_mt ON media(mt);CREATE INDEX idx_mediaRef ON media(mediaRef);"];
    
    /*
    if ([self sqlExecWithSQLStatement:@"CREATE INDEX idx_mt ON media(mt)"]) {
#ifdef DEBUG
        NSLog(@"sync.createSchema.CREATE_INDEX.idx_mt");
#endif
    }
    if ([self sqlExecWithSQLStatement:@"CREATE INDEX idx_mediaRef ON media(mediaRef)"]){
#ifdef DEBUG
        NSLog(@"sync.createSchema.CREATE_INDEX.idx_mediaRef");
#endif
    }*/
    
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"container TEXT"];
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS media_container;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE media_container (%@);",[fields componentsJoinedByString:@","]]];
    
    /*
    if (![self createTable:@"media_container" withFields:fields]) {
        [fields release];
        return false;
    }*/
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"tag TEXT"];
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS media_tags;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE media_tags (%@);",[fields componentsJoinedByString:@","]]];
    
    /*
    if (![self createTable:@"media_tags" withFields:fields]) {
        [fields release];
        return false;
    }*/
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS media_deleted;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE media_deleted (%@);",[fields componentsJoinedByString:@","]]];
    /*
    if (![self createTable:@"media_deleted" withFields:fields]) {
        [fields release];
        return false;
    }*/
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"businessArea TEXT"];
    [fields addObject:@"tag INTEGER"];
    [fields addObject:@"name TEXT"];
    [fields addObject:@"state INTEGER"];
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS containers;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE containers (%@);",[fields componentsJoinedByString:@","]]];
    /*
    if (![self createTable:@"containers" withFields:fields]) {
        [fields release];
        return false;
    }*/
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"sender TEXT"];
    [fields addObject:@"recipient TEXT"];
    [fields addObject:@"subject TEXT"];
    [fields addObject:@"body TEXT"];
    [fields addObject:@"assets TEXT"];
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS email;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE email (%@);",[fields componentsJoinedByString:@","]]];
    
    
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"ctime DATE"];
    [fields addObject:@"user TEXT"];
    [fields addObject:@"type INT"];//0=file, 1 = folder
    [fields addObject:@"fdep INT"];         //depth of inode
    [fields addObject:@"path TEXT"];
    //cached meta-data from carousel and search APIs (empty if folder)
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"mediaType TEXT"];
    [fields addObject:@"title TEXT"];
    [fields addObject:@"thumbnail TEXT"];
    [fields addObject:@"url TEXT"];
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS idrive;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE idrive (%@);",[fields componentsJoinedByString:@","]]];
    
    //self.journalTable = @"idrive_journal";
    [fields removeAllObjects];
    
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"direction INT"];            //0 == outgoing, 1 == incoming
    [fields addObject:@"user TEXT"];
    [fields addObject:@"op TEXT"];
    [fields addObject:@"params TEXT"];
    [fields addObject:@"mtime DATE"];
    
    
    sqlMaster = [sqlMaster stringByAppendingString:@"DROP TABLE IF EXISTS idrive_journal;"];
    sqlMaster = [sqlMaster stringByAppendingString: [NSString stringWithFormat:@"CREATE TABLE idrive_journal (%@);",[fields componentsJoinedByString:@","]]];
    
    BOOL success = [self.db executeStatements:sqlMaster];
    
    [self closeDB];
    
    /*
    if (![self createTable:@"email" withFields:fields]) {
        [fields release];
        return false;
    }
    [fields release];
    */
    
    return success;
}


- (NSString *)databasePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent: @"dam.sqlite"];
    
    NSLog(@"db path %@", path);
    return path;
}

- (BOOL) openDB {
    if (![self.db open]) {
        [self.db release];
        return NO;
    }
    return YES;
}

- (void) closeDB {
    [self.db close];
}

#pragma mark - Get Download Count
- (void) downloadCount:(NSArray *)mediaTypes successCallback:(void (^)(int count))successCalback {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *db) {
        db.logsErrors = kDB_Errors_Logs;
        int totalDownloads = 0;
        //get total download first
        FMResultSet *results = [db executeQuery: @"SELECT count(0) AS count FROM download_queue", nil];
        while([results next]) {
            totalDownloads += [results intForColumnIndex:0];
            break;
        }
        
        [results close];
        
        for (NSString *mediaType in mediaTypes) {
            FMResultSet *results = [db executeQuery:[NSString stringWithFormat:@"SELECT count(0) AS count FROM download_queue_original where mediaType='%@'", mediaType], nil];
            while([results next]) {
                totalDownloads += [results intForColumnIndex:0];
                break;
            }
            [results close];
        }
        //return data
        successCalback (totalDownloads);
    }];
}

/*
 ["feet", "geese", "meese", "kangareese"] ==> ?,?,?,?
 try this on JS console: alert(["a","b","c"].map(function(e){ return "?" }).join(",")); ==> ?,?,?*/
+ (NSString *) placeholdersForArray: (NSArray *) array {
    NSString *result = @"";
    if (array == NULL || [array count] == 0) {
        return @"";
    }
    for (id temp in array) {
        result = [result stringByAppendingString:@"?,"];
    }
    return [result substringToIndex:[result length] - 1];
    //return array.map(function(e){ return "?" }).join(",");
}

- (void) executeQuery:(NSString *) sqlQuery params: (NSArray *) params successCallback:(void (^)(id resp))successCalback {
    
    //NSLog(@"exec query %@", sqlQuery);
    
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *db) {
        db.logsErrors = kDB_Errors_Logs;
        FMResultSet *results = [db executeQuery: sqlQuery withArgumentsInArray: params];
        NSMutableArray * data = [NSMutableArray array];
        while([results next]) {
            [data addObject: [results resultDictionary]];
        }
        [results close];

        //return data
        successCalback (data);
    }];
}

- (void) executeQueryMap: (NSString *) sqlQuery params: (NSArray *) parameters successCallback:(void (^)(id resp))successCalback {
    [self executeQuery: sqlQuery
                params: parameters
       successCallback:^(NSArray *resultSet) {
           
           //NSArray *resultSet = [NSArray arrayWithObject:resp];
           if (resultSet != nil) {
               //NSLog(@"db.map resultSet.length: %d", resultSet.count);
               
               NSMutableArray *result = [NSMutableArray array];
               for (NSDictionary *row in resultSet) {
                   if ([row isKindOfClass: [NSDictionary class]] && [row objectForKey:@"meta"] != nil) {
                       //clone, so we can alter object (original is immutable)
                       //                    var clone = jQuery.extend(true, {}, row);
                       NSMutableDictionary * clone = [NSDictionary dictionaryWithJSONString:[row objectForKey:@"meta"] error:nil];
                       [result addObject: clone];
                   } else {
                       [result addObject: row];
                   }
               }
               //return result;
               NSMutableArray *data = [NSMutableArray arrayWithArray: result];
               successCalback (data);
           }else{
               successCalback([NSArray array]);
           }
    }];
}

/*
- (NSMutableArray *) map: (NSString *) sql params: (NSArray *) parameters{
    //        var params = opts && opts.params ? opts.params : [];
    NSLog(@"db.map sql: %@ , params: %@" , sql, parameters);
    
    NSArray *resultSet = [self sql:sql params:parameters];
    //[self sql:sql params:parameters success:^(NSMutableArray *resultSet) {
    if (resultSet != nil && [resultSet count] > 0) {
        NSLog(@"db.map resultSet.length: %@", resultSet);
        
        NSMutableArray *result = [[NSMutableArray alloc] init];
        for (NSDictionary *row in resultSet) {
            if ([row objectForKey:@"meta"] != nil) {
                //clone, so we can alter object (original is immutable)
                //                    var clone = jQuery.extend(true, {}, row);
                NSMutableDictionary * clone = [NSMutableDictionary dictionaryWithDictionary: [NSDictionary dictionaryWithJSONString:[row objectForKey:@"meta"] error:nil]];
                [result addObject: clone];
            } else {
                [result addObject: row];
            }
        }
        //return result;
        NSMutableArray *data = [NSMutableArray arrayWithArray: result];
        [result release];
        //callbackFunc ( data);
        return data;
    }
    //}];
    
    return nil;
}*/


/*
- (NSMutableArray *) sql: (NSString *)sqlStatement params: (NSArray *) parameters{
    
    if (![self openDB]) {
        return nil;
    }
    
    FMResultSet *results = [self.db executeQuery:sqlStatement, parameters];
    NSMutableArray * data = [NSMutableArray array];
    while([results next]) {
        [data addObject: [results resultDictionary]];
    }
    
    [self closeDB];
    
    return data;
    
    //if (![self openDB]) return nil;
    /*
    sqlite3_stmt *statement;
    NSMutableArray *result = nil;
    
    if(sqlite3_prepare_v2(db, [sqlStatement UTF8String], -1, &statement, NULL)!=SQLITE_OK){
#ifdef DEBUG
        NSLog(@"DAMApplicationModel.getRows>>error %@", sqlStatement);
#endif
    } else {
        //bind params
        //NSLog(@"%@ parameters %@", sqlStatement, parameters);
        if (parameters != nil && ![parameters isEqual: [NSNull null]] && [parameters count] > 0) {
            int index = 1;
            for (NSString *param in parameters) {
                sqlite3_bind_text(statement, index,param && param != nil ? [param UTF8String] : [@"" UTF8String] , -1, SQLITE_TRANSIENT);
                index++;
            }
        }
        
        result = [[NSMutableArray alloc] init];
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
            for (int i=0; i<sqlite3_column_count(statement); i++) {
                int colType = sqlite3_column_type(statement, i);
                NSString *rowName = [NSString stringWithFormat:@"%s", sqlite3_column_name(statement, i)];
                id value;
                if (colType == SQLITE_TEXT) {
                    //const unsigned char *col = sqlite3_column_text(statement, i);
                    const unsigned char *col = sqlite3_column_text(statement, i);
                    //value = [NSString stringWithFormat:@"%s", col];
                    value = [NSString stringWithUTF8String: col];
                } else if (colType == SQLITE_INTEGER) {
                    int col = sqlite3_column_int(statement, i);
                    value = [NSNumber numberWithInt:col];
                } else if (colType == SQLITE_FLOAT) {
                    double col = sqlite3_column_double(statement, i);
                    value = [NSNumber numberWithDouble:col];
                } else if (colType == SQLITE_NULL) {
                    value = [NSNull null];
                } else {
#ifdef DEBUG
                    NSLog(@"[SQLITE] UNKNOWN DATATYPE");
#endif
                    value = [NSNull null];
                }
                [row setObject:value forKey:rowName];
            }
            
            [result addObject: [NSDictionary dictionaryWithDictionary: row]];
            
            [row release];
        }
    }
    
    if(statement) sqlite3_finalize(statement);
    
    NSMutableArray *data = [NSMutableArray arrayWithArray: result];
    [result release];
    
    //callbackFunc( data);
    return data;
    //return result;
    * /
}
*/

-(void)sqlExecWithSQLStatement:(NSString *)sql successCallback:(void (^)(BOOL resp))successCalback{
    if (sql == nil || sql.length == 0) {
        successCalback(true);
        return;
    }
    NSLog(@"sql %@", sql);
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        db.logsErrors = kDB_Errors_Logs;
        BOOL result = [db executeUpdate: sql];
        if (result) [db commit];
        
        successCalback(result);
    }];
    /*
    bool result = true;
    char *err = 0;
    if(sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) !=SQLITE_OK){
#ifdef DEBUG
        NSLog(@"DAMApplicationModel.sqlExec>>Problem with sqllite3_exec %s", err);
#endif
        sqlite3_free( err );
        result = false;
    }
    return result;
    */
}


/*
- (void) map: (NSString *)sqlStatement params: (NSArray *) parameters success:(void (^)(NSMutableArray *))callbackFunc {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        FMResultSet *rs = [bdb executeQuery: sqlStatement withArgumentsInArray: parameters];
        
        NSMutableArray *result = [[NSMutableArray alloc] init];
        //NSMutableArray *result = [NSMutableArray array];
        while ([rs next]) {
            
            NSDictionary *row =  [NSDictionary dictionaryWithDictionary:[rs resultDictionary]];
            
            if ([row objectForKey:@"meta"] != nil) {
                //clone, so we can alter object (original is immutable)
                //                    var clone = jQuery.extend(true, {}, row);
                NSMutableDictionary * clone = [NSMutableDictionary dictionaryWithDictionary: [NSDictionary dictionaryWithJSONString:[row objectForKey:@"meta"] error:nil]];
                
 
                [result addObject: clone];
            } else {
                [result addObject: row];
            }
            //[result addObject: [rs resultDictionary]];
        }
        //NSLog(@"sqlStatement %@ rs == %@",sqlStatement, result);
        NSMutableArray *data = [NSMutableArray arrayWithArray: result];
        [result release];
        if (callbackFunc)callbackFunc(data);
    }];
}

- (void) sql: (NSString *)sqlStatement params: (NSArray *) parameters success:(void (^)(NSMutableArray *))callbackFunc {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        FMResultSet *rs = [bdb executeQuery: sqlStatement withArgumentsInArray: parameters];
        
        NSMutableArray *result = [[NSMutableArray alloc] init];
        while ([rs next]) {
            [result addObject: [NSDictionary dictionaryWithDictionary:[rs resultDictionary]]];
        }
        //NSLog(@"sqlStatement %@ rs == %@",sqlStatement, result);
        NSMutableArray *data = [NSMutableArray arrayWithArray: result];
        [result release];
        //[rs close];
        if (callbackFunc)callbackFunc(data);
    }];
}*/

-(void)sqlExec:(NSDictionary *)batch successCallback:(void (^)(BOOL resp))successCalback{
    if (batch == nil || [batch objectForKey:@"params"] == nil) {
        successCalback(true);
        return;
    }
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        db.logsErrors = kDB_Errors_Logs;
        NSArray *params = [batch objectForKey:@"params"];
		NSString *sql = [batch objectForKey: @"sql"];
        BOOL result = [db executeUpdate: sql withArgumentsInArray:params];
        
        if (result)[db commit];
        
        successCalback (result);
    }];
    
    /*
    //if (![self openDB]) return false;
    char* errorMessage;
    sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &errorMessage);
    int result = 0;
    sqlite3_stmt *insert_statement = nil;
    @try {
		const char *sql;
        NSArray *params = [batch objectForKey:@"params"];
		sql = [[batch objectForKey: @"sql"] UTF8String];
        //NSLog(@"sql insert %@ - %@",  [batch objectForKey: @"sql"], [params objectAtIndex:0]);
		if (sqlite3_prepare_v2(db, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
        
        if (params && [params count] > 0) {
            int index = 1;
            for (NSString *param in params) {
                sqlite3_bind_text(insert_statement, index, (param != NULL ? [param UTF8String] : [@"" UTF8String]), -1, SQLITE_TRANSIENT);
                index++;
            }    
        }
		result = sqlite3_step(insert_statement);
		
		if (result == SQLITE_ERROR) {
			NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(db));
#ifdef DEBUG
            NSLog(@"Error mediaRef %@", [params objectAtIndex:0]);
#endif
		} 
        sql = nil;
	}@catch (NSException * e) {
	}@finally {
	}
    sqlite3_exec(db, "COMMIT TRANSACTION", NULL, NULL, &errorMessage);
    //reset to save memory
    if(insert_statement) sqlite3_finalize(insert_statement);
    return result;
    */
}

/*batch insert, all object in batch MUST BE same insert sql*/
- (void) batchInsert: (NSArray *) batches successCallback:(void (^)(BOOL resp))successCalback{
    if (batches == nil || batches.count == 0) {
        successCalback(true);
        return;
    }
    
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        db.logsErrors = kDB_Errors_Logs;
        BOOL result;
        for (NSDictionary * batch in batches) {
            NSArray *params = [batch objectForKey:@"params"];
            NSString *sql = [batch objectForKey: @"sql"];
            result = [db executeUpdate: sql withArgumentsInArray:params];
        }
        if (result)[db commit];
        successCalback (true);
    }];
    /*
    //start a transaction
    char* errorMessage;
    sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &errorMessage);
    sqlite3_stmt *insert_statement = nil;
    
    const char *sql;
    sql = [[[batches objectAtIndex:0] objectForKey: @"sql"] UTF8String];
    //NSLog(@"sql insert %@ - %@",  [batch objectForKey: @"sql"], [params objectAtIndex:0]);
    if (sqlite3_prepare_v2(db, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
        NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
    }
    int result = 0;
    for (NSDictionary * batch in batches) {
        NSArray *params = [batch objectForKey:@"params"];
        if (params && [params count] > 0) {
            int index = 1;
            for (NSString *param in params) {
                sqlite3_bind_text(insert_statement, index, (param != NULL ? [param UTF8String] : [@"" UTF8String]), -1, SQLITE_TRANSIENT);
                index++;
            }    
        }
		result = sqlite3_step(insert_statement);
		
		if (result == SQLITE_ERROR) {
			NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(db));
#ifdef DEBUG
            NSLog(@"Error mediaRef %@", [params objectAtIndex:0]);
#endif
		} 
        sqlite3_reset(insert_statement);
        //[self sqlExec: insert];
    }
    sqlite3_exec(db, "COMMIT TRANSACTION", NULL, NULL, &errorMessage);
    if(insert_statement) sqlite3_finalize(insert_statement);
    */
}

- (void) clearJournal: (NSString *)where {
    if (![self openDB]) {
        return;
    }
    
}


@end
