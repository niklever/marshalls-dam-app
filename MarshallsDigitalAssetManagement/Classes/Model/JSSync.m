//
//  JSSync.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSSync.h"
#import "DAMAppDelegate.h"
#import "DAMViewController.h"
#import "DAMHomeView.h"

@implementation JSSync

@synthesize running;
@synthesize delegate;
@synthesize thumbHeightBuckets;
@synthesize mediaTable;
@synthesize containerTable;
@synthesize tagTable;
@synthesize downloadTable;
@synthesize downloadTableOriginal;
@synthesize deleteTable;
@synthesize downloadPlugin;
@synthesize settingsKey;
@synthesize settings;
@synthesize earliestPossibleDate;
@synthesize lsDateKey;

static JSSync *sharedInstance = nil;

+ (JSSync *)sharedInstance{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (void) setDelegate:(id)aDelegate {
    self.downloadPlugin.jsBridge = aDelegate;
}

- (id)init{
    self = [super init];
    if (self) {
        self.thumbHeightBuckets = [NSArray arrayWithObjects:@"181.0", @"243.0", @"368.0", @"493.0", nil];
        self.mediaTable = @"media";
        self.containerTable = @"media_container";
        self.tagTable = @"media_tags";
        self.downloadTable = @"download_queue";
        self.downloadTableOriginal = @"download_queue_original";
        self.deleteTable = @"media_deleted";
        self.settingsKey = @"sync.settings";
        self.downloadPlugin = [[DownloadPlugin alloc] init];
        self.running = false;
        self.earliestPossibleDate = kLastEarlyPossibleDate;
        self.lsDateKey = @"sync.lastUpdated";
        //syncInterval: 60 * 1000,
    }
    return self;
}


- (void) dealloc {
    //[self.downloadPlugin release];
    [super dealloc];
}

- (NSString *)lastSyncDate{
    NSString *lsData = [[JSLocalStorage sharedInstance] getItem: self.lsDateKey];
    if (!lsData || [lsData isEqual: [NSNull null]] || [lsData isEqualToString:@"null"]) {
        lsData = self.earliestPossibleDate;
        //also, automatically create db schema
        //[self createSchema];
    }
    NSLog(@"sync.lastSyncDate %@", lsData);
    //lsData = "2012-10-10";
    return lsData;
}

- (void) createSchema {
    /*console.log("sync.createSchema sync.schemaCols:"+sync.schemaCols);
    db.createTable(sync.downloadTable, sync.schemaCols);
    db.createTable(sync.mediaTable, sync.mediaCols, function(){
        db.sql("CREATE INDEX idx_mt ON " + sync.mediaTable + "(mt)",[],function(){console.log("sync.createSchema.CREATE_INDEX.idx_mt")});
        db.sql("CREATE INDEX idx_mediaRef ON " + sync.mediaTable + "(mediaRef)",[],function(){console.log("sync.createSchema.CREATE_INDEX.idx_mediaRef")});
    });
    db.createTable(sync.containerTable, sync.containerCols);
    db.createTable(sync.tagTable, sync.tagTableCols);
    db.createTable(sync.deleteTable, sync.deleteTableCols);
    commandQueue.finished();*/
}

- (void)setLastSyncAsNow{
    NSString * now = [JSMain getDate];
    [[JSLocalStorage sharedInstance] setItem: self.lsDateKey value: now];
    
#ifdef DEBUG
    NSLog(@"sync.setLastSyncAsNow now: %@",now);
#endif
    
}

/**
 * Run the synchronisation request, asking the server for a list of assets and/or meta-data that
 * has been updated since last time. The first this request is run, use a date that is
 * guarenteed to be earlier than the earliest asset in the CMS. Then we add to the download
 * quee and record current date and time for the next invocation of this methods.
 *
 */

- (void)run:(void (^)(void))callback {
    if ([JSMain sharedInstance].opsSuspended) {
        NSLog(@"sync.run opsSuspended == YES");
        return;
    }

    NSLog(@"sync.run");
    
    if (self.running) {
        
        NSLog(@"sync.run.WARN_ALREADY_RUNNING");
        
        return;
    }
    if (![[JSMain sharedInstance] online]) {
        
        NSLog(@"sync.run.WARN_OFFLINE");
        DAMViewController *damViewController = kAppDelegate.damViewController;
        dispatch_async(dispatch_get_main_queue(), ^{
            [damViewController.homeView updateNotificationLogs: kNoWifiToDownloadAssets];
            
            //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(showUpdatingMessage:) withData: @"Please connect to a Wifi connection to download assets"];
            
            kAppDelegate.damViewController.homeView.progressView.hidden = YES;
            kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = YES;
        });
        return;
    }else if ([[JSMain sharedInstance] userEmail] == NULL || [[[JSMain sharedInstance] userEmail] length] == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [kAppDelegate.damViewController.homeView updateNotificationLogs: kUpdateEmailInSettings];
            [kAppDelegate.damViewController.homeView setDisplayProgressView: YES];
            [kAppDelegate.damViewController.homeView setDisplayActivityIndicatorView: YES];
        });
        //kAppDelegate.damViewController.homeView.progressView.hidden = YES;
        //kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = YES;
        
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(showUpdatingMessage:) withData: @"Please update your email in Settings to sync contents."];
        
        
        return;
    }
    
    self.running = true;
    
    //self.settings = [self _getSettings];
    
    [self _getSettings:^(NSMutableArray *data) {
        self.settings = data;
        [self updateDownloadCount];
        
        NSMutableArray *resetSettings = [[NSMutableArray alloc] init];
        NSMutableArray *sinceLastSettings = [[NSMutableArray alloc] init];
        
        //NSLog(@"current settings == %@", self.settings);
        
        
        for(NSMutableDictionary *obj in self.settings) {
#ifdef DEBUG
            NSLog(@"obj == %@", obj);
#endif
            if ([[obj objectForKey:@"reset"] isEqualToString:@"true"]) {
                [resetSettings addObject: obj];
            }else if ([[obj objectForKey:@"on"] isEqualToString:@"true"]) {
                [sinceLastSettings addObject: obj];
            }
        }
        
        if ([resetSettings count] > 0) {
            
            NSLog(@"start syn with resetSettings %@", resetSettings);
            
            [self syncContainers: resetSettings
                           since: self.earliestPossibleDate
                        callback:^{
                NSMutableArray *temp = [[NSMutableArray alloc] init];
                for (NSDictionary * setting in self.settings) {
                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary: setting];
                    [tempDic setObject:@"false" forKey:@"reset"];
                    [temp addObject: tempDic];
                    [tempDic release];
                    //NSLog(@"setting.reset: %@", [setting objectForKey:@"reset"]);
                }
                self.settings = temp;
                [temp release];
                [[JSLocalStorage sharedInstance] setItem: self.settingsKey value: [self.settings JSONString]];
                
                NSLog(@"sync.run settings after run sycn with reset: %@ == sinceLastSettings.count == %d" , [self.settings JSONString], sinceLastSettings.count);
                
                if ([sinceLastSettings count] > 0) {
                    [self abortQueue: nil callback:^{
                        
                        NSLog(@"start syn with sinceLastSettings %@", sinceLastSettings);

                        [self syncContainers: sinceLastSettings
                                       since: [self lastSyncDate]
                                    callback:^{
                                        //sync._syncContainers(sinceLastSettings, sync.lastSyncDate(), function(){
                                        [self setLastSyncAsNow];
                                        self.running = false;
                                        //TODO
                                        //callback && callback();
                                        if (callback) {
                                            callback();
                                        }
                        }];
                    }];
                } else {
                    NSLog(@"sync.run.NO_SETTINGS_ON");
                    [self setLastSyncAsNow];
                    self.running = false;
                    //callback && callback();
                    if (callback) {
                        callback();
                    }
                }
            }];
            
        } else {
            
            NSLog(@"sync.run.NO_SETTINGS_RESET == sinceLastSettings.count == %d", sinceLastSettings.count);
            if ([sinceLastSettings count] > 0) {
                //sync.setLastSyncAsNow();
                [self syncContainers: sinceLastSettings
                               since: [self lastSyncDate]
                            callback:^{
                            [self setLastSyncAsNow];
                            self.running = false;
                            if (callback) {
                                callback();
                            }
                }];
            } else {
                NSLog(@"sync.run.NO_SETTINGS_ON");
                [self setLastSyncAsNow];
                self.running = false;
                //callback && callback();
                if (callback) {
                    callback();
                }
            }
        }
        [sinceLastSettings release];
        [resetSettings release];
        
    }];
}

/*
 
_syncContainers: function(settings, since, callback) {
 */
//- (void) syncContainers:(NSString *)_settings since:(NSString *)when callback: (void (^)(void))callbackFunc {
- (void) syncContainers:(NSArray *)_containers since:(NSString *)when callback: (void (^)(void))callbackFunc {
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for (id setting in _containers) {
        if ([setting isKindOfClass:[ NSString class]]) {
            [temp addObject: setting];
        }else{
            [temp addObject: [setting objectForKey:@"name"]];
        }
    }
    
    NSString *containerName = [[temp componentsJoinedByString:@"|"]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    containerName = [containerName stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    
    //NSString *endpoint = [NSString stringWithFormat:@"%@/api/sync?since=%@&containers=%@", [JSMain apiHost], when, containerName];
    [temp release];
    dispatch_async(dispatch_get_main_queue(), ^{
        kAppDelegate.damViewController.homeView.notificationLabel.hidden = NO;
        //[kAppDelegate.damViewController.homeView updateNotificationLogs: @"Syncing containers from server..."];
        [kAppDelegate.damViewController.homeView showUpdatingMessage: kSyncingContainers];
        kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = NO;
    });
    
    [[APICommon sharedInstance] syncAssetsFrom: when containerName: containerName
                                   successCallback:^(id resp) {
                                       
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = YES;
                                           kAppDelegate.damViewController.homeView.progressView.hidden = YES;
                                           kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = NO;
                                       });
                                       
                                       [kAppDelegate.damViewController.homeView showUpdatingMessage: @"Parsing response data..."];
                                       
                                       self.updatedAssetsList = nil;
                                       self.deletedAssetsList = nil;
                                       
                                       //NSString *responseString = [request responseString];
                                       [self parseSyncJsonContent: resp
                                                needDeleteUpdated:(![when isEqualToString: self.earliestPossibleDate])
                                                  sinceLastUpdate: when
                                                         callback:^{
                                                             
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                    kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = YES;
                                                             });
                                                               [kAppDelegate.damViewController.homeView showUpdatingMessage: kDoneSyncing];
                                                               
                                                               NSLog(@"Done parse Json content");
                                                               
                                                               [self updateDownloadCount];
                                                               
                                                               [kAppDelegate.damViewController.homeView notificationTimerFired: NULL];
                                                               
                                                               if (callbackFunc) {
                                                                   callbackFunc();
                                                               }
                                       }];
                                   } andFailCallback:^(id msg) {
                                       if (callbackFunc) {
                                           callbackFunc();
                                       }
                                   }];
    
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1001) {
        if (buttonIndex == 1) {
            //user press OK button
            //
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performUpdateAssets: YES callback: self.syncAssetCallbackFunc];
            });
        }else {
            //cancel
            self.updatedAssetsList = nil;
            self.deletedAssetsList = nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performUpdateAssets: NO callback: self.syncAssetCallbackFunc];
            });
        }
    }
}



- (void) performUpdateAssets:(BOOL) updatedDelete callback:(void (^)(void))callbackFunc {
    NSMutableArray  * photos = [[NSMutableArray alloc] init];
    //NSMutableArray  * batch = [[NSMutableArray alloc] init];
    NSMutableArray  * mediaBatches = [[NSMutableArray alloc] init];
    NSMutableArray  * mediaContainersBatches = [[NSMutableArray alloc] init];
    NSMutableArray  * mediaTagsBatches = [[NSMutableArray alloc] init];
    NSMutableArray  * queueBatches = [[NSMutableArray alloc] init];
    NSMutableArray  * queueOriginalBatches = [[NSMutableArray alloc] init];
    
    //start parsing updated list
    if (self.updatedAssetsList != nil && [self.updatedAssetsList count] > 0) {
        
        [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Updating local database..."];
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(showUpdatingMessage:) withData: @"Updating local database..."];
        
        
        for (NSDictionary *object in self.updatedAssetsList) {
            //NSLog(@"object == %@", object);
            NSDictionary *thumbObject = [self allocateThumbnailIfPhoto: object];
            
            //NSLog(@"thumbObject == %@", thumbObject);
            
            if ([thumbObject objectForKey:@"thumbnail"] == NULL) {
                
                NSLog(@"GARBAGE thumbnail of mediaRef == %@", [thumbObject objectForKey:@"mediaRef"]);
            }
            if ([thumbObject objectForKey:@"url"] == NULL) {
                NSLog(@"GARBAGE url of mediaRef == %@", [thumbObject objectForKey:@"mediaRef"]);
            }
            
            //TODO: need to clear current asset
            if (updatedDelete) {
                [self assetDeleteDetected: [NSArray arrayWithObject: [thumbObject objectForKey:@"mediaRef"]]];
            }
            //[batch addObjectsFromArray: [self addMediaObjectToBatch: thumbObject != nil ? thumbObject : object]];
            [mediaBatches addObjectsFromArray: [self addMediaObjectToBatch: thumbObject != NULL ? thumbObject : object]];
            [mediaContainersBatches addObjectsFromArray: [self addMediaObjectToMediaContainerBatch:thumbObject != NULL ? thumbObject : object]];
            [mediaTagsBatches addObjectsFromArray: [self addMediaObjectToMediaTagsBatch:thumbObject != NULL ? thumbObject : object]];
            
            if ([thumbObject objectForKey:@"thumbnail"] != NULL) {
                [queueBatches addObject: [self addToQueueBatch:[thumbObject objectForKey:@"mediaRef"] mediaType:[thumbObject objectForKey:@"mediaType"] url:[thumbObject objectForKey:@"thumbnail"] isVideos: FALSE]];
            }
            
            //pre-load videos and add them to the iDrive.
            if ([thumbObject objectForKey:@"url"] != NULL && [[thumbObject objectForKey:@"mediaType"] isEqualToString:kMediaType_Videos]) {
                //adding as=1 as original
                NSString *baseUrl = [NSString stringWithFormat:@"%@", [thumbObject objectForKey:@"url"]];
                [queueOriginalBatches addObject: [self addToQueueBatch:[thumbObject objectForKey:@"mediaRef"] mediaType:[thumbObject objectForKey:@"mediaType"] url:baseUrl isVideos: TRUE]];
                //[[JSiDrive sharedInstance] cp: thumbObject folder: @"/Videos"];
                //iDrive.cp(object, "/Videos");
            }
            //pre-load Samples
            if ([thumbObject objectForKey:@"url"] != NULL && [[thumbObject objectForKey:@"mediaType"] isEqualToString:kMediaType_Samples]) {
                NSString *baseUrl = [NSString stringWithFormat:@"%@", [thumbObject objectForKey:@"url"]];
                [queueOriginalBatches addObject: [self addToQueueBatch:[thumbObject objectForKey:@"mediaRef"] mediaType:[thumbObject objectForKey:@"mediaType"] url:baseUrl isVideos: TRUE]];
            }
            //pre-load Brochures
            if ([thumbObject objectForKey:@"url"] != NULL && [[thumbObject objectForKey:@"mediaType"] isEqualToString:kMediaType_Brochures]) {
                NSString *baseUrl = [NSString stringWithFormat:@"%@", [thumbObject objectForKey:@"url"]];
                [queueOriginalBatches addObject: [self addToQueueBatch:[thumbObject objectForKey:@"mediaRef"] mediaType:[thumbObject objectForKey:@"mediaType"] url:baseUrl isVideos: TRUE]];
            }
            
            if ([[thumbObject objectForKey:@"mediaType"] isEqualToString:kMediaType_Photos] || [[thumbObject objectForKey:@"mediaType"] isEqualToString:kMediaType_SalesPhotos]) {
                [photos addObject: thumbObject];
            }
            
        }//end loop
        
        //[self updateDownloadCount: [mediaBatches count] + [mediaContainersBatches count] + [mediaTagsBatches count] + [queueBatches count]];
    }//end updated list
    
    
    [[JSDBV2 sharedInstance] batchInsert: mediaBatches successCallback:^(BOOL resp) {
        NSLog(@"mediaBatches finished!!!!!!!!!!!!!!!!!!!!!!");
        [[JSDBV2 sharedInstance] batchInsert: mediaTagsBatches successCallback:^(BOOL resp) {
            NSLog(@"mediaTagsBatches finished!!!!!!!!!!!!!!!!!!!!!!");
            [[JSDBV2 sharedInstance] batchInsert: mediaContainersBatches successCallback:^(BOOL resp) {
                NSLog(@"mediaContainersBatches finished!!!!!!!!!!!!!!!!!!!!!!");
                [[JSDBV2 sharedInstance] batchInsert: queueBatches successCallback:^(BOOL resp) {
                    NSLog(@"queueBatches finished!!!!!!!!!!!!!!!!!!!!!!");
                    [[JSDBV2 sharedInstance] batchInsert: queueOriginalBatches successCallback:^(BOOL resp) {
                        NSLog(@"queueOriginalBatches finished!!!!!!!!!!!!!!!!!!!!!!");
                        
                        if (self.deletedAssetsList != nil && [self.deletedAssetsList count] > 0) {
                            [self assetDeleteDetected: self.deletedAssetsList];
                        }
                        
                        if (self.updatedAssetsList != nil && [self.updatedAssetsList count] > 0) {
                            
                            NSLog(@"sync.run.SYNC_NEW_DATA");
                            
                            //bit hacky, waiting arbitrary period before sending event
                            /*setTimeout(function(){
                             console.log("sync.run.SYNC_NEW_DATA");
                             $(document).trigger("SYNC_NEW_DATA");
                             sync.availableMediaTypes();
                             }, 2000);*/
                            [self synAvailableMediaTypes];
                            //[self performSelector:@selector(synAvailableMediaTypes) withObject:nil afterDelay:2.0];
                        }
                        
                        if (callbackFunc) {
                            callbackFunc();
                        }
                    }];
                }];
            }];
        }];
    }];
    /*
    if ([mediaBatches count] > 0) {
        [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Executing batch updates..."];
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(showUpdatingMessage:) withData: @"Executing batch updates..."];
        [self _batchInsertInBackground: mediaBatches successCallback:^{
            
        }];
    }
    if ([mediaTagsBatches count] > 0) {
        [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Executing batch updates..."];
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(showUpdatingMessage:) withData: @"Executing batch updates..."];
        //[self performSelectorOnMainThread:@selector(_batchInsertInBackground:) withObject:mediaTagsBatches waitUntilDone: NO];
        [self _batchInsertInBackground:mediaTagsBatches];
    }
    if ([mediaContainersBatches count] > 0) {
        [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Executing batch updates..."];
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(showUpdatingMessage:) withData: @"Executing batch updates..."];
        //[self performSelectorOnMainThread:@selector(_batchInsertInBackground:) withObject:mediaContainersBatches waitUntilDone: NO];
        [self _batchInsertInBackground:mediaContainersBatches];
    }
    
    if ([queueBatches count] > 0) {
        NSLog(@"queue batch insert length %d", [queueBatches count]);
        [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Executing batch updates..."];
        [self _batchInsertInBackground: queueBatches];
    }
    
    if ([queueOriginalBatches count] > 0) {
        NSLog(@"queueOriginalBatches batch insert length %d", [queueOriginalBatches count]);
        [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Executing batch updates..."];
        [self _batchInsertInBackground: queueOriginalBatches];
    }*/
    
    //release memory
    //[batch release];
    [mediaBatches release];
    [mediaContainersBatches release];
    [mediaTagsBatches release];
    [photos release];
    [queueBatches release];
    [queueOriginalBatches release];
}



/* This function parse the json content return by updateDownloadList function
 this is function _syncContainers: function(settings, since, callback) in sync.js
 */
- (void)parseSyncJsonContent: (NSDictionary *)dictionary needDeleteUpdated:(BOOL) updatedDelete sinceLastUpdate: (NSString *) lastUpdate callback:(void (^)(void))callbackFunc {

    //NSDictionary *dictionary = [NSDictionary dictionaryWithJSONString:jsonContent error:nil];
    if (dictionary == NULL) {
        
        //[JSMain showAlertWithMessage:@"Bad JSON content received."];
        [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Bad JSON content received. Trying to reload JSON content."];
        
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(showUpdatingMessage:) withData: @"Bad JSON content received. Trying to reload JSON content."];
        UIProgressView *progressView = kAppDelegate.damViewController.homeView.progressView;
        progressView.hidden = YES;
        //return false;
        if (callbackFunc) {
            callbackFunc();
        }
        return;
    }
    
    //get updated media list
    self.updatedAssetsList = [dictionary objectForKey:@"updated"];
    //get deleted media list          
    self.deletedAssetsList = [dictionary objectForKey:@"deleted"];
    
    
    //PKT update logic for asking user permission before do update
    if ( (self.updatedAssetsList != nil && [self.updatedAssetsList count] > 0) || (self.deletedAssetsList != nil && [self.deletedAssetsList count] > 0) ) {
        //if this is not first time of syncing
        if (![lastUpdate isEqualToString: kLastEarlyPossibleDate]) {
            
            self.syncAssetCallbackFunc = callbackFunc;
            
            UIAlertView *alvInform    =   [[UIAlertView alloc] initWithTitle: nil
                                                                     message: kNewMediaAssetsUpdated
                                                                    delegate: self
                                                           cancelButtonTitle: @"Ignore"
                                                           otherButtonTitles: @"OK", nil];
            alvInform.tag = 1001;
            [alvInform show];
            [alvInform release];
            
            return;
        }else{
            [self performUpdateAssets: updatedDelete callback: callbackFunc];
        }
    }else{
        [self performUpdateAssets: updatedDelete callback: callbackFunc];
    }
}//end function


- (void)_batchInsertInBackground:(NSArray *)batch successCallback:(void (^)(void))successCalback{
    if ([batch count] > 0) {
        [[JSDBV2 sharedInstance] batchInsert: batch successCallback:^(BOOL resp) {
            NSLog(@"batch insert finished");
            successCalback();
        }];
    }
}

-(void) assetDeleteDetected:(NSArray *) mediaRefs {

#ifdef DEBUG
    NSLog(@"sync.assetDeleteDetected mediaRefs.length: %d", [mediaRefs count]);
#endif
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSString *mediaRef in mediaRefs) {
        NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
        [singleBatch setObject:[NSString stringWithFormat:@"INSERT INTO %@ (mediaRef) VALUES (?)", deleteTable] forKey:@"sql"];
        NSMutableArray *params = [[NSMutableArray alloc] init];
        [params addObject: mediaRef];
        [singleBatch setObject: params forKey:@"params"];
        [params release];
        [result addObject: singleBatch];
        [singleBatch release];
    }
    [[JSDBV2 sharedInstance] batchInsert: result successCallback:^(BOOL resp) {
        NSLog(@"assetDeleteDetected batch insert done!!!!!!!!!!!!!!!!!!!!!!!!!");
        [self clearAssetsDeleted];
    }];
    
    [result release];
    /*
    $(mediaRefs).each(function(index, mediaRef){
        db.sql("INSERT INTO " + sync.deleteTable + " (mediaRef) VALUES (?)", [mediaRef]);
    });*/
    //sync.clearAssetsDeleted();
}

- (void) abortQueue :(NSArray *) media callback:(void (^)(void))succCallback{
    [self.downloadPlugin abortQueue: media callback:^{
        succCallback();
    }];
}

#pragma mark REMOVE MEDIAS from download table

//array of mediaRefs
- (void) _removeMedia: (NSArray *) media callback:(void (^)(void))succCallback {
    if ([media count] > 0) {
        //we need to abort all of current thread if exist
        [self abortQueue: media callback:^{
            NSString *placeholders = [NSString stringWithFormat:@"(%@)", [JSDBV2 placeholdersForArray: media]];
            //console.log("_removeMedia placeholders:"+placeholders);
            
            //NSLog(@"_removeMedia media: %@" ,media);
            
            //NSMutableArray *batches = [NSMutableArray array];
            NSMutableDictionary *singleBatch = [NSMutableDictionary dictionary];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", mediaTable,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            //adding to batches
            //[batches addObject: singleBatch];
            
            [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
                NSMutableDictionary * singleBatch = [NSMutableDictionary dictionary];
                [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", containerTable,placeholders] forKey:@"sql"];
                [singleBatch setObject: media forKey:@"params"];
                [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
                    NSMutableDictionary * singleBatch = [NSMutableDictionary dictionary];
                    [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", tagTable,placeholders] forKey:@"sql"];
                    [singleBatch setObject: media forKey:@"params"];
                    [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
                        NSMutableDictionary * singleBatch = [NSMutableDictionary dictionary];
                        [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", downloadTable,placeholders] forKey:@"sql"];
                        [singleBatch setObject: media forKey:@"params"];
                        [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
                            NSMutableDictionary * singleBatch = [NSMutableDictionary dictionary];
                            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", downloadTableOriginal,placeholders] forKey:@"sql"];
                            [singleBatch setObject: media forKey:@"params"];
                            [[JSDBV2 sharedInstance] sqlExec: singleBatch successCallback:^(BOOL resp) {
                                if (succCallback) {
                                    succCallback();
                                }
                            }];
                        }];
                    }];
                }];
            }];
            /*
            singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", tagTable,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            //adding to batches
            //[batches addObject: singleBatch];
            [[JSDBV2 sharedInstance] sqlExec: singleBatch];
            
            [singleBatch release];
            
            singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", downloadTable,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            //adding to batches
            //[batches addObject: singleBatch];
            [[JSDBV2 sharedInstance] sqlExec: singleBatch];
            [singleBatch release];
            
            singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", downloadTableOriginal,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            //adding to batches
            //[batches addObject: singleBatch];
            [[JSDBV2 sharedInstance] sqlExec: singleBatch];
            [singleBatch release];
            
            //[[JSDBV2 sharedInstance] batchInsert: batches];
            */
            /*
            NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", mediaTable,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            if ([[JSDBV2 sharedInstance] sqlExec: singleBatch]) {
                
            };
            [singleBatch release];
            
            singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", containerTable,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            [[JSDBV2 sharedInstance] sqlExec: singleBatch];
            [singleBatch release];
            
            singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", tagTable,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            [[JSDBV2 sharedInstance] sqlExec: singleBatch];
            [singleBatch release];
            
            singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", downloadTable,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            [[JSDBV2 sharedInstance] sqlExec: singleBatch];
            [singleBatch release];
            
            singleBatch = [[NSMutableDictionary alloc] init];
            [singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef IN %@", downloadTableOriginal,placeholders] forKey:@"sql"];
            [singleBatch setObject: media forKey:@"params"];
            [[JSDBV2 sharedInstance] sqlExec: singleBatch];
            [singleBatch release];
            */
        }];
        
        /*
        db.sql("DELETE FROM " + sync.mediaTable + " WHERE mediaRef IN " + placeholders, media, function(){
            callback && callback();
        });
        db.sql("DELETE FROM " + sync.containerTable + " WHERE mediaRef IN " + placeholders, media);
        db.sql("DELETE FROM " + sync.tagTable + " WHERE mediaRef IN " + placeholders, media);
        db.sql("DELETE FROM " + sync.downloadTable + " WHERE mediaRef IN " + placeholders, media);
        */ 
    } else {
        
        NSLog(@"sync._removeMedia.WARN_NO_FILES");
    }
}

-(void)clearAssetsDeleted{
    
    NSLog(@"sync.clearAssetsDeleted");
    
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT d.mediaRef, m.url, m.thumbnail FROM media_deleted d, media m where m.mediaRef = d.mediaRef"];
    //NSMutableArray *rs = [[JSDBV2 sharedInstance] sql:sql params:nil];
    [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray *rs) {
        if (rs && [rs count] > 0) {
            NSMutableArray * media = [NSMutableArray array];
            for (NSDictionary *row in rs) {
                NSString *mediaRef = [row nonNullValueForKey:@"mediaRef"];
                NSString *thumbnail =  [row nonNullValueForKey:@"thumbnail"];
                NSString *url =  [row nonNullValueForKey:@"url"];
                
                [self.downloadPlugin deleteMedia: [JSMain getRealMediaFileName: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], url]]];
                
                [self.downloadPlugin deleteMedia: [JSMain getRealMediaFileName: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], thumbnail]]];
                
                [media addObject: mediaRef];
                //remove media from cached
                //[self.downloadPlugin deleteMedia: [row objectForKey:@"mediaRef"]];
                //NSArray * temp = [self mediaCachedFileNameWithExtension: [row objectForKey:@"mediaRef"]];
                /*
                [self mediaCachedFileNameWithExtension: [row objectForKey:@"mediaRef"] callback:^(NSMutableArray *temp) {
                    if (temp && [temp count] > 0) {
                        for (NSString *mr in temp) {
                            
                        }
                    }
                }];*/
                
                //[self.downloadPlugin deleteMedia: [self mediaCachedFileNameWithExtension: [row objectForKey:@"mediaRef"]]];
            }
            //TODO
            [self _removeMedia: media callback:^{
                //TODO
                [[JSiDrive sharedInstance] _removeMedia: media];
                //iDrive._removeMedia(media);
                //[singleBatch setObject: [NSString stringWithFormat:@"DELETE FROM %@", deleteTable] forKey:@"sql"];
                [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:[NSString stringWithFormat:@"DELETE FROM %@", deleteTable] successCallback:^(BOOL resp) {
                    
                }];
            }];
            //[media release];
        }
    }];
    
}

-(void)synAvailableMediaTypes {
    
    NSLog(@"sync.run.SYNC_NEW_DATA");
    //[self downloadQueueTop: 2];
    [self updateDownloadCount];
    [kAppDelegate.damViewController.homeView notificationTimerFired: nil];
    [self availableMediaTypes];
}


/*
 if total > 0 then we need to use that to update, using to prevent the case that application freeze long time
 */
- (void)updateDownloadCount{

    //__block NSString *sql = [NSString stringWithFormat:@"SELECT count(0) AS count FROM %@", downloadTable];
    //__block int count = 0;//[[[JSLocalStorage sharedInstance] getItem:@"sync.itemsToDownload"] intValue];
    //NSArray * result = [[JSDBV2 sharedInstance] sql: sql params: nil];
    NSMutableArray *mediaTypes = [NSMutableArray array];
    if ([[JSMain sharedInstance] isDownloadAllVideos]) {
        [mediaTypes addObject: kMediaType_Videos];
    }
    if ([[JSMain sharedInstance] isDownloadAllBrochures]) {
        [mediaTypes addObject: kMediaType_Brochures];
    }
    if ([[JSMain sharedInstance] isDownloadAllSamples]) {
        [mediaTypes addObject: kMediaType_Samples];
    }
    [[JSDBV2 sharedInstance] downloadCount: mediaTypes successCallback:^(int count) {
        NSLog(@"count found %d", count);
        [[JSLocalStorage sharedInstance] setItem: @"sync.itemsToDownload" value: [NSString stringWithFormat:@"%d", count]];
    }];
    
    /*
    [[JSDBV2 sharedInstance] executeQuery: sql params: nil successCallback:^(id result) {
        
        if (result && [result count] > 0) {
            count = [[(NSDictionary *)[result objectAtIndex:0] objectForKey:@"count"] intValue];
        }
        
        /*checking Videos and Brochures and Samples* /
        if ([[JSMain sharedInstance] isDownloadAllVideos]) {
            //count video
            sql = [NSString stringWithFormat:@"SELECT count(0) AS count FROM %@ where mediaType='Videos'", downloadTableOriginal];
            
            //NSArray * result = [[JSDBV2 sharedInstance] sql: sql params: nil];
            [[JSDBV2 sharedInstance] executeQuery: sql params: nil successCallback:^(id result) {
                if (result && [result count] > 0) {
                    count += [[(NSDictionary *)[result objectAtIndex:0] objectForKey:@"count"] intValue];
                }
            }];
        }
        if ([[JSMain sharedInstance] isDownloadAllBrochures]) {
            //count video
            sql = [NSString stringWithFormat:@"SELECT count(0) AS count FROM %@ where mediaType='Brochures'", downloadTableOriginal];
            NSArray * result = [[JSDBV2 sharedInstance] sql: sql params: nil];
            int temp = 0;
            if (result && [result count] > 0) {
                temp = [[(NSDictionary *)[result objectAtIndex:0] objectForKey:@"count"] intValue];
            }
            count += temp;
            
        }
        if ([[JSMain sharedInstance] isDownloadAllSamples]) {
            //count video
            sql = [NSString stringWithFormat:@"SELECT count(0) AS count FROM %@ where mediaType='Samples'", downloadTableOriginal];
            NSArray * result = [[JSDBV2 sharedInstance] sql: sql params: nil];
            int temp = 0;
            if (result && [result count] > 0) {
                temp = [[(NSDictionary *)[result objectAtIndex:0] objectForKey:@"count"] intValue];
            }
            count += temp;
        }
        
        [[JSLocalStorage sharedInstance] setItem: @"sync.itemsToDownload" value: [NSString stringWithFormat:@"%d", count]];
    }];
    */
}




/**
 *
 * Remove an item from the download queue (because the binary has been downloaded)
 *
 * @param queueItemId the item to clear from the queue, pKt updated to use mediaRef value
 */

- (void) downloadComplete: (NSDictionary *) queueItemId {
    //NSLog(@"queueItemId == %@", queueItemId);
    /*int size = [self downloadQueueSize];
    size--;
    [[JSLocalStorage sharedInstance] setItem: @"sync.itemsToDownload" value: [NSString stringWithFormat:@"%d", size]];
    */
    
    int orignal = [[queueItemId objectForKey:@"original"] intValue];
    
    if (orignal == 1) {
        NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef = '%@' and url = '%@'", downloadTableOriginal, [queueItemId objectForKey:@"mediaRef"], [queueItemId objectForKey:@"url"]];
        [[JSDBV2 sharedInstance] sqlExecWithSQLStatement: sql successCallback:^(BOOL resp) {
            [self updateDownloadCount];
        }];
    }else{
        NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE mediaRef = '%@' and url = '%@'", downloadTable, [queueItemId objectForKey:@"mediaRef"], [queueItemId objectForKey:@"url"]];
        [[JSDBV2 sharedInstance] sqlExecWithSQLStatement: sql successCallback:^(BOOL resp) {
            [self updateDownloadCount];
        }];
    }
}

/**
 * Ask for a notification of the top item on the download queue via the PhoneGap.exec()
 * mechanism. Will call the DownloadPlugin.queueData method with a JSON object indicating the URL from
 * which to download, and an identifier. The download queue should be notified when the
 * item is complete via the downloadComplete() method.
 *
 */

- (void)downloadQueueTop:(int)limit {

    __block NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ order by priority desc LIMIT %d", downloadTable, limit];
    //NSArray *rows = [[JSDBV2 sharedInstance] sql:sql params:nil];
    
    [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray *rows) {
        if (rows && [rows count] > 0) {
            [self.downloadPlugin queueData: rows];
            [self updateDownloadCount];
            [kAppDelegate.damViewController.homeView notificationTimerFired: nil];
        }else if ([[JSMain sharedInstance] isDownloadAllVideos]){
            sql = [NSString stringWithFormat:@"SELECT * FROM %@ where mediaType='Videos' order by priority desc LIMIT %d", downloadTableOriginal, limit];
            //rows = [[JSDBV2 sharedInstance] sql:sql params:nil];
            
            [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray * rows) {
                if (rows && [rows count] > 0) {
                    [self.downloadPlugin queueData: rows];
                }
                [self updateDownloadCount];
                [kAppDelegate.damViewController.homeView notificationTimerFired: nil];
            }];
        }else if ([[JSMain sharedInstance] isDownloadAllBrochures]){
            sql = [NSString stringWithFormat:@"SELECT * FROM %@ where mediaType='Brochures' order by priority desc LIMIT %d", downloadTableOriginal, limit];
            //rows = [[JSDBV2 sharedInstance] sql:sql params:nil];
            
            [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray * rows) {
                if (rows && [rows count] > 0) {
                    [self.downloadPlugin queueData: rows];
                }
                [self updateDownloadCount];
                [kAppDelegate.damViewController.homeView notificationTimerFired: nil];
            }];
            
        }else if ([[JSMain sharedInstance] isDownloadAllSamples]){
            sql = [NSString stringWithFormat:@"SELECT * FROM %@ where mediaType='Samples' order by priority desc LIMIT %d", downloadTableOriginal, limit];
            //rows = [[JSDBV2 sharedInstance] sql:sql params:nil];
            [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray * rows) {
                NSLog(@"JSSync.downloadTableOriginal Samples: rows count = %d", rows.count);
                if (rows && [rows count] > 0) {
                    [self.downloadPlugin queueData: rows];
                }
                
                [self updateDownloadCount];
                [kAppDelegate.damViewController.homeView notificationTimerFired: nil];
            }];
        }else {
            [self updateDownloadCount];
            [kAppDelegate.damViewController.homeView notificationTimerFired: nil];
        }
    }];
    
}

/*get the media file name with ext by mediaRef. we need to get file + ext in order to perform delete from cached folder*/
- (void)mediaCachedFileNameWithExtension:(NSString *)mediaRef callback:(void (^)(NSMutableArray *media))successCalback{
    NSString *sql = [NSString stringWithFormat:@"select * from %@ where mediaRef = '%@'", mediaTable, mediaRef];
    //NSArray *rows =[[JSDBV2 sharedInstance] sql: sql params:nil];
    [[JSDBV2 sharedInstance] executeQuery: sql params:nil successCallback:^(NSMutableArray *rows) {
        if (rows && [rows count] > 0) {
            NSMutableArray *result = [NSMutableArray array];
            NSDictionary *row = [rows objectAtIndex:0];
            NSString *thumbnail =  [row objectForKey:@"thumbnail"];
            NSString *url =  [row objectForKey:@"url"];
            
            
            [result addObject:[JSMain getRealMediaFileName: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], url]]];
            [result addObject:[JSMain getRealMediaFileName: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], thumbnail]]];
            
            /*
             [result addObject:[self.downloadPlugin getRealMediaFileName: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], url]]];
             [result addObject:[self.downloadPlugin getRealMediaFileName: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], thumbnail]]];
             */
            
            successCalback(result);
        }else{
            successCalback(nil);
        }
    }];
    
}

/*get current settings from app data*/
- (void) _getSettings: (void (^)(NSMutableArray *))callbackFunc{
    NSString *configs = [[JSLocalStorage sharedInstance] getItem: self.settingsKey];
    NSMutableArray *_settings = [configs objectFromJSONString];
    if (_settings == nil || [_settings count] == 0) {
        
#ifdef DEBUG
        NSLog(@"sync._getSettings.CREATE_DEFAULTS");
#endif
        
        //NSArray *rows = [[JSDBV2 sharedInstance] sql:[NSString stringWithFormat:@"SELECT * FROM containers"] params:nil];
        [[JSDBV2 sharedInstance] executeQuery:@"SELECT * FROM containers"  params:nil successCallback:^(NSMutableArray *rows) {
            if (rows && [rows count] > 0) {
                NSMutableArray *_settings = [NSMutableArray array];
                for (NSDictionary *row in rows) {
                    //{ "busArea":"domestic", "tag":"0", "name":"Marshalls" }
                    NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
                    [item setObject: [row objectForKey:@"businessArea"] forKey: @"busArea"];
                    [item setObject: [row objectForKey:@"tag"] forKey: @"tag"];
                    [item setObject: [row objectForKey:@"name"] forKey: @"name"];
                    [_settings addObject: item];
                    [item release];
                }
                NSString *jsonString = [_settings JSONString];
                
#ifdef DEBUG
                NSLog(@"Save settings string [%@]", jsonString);
#endif
                
                //Do something with jsonData
                [[JSLocalStorage sharedInstance] setItem: self.settingsKey value: jsonString];
                //_settings = [jsonString objectFromJSONString];
                callbackFunc(_settings);
            }else{
                callbackFunc(nil);
            }
        }];
    }else{
        callbackFunc(_settings);
    }
}


//we need to reset all key
- (void) resetAllStorageValues {
    //rest since last
    [[JSLocalStorage sharedInstance] setItem: self.lsDateKey value: NULL];
    [[JSLocalStorage sharedInstance] setItem: self.settingsKey value: NULL];
    [[JSLocalStorage sharedInstance] setItem: @"sync.itemsToDownload" value: NULL];
}

/**
 * Add new containers for synchronisations.
 *
 * @param containerNames
 */

- (void) addContainers:(NSString *) containerNames {
    
    [self resetAllStorageValues];
    
    //NSMutableArray *_settings = [self _getSettings];
    [self _getSettings:^(NSMutableArray *_settings) {
       
        NSMutableArray *_copySettings = [[NSMutableArray alloc] init];
        for (NSMutableDictionary *setting in _settings) {
            if ([containerNames rangeOfString: [setting objectForKey: @"name"]].location != NSNotFound) {
                NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary: setting];
                [temp setObject:@"true" forKey:@"on"];
                [temp setObject:@"true" forKey:@"reset"];
                [_copySettings addObject: temp];
            }else{
                [_copySettings addObject: setting];
            }
        }
        //NSLog(@"addContainers %@ == settings == %@", containerNames, [_copySettings JSONString]);
        
        [[JSLocalStorage sharedInstance] setItem: self.settingsKey value: [_copySettings JSONString]];
        
#ifdef DEBUG
        NSLog(@"current containers == %@", [self _getSettings]);
        
#endif
        
        
        [_copySettings release];
        
        [self run:^{
            
#ifdef DEBUG
            NSLog(@"Done addContainers");
#endif
            
            [self updateDownloadCount];
        }];
    }];
   

}

/*perform clear cached media for removed containers from Settings screen*/
- (void) clearCachedMediaForContainers:(NSArray *) containerNames {
    //NSArray *medias = [self mediaWithContainers: containerNames];
    //then we need to clean up cached media files at device
    [self mediaRemovedWhenRemovingContainers: containerNames callback:^(NSMutableArray *media) {
        for (NSString *singMedia in media) {
            //[self.downloadPlugin deleteMedia: [self mediaCachedFileNameWithExtension: singMedia]];
            //NSArray * temp = [self mediaCachedFileNameWithExtension: singMedia];
            [self mediaCachedFileNameWithExtension: singMedia callback:^(NSMutableArray *temp) {
                if (temp && [temp count] > 0) {
                    for (NSString *mr in temp) {
                        [self.downloadPlugin deleteMedia:mr];
                    }
                }
            }];
        }
    }];
}

/**
 * Stop synchronising media in the supplied containers (specialities in CMS speak) and delete
 * any assets that had only belonged in that container... ???
 *
 * @param settingNames
 */
- (void)clearContainers:(NSArray *)containerNames listOnContainers:(NSArray *) onLists{
    
    NSLog(@"sync.clearContainers %@", containerNames);
    
    //self.settings = [self _getSettings];
    [self _getSettings:^(NSMutableArray *data) {
        self.settings = data;
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        NSString *_containerNames = [containerNames componentsJoinedByString:@","];
        for (NSDictionary *setting in self.settings) {
            if ([_containerNames rangeOfString: [setting objectForKey: @"name"]].location != NSNotFound) {
                NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:setting];
                [tempDic setObject:@"false" forKey:@"on"];
                [temp addObject: tempDic];
                [tempDic release];
            }else {
                [temp addObject: setting];
            }
        }
        
        [[JSLocalStorage sharedInstance] setItem : self.settingsKey value:[temp JSONString]];
        self.settings = temp;
        [temp release];
        //TODO
        [[JSiDrive sharedInstance] removeFilesAffectedByContainerRemoval: onLists];
        
        [self clearCachedMediaForContainers: onLists];
        //stop the downloading queue
        [self _removeFilesAffectedByContainerRemoval: onLists callback:^{
            [self availableMediaTypes];
            
            NSLog(@"sync.clearContainers.DONE_DELETE");
            
            [self updateDownloadCount];
        }];
    }];
    
}


-(void) mediaRemovedWhenRemovingContainers: (NSArray *)containers callback:(void (^)(NSMutableArray *media))successCalback{
    /*sync.mediaWithContainers(containers, function(media){
        result = Object.keys(media);
        console.log("sync.mediaRemovedWhenRemovingContainers result:" + JSON.stringify(result));
        callback && callback(result);
    });*/
    [self mediasRemovingExcludeFromKeepingContainers: containers callback:^(NSMutableArray *result) {

#ifdef DEBUG
        NSLog(@"sync.mediasRemovingExcludeFromKeepingContainers result: %d", [result count]);
#endif
        if (successCalback) {
            successCalback (result);
        }
    }];
    /*[self _removeMedia:[self mediaWithContainers:containers]];
    if (callback) callback();
    */ 
}


//- (NSArray*) mediaWithContainers: (NSArray*) containers
/*try to use block function here*/
- (void) mediasRemovingExcludeFromKeepingContainers: (NSArray*) oncontainers callback:(void (^)(NSMutableArray *media))successCalback{
    
#ifdef DEBUG
    NSLog(@"sync.mediasRemovingExcludeFromKeepingContainers containers: %@", [oncontainers JSONString]);
#endif
    
    NSString *containerPlaceholders = [JSDBV2 placeholdersForArray: oncontainers];
    
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT mediaRef FROM media_container WHERE mediaRef not in (SELECT distinct mediaRef FROM media_container WHERE container in ( %@ ))", containerPlaceholders];
    
#ifdef DEBUG
    NSLog(@"sync.mediasRemovingExcludeFromKeepingContainers sql: %@", sql);
#endif
    
    //NSArray *rows = [[JSDBV2 sharedInstance] sql:sql params: oncontainers];
    [[JSDBV2 sharedInstance] executeQuery: sql params:oncontainers successCallback:^(NSMutableArray *rows) {
        if (rows && [rows count] > 0) {
            NSMutableArray *result = [NSMutableArray array];
            for (NSDictionary * row in rows) {
                NSString *mediaRef = [row objectForKey:@"mediaRef"];
                [result addObject: mediaRef];
            }
            if (successCalback) {
                successCalback(result);
            }
            //return result;
        }
    }];
    
}

- (void) mediaWithContainers: (NSArray*) containers callback:(void (^)(NSMutableArray *media))successCalback{
    
#ifdef DEBUG
    NSLog(@"sync.mediaWithContainers containers: %@", [containers JSONString]);
#endif
    
    NSString *containerPlaceholders = [JSDBV2 placeholdersForArray:containers];
    
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT mediaRef FROM %@ WHERE mediaRef in (SELECT distinct mediaRef FROM %@ WHERE container in ( %@ ))", containerTable, containerTable, containerPlaceholders];
    
    
#ifdef DEBUG
    NSLog(@"sync.mediaWithContainers sql: %@", sql);
#endif
    
    //NSArray *rows = [[JSDBV2 sharedInstance] sql:sql params: containers ];
    [[JSDBV2 sharedInstance] executeQuery:sql params:containers successCallback:^(NSMutableArray *rows) {
        if (rows && [rows count] > 0) {
            NSMutableArray *result = [NSMutableArray array];
            for (NSDictionary * row in rows) {
                NSString *mediaRef = [row objectForKey:@"mediaRef"];
                [result addObject: mediaRef];
            }
            if (successCalback) {
                successCalback(result);
            }
            //return result;
        }
    }];
    
   // return nil;
}

-(void)_removeFilesAffectedByContainerRemoval:(NSArray *)containers callback:(void (^)(void))succCallback {
    
#ifdef DEBUG
    NSLog(@"sync._removeFilesAffectedByContainerRemoval containers: %@" ,containers);
#endif
    
    [self mediaRemovedWhenRemovingContainers: containers callback:^(NSMutableArray *media) {
        [self _removeMedia: media callback:succCallback];
    }];
    //[self mediaRemovedWhenRemovingContainers: containers callback: callback];
    /*sync.mediaRemovedWhenRemovingContainers(containers, function(media) {
        sync._removeMedia(media, callback);
    });*/
}


- (void)getContainerConfig:(void (^)(NSString *))callbackFunc{
    [self _getSettings:^(NSMutableArray *data) {
        callbackFunc([data JSONString]);
    }];
    //return [[self _getSettings] JSONString];
}

- (void)getActiveContainers:(NSString *)businessAreas callback:(void (^)(NSString *))callbackFunc {
    //simple get on = 1 from DB
    //NSArray * temp =  [[JSDBV2 sharedInstance] sql:[NSString stringWithFormat:@"SELECT * FROM containers where state=1"] params:nil ];
    [[JSDBV2 sharedInstance] executeQuery:@"SELECT * FROM containers where state=1" params:nil successCallback:^(NSMutableArray *temp) {
        NSMutableArray *containers = [[NSMutableArray alloc] init];
        for (NSDictionary * setting in temp) {
            if ([businessAreas rangeOfString: [setting objectForKey: @"businessArea"] options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [containers addObject: [setting objectForKey:@"name"]];
            }
        }
        NSString *jsonResult = [containers JSONString];
        callbackFunc(jsonResult);
    }];
}

- (void) getActiveBusinessAreas:(void (^)(NSString *))callbackFunc{
    
    __block BOOL domestic = false;
    __block BOOL commercial = false;
    //NSArray * _settings = [self _getSettings];
    
    NSArray *filterAreas = [[JSMain sharedInstance] getBusinessAreaFilters];
    if (filterAreas != NULL && [filterAreas count] > 0) {
        for (NSString *cname in filterAreas) {
            //NSArray *rows = [[JSDBV2 sharedInstance] sql:[NSString stringWithFormat:@"select * from containers where name= '%@'", cname] params:nil];
            [[JSDBV2 sharedInstance] executeQuery:[NSString stringWithFormat:@"select * from containers where name= '%@'", cname] params:nil successCallback:^(NSMutableArray *rows) {
                if (rows && rows.count > 0) {
                    NSDictionary *setting = [rows objectAtIndex:0];
                    if ([[[setting objectForKey:@"businessArea"] uppercaseString] isEqualToString: [@"commercial" uppercaseString]]) {
                        commercial = true;
                    } else {
                        domestic = true;
                    }
                }
                NSMutableArray * result = [[NSMutableArray alloc] init];
                
                if (domestic) [result addObject:@"domestic"];
                if (commercial) [result addObject: @"commercial"];
                NSString *jsonResult = [result JSONString];
                [result release];
                
                callbackFunc(jsonResult);
           }];
        }
    }else {
        //NSArray *containers =  [[JSDBV2 sharedInstance] sql:[NSString stringWithFormat:@"SELECT * FROM containers where state=1"] params:nil];
        [[JSDBV2 sharedInstance] executeQuery:@"SELECT * FROM containers where state=1" params:nil successCallback:^(NSMutableArray *containers) {
            for (NSDictionary * cname in containers) {
                if ([[[cname objectForKey:@"businessArea"] uppercaseString] isEqualToString: [@"commercial" uppercaseString]]) {
                    commercial = true;
                } else {
                    domestic = true;
                }
            }
            
            NSMutableArray * result = [[NSMutableArray alloc] init];
            
            if (domestic) [result addObject:@"domestic"];
            if (commercial) [result addObject: @"commercial"];
            NSString *jsonResult = [result JSONString];
            [result release];
            
            callbackFunc(jsonResult);
        }];
    }
    
    /*var result = [];
    var domestic = false;
    var commercial = false;
    var settings = sync._getSettings();
    $(settings).each(function(index, setting){
        if (setting.on) {
            if (setting.busArea == "commercial") {
                commercial = true;
            } else {
                domestic = true;
            }
        }
    });
    if (domestic) result.push("domestic");
    if (commercial) result.push("commercial");
    
    return JSON.stringify(result);*/
}

-(void) availableMediaTypes{
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT mediaType FROM %@", self.mediaTable];
    //NSMutableArray *rs = [[JSDBV2 sharedInstance] sql:sql params:nil];
    [[JSDBV2 sharedInstance] executeQuery:sql params:nil successCallback:^(NSMutableArray * rs) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        if (rs && [rs count] > 0) {
            for (NSDictionary *row in rs) {
                [result addObject: [row objectForKey:@"mediaType"]];
            }
            //main.pgFire("DAMHomeView", "setupMediaTypeCarousel", result);
        }
        
        NSLog(@"sync.availableMediaTypes result: %@", result);
    
        //TODO
        [kAppDelegate.damViewController.homeView setupMediaTypeCarousel: result];
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(setupMediaTypeCarousel:) withData: result];
        [result release];
    }];
    
    /*db.sql("SELECT DISTINCT mediaType FROM " + sync.mediaTable, [], function(rows){
        result = [];
        for (i = 0; i < rows.length; i++) {
            row = rows.item(i);
            result.push(row.mediaType);
        }
        console.log("sync.availableMediaTypes result:" + result);
        main.pgFire("DAMHomeView", "setupMediaTypeCarousel", result);
    });*/
}

- (NSArray *) addMediaObjectToMediaTagsBatch: (NSDictionary *)object {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray *tags = [object objectForKey:@"tags"];
    for (NSString *tag in tags) {
        NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
        [singleBatch setObject:[NSString stringWithFormat:@"INSERT INTO %@ (mediaRef, tag) VALUES (?,?)", tagTable] forKey:@"sql"];
        NSMutableArray *params = [[NSMutableArray alloc] init];
        [params addObject:[object objectForKey:@"mediaRef"]];
        [params addObject: tag];
        [singleBatch setObject: params forKey:@"params"];
        [params release];
        //[singleBatch setObject:[NSString stringWithFormat:@"[%@, %@]", [object objectForKey:@"mediaRef"], tag] forKey:@"params"];
        [result addObject: singleBatch];
        [singleBatch release];
    }
    
    return result;
}

- (NSArray *) addMediaObjectToMediaContainerBatch: (NSDictionary *)object {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray *containers = [[NSArray alloc] initWithArray: [[object objectForKey:@"container"] componentsSeparatedByString:@","]];
    for (NSString *container in containers) {
        NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
        [singleBatch setObject:[NSString stringWithFormat:@"INSERT INTO %@ (mediaRef, container) VALUES (?,?)", containerTable] forKey:@"sql"];
        NSMutableArray *params = [[NSMutableArray alloc] init];
        [params addObject: [object objectForKey:@"mediaRef"]];
        [params addObject: container];
        [singleBatch setObject: params forKey:@"params"];
        [params release];
        //[singleBatch setObject:[NSString stringWithFormat:@"[%@, %@]", [object objectForKey:@"mediaRef"], container] forKey:@"params"];
        [result addObject: singleBatch];
        [singleBatch release];
    }
    [containers release];
    
    return result;
}

- (NSArray *) addMediaObjectToBatch: (NSDictionary *)object {
    //NSLog(@"object %@", object);
    NSString *mt = [self mediaTypeId : [object objectForKey:@"mediaType"]];
    NSString* meta = [JSMain stringify:object];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSMutableDictionary *singleBatch = [[NSMutableDictionary alloc] init];
    //[singleBatch setObject: [NSString stringWithFormat:@"INSERT INTO %@ (mediaRef, mt, mediaType, title, url, thumbnail, thumbWidth, thumbHeight,location,priority, updatedDate, meta) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", mediaTable] forKey:@"sql"];
    [singleBatch setObject: [NSString stringWithFormat:@"INSERT INTO %@ (mediaRef, mt, mediaType, title, url, thumbnail, thumbWidth, thumbHeight,priority, updatedDate, meta,hasCrop,cropRect,location,tags,products,width,height,mediaDesc,latitude,longitude) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", mediaTable] forKey:@"sql"];
    
    NSString *hasCrop = @"0";
    NSString *cropRect = @"";
    NSMutableArray *params = [[NSMutableArray alloc] init];
    
    [params addObject: [object nonNullValueForKey:@"mediaRef"]];
    [params addObject: mt];
    [params addObject: [object nonNullValueForKey:@"mediaType"]];
    [params addObject: [object nonNullValueForKey:@"title"]];
    /*
    if ([[object objectForKey:@"mediaType"] isEqualToString:kMediaType_Samples]) {
        //if samples we will add UseOriginal=true
        NSString *url = [object objectForKey: @"url"] == NULL ? @"" : [object objectForKey: @"url"];
        if ([url length] > 0) {
            if ([[url componentsSeparatedByString:@"?"] count] > 1) {
                //has ? then we just add &
                url = [url stringByAppendingString:@"&UseOriginal=true"];
            }else{
                url = [url stringByAppendingString:@"?UseOriginal=true"];
            }
        }
        [params addObject: url];
    }else{
     
    }*/
    [params addObject: [object nonNullValueForKey: @"url"]];
    [params addObject: [object nonNullValueForKey:@"thumbnail"]];
    [params addObject: [object nonNullValueForKey:@"thumbWidth"]];
    [params addObject: [object nonNullValueForKey:@"thumbHeight"]];
    
        
    //[params addObject: [NSString stringWithFormat:@"%@,%@",[object objectForKey:@"latitude"],[object objectForKey:@"longitude"]]];
    if ([object objectForKey:@"Priority"]) {
        [params addObject: [NSString stringWithFormat:@"%d", [[object objectForKey:@"Priority"] intValue]]];
    }else{
        [params addObject: @"0"];
    }
    [params addObject: [object nonNullValueForKey:@"UpdatedDate"]];
    [params addObject: meta];
    
    
    //add cropping processing for Samples
    if ([[object nonNullValueForKey:@"mediaType"] isEqualToString:kMediaType_Samples]) {
        //processing
        if ([[object objectForKey:@"HasCrop"] boolValue]) {
            hasCrop = @"1";
            //getting crop rectangle in format X1,Y1,X2,Y2
            cropRect = [NSString stringWithFormat:@"%d,%d,%d,%d", [[object objectForKey:@"CropX1"] intValue], [[object objectForKey:@"CropY1"] intValue], [[object objectForKey:@"CropX2"] intValue], [[object objectForKey:@"CropY2"] intValue]];
        }
    }
    [params addObject: hasCrop];
    [params addObject: cropRect];
    
    //location
    [params addObject: [object nonNullValueForKey:@"location"]];
    /*if ([object nonNullObjectForKey:@"latitude"] && [object nonNullObjectForKey:@"longitude"]) {
        [params addObject: [NSString stringWithFormat:@"%f,%f", [[object nonNullObjectForKey:@"latitude"] floatValue] , [[object nonNullObjectForKey:@"longitude"] floatValue]]];
    }else{
        [params addObject: @""];
    }*/
    NSArray *tags = [object objectForKey:@"tags"];
    if (tags == NULL || [tags count] == 0) {
        [params addObject: @""];
    }else{
        [params addObject: [tags componentsJoinedByString:@","]];
    }
    //products
    if ([object objectForKey:@"Products"]) {
        NSArray *Products = [object objectForKey:@"Products"];
        NSString *strProducts = @"";
        for (NSDictionary *product in Products) {
            strProducts = [strProducts stringByAppendingString: [product objectForKey:@"Name"]];
            strProducts = [strProducts stringByAppendingString: @", "];
        }
        [params addObject: strProducts];
    }else{
        [params addObject: @""];
    }
    
    [params addObject: [object objectForKey:@"width"] == NULL ? @"" : [NSString stringWithFormat:@"%f", [[object objectForKey:@"width"] floatValue]]];
    [params addObject: [object objectForKey:@"height"] == NULL ? @"" : [NSString stringWithFormat:@"%f", [[object objectForKey:@"height"] floatValue]]];
    //adding desc
    [params addObject: [object nonNullValueForKey:@"description"]];
    
    //lat and long
    if ([object nonNullObjectForKey:@"latitude"]) {
        [params addObject: [NSString stringWithFormat:@"%f", [[object nonNullObjectForKey:@"latitude"] floatValue]]];
    }else{
        [params addObject: @""];
    }
    
    if ([object nonNullObjectForKey:@"longitude"]) {
        [params addObject: [NSString stringWithFormat:@"%f", [[object nonNullObjectForKey:@"longitude"] floatValue]]];
    }else{
        [params addObject: @""];
    }
    
    [singleBatch setObject:params forKey:@"params"];
    
    [params release];
    
    [result addObject: singleBatch];
    
    [singleBatch release];
    
    /*batch.push({
    sql: "INSERT INTO " + sync.mediaTable + " (mediaRef, mt, mediaType, title, url, thumbnail, thumbWidth, thumbHeight, meta) VALUES (?,?,?,?,?,?,?,?,?)",
    params: [object.mediaRef, mt, object.mediaType, object.title, object.url, object.thumbnail, object.thumbWidth, object.thumbHeight, meta]
    });
    */
    
    //containers = object.container.split(",");
    
    /*
    $(containers).each(function(index, container) {
        batch.push({
        sql: "INSERT INTO " + sync.containerTable + " (mediaRef, container) VALUES (?,?)",
        params: [object.mediaRef, container]
        });
    });*/
    return result;
    /*
    $(object.tags).each(function(index, tag) {
        batch.push({
        sql: "INSERT INTO " + sync.tagTable + " (mediaRef, tag) VALUES (?,?)",
        params: [object.mediaRef, tag]
        });
    });
    */ 
    //        console.log("sync.addMediaObjectToBatch DONE");
}

-(NSDictionary *) addToQueueBatch:(NSString *) mediaRef mediaType:(NSString *) mt url: (NSString *) _url isVideos:(BOOL) videos{
    //console.log("sync.addToQueue mediaRef:" + mediaRef + "mediaType:" + mediaType + ", url:" + url);
    
#ifdef DEBUG
    //NSLog(@"sync.addToQueue mediaRef: %@ ; mediaType: [%@] ; url: %@",mediaRef, mt, _url);
#endif
    
    NSMutableDictionary *singleBatch = [NSMutableDictionary dictionary];
    
    if (videos) {
        [singleBatch setObject:[NSString stringWithFormat:@"INSERT INTO download_queue_original (mediaRef, mediaType, url, priority,original) VALUES (?,?,?,0,1)"] forKey:@"sql"];
    }else{
        [singleBatch setObject:[NSString stringWithFormat:@"INSERT INTO download_queue (mediaRef, mediaType, url, priority,original) VALUES (?,?,?,1,0)"] forKey:@"sql"];
    }
    
    NSMutableArray *params = [[NSMutableArray alloc] init];
    [params addObject: mediaRef];
    [params addObject: mt];
    [params addObject: [NSString stringWithFormat:@"%@%@", [JSMain apiHost], _url]];
    //[params addObject: videos ? @"0" : @"1"];
    //[params addObject: videos ? @"1" : @"0"];//if videos or samples or brochures, then this is original url of media
    [singleBatch setObject:params forKey:@"params"];
    [params release];
    return singleBatch;
    /*batch.push({
    sql: "INSERT INTO " + sync.downloadTable + " (mediaRef, media_type, url) VALUES (?,?,?)",
    params: [mediaRef, mediaType, main.apiHost + url]
    });
    */
}

- (int)downloadQueueSize{
    NSString *size = [[JSLocalStorage sharedInstance] getItem : @"sync.itemsToDownload"];
    return size ? [size intValue] : 0;
}

-(NSString *) mediaTypeId: (NSString *)mediaType {
    //TODO - complete this mapping. Time v. tight atm.
    if ([mediaType isEqualToString: kMediaType_Photos] || [mediaType isEqualToString: kMediaType_SalesPhotos])
        return @"1";
    return @"0";
}

-(NSDictionary *) allocateThumbnailIfPhoto:(NSDictionary *)asset {
    if ([[asset objectForKey: @"mediaType"] isEqualToString: kMediaType_Photos] || [[asset objectForKey: @"mediaType"] isEqualToString: kMediaType_SalesPhotos])  {
        NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithDictionary: asset];
        float aspect = 1.0f;
        if ([asset objectForKey: @"width"] != nil) {
            aspect =  [[asset objectForKey: @"width"] floatValue] / [[asset objectForKey: @"height"] floatValue];
            //aspect = [[asset objectForKey: @"aspectRatio"] floatValue];
        }
        
        //var bucketIndex = parseInt(asset.mediaRef[0], 16) >> 2;
        int bucketIndex = 0;
        unsigned hexDigit;
        NSString *firstChar = [[asset objectForKey:@"mediaRef"] substringToIndex:1];
        NSScanner* scanner = [NSScanner scannerWithString: firstChar];
        [scanner scanHexInt:&hexDigit];
        //NSLog(@"mediaRef == %@, firstChar == %@ hexDigit %d",[asset objectForKey:@"mediaRef"],firstChar, hexDigit);
        //thumbHeightBuckets: [181.0, 243.0, 368.0, 493.0],
        
        if (hexDigit < 1) {
            bucketIndex = 0;
        }else if (hexDigit < 7) {
            bucketIndex = 1;
        }else if (hexDigit < 13) {
            bucketIndex = 2;
        }else {
            bucketIndex = 3;
        }
        
        int tHeight = [[self.thumbHeightBuckets objectAtIndex:bucketIndex] intValue];
        //if tHeight is not >= height of image then we need to pick up smaller height
        while (tHeight > [[asset objectForKey: @"height"] floatValue] && bucketIndex > 0) {
            bucketIndex = bucketIndex  - 1;
            tHeight = [[self.thumbHeightBuckets objectAtIndex:bucketIndex] intValue];
        }
        
        //NSLog(@"mediaRef %@ bucketIndex: %d - thumbHeightBuckets count %d",[asset objectForKey:@"mediaRef"], bucketIndex, [self.thumbHeightBuckets count]);
        
        //float downloadHeight = [JSMain isIPad3] ? tHeight * 2 : tHeight;
        float tWidth = aspect * tHeight;
        [result setObject:[NSString stringWithFormat:@"%f", tWidth] forKey:@"thumbWidth"];
        [result setObject:[NSString stringWithFormat:@"%d", tHeight] forKey:@"thumbHeight"];
        [result setObject:[NSString stringWithFormat:@"%@?MaxHeight=%d",[asset objectForKey:@"url"], tHeight] forKey:@"thumbnail"];
        //asset.thumbnail = asset.url + "?MaxHeight=" + tHeight;
        //console.log("sync.allocateThumbnailIfPhoto " + JSON.stringify(asset));
        //console.log("sync.allocateThumbnailIfPhoto w=" + asset.width + " tw = " + tWidth + ", h=" + asset.height + " th = " + tHeight + ", tu = " + asset.thumbnail);
        return result;
    }else if ([[asset objectForKey: @"mediaType"] isEqualToString: kMediaType_Samples])  {
        NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithDictionary: asset];
        [result setObject:[NSString stringWithFormat:@"%@?MaxHeight=%d",[asset objectForKey:@"url"], 338] forKey:@"thumbnail"];
        return result;
    }
    //we just return the original one
    return asset;
}
@end
