//
//  JSDBV2.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 8/11/13.
//
//

#import "JSDBV3.h"
#import "NSDictionary_JSONExtensions.h"

@implementation JSDBV3

static JSDBV3 *sharedInstance = nil;

-(bool) initialiseDatabase{
    
    if ([self createTables]){
        return true;
    }
    return false;
    /*if ([self openDB]){
        if ([self createTables]){
            return true;
        }
    }
    return false;
    */
}

+ (JSDBV3 *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}



- (id)init{
    self = [super init];
    if (self) {
        // Work your initialising magic here as you normally would
        
    }
    return self;
}

#pragma Basic function

-(bool)createTables{
    NSMutableArray *fields = [[NSMutableArray alloc]init];
    
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"mediaType TEXT"];
    [fields addObject:@"priority int"];
    [fields addObject:@"url TEXT"];
    [fields addObject:@"original int"];/*added original field to indicate if this is original source or not*/
    if (![self createTable:@"download_queue" withFields:fields]) {
        [fields release];
        return false;
    }
    
    [fields removeAllObjects];
    
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"mediaType TEXT"];
    [fields addObject:@"priority int"];
    [fields addObject:@"url TEXT"];
    [fields addObject:@"original int"];/*added original field to indicate if this is original source or not*/
    if (![self createTable:@"download_queue_original" withFields:fields]) {
        [fields release];
        return false;
    }
    
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"mediaType TEXT"];
    [fields addObject:@"mt INTEGER"];
    [fields addObject:@"title TEXT"];
    [fields addObject:@"url TEXT"];
    [fields addObject:@"thumbnail TEXT"];
    [fields addObject:@"thumbWidth TEXT"];
    [fields addObject:@"thumbHeight TEXT"];
    [fields addObject:@"width TEXT"];
    [fields addObject:@"height TEXT"];
    [fields addObject:@"location TEXT"];
    [fields addObject:@"tags TEXT"];
    [fields addObject:@"products TEXT"];
    [fields addObject:@"priority int"];
    [fields addObject:@"updatedDate date"];
    [fields addObject:@"hasCrop INTEGER"];
    [fields addObject:@"cropRect TEXT"];//in format X1,Y1,X2,Y2
    [fields addObject:@"meta TEXT"];
    if (![self createTable:@"media" withFields:fields]) {
        [fields release];
        return false;
    }
    
    if ([self sqlExecWithSQLStatement:@"CREATE INDEX idx_mt ON media(mt)"]) {
#ifdef DEBUG
        NSLog(@"sync.createSchema.CREATE_INDEX.idx_mt");
#endif
    }
    if ([self sqlExecWithSQLStatement:@"CREATE INDEX idx_mediaRef ON media(mediaRef)"]){
#ifdef DEBUG
        NSLog(@"sync.createSchema.CREATE_INDEX.idx_mediaRef");
#endif
    }
    
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"container TEXT"];
    if (![self createTable:@"media_container" withFields:fields]) {
        [fields release];
        return false;
    }
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    [fields addObject:@"tag TEXT"];
    if (![self createTable:@"media_tags" withFields:fields]) {
        [fields release];
        return false;
    }
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"mediaRef TEXT"];
    if (![self createTable:@"media_deleted" withFields:fields]) {
        [fields release];
        return false;
    }
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"businessArea TEXT"];
    [fields addObject:@"tag INTEGER"];
    [fields addObject:@"name TEXT"];
    [fields addObject:@"state INTEGER"];
    if (![self createTable:@"containers" withFields:fields]) {
        [fields release];
        return false;
    }
    
    [fields removeAllObjects];
    [fields addObject:@"id INTEGER PRIMARY KEY"];
    [fields addObject:@"sender TEXT"];
    [fields addObject:@"recipient TEXT"];
    [fields addObject:@"subject TEXT"];
    [fields addObject:@"body TEXT"];
    [fields addObject:@"assets TEXT"];
    if (![self createTable:@"email" withFields:fields]) {
        [fields release];
        return false;
    }
    [fields release];
    
    return true;
}

-(bool)createTable:(NSString *)tableName withFields:(NSMutableArray *)fields{
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        NSString *fieldsStr = [fields componentsJoinedByString:@","];
        NSString *sqlStr = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", tableName];
        
        [bdb executeUpdate: sqlStr withParameterDictionary: nil];
        
        //sqlite3_exec(db, [sqlStr UTF8String], NULL, NULL, NULL);
        
        sqlStr = [NSString stringWithFormat:@"CREATE TABLE %@ (%@)", tableName, fieldsStr];
        NSLog(@"JSDB createTable sql:%@", sqlStr);
        
        [bdb executeUpdate: sqlStr withParameterDictionary: nil];
    }];
    return true;
}

- (NSString *) databasePath
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory=[paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"dam.sql"];
}

-(bool)openDB
{
    return true;
}
/*
-(bool)openDB
{
    db = [FMDatabase databaseWithPath: [self databasePath]];
    if (![db open]) {
        [db release];
        return false;
    }
    return true;
}

-(void)closeDB{
    [db close];
}*/


/*
 ["feet", "geese", "meese", "kangareese"] ==> ?,?,?,?
 try this on JS console: alert(["a","b","c"].map(function(e){ return "?" }).join(",")); ==> ?,?,?*/
+ (NSString *) placeholdersForArray: (NSArray *) array {
    NSString *result = @"";
    if (array == NULL || [array count] == 0) {
        return @"";
    }
    for (id temp in array) {
        result = [result stringByAppendingString:@"?,"];
    }
    return [result substringToIndex:[result length] - 1];
    //return array.map(function(e){ return "?" }).join(",");
}

-(void)closeDB {
    
}

#pragma Functionality

- (void) map: (NSString *)sqlStatement params: (NSArray *) parameters success:(void (^)(NSMutableArray *))callbackFunc {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        FMResultSet *rs = [bdb executeQuery: sqlStatement withArgumentsInArray: parameters];
        
        NSMutableArray *result = [[NSMutableArray alloc] init];
        //NSMutableArray *result = [NSMutableArray array];
        while ([rs next]) {
            
            NSDictionary *row =  [NSDictionary dictionaryWithDictionary:[rs resultDictionary]];
            
            if ([row objectForKey:@"meta"] != nil) {
                //clone, so we can alter object (original is immutable)
                //                    var clone = jQuery.extend(true, {}, row);
                NSMutableDictionary * clone = [NSMutableDictionary dictionaryWithDictionary: [NSDictionary dictionaryWithJSONString:[row objectForKey:@"meta"] error:nil]];
                
                /*NSDictionary * meta = [NSDictionary dictionaryWithJSONString:[row objectForKey:@"meta"] error:nil];
                 
                 //                    console.log("META: " + JSON.stringify(row.meta));
                 for (NSString *key in meta.keyEnumerator) {
                 [clone setObject:[meta objectForKey:key] forKey:key];
                 }
                 */
                [result addObject: clone];
            } else {
                [result addObject: row];
            }
            //[result addObject: [rs resultDictionary]];
        }
        //NSLog(@"sqlStatement %@ rs == %@",sqlStatement, result);
        NSMutableArray *data = [NSMutableArray arrayWithArray: result];
        [result release];
        if (callbackFunc)callbackFunc(data);
    }];
}

- (void) sql: (NSString *)sqlStatement params: (NSArray *) parameters success:(void (^)(NSMutableArray *))callbackFunc {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        FMResultSet *rs = [bdb executeQuery: sqlStatement withArgumentsInArray: parameters];
        
        NSMutableArray *result = [[NSMutableArray alloc] init];
        while ([rs next]) {
            [result addObject: [NSDictionary dictionaryWithDictionary:[rs resultDictionary]]];
        }
        //NSLog(@"sqlStatement %@ rs == %@",sqlStatement, result);
        NSMutableArray *data = [NSMutableArray arrayWithArray: result];
        [result release];
        //[rs close];
        if (callbackFunc)callbackFunc(data);
    }];
}

/*batchInsert: function(batch, success, fail) {*/
- (void) batchInsert: (NSArray *) batches {
    
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        for (NSDictionary * batch in batches) {
            NSArray *params = [batch objectForKey:@"params"];
            NSString *sql = [batch objectForKey: @"sql"];
            [bdb executeUpdate: sql withArgumentsInArray: params];
        }
    }];
}

- (bool)sqlExec:(NSDictionary *)batch {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        NSArray *params = [batch objectForKey:@"params"];
		NSString *sql = [batch objectForKey: @"sql"];
        [bdb executeUpdate: sql withArgumentsInArray: params];
    }];
    return true;
}

- (bool)sqlExecWithSQLStatement:(NSString *)sql {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        [bdb executeUpdate: sql];
    }];
    return true;
}

- (bool)sqlExecWithSQLStatement:(NSString *)sql params: (NSArray *) parameters{
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath: [self databasePath]];
    [queue inDatabase:^(FMDatabase *bdb) {
        [bdb executeUpdate: sql withArgumentsInArray:parameters];
    }];
    return true;
}

@end
