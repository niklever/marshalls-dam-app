//
//  JSPath.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSPath : NSObject {
    int _len;    
}

@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSMutableArray *_bits;

- (id)initWithPath:(NSString *)initialPath;
-(NSMutableArray *) bits;
- (int) length;
- (NSString *)parent;
- (NSString *)top;
-(void)navDown:(NSString *)dir;
@end
