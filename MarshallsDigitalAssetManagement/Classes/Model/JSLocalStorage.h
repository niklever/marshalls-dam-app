//
//  JSLocalStorage.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSLocalStorage : NSObject

+ (id)sharedInstance;

- (void) setItem:(NSString *)key value:(NSString *)val;
- (NSString *) getItem:(NSString *) key;
- (void)resetDefaults;
@end
