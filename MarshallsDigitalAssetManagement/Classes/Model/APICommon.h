//
//  APICommon.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 19/8/14.
//
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFURLRequestSerialization.h"

@interface APICommon : NSObject

+ (APICommon *)sharedInstance;

#pragma mark - Get containerList
- (void)getContainerList:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

#pragma mark - User Settings
//NSString *url = [NSString stringWithFormat:@"%@/webapi/UserSettings/%@",[JSMain apiHost], userEmail];
- (void)getUserSettings: (NSString *)userEmail successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

#pragma mark - Sync Assets
//NSString *endpoint = [NSString stringWithFormat:@"%@/api/sync?since=%@&containers=%@", [JSMain apiHost], when, containerName];
- (void)syncAssetsFrom: (NSString *)when containerName : (NSString *)cname successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

#pragma mark - ShareViaEmail
- (void)shareViaEmail:(NSDictionary *)params successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

#pragma mark - iDriver Push
- (void)iDrivePushMethod:(NSDictionary *)params successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

#pragma mark - iDriver Pull
- (void)iDrivePullMethod:(NSDictionary *)params successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

#pragma mark - Renew Token
- (void)doRenewAuthToken:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

#pragma mark - Post Media
/**
 *  Post Media
 *
 *  @param params          list of params details
 Name: The name of the media item being uploaded. string .  Required
 Location: The location the media item relates to. string . None.
 Description: The description of the media item. string . Max length: 140
 Specialities: A comma delimited list of Specialities with which to associate the media item. These Specialities are validated against the list stored on the server and checked for validity. string . None.
 Products: A comma delimited list of products with which to associate the media item. This uses the "Tagging" feature of the DAM library and the supplied list of items do not have to correspond to real products. string . None.
 Latitude: The geographical latitude co-ordinate the media item relates to. decimal number . None.
 Longitude: The geographical longitude co-ordinate the media item relates to. decimal number . None.
 AppUserEmail: The email address of the user who is uploading the media item from the client application. This email address is checked for validity on the server. string . Required

 *  @param data            nsdata of media
 *  @param successCallback <#successCallback description#>
 *  @param failedCallback  <#failedCallback description#>
 */
- (void)doPostMedia: (NSDictionary *) params mediaData: (NSData *)data successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback;

@end
