//
//  DownloadPlugin.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 05/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ASIHTTPRequest.h"
#import "DAMJSBridge.h"
#import "UIImageView+WebCache.h"


@interface DownloadPlugin : NSObject< /*ASIHTTPRequestDelegate,*/ SDWebImageManagerDelegate> {
    //id<DAMJSBridge> jsBridge;
    int itemFinished; // total item finished.
    int currentBatch;
}
@property (nonatomic, assign) id<DAMJSBridge> jsBridge;

- (void)kickQueue;
- (void)abortQueue: (NSArray *)mediaRefs callback:(void (^)(void))succCallback;
- (void)queueData:(NSArray *)data;
- (void)deleteMedia:(NSString *)mediaRef;
//- (NSString *) getRealMediaFileName: (NSString *)url;
@end
