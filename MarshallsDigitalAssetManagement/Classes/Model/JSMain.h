//
//  JSMain.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "SBJsonWriter.h"
#import "DAMAppDelegate.h"
#import "JSLocalStorage.h"
#import "JSiDrive.h"

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

@interface JSMain : NSObject {
    
}

@property (nonatomic, assign) BOOL opsSuspended;
@property (nonatomic, assign) BOOL networkReachable;
@property (nonatomic, assign) BOOL isOnWifi;

+ (BOOL)validateEmail:(NSString *)email;
+ (void)showAlertWithMessage:(NSString *)message;
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (BOOL)isIPad3;
+ (NSString *) stringify: (NSDictionary *) object;
+ (NSString*)apiHost;
+ (JSMain *)sharedInstance;
+ (NSString *) getRealMediaFileName: (NSString *)url;

- (void) pgFire:(NSString *)viewClass selector:(SEL) func withData:(id) data;

- (NSString *) userEmail;
- (void)setUserEmail:(NSString *)email;

- (NSString *) validUserEmail;
- (void)setValidUserEmail:(NSString *)email;
- (BOOL) isIOS7;

- (BOOL)online;
- (void) checkForCMSDeletions:(id)plugin method:(SEL)_medthod;
- (void) goOnline;
- (void) goOffline;

- (BOOL)onWifi;
- (void) goOnWifi;
- (void) goOffWifi;

- (BOOL) isDownloadAllVideos;
- (BOOL) isDownloadAllSamples;
- (BOOL) isDownloadAllBrochures;

- (void) updateDownloadAllVideos:(BOOL)state;
- (void) updateDownloadAllSamples:(BOOL)state;
- (void) updateDownloadAllBrochures:(BOOL)state;


+ (NSString *) getDate;
+ (NSDictionary*) deviceProperties;
- (void)carousel:(NSString *) mediaType containers: (NSArray *) _containers pageSize:(int)_pageSize pageNum: (int) _pageNum userInfo:(id)_userInfo plugin:(id)delegate method: (SEL) _method;
- (void)search: (NSString *)term mediaType:(NSString *)_mediaType containers:(NSArray *)_containers pageSize:(int)_pageSize pageNum:(int)_pageNum userInfo:(id)_userInfo plugin:(id)delegate method: (SEL) _method;
- (void) setBusinessAreaFilters:(NSArray *) filters;
- (NSArray *) getBusinessAreaFilters;
- (void) predictiveKeysWithKey:(NSString *)key mediaType:(NSString *)_mediaType containers:(NSArray *)_containers callback:(void (^)(NSArray *results))successCalback ;

- (void) globalSearch:(NSString *)queryString containers:(NSArray *)_containers callback:(void (^)(NSDictionary *result))successCalback;


+ (NSString *) getURLOfUserGuide;

@end
