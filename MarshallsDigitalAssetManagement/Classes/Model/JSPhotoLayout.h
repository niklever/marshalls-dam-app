//
//  JSPhotoLayout.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "JSDB.h"
#import "JSMain.h"

@interface JSPhotoLayout : NSObject {
    
    
}
@property (nonatomic)bool searchReset;
@property (nonatomic) int photoCount;
@property (nonatomic) float thumbsContainerHeight; // height of the tiled thumbnail area (px)
@property (nonatomic) float imageBorderWidth; // 3px border round all images = 6px gap between them
@property (nonatomic) bool useHTML; // speed-optimisation avoiding the use of jQuery; not working yet - do not use
@property (nonatomic) bool averageAspectRatiosCalculated;
@property (nonatomic) int _blockIndex;
@property (nonatomic) float blockWidthBuffer; // remember width of top row of current block, in order to make all rows as similar in length as possible; they will be exactly equalised later once the block is complete
@property (nonatomic) float rowWidthBuffer; // while building each row, compare its width with that of the top one
@property (nonatomic) float totalWidthUsed;
@property (nonatomic) int adjustImageIndex;
@property (nonatomic, retain) NSString *layoutQueryString;


@property (nonatomic, retain) NSMutableArray *layoutRows;

@property (nonatomic, assign) int layoutRowsIndex;
@property (nonatomic, retain) NSMutableArray *layoutColumns;
@property (nonatomic, assign) int layoutColumnsIndex;
@property (nonatomic, retain) NSMutableArray * layoutSplits;// pseudo-random sequence of which images will be split vertically into 2 (in a 2-row block only)
@property (nonatomic, assign) int layoutSplitsIndex;
@property (nonatomic, retain) NSMutableArray *layoutMerges;
@property (nonatomic, assign) int layoutMergesIndex;
@property (nonatomic, retain) NSMutableArray *pseudoRandomNumbers ;
@property (nonatomic, assign) int pseudoRandomNumbersIndex;

@property (nonatomic, retain) NSMutableArray *blockWidths; //block width

@property (nonatomic, retain) NSMutableArray *currentPhotos;
@property (nonatomic, retain) NSMutableArray *searchResults; // search results to go in here as objects, eg. {filename:"1.jpg", width:500, height:333}, sorted by ascending height then width
@property (nonatomic, retain) NSMutableArray *imageLayout; // keep a record of which image appears where in the search results page layout
@property (nonatomic, retain) NSMutableArray * imageHeights;
@property (nonatomic, retain) NSMutableArray * searchResultsHeightBoundaries;
@property (nonatomic, retain) NSMutableArray * averageAspectRatios;
@property (nonatomic, retain) NSMutableArray * minWidths;
@property (nonatomic, retain) NSMutableArray * maxWidths;
@property (nonatomic, retain) NSMutableArray * imagesAvailable;
@property (nonatomic, retain) NSArray * priorityList;
@property (nonatomic) int currentPriorityIndex;
//@property (nonatomic) int endOfCurrentPriorityIndex;
@property (nonatomic, retain) NSMutableArray *  blockContentsBuffer; // remember the entire contents of the current block, in order to tweak the image sizes (to equalize row lengths) afterwards

@property (nonatomic, assign) int previousBlock;
@property (nonatomic, assign) int previousBlockWidth;
@property (nonatomic, assign) int blockStartXPos;
@property (nonatomic, assign) int blockStartYPos;
@property (nonatomic, assign) int x;
@property (nonatomic, assign) int y;
@property (nonatomic, assign) int previousMergedRowHeight;

+ (JSPhotoLayout*)sharedInstance;

- (void) layoutInit:(id) plugin query:(NSString *)strQuery;
- (void)photos2: (NSString *)query callback:(void (^)(NSArray *results))successCalback;
- (void)layoutIterate: (id)plugin iterateCount: (int) num ;
- (NSArray*) mapTilingResults:(NSArray *) photos;
- (void) layoutInitWithTags: (id)plugin  tagSpec: (NSString *)_tagSpec;
- (void)photoTags:(id) delegate;
@end
