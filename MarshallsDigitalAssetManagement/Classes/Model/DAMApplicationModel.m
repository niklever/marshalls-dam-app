    //
//  DAMApplicationModel.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Adapted to remove PhoneGap 13/11/12 Nik Lever
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DAMApplicationModel.h"
//#import "JSONKit.h"
//#import "InvokedUrlCommand.h"
//#import "ASIDownloadCache.h"
//#import "ASIHTTPRequest.h"
//#import "ASINetworkQueue.h"
#import "NSDictionary_JSONExtensions.h"

#import "JSDBV2.h"

#import "DAMHomeView.h"


#define kDefaultNumCommandObjects   10

@interface DAMApplicationModel() {
    //DownloadPlugin *downloadPlugin;
}

@property (readwrite, assign) BOOL loadFromString;

- (NSDictionary*) deviceProperties;
//- (void)flushCommandQueue;
//- (BOOL) execute:(InvokedUrlCommand*)command;

//- (void)log:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options;

@end

@implementation DAMApplicationModel 
@synthesize commandObjects;
@synthesize loadFromString;


static DAMApplicationModel *sharedInstance = nil;

+ (DAMApplicationModel *)model{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
        [[JSSync sharedInstance] setDelegate: sharedInstance];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self model] retain];
}
/*
- (id)init{
    self = [super init];
    
    if (self) {
        // Work your initialising magic here as you normally would
    }
    return self;
}
*/

/*
DAMApplicationModel *model = nil;

+ (id)model {
    if (model == nil) {
        NSLog(@"DAMApplicationModel.model createdSingleton");
        model = [[[DAMApplicationModel alloc] init] retain];
        [[JSSync sharedInstance] setDelegate: model];
    }
    return model;
}
*/
- (id)init {
#ifdef DEBUG
    NSLog(@"DAMApplicationModel.init");
#endif
    self = [super init];
        
    if (self) {
    }
    return self;
}


- (void)queueFinished:(id)sender{
    
#ifdef DEBUG
    NSLog(@"checking container list queue finished");
#endif
    
    //[[DAMApplicationModel model] enableOps];
    //kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = YES;
    //kAppDelegate.damViewController.homeView.progressView.hidden = YES;
    
    /*
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    internetReach = [[Reachability reachabilityForInternetConnection] retain];
    [internetReach startNotifier];
    [self updateInterfaceWithReachability: internetReach];
    */
}


/*
//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus == kNotReachable) {
        //go offline
        [[JSMain sharedInstance] goOffline];
    }else{
        //checking if not wifi
        if (netStatus != kReachableViaWiFi) {
            [[JSMain sharedInstance] goOffWifi];
        }else{
            [[JSMain sharedInstance] goOnWifi];
        }
        //go online
        [[JSMain sharedInstance] goOnline];      
    }
}
*/




- (void)dealloc {
    //self.downloadPlugin = nil;
    self.commandObjects = nil;
    
    [super dealloc];
}

+ (NSString*) pathForResource:(NSString*)resourcepath
{
    NSBundle * mainBundle = [NSBundle mainBundle];
    NSMutableArray *directoryParts = [NSMutableArray arrayWithArray:[resourcepath componentsSeparatedByString:@"/"]];
    NSString       *filename       = [directoryParts lastObject];
    [directoryParts removeLastObject];
    
    NSString* directoryPartsJoined =[directoryParts componentsJoinedByString:@"/"];
    //NSString* directoryStr = [self wwwFolderName];
    NSString *directoryStr = @"";
    
    if ([directoryPartsJoined length] > 0) {
        //directoryStr = [NSString stringWithFormat:@"%@/%@", [self wwwFolderName], [directoryParts componentsJoinedByString:@"/"]];
    }
    
    return [mainBundle pathForResource:filename
                                ofType:@""
                           inDirectory:directoryStr];
}

- (void)startSyncingWithEmail:(NSString *)userEmail forceRemove:(bool)remove validUserCallback:(void (^)(void))validCallback invalidUserCallBack:(void (^)(void))invalidCallback{
    NSMutableArray *containersTurnedOff = [NSMutableArray array];
    
    
    //NSArray *containers =  [[JSDB sharedInstance] sql:[NSString stringWithFormat:@"SELECT * FROM containers"] params:nil success:nil fail:nil];
    
    if (userEmail && [userEmail length] > 0) {
        //NSString *url = [NSString stringWithFormat:@"%@/webapi/UserSettings/%@",[self apiHost], userEmail];
        
        //NSLog(@"startSyncingWithEmail.start syncing from API %@", url);
        
        [[APICommon sharedInstance] getUserSettings: userEmail
                                    successCallback:^(id resp) {
                                        NSDictionary *dictionary = resp;
                                        NSMutableArray *containersTurnedOn = [NSMutableArray array];
                                        if (dictionary != NULL) {
                                            NSArray *selectedContainers = [dictionary objectForKey:@"Specialities"];
                                            
                                            //NSMutableArray * containers = [[JSDBV2 sharedInstance] sql:@"SELECT * FROM containers" params: nil];
                                            [[JSDBV2 sharedInstance] executeQuery: @"SELECT * FROM containers"
                                                                           params: nil
                                                                  successCallback:^(id containers) {
                                                                      BOOL isOn = false;
                                                                      for (NSDictionary *dic2 in containers ) {
                                                                          isOn = false;
                                                                          for (NSString *name in selectedContainers) {
                                                                              if ([[dic2 objectForKey:@"name"] isEqualToString:name]) {
                                                                                  isOn = true;
                                                                                  if ([[dic2 objectForKey:@"state"] intValue] == 0) {
                                                                                      [containersTurnedOn addObject: name];
                                                                                  }
                                                                                  NSString *sql = [NSString stringWithFormat:@"UPDATE containers SET state=%d WHERE name='%@'", 1, [dic2 objectForKey:@"name"]];
                                                                                  [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:sql successCallback:^(BOOL resp) {
                                                                                    
                                                                                  }];
                                                                                  
                                                                                  break;
                                                                              }
                                                                          }
                                                                          if (!isOn) {
                                                                              [containersTurnedOff addObject: [dic2 objectForKey:@"name"]];
                                                                              NSString *sql = [NSString stringWithFormat:@"UPDATE containers SET state=%d WHERE name='%@'", 0, [dic2 objectForKey:@"name"]];
                                                                              [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:sql successCallback:^(BOOL resp) {
                                                                                  
                                                                              }];
                                                                          }
                                                                      }
                                                                      //call valid callback
                                                                      //NSLog(@"containers list when updated = %@", [[JSDB sharedInstance] sql:[NSString stringWithFormat:@"SELECT * FROM containers"] params:nil success:nil fail:nil]);
                                                                      //then we need to start syncing
                                                                      if (containersTurnedOff.count > 0) {
#ifdef DEBUG
                                                                          NSLog(@"startSyncingWithEmail.containersTurnedOff == %@" ,  containersTurnedOff);
#endif
                                                                          //[self settingsRemoveContainerNames:containersTurnedOff];
                                                                          [[JSSync sharedInstance] clearContainers: containersTurnedOff listOnContainers: selectedContainers];
                                                                          //reset business filters
                                                                          [[JSMain sharedInstance] setBusinessAreaFilters: NULL];
                                                                      }
                                                                      
                                                                      [[DAMApplicationModel model] enableOps];
                                                                      
                                                                      if (containersTurnedOn.count > 0) {
                                                                          
                                                                          
#ifdef DEBUG
                                                                          NSLog(@"startSyncingWithEmail.containersTurnedOn == %@" ,  containersTurnedOn);
#endif
                                                                          //update user email
                                                                          [[JSMain sharedInstance] setUserEmail: userEmail];
                                                                          //reset business filters
                                                                          [[JSMain sharedInstance] setBusinessAreaFilters: NULL];
                                                                          if (remove){
                                                                              [[JSSync sharedInstance] abortQueue: nil callback:^{
                                                                                  [JSSync sharedInstance].running = false;
                                                                                  [self settingsAddContainerNames:containersTurnedOn];
                                                                                  if (validCallback) {
                                                                                      validCallback();
                                                                                  }
                                                                              }];
                                                                          }else{
                                                                              [self settingsAddContainerNames:containersTurnedOn];
                                                                              if (validCallback) {
                                                                                  validCallback();
                                                                              }
                                                                          }
                                                                      }else{
                                                                          //we need to kick queue if has container off
                                                                          /*update download queue*/
                                                                          [[JSSync sharedInstance] updateDownloadCount];
                                                                          
                                                                          if (containersTurnedOff.count > 0) {
                                                                              DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                                                              [appDelegate.damViewController.homeView notificationTimerFired:nil];
                                                                          }else{
                                                                              DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                                                              [appDelegate.damViewController.homeView notificationTimerFired:nil];
                                                                          }
                                                                          
                                                                          if (validCallback) {
                                                                              validCallback();
                                                                          }
                                                                      }
                                                                  }];
                                            
                                            
                                            //}];
                                        }else if (remove){
                                            //invalid user remove all current container
                                            //NSMutableArray * containers = [[JSDBV2 sharedInstance] sql:@"SELECT * FROM containers" params: nil ];
                                            [[JSDBV2 sharedInstance] executeQuery:@"SELECT * FROM containers"
                                                                           params:nil
                                                                  successCallback:^(id containers) {
                                                                      for (NSDictionary *con in containers) {
                                                                          [containersTurnedOff addObject: [con objectForKey:@"name"]];
                                                                      }
                                                                      
                                                                      //update status
                                                                      NSString *sql = [NSString stringWithFormat:@"UPDATE containers SET state=0"];
                                                                      [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:sql successCallback:^(BOOL resp) {
                                                                          DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                                                          [appDelegate.damViewController.homeView showUpdatingMessage: kContactAdministrator];
                                                                          
                                                                          [[JSLocalStorage sharedInstance] setItem: @"sync.itemsToDownload" value: [NSString stringWithFormat:@"%d", 0]];
                                                                          
                                                                          [[JSSync sharedInstance] clearContainers: containersTurnedOff listOnContainers:NULL];
                                                                          //reset business filters
                                                                          [[JSMain sharedInstance] setBusinessAreaFilters: NULL];
                                                                          if (invalidCallback) {
                                                                              invalidCallback();
                                                                          }
                                                                      }];
                                            }];
                                        }else{
                                            DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                            [appDelegate.damViewController.homeView updateNotificationLogs: kContactAdministrator];
                                            [[JSLocalStorage sharedInstance] setItem: @"sync.itemsToDownload" value: [NSString stringWithFormat:@"%d", 0]];
                                            if (invalidCallback) {
                                                invalidCallback();
                                            }
                                        }
        }
                                    andFailCallback:^(id msg) {
                                        if (invalidCallback) {
                                            invalidCallback();
                                        }
        }];
    }else if (remove){
        //invalid user remove all current container
        //NSMutableArray * containers = [[JSDBV2 sharedInstance] sql:@"SELECT * FROM containers" params: nil ];
        [[JSDBV2 sharedInstance] executeQuery:@"SELECT * FROM containers"
                                       params: nil
                              successCallback:^(id containers) {
                                    for (NSDictionary *con in containers) {
                                        [containersTurnedOff addObject: [con objectForKey:@"name"]];
                                    }
                                    //update status
                                    [[JSDBV2 sharedInstance] sqlExecWithSQLStatement: @"UPDATE containers SET state=0" successCallback:^(BOOL resp) {
                                        DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                                        [appDelegate.damViewController.homeView updateNotificationLogs: kUpdateEmailInSettings];
                                        
                                        [[JSSync sharedInstance] clearContainers: containersTurnedOff listOnContainers:NULL];
                                        //reset business filters
                                        [[JSMain sharedInstance] setBusinessAreaFilters: NULL];
                                        if (invalidCallback) {
                                            invalidCallback();
                                        }
                                    }];
        }];
    }else{
        DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate.damViewController.homeView updateNotificationLogs: kUpdateEmailInSettings];
        if (invalidCallback) {
            invalidCallback();
        }
    }
}



//----------------------------------------------------------------------------------------------------
//Implementation of DAMJSBridge

- (NSString*)apiHost {
    return [JSMain apiHost];
}

- (void)suspendOps {
#ifdef DEBUG
    NSLog(@"DAMApplicationModel.suspendOps");
#endif
    [JSMain sharedInstance].opsSuspended = TRUE;
    //[webView stringByEvaluatingJavaScriptFromString:@"main.opsSuspended = true"];
}

- (void)enableOps {
#ifdef DEBUG
    NSLog(@"DAMApplicationModel.enableOps");
#endif
    [JSMain sharedInstance].opsSuspended = FALSE;
    
    //[kAppDelegate.damViewController.homeView resetNotificationLogs];
    //[self requestDownloadQueueOfSize: 1];
    
    //[webView stringByEvaluatingJavaScriptFromString:@"main.opsSuspended = false"];
}

- (BOOL)online {
    return [[JSMain sharedInstance] online];
}

- (NSString*)userEmail {
    return [[JSMain sharedInstance] userEmail];
}

- (void)setUserEmail:(NSString*)userEmail {
    [[JSMain sharedInstance] setUserEmail: userEmail];
}

- (void)settingsGetAvailableMediaTypes {
    [[JSSync sharedInstance] availableMediaTypes];
    //[webView stringByEvaluatingJavaScriptFromString:@"sync.availableMediaTypes()"];
}


- (NSArray*)settingsGetContainerConfig {
    return nil;
    /*
    NSString *sql = @"SELECT * FROM containers";
    //pKt update to use common function in JSDB
    return [[JSDB sharedInstance] sql:sql params:nil success:nil fail:nil];
    //return [self getRows:sql];
    */
}

- (void)settingsGetActiveContainersForBusinessAreas:(NSArray*)businessAreas callback:(void (^)(NSArray *))callbackFunc {
    [[JSSync sharedInstance] getActiveContainers:[businessAreas JSONString]  callback:^(NSString *js) {
        callbackFunc([js objectFromJSONString]);
    }];
    //return [[[JSSync sharedInstance] getActiveContainers: [businessAreas JSONString]] objectFromJSONString];
}

- (void)settingsActiveBusinessAreas:(void (^)(NSArray *))callbackFunc  {
    //NSString *jsResult = [[JSSync sharedInstance] getActiveBusinessAreas];
    [[JSSync sharedInstance] getActiveBusinessAreas:^(NSString *jsResult) {
        callbackFunc ([jsResult objectFromJSONString]);
    }];
    //return ;
}



- (void)settingsAddContainerNames:(NSArray*)containerNames {
#ifdef DEBUG
    NSLog(@"DAMApplicationModel.settingsAddContainerNames: %@", [containerNames componentsJoinedByString:@","]);
#endif
    
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.damViewController.homeView showUpdatingMessage: @"Updating media containers..."];
    
    [[JSSync sharedInstance] addContainers: [containerNames JSONString]];
}


- (void)downloadUpdateCount {
    [[JSSync sharedInstance] updateDownloadCount];
    //[webView stringByEvaluatingJavaScriptFromString:@"sync.updateDownloadCount()"];
}

/*handle download failed for specific mediaRef*/
- (void)downloadFailed:(NSDictionary*) item {
#ifdef DEBUG
    NSLog(@"downloadFailed called %@", item);
#endif
    DAMAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    //[appDelegate.damViewController.homeView updateNotificationLogs: [NSString stringWithFormat:@"Download failed for item %@", [item objectForKey:@"mediaRef"]]];
    [appDelegate.damViewController.homeView notificationTimerFired: item];
    
    //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(notificationTimerFired:) withData: downloadId];
}


- (void)downloadComplete:(NSDictionary*)downloadId {
    
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"sync.downloadComplete('%@')", downloadId];
    NSLog(@"downloadComplete called %@", script);
#endif
    [[JSSync sharedInstance] downloadComplete: downloadId];
    
    //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(notificationTimerFired:) withData: downloadId];

    //[webView stringByEvaluatingJavaScriptFromString:script];
}

- (int)downloadQueueSize {
    return [[JSSync sharedInstance] downloadQueueSize];
    /*
    NSString *jsResult = [webView stringByEvaluatingJavaScriptFromString:@"sync.downloadQueueSize()"];
    return [jsResult intValue];
    */ 
}

- (void)requestDownloadQueueOfSize:(int)size {
    NSLog(@"requestDownloadQueueOfSize called %d", size);
    if (![JSMain sharedInstance].opsSuspended) {
        [[JSSync sharedInstance] downloadQueueTop: size];
    }
    /*
    NSString *script = [NSString stringWithFormat:@"sync.downloadQueueTop(%i, function(){})", size];
    [webView stringByEvaluatingJavaScriptFromString:script];
    */ 
}

- (void)reportCMSDeletionsToPlugin:(id)plugin andMethod:(SEL)method {
    
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"main.checkForCMSDeletions('%@')", plugin];
    NSLog(@"script: %@", script);
#endif
    [[JSMain sharedInstance] checkForCMSDeletions: plugin method: method];
    //TODO: fix params
    //[webView stringByEvaluatingJavaScriptFromString:script];
}

- (void)clearCMSDeletions {
    [[JSSync sharedInstance] clearAssetsDeleted];
    //[webView stringByEvaluatingJavaScriptFromString:@"sync.clearAssetsDeleted()"];
}

- (void)iDriveListFolder:(NSString *)folder delegate:(id)aDel {
    //NSString *script = [NSString stringWithFormat:@"iDrive.ls(%@, '%@')", [folder JSONString], plugin];
    [[JSiDrive sharedInstance] ls: folder delegate:aDel callback:NULL];
    //[webView stringByEvaluatingJavaScriptFromString:script];
}

- (void)iDriveListFolderFolders:(NSString *)folder delegate:(id)aDel {
    [[JSiDrive sharedInstance] lsdir: folder delegate:aDel callback: NULL];
}


- (void)iDriveCopyAssets:(NSArray*)assets toFolder:(NSString*)folder {
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"iDrive.cpMultiple(%@, %@)", [assets JSONString], [folder JSONString] ];
    NSLog(@"DAMApplicationModel.iDriveCopyAssets script:%@", script);
#endif
    //[webView stringByEvaluatingJavaScriptFromString:script];
    [[JSiDrive sharedInstance] cpMultiple: assets folder: folder];
}

- (void)iDriveDelete:(NSArray*)items {
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"iDrive.rmMultiple(%@)", [items JSONString]];
    NSLog(@"DAMApplicationModel.iDriveDelete script:%@", script);
#endif
    [[JSiDrive sharedInstance] rmMultiple: items];
    //[webView stringByEvaluatingJavaScriptFromString:script];
}

- (void)iDriveMove:(NSArray*)items toFolder:(NSString*)folder {
    //NSString *script = [NSString stringWithFormat:@"iDrive.mvMultiple(%@, %@)", [items JSONString], [folder JSONString] ];
    //NSLog(@"DAMApplicationModel.iDriveMove script:%@", script);
    //[webView stringByEvaluatingJavaScriptFromString:script];
    [[JSiDrive sharedInstance] mvMultiple: items folder: folder];
}

- (void)iDriveCreateFolder:(NSString*)folderName {
    
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"iDrive.mkdir(%@)", [folderName JSONString]];
    NSLog(@"DAMApplicationModel.iDriveCreateFolder script:%@", script);
#endif
    [[JSiDrive sharedInstance] mkdir: folderName];
    //[webView stringByEvaluatingJavaScriptFromString:script];
}

- (void)iDriveListFilesAffectedByRemovingContainers:(NSArray*)containers toPlugin:(NSString*)plugin andMethod:(SEL)method {
    
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"iDrive.filesAffectedByContainerRemoval(%@, '%@')", [containers JSONString], plugin];
    NSLog(@"DAMApplicationModel.iDriveListFilesAffectedByRemovingContainers script:%@", script);
#endif
    [[JSiDrive sharedInstance] filesAffectedByContainerRemoval: containers plugin: plugin callback: method];
    //[webView stringByEvaluatingJavaScriptFromString:script];
}

- (void)emailAssets:(NSArray*)assets toRecipient:(NSString*)recipient withSubject:(NSString*)subject andMessage:(NSString*)message {
    NSString *sender = ([self userEmail] == NULL ? @"" : [self userEmail]);
    /*NSDictionary *emailParams = [NSDictionary dictionaryWithObjectsAndKeys:recipient, @"to",
        sender, @"from", subject, @"subject", message, @"body", assets, @"mediaRefs", nil];
    */
    NSMutableDictionary *emailParams = [[NSMutableDictionary alloc] init];
    [emailParams setObject: recipient forKey:@"to"];
    [emailParams setObject:sender forKey:@"from"];
    [emailParams setObject:subject forKey:@"subject"];
    [emailParams setObject:message forKey:@"body"];
    [emailParams setObject:assets forKey:@"mediaRefs"];
    
    
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"share.email(%@)", [emailParams JSONString]];
    NSLog(@"DAMApplicationModel.emailAssets script:%@ params %@", script, emailParams);
#endif
    
    //[webView stringByEvaluatingJavaScriptFromString:script];
    [[JSShare sharedInstance] email: emailParams];  
    [emailParams release];
}

- (void)photoGetTags:(id)delegate mediaType: (NSString *)mediaType{
    //[[JSPhotoLayout sharedInstance] photoTags: delegate];
    [[JSPhotoLayoutV2 sharedInstance] photoTags: delegate mediaType: mediaType];
}

- (void)photoInitLayoutForPlugin:(id)plugin withQuery:(NSString*)query mediaType: (NSString *)mediaType{
    [[JSPhotoLayoutV2 sharedInstance] layoutInit:plugin query: query mediaType: mediaType];
}


- (void)photoInitLayoutForPlugin:(id)plugin withTags:tagSpec mediaType: (NSString *)mediaType{
    //[[JSPhotoLayout sharedInstance] layoutInitWithTags:plugin tagSpec:tagSpec];
    [[JSPhotoLayoutV2 sharedInstance] layoutInitWithTags:plugin tagSpec:tagSpec mediaType: mediaType];
}

- (void)photoLayoutIteratorForPlugin:(id)plugin {
}


- (void)offlineDataGetCarouselForMediaType:(NSString*)mediaType inContainers:(NSArray*)containers withPageSize:(int)pageSize andPageNum:(int)pageNum withUserInfo:(NSDictionary*)userInfo plugin:(id)delegate method: (SEL) _method{
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"main.carousel('%@', %@, %i, %i, %@)", mediaType, [containers JSONString], pageSize, pageNum, [userInfo JSONString]];
    NSLog(@"DAMApplicationModel.offlineDataGetCarouselForMediaType script:%@", script);
#endif
    [[JSMain sharedInstance] carousel:mediaType containers: containers pageSize:pageSize pageNum:pageNum userInfo:userInfo plugin:delegate method:_method];
    //[webView stringByEvaluatingJavaScriptFromString:script];
    //[[JSMain sharedInstance] carousel:mediaType containers: containers pageSize:pageSize pageNum:pageNum userInfo:userInfo ];
}

- (void)offlineDataGetCarouselForSearchTerm:(NSString*)searchTerm ofMediaType:(NSString*)mediaType inContainers:(NSArray*)containers withPageSize:(int)pageSize andPageNum:(int)pageNum withUserInfo:(NSDictionary*)userInfo plugin:(id)delegate method: (SEL) _method{
#ifdef DEBUG
    NSString *script = [NSString stringWithFormat:@"main.search(%@, '%@', %@, %i, %i, %@)", [searchTerm JSONString], mediaType, [containers JSONString], pageSize, pageNum, [userInfo JSONString]];
    NSLog(@"DAMApplicationModel.offlineDataGetCarouselForSearchTerm script:%@", script);
#endif
    [[JSMain sharedInstance] search:searchTerm mediaType: mediaType containers:containers pageSize:pageSize pageNum:pageNum userInfo:userInfo plugin:delegate method:_method];
}

//This method was needed when there where different rules for accessing an asset's thumbnail
- (NSString*)thumbnailForAsset:(NSDictionary*)asset inView:(UIView*)view {
    
//    NSString *imageUrl;
    NSString *mediaRef = [asset objectForKey:@"mediaRef"];
    NSString *mediaType = [asset objectForKey:@"mediaType"];

    NSString *imageUrl;

    if ([mediaType isEqualToString:kMediaType_Photos] || [mediaType isEqualToString:kMediaType_SalesPhotos]) {
        float width = view.frame.size.width;
        imageUrl = [NSString stringWithFormat:@"/AssetStore/%@.jpg?MaxWidth=%i", mediaRef, (int)width];
    } else {
        imageUrl = [asset objectForKey:@"thumbnail"];
    }
    
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1)         {
//        imageUrl = [NSString stringWithFormat:@"/AssetStore/%@.jpg?MaxWidth=500", mediaRef];
//    } else {
//        imageUrl = [NSString stringWithFormat:@"/AssetStore/%@.jpg?MaxWidth=250", mediaRef];
//    }
    
    //Now just using whatever came back from the server-side.
    imageUrl = [asset objectForKey:@"thumbnail"];
                
    return imageUrl;
}


@end
