//
//  JSPhotoLayoutV2.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 3/5/13.
//
//

#import <Foundation/Foundation.h>
#import "JSDBV2.h"
#import "JSMain.h"
#import "JSSync.h"
#import "JSTiling.h"

@interface JSPhotoLayoutV2 : NSObject

//@property (nonatomic, retain) NSMutableArray *currentPhotos;
//@property (nonatomic, retain) NSMutableArray *searchResults; // search results to go in here as objects, eg. {filename:"1.jpg", width:500, height:333}, sorted by ascending height then width
@property (nonatomic, retain) NSMutableArray *imageLayout;
@property (nonatomic)BOOL searchReset;

+ (JSPhotoLayoutV2*)sharedInstance;

- (void) layoutInit:(id) plugin query:(NSString *)strQuery mediaType: (NSString *)mediaType;
- (void)photos2: (NSString *)query mediaType: (NSString *)mediaType callback:(void (^)(NSArray *results))successCalback;
- (void)layoutIterate: (id)plugin iterateCount: (int) num ;
- (NSArray*) mapTilingResults:(NSArray *) photos;
- (void) layoutInitWithTags: (id)plugin  tagSpec: (NSString *)_tagSpec mediaType: (NSString *)mediaType;
- (void) photoTags:(id) delegate mediaType: (NSString *)mediaType;

@end
