//
//  DAMApplicationModel.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 16/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ASIHTTPRequest.h"
//#import "ASIHTTPRequestDelegate.h"
//#import "ASIFormDataRequest.h"
#import "DownloadPlugin.h"
#import "NSDictionary_JSONExtensions.h"
#import "JSSync.h"
//#import "JSPhotoLayout.h"
#import "JSPhotoLayoutV2.h"
#import "JSMain.h"
//#import "Reachability.h"
#import "JSiDrive.h"
#import "JSShare.h"

@interface DAMApplicationModel : NSObject</*UIWebViewDelegate,*/ DAMJSBridge> {
    //UIWebView *webView;
    NSDictionary *commandObjects;
    NSString *downloadJSON;
    NSMutableData *receivedData;
    //Reachability *internetReach;
}
@property (nonatomic, retain) NSDictionary *commandObjects;

+ (id)model;

@end
