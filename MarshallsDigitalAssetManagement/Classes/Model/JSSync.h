//
//  JSSync.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ASIHTTPRequest.h"
//#import "ASIHTTPRequestDelegate.h"
//#import "ASIFormDataRequest.h"
#import "NSDictionary_JSONExtensions.h"
#import "JSMain.h"
#import "JSDBV2.h"
#import "DownloadPlugin.h"
#import "JSLocalStorage.h"
//#import "JSONKit.h"
#import "JSiDrive.h"


typedef void(^SuccessCallback)(void);

@interface JSSync : NSObject{
    NSArray *thumbHeightBuckets;
    NSString *mediaTable;
    NSString *containerTable;
    NSString *tagTable;
    NSString *downloadTable;
    
    NSString *deleteTable;
}

@property (nonatomic, retain) NSString *lsDateKey;
@property (nonatomic, assign) BOOL running;
@property (nonatomic, retain)NSArray *thumbHeightBuckets;
@property (nonatomic, retain) id delegate;
@property (nonatomic, retain)NSString *mediaTable;
@property (nonatomic, retain)NSString *containerTable;
@property (nonatomic, retain)NSString *tagTable;
@property (nonatomic, retain)NSString *downloadTable;
@property (nonatomic, retain)NSString *downloadTableOriginal;
@property (nonatomic, retain)NSString *deleteTable;
@property (nonatomic, retain) NSString *settingsKey;
@property (nonatomic, retain) DownloadPlugin *downloadPlugin;
@property (nonatomic, retain) NSArray *settings;
@property (nonatomic, retain) NSString *earliestPossibleDate;
@property (nonatomic, retain) NSArray *updatedAssetsList;
@property (nonatomic, retain) NSArray *deletedAssetsList;

@property (nonatomic, strong) void (^syncAssetCallbackFunc)(void);


/*_syncContainers: function(settings, since, callback*/
-(void) syncContainers: (NSArray *) settings since: (NSString *) when callback: (void (^)(void))callbackFunc;
+ (JSSync *) sharedInstance;
-(void) availableMediaTypes;
- (int) downloadQueueSize;

//- (int) remainedAssetDownload:(NSString *)mt;

- (void) downloadQueueTop:(int)limit;
- (void)updateDownloadCount;
- (void) downloadComplete: (NSDictionary *) queueItemId ;
- (void)getContainerConfig:(void (^)(NSString *))callbackFunc;
- (void)getActiveContainers:(NSString *)businessAreas callback:(void (^)(NSString *))callbackFunc;
- (void) getActiveBusinessAreas:(void (^)(NSString *))callbackFunc;
- (void) addContainers:(NSString *) containerNames;
- (void) mediaWithContainers: (NSArray*) containers callback:(void (^)(NSMutableArray *media))successCalback;
- (void)clearContainers:(NSArray *)containerNames listOnContainers:(NSArray *) onLists;
- (BOOL)downloadInProgress;
- (NSArray *)mediaCachedFileNameWithExtension:(NSString *)mediaRef;
- (void) clearCachedMediaForContainers:(NSArray *) containerNames;
- (void)_getSettings:(void (^)(NSMutableArray *))callbackFunc;
- (void)run:(void (^)(void))callback;
-(void) mediaRemovedWhenRemovingContainers: (NSArray *)containers callback:(void (^)(NSMutableArray *media))successCalback;
-(void)clearAssetsDeleted;
- (void)addToQueue: (NSString *)mediaRef mediaType:(NSString *)mt url:(NSString *)_url;
- (void) abortQueue :(NSArray *) media callback:(void (^)(void))succCallback;



@end
