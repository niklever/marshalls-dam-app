//
//  DAMJSBridge.h
//  DAMWebHarness
//
//  Created by Steve Tickle on 05/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DAMJSBridge <NSObject>

- (NSString*)apiHost;

- (void)suspendOps;
- (void)enableOps;

- (BOOL)online;

- (NSString*)userEmail;
- (void)setUserEmail:(NSString*)userEmail;

- (void)startSyncingWithEmail:(NSString *)userEmail forceRemove:(bool)remove validUserCallback:(void (^)(void))validCallback invalidUserCallBack:(void (^)(void))invalidCallback;

- (void)settingsGetAvailableMediaTypes;
- (void)settingsGetContainerConfig:(void (^)(NSArray*))validCallback;
- (void)settingsGetActiveContainersForBusinessAreas:(NSArray*)businessAreas callback:(void (^)(NSArray*))validCallback;
- (void)settingsActiveBusinessAreas:(void (^)(NSArray*))validCallback;
- (void)settingsAddContainerNames:(NSArray*)containerNames;
//- (void)settingsRemoveContainerNames:(NSArray*)containerNames;

- (void)downloadUpdateCount;
- (void)downloadComplete:(NSDictionary*)item;
- (void)downloadFailed:(NSDictionary*)item;
- (void)requestDownloadQueueOfSize:(int)size;
- (int)downloadQueueSize;

/*- (void)iDriveListFolder:(NSString *)folder toPlugin:(NSString*)plugin;
- (void)iDriveListFolderFolders:(NSString *)folder toPlugin:(NSString*)plugin;
*/
- (void)iDriveListFolder:(NSString *)folder delegate:(id)aDel;
- (void)iDriveListFolderFolders:(NSString *)folder delegate:(id)aDel;
- (void)iDriveCopyAssets:(NSArray*)assets toFolder:(NSString*)folder;
- (void)iDriveDelete:(NSArray*)items;
- (void)iDriveMove:(NSArray*)items toFolder:(NSString*)folder;
- (void)iDriveCreateFolder:(NSString*)folderName;
- (void)iDriveListFilesAffectedByRemovingContainers:(NSArray*)containers toPlugin:(NSString*)plugin andMethod:(SEL)method;

- (void) reportCMSDeletionsToPlugin:(id)plugin andMethod:(SEL)method;
- (void)clearCMSDeletions;

- (void)emailAssets:(NSArray*)assets toRecipient:(NSString*)recipient withSubject:(NSString*)subject andMessage:(NSString*)message;

- (void)photoGetTags:(id)delegate mediaType: (NSString *)mediaType;
//- (void)photoLayoutOnPage:(int)page forPlugin:(NSString*)plugin withPhotos:(NSArray*)photos andPageKey:(NSString*)pageKey;
//- (void)photoLayoutForPlugin:(NSString*)plugin;
//- (void)photoLayoutForPlugin:(NSString*)plugin withQuery:(NSString*)query;

- (void)photoInitLayoutForPlugin:(id)plugin withQuery:(NSString*)query mediaType: (NSString *)mediaType;
- (void)photoInitLayoutForPlugin:(id)plugin withTags:tagSpec mediaType: (NSString *)mediaType;

- (void)photoLayoutIteratorForPlugin:(id)plugin mediaType: (NSString *)mediaType;

- (void)offlineDataGetCarouselForMediaType:(NSString*)mediaType inContainers:(NSArray*)containers withPageSize:(int)pageSize andPageNum:(int)pageNum withUserInfo:(NSDictionary*)userInfo plugin:(id)delegate method: (SEL) _method;
- (void)offlineDataGetCarouselForSearchTerm:(NSString*)searchTerm ofMediaType:(NSString*)mediaType inContainers:(NSArray*)containers withPageSize:(int)pageSize andPageNum:(int)pageNum withUserInfo:(NSDictionary*)userInfo plugin:(id)delegate method: (SEL) _method;

//- (void)carouselForMediaType:(NSString*)mediaType withPageSize:(int)pageSize andPageNum:(int)pageNum withUserInfo:(NSDictionary*)userInfo forPlugin:(NSString*)plugin andMethod:(NSString*)method;

- (NSString*)thumbnailForAsset:(NSDictionary*)asset inView:(UIView*)view;

@end
