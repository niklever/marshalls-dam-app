//
//  JSShare.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 1/9/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ASINetworkQueue.h"

@interface JSShare : NSObject {
    //ASINetworkQueue *queueSendingEmails;
}
@property(nonatomic, assign) int emailPushInterval;
@property (nonatomic, retain)NSString * lsDateKey;
@property (nonatomic, retain)NSString *method;
//@property (nonatomic, retain) ASINetworkQueue *queueSendingEmails;

+ (JSShare *) sharedInstance;
- (void)email: (NSDictionary *) params;
@end
