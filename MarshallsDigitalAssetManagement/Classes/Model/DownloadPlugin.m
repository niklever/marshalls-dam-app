//
//  DownloadPlugin.m
//  DAMWebHarness
//
//  Created by Steve Tickle on 05/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadPlugin.h"
//#import "ASINetworkQueue.h"
//#import "ASIDownloadCache.h"
#import "DAMAppDelegate.h"
#import "DAMViewController.h"
#import "DAMHomeView.h"
#import "AFDownloadRequestOperation.h"


const int downloadBatchSize = 2; //24

@interface DownloadPlugin() {

    //ASINetworkQueue *queue;
}

@property (nonatomic, retain) AFHTTPRequestOperationManager *queue;
//@property (nonatomic, retain) ASINetworkQueue *queue;
@property (nonatomic, assign) BOOL kickInProgress;


@end

@implementation DownloadPlugin
//@synthesize queue;
@synthesize jsBridge;

- (id)init {
    self = [super init];
    if (self) {
        NSFileManager *filemgr;
        
        filemgr = [NSFileManager defaultManager];
        
        if ([filemgr fileExistsAtPath: [self basePathToThumbMedia] ] == NO){
            //create base folder
            [filemgr createDirectoryAtPath:[self basePathToThumbMedia] withIntermediateDirectories: YES attributes:nil error: NULL];
        }
        
        [self initQueue];
        
        itemFinished = 0;
    }
    return self;
}

-(void)initQueue {
    NSLog(@"initQueue called..................");
    self.queue = [AFHTTPRequestOperationManager manager];
    [self.queue.operationQueue setMaxConcurrentOperationCount: 1];
    
    //[self.queue addObserver:self forKeyPath:@"operations" options:0 context:NULL];
}

/*
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                         change:(NSDictionary *)change context:(void *)context
{
    if (object == self.queue && [keyPath isEqualToString:@"operations"]) {
        if (self.queue.operationQueue.operationCount == 0) {
            // Do something here when your queue has completed
            NSLog(@"queue has completed");
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object
                               change:change context:context];
    }
}*/

- (void)dealloc {
    //self.queue = nil;
    [super dealloc];
}

- (NSString *)basePathToMainMedia {
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory=[paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"MainMedia"];
    
    /*return [NSString stringWithFormat:@"%@/Caches/MainMedia/",[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0]];*/
}

- (NSString *)basePathToThumbMedia {
    
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory=[paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"ThumbMedia"];
    
    /*
    return [NSString stringWithFormat:@"%@/Caches/ThumbMedia/",[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0]];
    */
}


/*Get real file name from medial url
 url: url of media to download. Sample: http://www.marshalls.co.uk/dam-svc/AssetStore/145b377d-248f-4104-9fe5-69c76e594f43.jpg?MaxHeight=243.000000
 return: file name of media with out ?. We will return 145b377d-248f-4104-9fe5-69c76e594f43.jpg from above input
 
 sample video
 },
 {
 id = 2592;
 mediaRef = "fafb17b0-6584-4500-a7bd-1bdccaa40354";
 mediaType = Videos;
 url = "http://www.marshalls.co.uk/dam-svc/Thumbnails/fafb17b0-6584-4500-a7bd-1bdccaa40354.png";
 },
 {
 id = 2593;
 mediaRef = "fafb17b0-6584-4500-a7bd-1bdccaa40354";
 mediaType = Videos;
 url = "http://www.marshalls.co.uk/dam-svc/AssetStore/fafb17b0-6584-4500-a7bd-1bdccaa40354.mp4";
 },
 */
/*
- (NSString *) getRealMediaFileName: (NSString *)url{
    NSString *filename = [url lastPathComponent];
    NSRange range = [filename rangeOfString:@"?"];
    if (range.location != NSNotFound) {
        return [[filename componentsSeparatedByString:@"?"] objectAtIndex: 0];
    }
    if ([url rangeOfString:@"Thumbnails"].location != NSNotFound || [url rangeOfString:@"MaxHeight"].location != NSNotFound) {
        //this is thumbnail url
        filename = [NSString stringWithFormat:@"t-%@", filename];
    }else if ([url rangeOfString:@"AssetStore"].location != NSNotFound) {
        filename = [NSString stringWithFormat:@"as-%@", filename];
    }
    
    return filename;
}*/

- (bool) isImage:(NSString *) url {
    //TODO: we got problem with downloading so need to turn on image temporary. 
    return false;
    /*
    BOOL isImage = false;
    NSRange textRange;
    textRange = [[url lowercaseString] rangeOfString:@".jpg"];
    if(textRange.location != NSNotFound)
    {
        //Does contain the substring
        isImage = true;
    }else if ([[url lowercaseString] rangeOfString:@".png"].location != NSNotFound){
        isImage = true;
    }else if ([[url lowercaseString] rangeOfString:@".gif"].location != NSNotFound){
        isImage = true;
    }else if ([[url lowercaseString] rangeOfString:@".jpeg"].location != NSNotFound){
        isImage = true;
    }
    return isImage;
    */ 
}

/*
 pKt updated queue data class
 downloadItems params is NSArray of rows (NSDictionary) in table download_queue
 */
- (void)queueData:(NSArray *)downloadItems {
    NSLog(@"DownloadPlugin.queueData count == %d", downloadItems.count);
    
    if (self.queue && self.queue.operationQueue.operationCount > 0 && self.kickInProgress) {
        NSLog(@"DownloadPlugin.queueData.WARN_OP_IN_PROGRESS");
        return;
    }
    
    /*
    if (queue == nil) {
        [self initQueue];
    }*/
    
    currentBatch = [downloadItems count];
    itemFinished = 0;
    self.kickInProgress = NO;
    
    
    UIProgressView *progressView = kAppDelegate.damViewController.homeView.progressView;
    progressView.progress = 0.0f;
    progressView.hidden = NO;
    [kAppDelegate.damViewController.homeView setDisplayActivityIndicatorView: NO];
    
    for (NSDictionary *item in downloadItems) {
            NSString *str = [item objectForKey:@"url"];
            /*checking download all selections*/
            int original = [[item objectForKey:@"original"] intValue];
        NSString *storeFileName = [JSMain getRealMediaFileName: str];
        
        if (storeFileName == nil) {
            [jsBridge downloadComplete: item];
            continue;
        }
        
            
            if ([[item objectForKey:@"mediaType"] isEqualToString:kMediaType_Videos] && original == 1  && ![[JSMain sharedInstance] isDownloadAllVideos]) {
                /*ignore this one*/
                [jsBridge downloadComplete: item];
                continue;
            }
            
            if ([[item objectForKey:@"mediaType"] isEqualToString:kMediaType_Samples]  && original == 1  && ![[JSMain sharedInstance] isDownloadAllSamples]) {
                /*ignore this one*/
                [jsBridge downloadComplete: item];
                continue;
            }
            
            if ([[item objectForKey:@"mediaType"] isEqualToString:kMediaType_Brochures]  && original == 1  && ![[JSMain sharedInstance] isDownloadAllBrochures]) {
                /*ignore this one*/
                [jsBridge downloadComplete: item];
                continue;
            }
            
            if ([[item objectForKey:@"mediaType"] isEqualToString:kMediaType_Samples]) {
                //if samples we will add UseOriginal=true
                if ([str length] > 0) {
                    if ([[str componentsSeparatedByString:@"?"] count] > 1) {
                        //has ? then we just add &
                        str = [str stringByAppendingString:@"&UseOriginal=true"];
                    }else{
                        str = [str stringByAppendingString:@"?UseOriginal=true"];
                    }
                }
            }
            //str = @"http://ios.dev2.gkxim.com/damipa/b44de9b8-b18e-41b3-a54b-607dcf28070b.png";
            //str = @"http://www.marshalls.co.uk/dam-svc/AssetStore/a85b9392-e982-43df-ad6b-9698454691b7.pdf";
        
        
        NSURL *url = [NSURL URLWithString: str];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSString *targetPath = [NSString stringWithFormat:@"%@/%@", [self basePathToThumbMedia], storeFileName];
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath: targetPath]) {
            //delete from disk
            NSError *error = nil;
            if (![[NSFileManager defaultManager] removeItemAtPath: targetPath error: &error]) {
                NSLog(@"failed to remove file %@ with error %@", targetPath, error);
            }
        }
        
        
        //AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        AFDownloadRequestOperation *op = [[AFDownloadRequestOperation alloc] initWithRequest: request targetPath:targetPath shouldResume: NO];
        //op.outputStream = [NSOutputStream outputStreamToFileAtPath:targetPath append:NO];
        op.userInfo = item;
        [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            //NSLog(@"request completed........................");
            NSLog(@"DownloadPlugin.requestFinished url:%@", operation);
            
            //NSLog(@"complete header == %@  request header == %@", request.responseHeaders, request.requestHeaders);
            [jsBridge downloadComplete: operation.userInfo];
            itemFinished++;
            
            if (self.queue.operationQueue.operationCount == 0) {
                [kAppDelegate.damViewController.homeView resetNotificationLogs];
                [jsBridge requestDownloadQueueOfSize:downloadBatchSize];
            }
            
            
            int downloadQueueSize = [[DAMApplicationModel model] downloadQueueSize];
            if (downloadQueueSize > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    kAppDelegate.damViewController.homeView.notificationLabel.text = [NSString stringWithFormat: kDownloadingItemRemained, downloadQueueSize];
                });
            }
            
            
        }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      
                                      NSLog(@"download error %@", error);
                                      
                                      [jsBridge downloadFailed: operation.userInfo];
                                      itemFinished++;
                                      
                                      if (self.queue.operationQueue.operationCount == 0) {
                                          [kAppDelegate.damViewController.homeView resetNotificationLogs];
                                          [jsBridge requestDownloadQueueOfSize:downloadBatchSize];
                                      }
                                      
                                      int downloadQueueSize = [[DAMApplicationModel model] downloadQueueSize];
                                      if (downloadQueueSize > 0) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              kAppDelegate.damViewController.homeView.notificationLabel.text = [NSString stringWithFormat: kDownloadingItemRemained, downloadQueueSize];
                                          });
                                      }
                                      
                                      //remove downloading progress and show failed message
                                      [kAppDelegate.damViewController.homeView setDisplayProgressView: YES];
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          kAppDelegate.damViewController.homeView.progressLabel.hidden = NO;
                                          kAppDelegate.damViewController.homeView.progressLabel.text = [error localizedDescription];
                                      });
                                      
                                      //failure case
                                      NSLog(@"BaaZ  File NOT Saved %@", targetPath);
                                      /*
                                      //remove the file if saved a part of it!
                                      NSFileManager *fileManager = [NSFileManager defaultManager];
                                      [fileManager removeItemAtPath:targetPath error:&error];
                                      
                                      if (error) {
                                          NSLog(@"error dude");
                                      }
                                      */
                                      if ([operation isCancelled]) {
                                          //that doesn't work.
                                          NSLog(@"Canceled");
                                      }
                                  }];
        
        [op setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            if (totalBytesExpectedToRead > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UILabel *progressLabel = kAppDelegate.damViewController.homeView.progressLabel;
                    if(progressLabel.hidden) {
                        progressLabel.hidden = NO;
                        progressLabel.text = [NSString stringWithFormat:@"%@", storeFileName];
                    }
                    kAppDelegate.damViewController.homeView.progressView.progress = (float)totalBytesRead / totalBytesExpectedToRead;
                    //progressView.center = CGPointMake(progressView.center.x, 186);
                    if (kAppDelegate.damViewController.homeView.progressView.hidden) {
                        kAppDelegate.damViewController.homeView.progressView.hidden = NO;
                    }
                    //kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = YES;
                    [kAppDelegate.damViewController.homeView setDisplayActivityIndicatorView: YES];
                });
            }
        }];
        
        [self.queue.operationQueue addOperation: op];
    }
    
    NSLog(@"self.queue.operationCount == %d", self.queue.operationQueue.operationCount);
    
    if (self.queue.operationQueue.operationCount > 0) {
        
        [kAppDelegate.damViewController.homeView setDisplayActivityIndicatorView:NO];
        NSLog(@"Queue start........................");
        self.kickInProgress = YES;
    }
}


- (void) checkKickoffNewQueue {
    if (itemFinished >= currentBatch) {
        NSLog(@"DownloadPlugin.checkKickoffNewQueue %d", self.queue.operationQueue.operationCount);
        //Ask for some more stuff to download from the JS model, if any.
        //queueData will be called again if there are items available.
        [jsBridge requestDownloadQueueOfSize:downloadBatchSize];
    }
}

- (void)kickQueue {
    if (/*self.queue && self.queue.operationQueue.operationCount == 0 &&*/ !self.kickInProgress) {
        self.kickInProgress = YES;
        NSLog(@"queue is kicking");
        [jsBridge requestDownloadQueueOfSize:downloadBatchSize];
        
    }else{
        NSLog(@"queue is running");
    }
}

/*Abort current queue because of there is updated on Settings*/
- (void)abortQueue: (NSArray *)mediaRefs callback:(void (^)(void))succCallback{
    
    NSLog(@"abortQueue called of current queue %d", self.queue.operationQueue.operationCount);

    if (self.queue && self.kickInProgress) {
        
        for (AFHTTPRequestOperation *ap in self.queue.operationQueue.operations) {
            NSLog(@"cancel operation %@", ap.userInfo);
            [ap cancel];
        }
        
        [self.queue.operationQueue cancelAllOperations];
        
        /*for (ASIHTTPRequest *req in self.queue.operations)
        {
            [req setDelegate:nil];
            [req cancel];
        }*/
        
        self.queue = nil;
        
        //need to init new queue
        [self initQueue];
        
        
        self.kickInProgress = false;
        
        [kAppDelegate.damViewController.homeView resetDownloadingMsg: nil];
        //[[JSMain sharedInstance] pgFire:@"DAMHomeView" selector:@selector(resetDownloadingMsg:) withData: nil];
        
        if (succCallback) {
            succCallback();
        }
    }else{
        if (succCallback) {
            succCallback();
        }
    }
}

/*delete media inside thumb with mediaRef*/
- (void)deleteMedia:(NSString *)mediaRef {
    if (!mediaRef || [mediaRef length] == 0) {
        return;
    }
    
    NSFileManager *filemgr;
    filemgr = [NSFileManager defaultManager];
    
    NSError *error = nil;  
    NSString *filePath = [[self basePathToThumbMedia] stringByAppendingPathComponent: mediaRef];
    //NSLog(@"File : %@", filePath);
    if ([filemgr fileExistsAtPath: filePath]) {
        BOOL fileDeleted = [filemgr removeItemAtPath:filePath error:&error];
        
        if (fileDeleted != YES || error != nil)
        {
            // Deal with the error...
#ifdef DEBUG
            NSLog(@"deleteMedia media %@ failed. %@", filePath, error);
#endif
        }
    }else{
        //NSLog(@"deleteMedia media %@ is not EXIST. ignore it.", filePath);
    }
}

@end
