//
//  JSPath.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSPath.h"

@implementation JSPath
@synthesize path;
@synthesize _bits;

- (id)initWithPath:(NSString *)initialPath{
    self = [super init];
    if (self) {
        self.path = @"/";

        if (initialPath != NULL) {
            self.path = initialPath;
            if ([initialPath isEqualToString:@"/"]) {
                self._bits = [[NSMutableArray alloc] init];
                _len = 0;
            } else {
                self._bits = [[NSMutableArray alloc] initWithArray: [initialPath componentsSeparatedByString:@"/"]];
                ;//[NSMutableArray arrayWithArray:[initialPath componentsSeparatedByString:@"/"]];
                //this._bits.shift(); Removes the first element from an array 
                [self._bits removeObjectAtIndex:0];
                _len = [self._bits count];
            }
        }else{
            self._bits = [[NSMutableArray alloc] init];//[NSMutableArray array];
            _len = 0;
        }
        
    }
    return self;
}

-(NSMutableArray *) bits{
    return self._bits;
}

- (int) length{
    return _len;
}

- (NSString *)parent{
    if (_len > 1) {
        NSString *result = @"";
        for (int i = 0 ; i < [self._bits count] - 1; i++) {
            result = [NSString stringWithFormat:@"%@%@", result, [self._bits objectAtIndex: i]];
            //add /
            if (i < [self._bits count] - 1) {
                result = [NSString stringWithFormat:@"%@/", result];
            }
        }
        return [NSString stringWithFormat:@"/%@", result];
    }
    //return _len > 1 ? "/" + this._bits.slice(0, this._len - 1).join("/") : "/";
    return @"/";
}

- (NSString *)top{
    return [self._bits lastObject];
}

-(void)navDown:(NSString *)dir {
    [self._bits addObject:dir];
    _len = [self._bits count];
    NSString *result = @"";
    for (int i = 0 ; i < [self._bits count]; i++) {
        result = [NSString stringWithFormat:@"%@%@", result, [self._bits objectAtIndex: i]];
        //add /
        if (i < [self._bits count]) {
            result = [NSString stringWithFormat:@"%@/", result];
        }
    }
    self.path = [NSString stringWithFormat:@"/%@", result];
    //this.path = "/" + this._bits.join("/");
}

-(void)dealloc {
    [super dealloc];
}

@end
