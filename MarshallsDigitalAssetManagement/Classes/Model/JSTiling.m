//
//  JSPhotoLayoutV2.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 3/5/13.
//
//

#import "JSTiling.h"

@implementation JSTiling
@synthesize thumbsContainerHeight,imageBorderWidth,searchResults,imageRanges,rowsInCurrentBlock,rowsInPreviousBlock,totalWidthUsed;
@synthesize imagePriorities,imagePriorityIndex,imageHeights,imageHeightIndex;
@synthesize pseudoRandomNumbers, pseudoRandomNumbersIndex;
@synthesize call;
@synthesize blockIndex,imageLayout, x,y;
//,previousBlock,previousBlockWidth,blockStartXPos,blockStartYPos,x,y,previousMergedRowHeight,imageLayout;
@synthesize imagesCountingAvailable;
@synthesize isCancel;
#define undefined -1


static JSTiling *sharedInstance = nil;

/*
+ (JSTiling *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}


 + (id)allocWithZone:(NSZone *)zone {
     return [[self sharedInstance] retain];
 }
 */

-(id)init {
    self = [super init];
    return self;
}

- (void) internalInit {
    self.isCancel = NO;
    self.call = 0;
    self.thumbsContainerHeight = 748; // height of the tiled thumbnail area (px)
    self.imageBorderWidth = 3; // 3px border round all images = 6px gap between them
    
    //var searchResults = []; // search results to go in here as objects, eg. {filename:"1.jpg", width:500, height:333, priority:3}, sorted by priority, then height, then width
    
    if (self.imageRanges != nil) {
        [self.imageRanges release];
        self.imageRanges = nil;
    }
    if (self.imagePriorities != nil) {
        [self.imagePriorities release];
        self.imagePriorities = nil;
    }
    
    if (self.imageHeights != nil) {
        [self.imageHeights release];
        self.imageHeights = nil;
    }
    if (self.pseudoRandomNumbers != nil) {
        [self.pseudoRandomNumbers release];
        self.pseudoRandomNumbers = nil;
    }
    if (self.imagesCountingAvailable != nil) {
        [self.imagesCountingAvailable release];
        self.imagesCountingAvailable = nil;
    }
    
    if (self.imageLayout != nil) {
        [self.imageLayout release];
        self.imageLayout = nil;
    }
    
    if (self.searchResults != nil) {
        [self.searchResults release];
        self.searchResults = nil;
    }
    
    self.imageRanges = [NSMutableDictionary dictionary]; // for each priority/height combination in the search results, we need to know the 'range' (ie. first and last image), in order to speed searching later
    self.rowsInCurrentBlock = 3;
    self.rowsInPreviousBlock = 2;
    self.totalWidthUsed = 0;
    
    
    
    
    self.imagePriorities =  [NSArray arrayWithObjects:[NSNumber numberWithInt:3],[NSNumber numberWithInt:2],[NSNumber numberWithInt:1],[NSNumber numberWithInt:0], nil];  //[3, 2, 1, 0]; // all the possible image priority values, from the most to the least important
    self.imagePriorityIndex = 0;
    
    
    
    self.imageHeights =  [NSArray arrayWithObjects: [NSNumber numberWithFloat:181.0],[NSNumber numberWithFloat: 243.0], [NSNumber numberWithFloat: 368.0], [NSNumber numberWithFloat: 493.0], nil] ;//[181, 243, 368, 493]; // all the possible image heights, from smallest to largest
    self.imageHeightIndex = 0;
    
    
    
    self.pseudoRandomNumbers = [NSMutableArray arrayWithObjects:[NSNumber numberWithDouble:0.46528182671942275],[NSNumber numberWithDouble:0.6086116036657842],[NSNumber numberWithDouble:0.8087498911518594],[NSNumber numberWithDouble:0.9651020509099587],[NSNumber numberWithDouble:0.9973051245664357],[NSNumber numberWithDouble:0.4788800726192548],[NSNumber numberWithDouble:0.14105862284387916],[NSNumber numberWithDouble:0.4170338651530666],[NSNumber numberWithDouble:0.1568700039960672],[NSNumber numberWithDouble:0.19490288495387076],[NSNumber numberWithDouble:0.42609715123363756],[NSNumber numberWithDouble:0.16525019181338574],[NSNumber numberWithDouble:0.9681423274131673],[NSNumber numberWithDouble:0.19515572515153645],[NSNumber numberWithDouble:0.8591952927614569],[NSNumber numberWithDouble:0.41106402532047004],[NSNumber numberWithDouble:0.2509735348758656],[NSNumber numberWithDouble:0.1552192092112693],[NSNumber numberWithDouble:0.7058209902262059],[NSNumber numberWithDouble:0.7563102093032771],[NSNumber numberWithDouble:0.8702403159165614],[NSNumber numberWithDouble:0.25146400039806915],[NSNumber numberWithDouble:0.48523483353723973],[NSNumber numberWithDouble:0.03376772038297837],[NSNumber numberWithDouble:0.1730804695886906],[NSNumber numberWithDouble:0.4760373749636351],[NSNumber numberWithDouble:0.880267470298855],[NSNumber numberWithDouble:0.5871465137172949],[NSNumber numberWithDouble:0.7692538208175426],[NSNumber numberWithDouble:0.022155791043057982],[NSNumber numberWithDouble:0.96285739123048],[NSNumber numberWithDouble:0.537058143090162],[NSNumber numberWithDouble:0.17295454101472085],[NSNumber numberWithDouble:0.3753795607520214],[NSNumber numberWithDouble:0.9425579552465277],[NSNumber numberWithDouble:0.27813233250997926],[NSNumber numberWithDouble:0.8410111167008298],[NSNumber numberWithDouble:0.019238634942301602],[NSNumber numberWithDouble:0.2566836040700138],[NSNumber numberWithDouble:0.24890221496609544],[NSNumber numberWithDouble:0.5900169971688878],[NSNumber numberWithDouble:0.7401996453723709],[NSNumber numberWithDouble:0.24506634372629055],[NSNumber numberWithDouble:0.595210221535074],[NSNumber numberWithDouble:0.510938557797881],[NSNumber numberWithDouble:0.4952522767855486],[NSNumber numberWithDouble:0.2050048774951978],[NSNumber numberWithDouble:0.04731068112784764],[NSNumber numberWithDouble:0.3848415778837615],[NSNumber numberWithDouble:0.7607242730678508],[NSNumber numberWithDouble:0.9210416448327311],[NSNumber numberWithDouble:0.21296988112122828],[NSNumber numberWithDouble:0.7319692159919754],[NSNumber numberWithDouble:0.7623804239798945],[NSNumber numberWithDouble:0.8159603287438819],[NSNumber numberWithDouble:0.4234348210160843],[NSNumber numberWithDouble:0.25628665482969815],[NSNumber numberWithDouble:0.6245553273208275],[NSNumber numberWithDouble:0.3274046450229524],[NSNumber numberWithDouble:0.85996270890513],[NSNumber numberWithDouble:0.9837409952656273],[NSNumber numberWithDouble:0.27001591364739796],[NSNumber numberWithDouble:0.9787767666475191],[NSNumber numberWithDouble:0.3141592653589793], nil]; // pseudo-random numbers to replace Math.random while giving the same results every time
    
    self.pseudoRandomNumbersIndex = 0;
    blockIndex = -1;
    x = 0;
    y = 0;
    
    
    
    self.imagesCountingAvailable = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], @"h181",[NSNumber numberWithInt:0], @"h243",[NSNumber numberWithInt:0], @"h368",[NSNumber numberWithInt:0], @"h493",[NSNumber numberWithInt:3],@"priority", nil],@"p3",
                                    [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], @"h181",[NSNumber numberWithInt:0], @"h243",[NSNumber numberWithInt:0], @"h368",[NSNumber numberWithInt:0], @"h493",[NSNumber numberWithInt:2],@"priority", nil],@"p2",
                                    [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], @"h181",[NSNumber numberWithInt:0], @"h243",[NSNumber numberWithInt:0], @"h368",[NSNumber numberWithInt:0], @"h493",[NSNumber numberWithInt:1],@"priority", nil],@"p1",
                                    [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], @"h181",[NSNumber numberWithInt:0], @"h243",[NSNumber numberWithInt:0], @"h368",[NSNumber numberWithInt:0], @"h493",[NSNumber numberWithInt:0],@"priority", nil],@"p0",
                                    nil];
    
    
    
    self.imageLayout = [NSMutableArray array];
}



- (void) dealloc {
    if (self.imageRanges != nil) {
        //[self.imageRanges release];
        self.imageRanges = nil;
    }
    if (self.imagePriorities != nil) {
        //[self.imagePriorities release];
        self.imagePriorities = nil;
    }
    
    if (self.imageHeights != nil) {
        //[self.imageHeights release];
        self.imageHeights = nil;
    }
    if (self.pseudoRandomNumbers != nil) {
        //[self.pseudoRandomNumbers release];
        self.pseudoRandomNumbers = nil;
    }
    if (self.imagesCountingAvailable != nil) {
        //[self.imagesCountingAvailable release];
        self.imagesCountingAvailable = nil;
    }
    
    if (self.imageLayout != nil) {
        //[self.imageLayout release];
        self.imageLayout = nil;
    }
    
    if (self.searchResults != nil) {
        //[self.searchResults release];
        self.searchResults = nil;
    }
    [super dealloc];
}


- (NSArray *) getTiling:(NSArray *) data {
    
    //@synchronized(self) {
    
    [self internalInit];
    
    self.searchResults = [data retain];
    
    int countImagesAvailableNow = [self initCountImagesAvailable];
    //NSLog(@"imagesCountingAvailable == %@", self.imagesCountingAvailable);
    
    [self getImageRanges];
	self.totalWidthUsed = 0;
    
    //int countImagesAvailableNow = [self countImagesAvailable: undefined lowerPriorityAcceptable:false imageHeight:undefined tallerImageAcceptable: false];
    
    int countImagesAvailableBuffer = 0;
    while (countImagesAvailableNow != countImagesAvailableBuffer && !self.isCancel) { // keep building blocks as long as there are still images being used
		while (([self countImagesAvailable: [[self.imagePriorities objectAtIndex: self.imagePriorityIndex]intValue] lowerPriorityAcceptable:false imageHeight:undefined tallerImageAcceptable: false] == 0) && (self.imagePriorityIndex < self.imagePriorities.count - 1)) { self.imagePriorityIndex++;} // before building each block, check for available images of the current priority; if there are none, move to a lower priority that has images available
        [self buildBlock];
        countImagesAvailableBuffer = countImagesAvailableNow;
        countImagesAvailableNow = [self countImagesAvailable: undefined lowerPriorityAcceptable:false imageHeight:undefined tallerImageAcceptable: false];
    }
    //NSLog(@"total calling %d", self.call);
    //NSLog(@"imagesCountingAvailable == %@", self.imagesCountingAvailable);
    //then we need to tiling photo as array with correct position
    NSArray * tilingResult = [NSMutableArray array];
    self.blockIndex = 0;
    float rowHeight = 0;
    float internalBlockWidth = 0;
    self.x = imageBorderWidth;
    self.y = imageBorderWidth;
    float blockXPos = imageBorderWidth;
    int sequenceNumber = 0;
    bool hasRow = false;
    for (NSDictionary *block in self.imageLayout) {
        self.y = imageBorderWidth;
        self.x = blockXPos;
        internalBlockWidth = 0;
        for (int r = 0 ; r < [(NSMutableArray *)[block objectForKey:@"row"] count] ; r++) {
            hasRow = false;
            rowHeight = 0;
            NSDictionary *aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: r];
            for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                NSDictionary *image = (NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c];
                
                //NSLog(@"aa blockIndex == %d self.y == %d",blockIndex, self.y);
                
                [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: self.x] forKey:@"layoutX"];
                [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: self.y] forKey:@"layoutY"];
                [(NSMutableDictionary *)image setObject:[image objectForKey:@"width"] forKey:@"layoutW"];
                [(NSMutableDictionary *)image setObject:[image objectForKey:@"height"]  forKey:@"layoutH"];
                [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: blockIndex] forKey:@"blockIndex"];
                [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: sequenceNumber++] forKey:@"sequenceNumber"];
                [(NSMutableDictionary *)image setObject:[image objectForKey:@"priority"] forKey:@"priority"];
                
                //adding lat and long
                if ([image objectForKey:@"latitude"] && [image objectForKey:@"longitude"]) {
                    [(NSMutableDictionary *)image setObject:[image objectForKey:@"latitude"] forKey:@"latitude"];
                    [(NSMutableDictionary *)image setObject:[image objectForKey:@"longitude"] forKey:@"longitude"];
                }
                
                
                /*[photoModel setObject:[NSNumber numberWithInt:sequenceNumber] forKey:@"sequenceNumber"];
                [photoModel setObject: [photo objectForKey:@"left"] forKey:@"layoutX"];
                [photoModel setObject: [photo objectForKey:@"top"] forKey:@"layoutX"];
                [photoModel setObject: [photo objectForKey:@"width"] forKey:@"layoutW"];
                [photoModel setObject: [photo objectForKey:@"height"] forKey:@"layoutH"];
                [photoModel setObject: [photo objectForKey:@"priority"] forKey:@"priority"];
                 */
                
                [(NSMutableArray *) tilingResult addObject: image];
                
                rowHeight = MAX(rowHeight, [[image objectForKey:@"height"] floatValue]);
                
                self.x += [[image objectForKey:@"width"] floatValue] + imageBorderWidth * 2;
                
                //internalBlockWidth = MAX(internalBlockWidth,[[image objectForKey:@"width"] floatValue] + imageBorderWidth * 2);
                internalBlockWidth = MAX(internalBlockWidth,[[image objectForKey:@"width"] floatValue] + imageBorderWidth * 2);
                hasRow = true;
            }
            if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                //reset x pos
                self.x = blockXPos;
                if (hasRow)
                    self.y += rowHeight + imageBorderWidth * 2;
                
                for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                    NSDictionary *image = (NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c];
                   
                    //NSLog(@"blockIndex == %d self.y == %d",blockIndex, self.y);
                    
                    [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: self.x] forKey:@"layoutX"];
                    [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: self.y] forKey:@"layoutY"];
                    [(NSMutableDictionary *)image setObject:[image objectForKey:@"width"] forKey:@"layoutW"];
                    [(NSMutableDictionary *)image setObject:[image objectForKey:@"height"]  forKey:@"layoutH"];
                    [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: blockIndex] forKey:@"blockIndex"];
                    [(NSMutableDictionary *)image setObject:[NSNumber numberWithInt: sequenceNumber++] forKey:@"sequenceNumber"];
                    
                    //adding lat and long
                    if ([image objectForKey:@"latitude"] && [image objectForKey:@"longitude"]) {
                        [(NSMutableDictionary *)image setObject:[image objectForKey:@"latitude"] forKey:@"latitude"];
                        [(NSMutableDictionary *)image setObject:[image objectForKey:@"longitude"] forKey:@"longitude"];
                    }
                    
                    [(NSMutableArray *) tilingResult addObject: image];
                    
                    internalBlockWidth = MAX(internalBlockWidth,[[image objectForKey:@"width"] floatValue] + imageBorderWidth * 2);
                    
                    self.y += [[image objectForKey:@"height"] floatValue] + imageBorderWidth * 2;
                    
                }
            }else if (hasRow){
                //move to new row, increase y
                self.y += rowHeight + imageBorderWidth * 2;
            }
        }
        blockXPos += internalBlockWidth;
        blockIndex ++;
        if (blockIndex > 5) {
            //break;
        }
    }
    
    //release object
    if (self.imageRanges != nil) {
        //[self.imageRanges release];
        self.imageRanges = nil;
    }
    if (self.imagePriorities != nil) {
        //[self.imagePriorities release];
        self.imagePriorities = nil;
    }
    
    if (self.imageHeights != nil) {
        //[self.imageHeights release];
        self.imageHeights = nil;
    }
    if (self.pseudoRandomNumbers != nil) {
        //[self.pseudoRandomNumbers release];
        self.pseudoRandomNumbers = nil;
    }
    if (self.imagesCountingAvailable != nil) {
        //[self.imagesCountingAvailable release];
        self.imagesCountingAvailable = nil;
    }
    
    if (self.imageLayout != nil) {
        //[self.imageLayout release];
        self.imageLayout = nil;
    }
    
    if (self.searchResults != nil) {
        //[self.searchResults release];
        self.searchResults = nil;
    }
    
    return tilingResult;
    //};
}

- (void) cancelTiling {
    self.isCancel = true;
    if (self.imageRanges != nil) {
        //[self.imageRanges release];
        self.imageRanges = nil;
    }
    if (self.imagePriorities != nil) {
        //[self.imagePriorities release];
        self.imagePriorities = nil;
    }
    
    if (self.imageHeights != nil) {
        //[self.imageHeights release];
        self.imageHeights = nil;
    }
    if (self.pseudoRandomNumbers != nil) {
        //[self.pseudoRandomNumbers release];
        self.pseudoRandomNumbers = nil;
    }
    if (self.imagesCountingAvailable != nil) {
        //[self.imagesCountingAvailable release];
        self.imagesCountingAvailable = nil;
    }
    
    if (self.imageLayout != nil) {
        //[self.imageLayout release];
        self.imageLayout = nil;
    }
    
    if (self.searchResults != nil) {
        //[self.searchResults release];
        self.searchResults = nil;
    }
}

- (void)buildBlock {
	self.rowsInCurrentBlock = 2;
	int rowMerge = -1; // in a 3-row block, -1 means there will be no merge in this block, 0 means merge the top two rows; 1 means merge the bottom two
    int availableMergeImageIndex = undefined; // NEW: added this line // when there is going to be a merge, but very few maximum-height images are available, we will choose one of them and prevent it from being used on a normal-size top row
	BOOL topSplit = false; // whether to split the top row of a 2-row block
	BOOL bottomSplit = false; // whether to split the bottom row of a 2-row block
    
    if (self.rowsInPreviousBlock == 2) { // attempt to follow a 2-row block with a 3-row block
		if ([self countImagesAvailable: [[self.imagePriorities objectAtIndex: self.imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:1] floatValue] tallerImageAcceptable:false] >= 3) { // correct priority; 3 rows; random merge if possible
            //console.log("buildBlock() correct priority; 3 rows; random merge if possible");
			self.rowsInCurrentBlock = 3;
			if ([self countImagesAvailable:[[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable:false imageHeight:[[self.imageHeights objectAtIndex:3] floatValue] tallerImageAcceptable:false] >= 1) {
				if ([self getPseudoRandom] > 0.6f) {
					rowMerge = round([self getPseudoRandom]);
				}
			}
		} else if ([self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:1] floatValue] tallerImageAcceptable:false] >= 1 && ([self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:3] floatValue] tallerImageAcceptable:false] >= 1)) { // NEW: changed this line // correct priority; 3 rows; forced merge
            //console.log("buildBlock() correct priority; 3 rows; forced merge");
			self.rowsInCurrentBlock = 3;
			rowMerge = round([self getPseudoRandom]);
		} else if ([self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:3] floatValue] tallerImageAcceptable:false] >= 2) { // NEW: changed this line // correct priority; 3 rows; at least 1 vertical crop and 1 forced merge
            //console.log("buildBlock() correct priority; 3 rows; 1 vertical crop and 1 forced merge");
			self.rowsInCurrentBlock = 3;
			rowMerge = round([self getPseudoRandom]);
		} else if ([self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:1] floatValue] tallerImageAcceptable:true] >= 3) { // correct priority; 3 rows; vertical crop(s) if needed; random merge if possible
            //console.log("correct priority; 3 rows; vertical crop(s) if needed; random merge if possible");
			self.rowsInCurrentBlock = 3;
			if ([self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:3] floatValue] tallerImageAcceptable:false] >= 1) {
				if ([self getPseudoRandom] > 0.6f) {
					rowMerge = round([self getPseudoRandom]);
				}
			}
		}
        
        if (rowMerge > -1) { // NEW: added this if statement
			int mergeImagesAvailable = [self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:3] floatValue] tallerImageAcceptable:false];
			if (mergeImagesAvailable <= 2) {
				availableMergeImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable:false imageHeight:[[self.imageHeights objectAtIndex:3] floatValue] tallerImageAcceptable:false];
			}
		}
	}
    if (self.rowsInCurrentBlock == 2) { // if previous block had 3 rows, or we are unable to follow a 2-row block with a 3-row block, use 2 rows
		if ([self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:2] floatValue] tallerImageAcceptable:true] >= 2) { // correct priority; random splits if possible
			topSplit = (round([self getPseudoRandom] > 0.75));
			bottomSplit = (round([self getPseudoRandom] > 0.75));
		} else if ([self countImagesAvailable: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: [[self.imageHeights objectAtIndex:2] floatValue] tallerImageAcceptable:true] == 1) {
			topSplit = false;
			bottomSplit = true;
		} else { // forced split on both rows; looks like a 4-row block
			topSplit = true;
			bottomSplit = true;
		}
	}
    //NSLog(@"buildBlock() rowsInCurrentBlock: %d - rowsInPreviousBlock == %d, rowMerge == %d ",rowsInCurrentBlock,rowsInPreviousBlock, rowMerge);
    //console.log("buildBlock() rowsInCurrentBlock: " + rowsInCurrentBlock);
    //console.log("buildBlock() rowMerge: " + rowMerge);
    //console.log("buildBlock() topSplit: " + topSplit);
    //console.log("buildBlock() bottomSplit: " + bottomSplit);
    
    /*
	var thumbs = $("div.thumbs");
	thumbs.append("<div class=\"block\"></div>");
	var block = $("div.thumbs div.block").last();
    */
    NSDictionary *block = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSMutableArray array], @"row", nil];
    
    
    
	NSArray* rowWidths = [NSMutableArray array];
	float minRowWidth = undefined;
	float maxRowWidth = undefined;
	float rowHeight;
	float splitHeight;
	int mergeImageIndex = undefined; // remember which images are used in a block containing a merge, for use later in deciding whether the layout benefits from having 2 images on the smaller row
	int nonMergeFirstImageIndex = undefined;
	int nonMergeSecondImageIndex = undefined;
    
    
	for (int rowIndex=0; rowIndex < self.rowsInCurrentBlock; rowIndex++) {
        if (rowIndex == rowMerge) { // top row of 2 that are merged; build a double-height row
            rowHeight = [[self.imageHeights objectAtIndex:3] floatValue];
		} else if ((rowMerge > -1) && (rowIndex == rowMerge + 1)) { // bottom row of 2 that are merged; don't do anything here
            [(NSMutableArray *)rowWidths addObject: [NSNumber numberWithInt:undefined]];
			continue;
		} else if (((rowIndex == 0) && (topSplit)) || ((rowIndex == 1) && (bottomSplit))) { // split row
			rowHeight = [[self.imageHeights objectAtIndex:2] floatValue]; // this is the height of the entire row, not of each image in the split
			splitHeight = [[self.imageHeights objectAtIndex:0] floatValue];
		} else {
			rowHeight = (self.rowsInCurrentBlock == 3 ? [[self.imageHeights objectAtIndex:1] floatValue] : [[self.imageHeights objectAtIndex:2] floatValue]);
		}
		//TODO: append row then we need to increase row index
        /*block.append("<div class=\"row\"></div>");
		var row = block.children().last();*/
        NSDictionary *row = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSMutableArray array],@"col", nil];
        //thumbRowIndex++;
        
		int firstImageIndex = undefined; // select image(s)
		int secondImageIndex = undefined;
		
		if ((rowMerge > -1) && ((rowIndex == rowMerge - 1) || (rowIndex == rowMerge + 2))) { // row above or below a merge; attempt to fit in 2 images side-by-side
            BOOL usedBuffer = NO;
            if ((minRowWidth == undefined) && (availableMergeImageIndex != undefined)) { // NEW: added this if statement // if a maximum-height image has been chosen for a merge, temporarily set it as used so that it will not get picked for use in this normal-size row...
                usedBuffer = [[(NSMutableDictionary*)[self.searchResults objectAtIndex: availableMergeImageIndex] objectForKey:@"used"] boolValue];// searchResults[availableMergeImageIndex].used;
                [self setImageUsed: availableMergeImageIndex imageUsed: TRUE];
			}
            
			firstImageIndex = [self getImageByWidth:0 imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable:false]; // start with narrowest available image, to increase the chance of getting 2 in there
			if (firstImageIndex == undefined) {firstImageIndex = [self getImageByWidth:0 imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: true];}
			if (firstImageIndex == undefined) {firstImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: rowHeight tallerImageAcceptable: true] ;}
			if (firstImageIndex != undefined) {
				nonMergeFirstImageIndex = firstImageIndex;
				[self setImageUsed: firstImageIndex imageUsed: true];
				if (minRowWidth == undefined) { // above the merge; use narrowest available image, to increase the chance of getting 2 in there
					secondImageIndex = [self getImageByWidth:0 imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: false];
					if (secondImageIndex == undefined) {secondImageIndex = [self getImageByWidth:0 imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: true];}
					if (secondImageIndex == undefined) {secondImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: rowHeight tallerImageAcceptable: true];}
				} else { // below the merge; attempt to fit the available width
					secondImageIndex = [self getImageByWidth: minRowWidth -  [[(NSDictionary *)[searchResults objectAtIndex:firstImageIndex] objectForKey:@"width"] floatValue] imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: false];
                    if (secondImageIndex == undefined) {secondImageIndex = [self getImageByWidth: minRowWidth -  [[(NSDictionary *)[searchResults objectAtIndex:firstImageIndex] objectForKey:@"width"] floatValue] imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: true];}
                    if (secondImageIndex == undefined) {secondImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: rowHeight tallerImageAcceptable: true];}
				}
				nonMergeSecondImageIndex = secondImageIndex;
			}
            
            if ((minRowWidth == undefined) && (availableMergeImageIndex != undefined)) { // NEW: added this if statement // ...and then, put the availability of the maximum-height merge image back to whatever it had been before
				[self setImageUsed: availableMergeImageIndex imageUsed: usedBuffer];
			}
            
		} else if ((rowIndex == 0) && (topSplit)) { // split top row
			firstImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: splitHeight tallerImageAcceptable: false];
			if (firstImageIndex == undefined) {firstImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: splitHeight tallerImageAcceptable: true];}
			if (firstImageIndex != undefined) {
				[self setImageUsed:firstImageIndex imageUsed: true];
				secondImageIndex = [self getImageByWidth: [[(NSDictionary *)[searchResults objectAtIndex:firstImageIndex] objectForKey:@"width"] floatValue] imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: splitHeight tallerImageAcceptable: false];
				if (secondImageIndex == undefined) {secondImageIndex = [self getImageByWidth: [[(NSDictionary *)[searchResults objectAtIndex:firstImageIndex] objectForKey:@"width"] floatValue] imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: splitHeight tallerImageAcceptable: true];}
				if (secondImageIndex == undefined) {secondImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: splitHeight tallerImageAcceptable: true];}
			}
            
		} else if ((rowIndex == 1) && (bottomSplit)) { // split bottom row
            if (minRowWidth != undefined) {
                firstImageIndex = [self getImageByWidth: minRowWidth imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: splitHeight tallerImageAcceptable: false];
                if (firstImageIndex == undefined) {firstImageIndex = [self getImageByWidth: minRowWidth imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: splitHeight tallerImageAcceptable: true];}
				if (firstImageIndex == undefined) {firstImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: splitHeight tallerImageAcceptable: true];}
				if (firstImageIndex != undefined) {
					[self setImageUsed:firstImageIndex imageUsed:true];
					secondImageIndex = [self getImageByWidth: minRowWidth imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: splitHeight tallerImageAcceptable: false];
					if (secondImageIndex == undefined) {secondImageIndex = [self getImageByWidth: minRowWidth imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: splitHeight tallerImageAcceptable: true];}
					if (secondImageIndex == undefined) {secondImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: splitHeight tallerImageAcceptable: true];}
                }
			}
            
		} else if (rowIndex == 0) { // normal or merged top row
			firstImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: false];
			if (firstImageIndex == undefined) {firstImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: rowHeight tallerImageAcceptable: true];}
			if (rowIndex == rowMerge) {mergeImageIndex = firstImageIndex;}
            
		} else if (minRowWidth != undefined) { // normal or merged non-top row
            if (minRowWidth != undefined) {
                firstImageIndex = [self getImageByWidth: minRowWidth imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: false];
                if (firstImageIndex == undefined) {firstImageIndex = [self getImageByWidth: minRowWidth imagePriority: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: false imageHeight: rowHeight tallerImageAcceptable: true];}
                if (firstImageIndex == undefined) {firstImageIndex = [self getRandomImage: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue] lowerPriorityAcceptable: true imageHeight: rowHeight tallerImageAcceptable: true];}
                if (rowIndex == rowMerge) {mergeImageIndex = firstImageIndex;}
			}
		}
        
        if ((rowMerge > -1) && ((rowIndex == rowMerge - 1) || (rowIndex == rowMerge + 2))) { // write 2 images side-by-side (above or below a merge)
            float rowWidth = -1;
			if (firstImageIndex != undefined) {
                //TODO
                //row.append(getThumbHTML(firstImageIndex, searchResults[firstImageIndex].width, rowHeight));
                [(NSMutableArray *)[row objectForKey:@"col"] addObject: [self getThumbHTML: firstImageIndex imageWidth: [[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue] imageHeight: rowHeight]];
                
                rowWidth = [[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue];
			}
			if (secondImageIndex != undefined) {
                //TODO
                //row.append(getThumbHTML(secondImageIndex, searchResults[secondImageIndex].width, rowHeight));
                [(NSMutableArray *)[row objectForKey:@"col"] addObject: [self getThumbHTML: secondImageIndex imageWidth: [[(NSDictionary *)[self.searchResults objectAtIndex: secondImageIndex] objectForKey:@"width"] floatValue] imageHeight: rowHeight]];
                
                [self setImageUsed: secondImageIndex imageUsed: true];
				rowWidth += [[(NSDictionary *)[self.searchResults objectAtIndex: secondImageIndex] objectForKey:@"width"] floatValue];
			}
			[(NSMutableArray *)rowWidths addObject:[ NSNumber numberWithFloat: rowWidth]];
            if ((rowWidth > -1) && ((rowWidth < minRowWidth) || (minRowWidth == undefined))) {minRowWidth = rowWidth;}
            if ((rowWidth > -1) && ((rowWidth > maxRowWidth) || (maxRowWidth == undefined))) {maxRowWidth = rowWidth;}
            
		} else if (((rowIndex == 0) && (topSplit)) || ((rowIndex == 1) && (bottomSplit))) { // write 2 images stacked vertically (split)
            //TODO
            //row.append("<div class=\"split\"></div>");
            //var split = row.children().last();
            [(NSMutableDictionary *) row setObject:[NSMutableArray array] forKey:@"split"];
            //thumbColIndex = 0;
			float rowWidth = -1;
			if (firstImageIndex != undefined) {
                //TODO
                //split.append(getThumbHTML(firstImageIndex, searchResults[firstImageIndex].width, splitHeight));
                [(NSMutableArray *)[row objectForKey:@"split"] addObject: [self getThumbHTML: firstImageIndex imageWidth: [[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue] imageHeight: splitHeight]];
                
				rowWidth = [[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue];
			}
			if (secondImageIndex != undefined) {
                //TODO
                //split.append(getThumbHTML(secondImageIndex, searchResults[secondImageIndex].width, splitHeight));
                [(NSMutableArray *)[row objectForKey:@"split"] addObject: [self getThumbHTML: secondImageIndex imageWidth: [[(NSDictionary *)[self.searchResults objectAtIndex: secondImageIndex] objectForKey:@"width"] floatValue] imageHeight: splitHeight]];
                [self setImageUsed: secondImageIndex imageUsed:true];
				if ( [[(NSDictionary *)[self.searchResults objectAtIndex: secondImageIndex] objectForKey:@"width"] floatValue] < rowWidth) {rowWidth = [[(NSDictionary *)[self.searchResults objectAtIndex: secondImageIndex] objectForKey:@"width"] floatValue];}
			}
			[(NSMutableArray *)rowWidths addObject:[ NSNumber numberWithFloat: rowWidth]];
            if ((rowWidth > -1) && ((rowWidth < minRowWidth) || (minRowWidth == undefined))) {minRowWidth = rowWidth;}
            if ((rowWidth > -1) && ((rowWidth > maxRowWidth) || (maxRowWidth == undefined))) {maxRowWidth = rowWidth;}
            
		} else if (firstImageIndex != undefined) { // write single image
			//TODO
            //row.append(getThumbHTML(firstImageIndex, searchResults[firstImageIndex].width, rowHeight));
            [(NSMutableArray *)[row objectForKey:@"col"] addObject: [self getThumbHTML: firstImageIndex imageWidth: [[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue] imageHeight: rowHeight]];
            
			[self setImageUsed: firstImageIndex imageUsed: true];
			[(NSMutableArray *)rowWidths addObject: [(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"]];
            if (([[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue] < minRowWidth) || (minRowWidth == undefined)) {minRowWidth = [[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue];}
			if (([[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue] > maxRowWidth) || (maxRowWidth == undefined)) {maxRowWidth = [[(NSDictionary *)[self.searchResults objectAtIndex: firstImageIndex] objectForKey:@"width"] floatValue];}
		}
        //end row add to block
        [(NSMutableArray *)[block objectForKey:@"row"] addObject: row];
    }//loop row
    
    //console.log("buildBlock() rowWidths: " + rowWidths + ", minRowWidth: " + minRowWidth + ", maxRowWidth: " + maxRowWidth + ", mergeImageIndex: " + mergeImageIndex + ", nonMergeFirstImageIndex: " + nonMergeFirstImageIndex + ", nonMergeSecondImageIndex: " + nonMergeSecondImageIndex);
    
	if (rowMerge > -1 ) { // if there has been a merge, check whether we can improve the layout by deleting one of the images on the non-merged row
        float bothImagesWidthDifference = 0;
        float firstImageWidthDifference = 0;
        float secondImageWidthDifference = 0;
        if ((mergeImageIndex != undefined) && (nonMergeFirstImageIndex != undefined) && (nonMergeSecondImageIndex != undefined)) { // only attempt this mode of layout adjustment if the block contains all 3 of the images that it has room for (ie. the supply of images did not run out mid-block)
            bothImagesWidthDifference = ABS([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] - [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] - [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue]);
            firstImageWidthDifference = ABS([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] - [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue]);
            secondImageWidthDifference = ABS([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] - [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue]);
		}
        if ((firstImageWidthDifference < bothImagesWidthDifference) && (firstImageWidthDifference < secondImageWidthDifference)) { // first image on its own fits best; remove second
			//TODO
            //block.find(".row").eq(rowMerge == 0 ? 1 : 0).find(".image").eq(1).remove();
            [(NSMutableArray *)[(NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: rowMerge == 0 ? 1 : 0] objectForKey:@"col"] removeObjectAtIndex:1];
			[self setImageUsed: nonMergeSecondImageIndex imageUsed: false];
			//TODO
            //block.find("img").css({width: Math.min(searchResults[mergeImageIndex].width, searchResults[nonMergeFirstImageIndex].width)});
            //need to fix width of all image in current block
            for (int r = 0 ; r < [(NSMutableArray *)[block objectForKey:@"row"] count] ; r++) {
                NSDictionary *aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: r];
                for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                    float newWidth = MIN([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] , [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue]);
                    [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                }
                if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                    for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                        float newWidth = MIN([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] , [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue]);
                        [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                    }
                }
            }
            
		}	else if ((secondImageWidthDifference < bothImagesWidthDifference) && (secondImageWidthDifference < firstImageWidthDifference)) { // second image on its own fits best; remove first
			//TODO
            //block.find(".row").eq(rowMerge == 0 ? 1 : 0).find(".image").eq(0).remove();
            [(NSMutableArray *)[(NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: rowMerge == 0 ? 1 : 0] objectForKey:@"col"] removeObjectAtIndex:0];
			[self setImageUsed:nonMergeFirstImageIndex imageUsed:false];
            //TODO
			//block.find("img").css({width: Math.min(searchResults[mergeImageIndex].width, searchResults[nonMergeSecondImageIndex].width)});
            for (int r = 0 ; r < [(NSMutableArray *)[block objectForKey:@"row"] count] ; r++) {
                NSDictionary *aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: r];
                for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                    float newWidth = MIN([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] , [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue]);
                    [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                }
                if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                    for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                        float newWidth = MIN([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] , [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue]);
                        [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                    }
                }
            }
		} else { // keep both images; adjust layout to remove empty space
            if ((mergeImageIndex != undefined) && (nonMergeFirstImageIndex != undefined) && (nonMergeSecondImageIndex != undefined)) { // only attempt this mode of layout adjustment if the block contains all 3 of the images that it has room for (ie. the supply of images did not run out mid-block)
				if ([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] >  [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] + [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue]) { // crop large image to match small
					//TODO
                    //block.find(".row").eq(rowMerge == 0 ? 0 : 1).find("img").css({width: searchResults[nonMergeFirstImageIndex].width + searchResults[nonMergeSecondImageIndex].width + (imageBorderWidth * 2)});
                    NSDictionary *aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: rowMerge == 0 ? 0 : 1];
                    for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                        float newWidth = [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] + [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue] + (imageBorderWidth * 2);
                        [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                    }
                    if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                        for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                            float newWidth = [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] + [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue] + (imageBorderWidth * 2);
                            [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                        }
                    }
				} else { // crop small images to match large
					//TODO
                    //block.find(".row").eq(rowMerge == 0 ? 1 : 0).find("img").eq(0).css({width: (searchResults[nonMergeFirstImageIndex].width * (searchResults[mergeImageIndex].width / (searchResults[nonMergeFirstImageIndex].width + searchResults[nonMergeSecondImageIndex].width)) - imageBorderWidth)});
                    NSDictionary *aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: rowMerge == 0 ? 1 : 0];
                    for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                        float newWidth = ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] * ([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] / ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue]  + [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue])) - imageBorderWidth);
                        [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                        break;
                    }
                    if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                        for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                            float newWidth = ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] * ([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] / ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue]  + [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue])) - imageBorderWidth);
                            [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                            break;
                        }
                    }
					//TODO
                    //block.find(".row").eq(rowMerge == 0 ? 1 : 0).find("img").eq(1).css({width: (searchResults[nonMergeSecondImageIndex].width * (searchResults[mergeImageIndex].width / (searchResults[nonMergeFirstImageIndex].width + searchResults[nonMergeSecondImageIndex].width)) - imageBorderWidth)});
                    
                    aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: rowMerge == 0 ? 1 : 0];
                    for (int c = 1 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                        float newWidth = ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue] * ([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] / ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] + [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue])) - imageBorderWidth);
                        [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                        break;
                    }
                    if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                        for (int c = 1 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                            float newWidth = ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue] * ([[(NSDictionary *)[self.searchResults objectAtIndex: mergeImageIndex] objectForKey:@"width"] floatValue] / ([[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeFirstImageIndex] objectForKey:@"width"] floatValue] + [[(NSDictionary *)[self.searchResults objectAtIndex: nonMergeSecondImageIndex] objectForKey:@"width"] floatValue])) - imageBorderWidth);
                            [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                            break;
                        }
                    }
                    
				}
			} else if ((mergeImageIndex != undefined) && ((nonMergeFirstImageIndex != undefined) || (nonMergeSecondImageIndex != undefined)) && (minRowWidth != undefined)) { // block is not full; just do the best we can with it
                //TODO
                //block.find("img").css({width: minRowWidth});
                for (int r = 0 ; r < [(NSMutableArray *)[block objectForKey:@"row"] count] ; r++) {
                    NSDictionary *aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: r];
                    for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                        float newWidth = minRowWidth;
                        [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                    }
                    if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                        for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                            float newWidth = minRowWidth;
                            [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                        }
                    }
                }
			}
		}
        
	} else { // no merge; just set all images in the block to the width of the smallest one
		//TODO
        //block.find("img").css({width: minRowWidth});
        for (int r = 0 ; r < [(NSMutableArray *)[block objectForKey:@"row"] count] ; r++) {
            NSDictionary *aRow = (NSMutableDictionary *)[(NSMutableArray *)[block objectForKey:@"row"] objectAtIndex: r];
            for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"col"] count] ; c++) {
                float newWidth = minRowWidth;
                [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"col"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
            }
            
            if (![[aRow objectForKey:@"split"] isEqual: [NSNull null]]) {
                for (int c = 0 ; c < [(NSMutableArray *)[aRow objectForKey:@"split"] count] ; c++) {
                    float newWidth = minRowWidth;
                    [(NSMutableDictionary *)[(NSMutableArray *)[aRow objectForKey:@"split"] objectAtIndex:c] setObject:[NSNumber numberWithFloat: newWidth] forKey:@"width"];
                }
            }
        }
	}
    [(NSMutableArray *) self.imageLayout addObject: block];

	if (minRowWidth != undefined) {self.totalWidthUsed += minRowWidth;}
    //console.log("buildBlock() totalWidthUsed: " + totalWidthUsed);
    self.rowsInPreviousBlock = self.rowsInCurrentBlock; // remember how many rows were used in this block, so that we can try to avoid 2 consecutive blocks of the same row count
}

-(NSDictionary *) getThumbHTML:(int)imageIndex imageWidth:(float)imageWidth imageHeight:(float)imageHeight { // generate the html for displaying an image in the page
    NSMutableDictionary *imageData = [NSMutableDictionary dictionary];
    if ((imageIndex != undefined) && (imageIndex < self.searchResults.count)) {
        //float backgroundLeft = imageBorderWidth - (([[(NSDictionary *)[self.searchResults objectAtIndex: imageIndex] objectForKey:@"width"] floatValue] - imageWidth) * 0.5);
        //if (backgroundLeft > imageBorderWidth) {backgroundLeft = imageBorderWidth;}
        //float backgroundTop = imageBorderWidth - (([[(NSDictionary *)[self.searchResults objectAtIndex: imageIndex] objectForKey:@"height"] floatValue] - imageHeight) * 0.66);
        
        NSDictionary *tempItem = (NSDictionary *)[self.searchResults objectAtIndex: imageIndex];
        
        /*if ([[tempItem objectForKey:@"filename"] isEqual: [NSNull null]]) {
            return nil;
        }*/
        
        
        imageData = [NSMutableDictionary dictionaryWithDictionary: tempItem];
        
        if ([tempItem objectForKey:@"filename"] == nil) {
            imageData = [NSMutableDictionary dictionary];
            
            [imageData setObject: [NSNumber numberWithInt: -1] forKey:@"imageIndex"];
            [imageData setObject: @"" forKey:@"filename"];
            [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"left"];
            [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"top"];
            [imageData setObject: [NSNumber numberWithInt: imageWidth] forKey:@"width"];
            [imageData setObject: [NSNumber numberWithInt: imageHeight] forKey:@"height"];
            [imageData setObject: [NSNumber numberWithInt: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue]] forKey:@"priority"];
            
            return imageData;
        }
        
        [imageData setObject: [NSNumber numberWithInt: imageIndex] forKey:@"imageIndex"];
        [imageData setObject: [tempItem objectForKey:@"filename"] forKey:@"filename"];
        [imageData setObject: [tempItem objectForKey:@"thumbnail"] forKey:@"thumbnail"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"left"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"top"];
        [imageData setObject: [NSNumber numberWithFloat: imageWidth] forKey:@"width"];
        [imageData setObject: [NSNumber numberWithFloat: imageHeight] forKey:@"height"];
        [imageData setObject: [tempItem objectForKey:@"priority"] forKey:@"priority"];
        
        //thumbHTML = "<div class=\"image\" style=\"background-image:url('" + imagesFolder + "/" + searchResults[imageIndex].filename + "'); background-position:" + backgroundLeft + "px " + backgroundTop + "px; background-repeat:no-repeat; opacity:" + ((searchResults[imageIndex].priority + 1) * 0.25) + "\"><img src=\"images/priority" + searchResults[imageIndex].priority + ".png\" width=\"" + imageWidth + "\" height = \"" + Math.round(imageHeight) + "\" alt=\"\" /></div>";
    } else {
        [imageData setObject: [NSNumber numberWithInt: -1] forKey:@"imageIndex"];
        [imageData setObject: @"" forKey:@"filename"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"left"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"top"];
        [imageData setObject: [NSNumber numberWithInt: imageWidth] forKey:@"width"];
        [imageData setObject: [NSNumber numberWithInt: imageHeight] forKey:@"height"];
        [imageData setObject: [NSNumber numberWithInt: [[self.imagePriorities objectAtIndex:imagePriorityIndex] intValue]] forKey:@"priority"];
        //thumbHTML = "<div class=\"image\"><img src=\"images/spacer.png\" width=\"" + imageWidth + "\" height = \"" + Math.round(imageHeight) + "\" alt=\"\" /></div>";
    }
    return imageData;
}

-(int) getRandomImage:(int)imagePriority lowerPriorityAcceptable:(BOOL)lowerPriorityAcceptable imageHeight:(float)imageHeight tallerImageAcceptable:(BOOL)tallerImageAcceptable { // select an image randomly from the search results
    //console.log("getRandomImage() imagePriority: " + imagePriority + ", lowerPriorityAcceptable: " + lowerPriorityAcceptable + ", imageHeight: " + imageHeight + ", tallerImageAcceptable: " + tallerImageAcceptable);
	int foundImageIndex = undefined;
	int startImagePriorityIndex = [self getImagePriorityIndex: imagePriority];
	int endImagePriorityIndex = (lowerPriorityAcceptable ? self.imagePriorities.count - 1 : startImagePriorityIndex);
	int startImageHeightIndex = [self getImageHeightIndex:imageHeight];
	int endImageHeightIndex = (tallerImageAcceptable ? self.imageHeights.count - 1 : startImageHeightIndex);
    
	for (int searchImagePriorityIndex=startImagePriorityIndex; searchImagePriorityIndex<=endImagePriorityIndex; searchImagePriorityIndex++) {
        //console.log("getRandomImage() searchImagePriorityIndex: " + searchImagePriorityIndex);
		for (int searchImageHeightIndex=startImageHeightIndex; searchImageHeightIndex<=endImageHeightIndex; searchImageHeightIndex++) {
            //console.log("getRandomImage() searchImageHeightIndex: " + searchImageHeightIndex);
            NSDictionary *tObj = [(NSArray *)[(NSDictionary *)[(NSArray *)[self.imageRanges objectForKey:@"priorities"] objectAtIndex: searchImagePriorityIndex] objectForKey:@"heights"] objectAtIndex: searchImageHeightIndex];
			int firstImageIndex = [[tObj objectForKey: @"firstImageIndex"] intValue];
			int lastImageIndex = [[tObj objectForKey: @"lastImageIndex"] intValue];
			if (firstImageIndex > -1) {
				int startImageIndex = firstImageIndex + round([self getPseudoRandom] * (lastImageIndex - firstImageIndex));
				int searchImageIncrement = ([self getPseudoRandom] >= 0.5 ? 1 : -1);
                //console.log("    getRandomImage() firstImageIndex: " + firstImageIndex);
                //console.log("    getRandomImage() lastImageIndex: " + lastImageIndex);
                //console.log("    getRandomImage() startImageIndex: " + startImageIndex);
                //console.log("    getRandomImage() searchImageIncrement: " + searchImageIncrement);
				for (int searchImageIndex=startImageIndex; searchImageIndex>=firstImageIndex&&searchImageIndex<=lastImageIndex; searchImageIndex+=searchImageIncrement) { // search from a random point in a random direction
                    //console.log("        getRandomImage() searchImageIndex: " + searchImageIndex);
					if (! [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"used"] boolValue]) {
						foundImageIndex = searchImageIndex;
                        //console.log("            getRandomImage() foundImageIndex: " + foundImageIndex);
						break;
					}
				}
				if (foundImageIndex == undefined) { // if no available images found, search in opposite direction
					searchImageIncrement = -searchImageIncrement;
                    //console.log("    getRandomImage() searchImageIncrement: " + searchImageIncrement);
					for (int searchImageIndex=startImageIndex; searchImageIndex>=firstImageIndex&&searchImageIndex<=lastImageIndex; searchImageIndex+=searchImageIncrement) {
                        //console.log("        getRandomImage() searchImageIndex: " + searchImageIndex);
						if (! [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"used"] boolValue]) {
							foundImageIndex = searchImageIndex;
                            //console.log("            getRandomImage() foundImageIndex: " + foundImageIndex);
							break;
						}
					}
				}
			}
            if (foundImageIndex != undefined) break;
		}
		if (foundImageIndex != undefined) break;
	}
	return foundImageIndex;
}



-(int)getImageByWidth:(float)imageWidth imagePriority:(int)imagePriority lowerPriorityAcceptable:(BOOL)lowerPriorityAcceptable imageHeight:(float)imageHeight tallerImageAcceptable:(BOOL) tallerImageAcceptable { // select an image from the search results by width, in order to fill a specific amount of screen space as closely as possible
    //console.log("getImageByWidth() imageWidth: " + imageWidth + ", imagePriority: " + imagePriority + ", lowerPriorityAcceptable: " + lowerPriorityAcceptable + ", imageHeight: " + imageHeight + ", tallerImageAcceptable: " + tallerImageAcceptable);
	int foundImageIndex = undefined;
	int foundImageIndexSamePriority = undefined;
	int foundImageIndexSameHeight = undefined;
	int startImagePriorityIndex = [self getImagePriorityIndex: imagePriority];
	int endImagePriorityIndex = (lowerPriorityAcceptable ? self.imagePriorities.count - 1 : startImagePriorityIndex);
	int startImageHeightIndex = [self getImageHeightIndex: imageHeight];
	int endImageHeightIndex = (tallerImageAcceptable ? self.imageHeights.count - 1 : startImageHeightIndex);
    float widthDifference = 0.0f;
	float minWidthDifference = undefined;
	float minWidthDifferenceSamePriority = undefined;
	float minWidthDifferenceSameHeight = undefined;
    
	for (int searchImagePriorityIndex=startImagePriorityIndex; searchImagePriorityIndex<=endImagePriorityIndex; searchImagePriorityIndex++) {
        //console.log("getImageByWidth() searchImagePriorityIndex: " + searchImagePriorityIndex);
		for (int searchImageHeightIndex=startImageHeightIndex; searchImageHeightIndex<=endImageHeightIndex; searchImageHeightIndex++) {
            //console.log("getImageByWidth() searchImageHeightIndex: " + searchImageHeightIndex);
            NSDictionary *tObj = [(NSArray *)[(NSDictionary *)[(NSArray *)[self.imageRanges objectForKey:@"priorities"] objectAtIndex: searchImagePriorityIndex] objectForKey:@"heights"] objectAtIndex: searchImageHeightIndex];
			int firstImageIndex = [[tObj objectForKey: @"firstImageIndex"] intValue];
			int lastImageIndex = [[tObj objectForKey: @"lastImageIndex"] intValue];
			if (firstImageIndex > -1) {
                //console.log("    getImageByWidth() firstImageIndex: " + firstImageIndex);
                //console.log("    getImageByWidth() lastImageIndex: " + lastImageIndex);
				for (int searchImageIndex=firstImageIndex; searchImageIndex<=lastImageIndex; searchImageIndex++) {
                    //console.log("        getImageByWidth() searchImageIndex: " + searchImageIndex);
					if (! [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"used"] boolValue]) {
						widthDifference = ABS(imageWidth - [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"width"] floatValue]);
                        //console.log("            getImageByWidth() widthDifference: " + widthDifference);
						if ((widthDifference < minWidthDifference) || (minWidthDifference == undefined)) {
							minWidthDifference = widthDifference;
							foundImageIndex = searchImageIndex;
                            //console.log("            getImageByWidth() foundImageIndex: " + foundImageIndex);
							if ([[self.imagePriorities objectAtIndex:searchImagePriorityIndex] intValue] == imagePriority) {
								minWidthDifferenceSamePriority = widthDifference;
								foundImageIndexSamePriority = searchImageIndex;
                                //console.log("            getImageByWidth() foundImageIndexSamePriority: " + foundImageIndexSamePriority);
                            }
							if ([[self.imageHeights objectAtIndex:searchImageHeightIndex] floatValue] == imageHeight) {
								minWidthDifferenceSameHeight = widthDifference;
								foundImageIndexSameHeight = searchImageIndex;
                                //console.log("            getImageByWidth() foundImageIndexSameHeight: " + foundImageIndexSameHeight);
							}
						}
					}
				}
			}
		}
	}
    //console.log("getImageByWidth() foundImageIndexSamePriority: " + foundImageIndexSamePriority);
    //console.log("getImageByWidth() foundImageIndexSameHeight: " + foundImageIndexSameHeight);
    //console.log("getImageByWidth() foundImageIndex: " + foundImageIndex);
    if (foundImageIndexSamePriority != undefined) {
        return foundImageIndexSamePriority;
	} else if (foundImageIndexSameHeight != undefined) {
        return foundImageIndexSameHeight;
	} else {
        return foundImageIndex;
	}
}


- (void) getImageRanges { // for each priority/height combination in the search results, we need to know the 'range' (ie. first and last image), in order to speed searching later
	//imageRanges = {priorities: []};
    [(NSMutableDictionary *)self.imageRanges setObject:[NSMutableArray array] forKey: @"priorities"];
	for (imagePriorityIndex=0; imagePriorityIndex < self.imagePriorities.count; imagePriorityIndex++) {
		
        NSDictionary *obj = [NSMutableDictionary dictionaryWithObjectsAndKeys: [imagePriorities objectAtIndex:imagePriorityIndex], @"priority", [NSMutableArray array], @"heights" , nil];
        [(NSMutableArray *)[imageRanges objectForKey:@"priorities"] addObject: obj];
        //imageRanges.priorities.push({priority: imagePriorities[imagePriorityIndex], heights: []});
        
		for (imageHeightIndex=0; imageHeightIndex < self.imageHeights.count; imageHeightIndex++) {
            NSDictionary *tempObj = [NSMutableDictionary dictionaryWithObjectsAndKeys: [self.imageHeights objectAtIndex:imageHeightIndex], @"height", [NSNumber numberWithInt: undefined], @"firstImageIndex" , [NSNumber numberWithInt: undefined], @"lastImageIndex", nil];
            [(NSMutableArray *)[(NSDictionary *)[(NSArray *)[imageRanges objectForKey:@"priorities"] objectAtIndex: imagePriorityIndex] objectForKey:@"heights"] addObject: tempObj];
            
            //imageRanges.priorities[imagePriorityIndex].heights.push({height:imageHeights[imageHeightIndex], firstImageIndex: -1, lastImageIndex: -1/*, minWidth: 0, maxWidth: 0, averageAspectRatio: -1, available: 0*/});
		}
	}
    
	if (self.searchResults.count > 0) {
        
        //NSLog(@"self.searchResults %@", self.searchResults);
        
        NSDictionary *firstItem = (NSDictionary *)[self.searchResults objectAtIndex:0];
		imagePriorityIndex = [self getImagePriorityIndex: [[firstItem objectForKey:@"priority"] intValue]];
		imageHeightIndex = [self getImageHeightIndex: [[firstItem objectForKey:@"height"] floatValue]];
        self.call++;
		for (int searchImageIndex=0; searchImageIndex < searchResults.count; searchImageIndex++) {
            NSDictionary *thisItem = (NSDictionary *)[self.searchResults objectAtIndex: searchImageIndex];
			if ([[thisItem objectForKey:@"priority"] intValue] != [[imagePriorities objectAtIndex:imagePriorityIndex] intValue]) {
				imagePriorityIndex = [self getImagePriorityIndex: [[thisItem objectForKey:@"priority"] intValue]];
			}
			if ([[thisItem objectForKey:@"height"] floatValue] != [[self.imageHeights objectAtIndex:imageHeightIndex] floatValue]) {
				imageHeightIndex = [self getImageHeightIndex: [[thisItem objectForKey:@"height"] floatValue]];
            }
            /*FIXED*/
            if (imageHeightIndex < 0) {
                imageHeightIndex = 0;
            }
            NSDictionary *tObj = [(NSArray *)[(NSDictionary *)[(NSArray *)[imageRanges objectForKey:@"priorities"] objectAtIndex: imagePriorityIndex] objectForKey:@"heights"] objectAtIndex: imageHeightIndex];
            
			if ((searchImageIndex < [[tObj objectForKey: @"firstImageIndex"] intValue]) || ([[tObj objectForKey: @"firstImageIndex"] intValue] == -1)) {
                [(NSMutableDictionary *)[(NSArray *)[(NSDictionary *)[(NSArray *)[imageRanges objectForKey:@"priorities"] objectAtIndex: imagePriorityIndex] objectForKey:@"heights"] objectAtIndex: imageHeightIndex] setObject:[ NSNumber numberWithInt: searchImageIndex] forKey: @"firstImageIndex"];
			}
			if (searchImageIndex > [[tObj objectForKey: @"lastImageIndex"] intValue]) {
				[(NSMutableDictionary *)[(NSArray *)[(NSDictionary *)[(NSArray *)[imageRanges objectForKey:@"priorities"] objectAtIndex: imagePriorityIndex] objectForKey:@"heights"] objectAtIndex: imageHeightIndex] setObject:[ NSNumber numberWithInt: searchImageIndex] forKey: @"lastImageIndex"];
			}
		}
		imagePriorityIndex = [self getImagePriorityIndex: [[firstItem objectForKey:@"priority"] intValue]]; // reset these ready for the tiling algorithm
		imageHeightIndex = [self getImageHeightIndex: [[firstItem objectForKey:@"height"] floatValue]];
	}
}

- (int) getImagePriorityIndex: (int)priority {
	for (int searchImagePriorityIndex=0; searchImagePriorityIndex < self.imagePriorities.count; searchImagePriorityIndex++) {
		if ([[self.imagePriorities objectAtIndex:searchImagePriorityIndex] intValue] == priority) {
            return searchImagePriorityIndex;
		}
	}
    return undefined;
}

- (int) getImageHeightIndex:(float)height {
	for (int searchImageHeightIndex=0; searchImageHeightIndex < self.imageHeights.count; searchImageHeightIndex++) {
		if ([[self.imageHeights objectAtIndex:searchImageHeightIndex] floatValue] == height) {
            return searchImageHeightIndex;
		}
	}
    return undefined;
}

- (int) initCountImagesAvailable { // count the total unused images, or the number of unused images of a specific priority and/or height
	int imageCount = 0;
    
	for (int searchImageIndex=0; searchImageIndex < self.searchResults.count; searchImageIndex++) {
		//thisImageAvailable = false;
        NSDictionary *temp = (NSDictionary*)[self.searchResults objectAtIndex: searchImageIndex];
        int priority = [[temp objectForKey: @"priority"] intValue];
        float height = [[temp objectForKey: @"height"] floatValue];
		if (![[temp objectForKey:@"used"] boolValue]) {
			//thisImageAvailable = true;
            int temp = [[(NSMutableDictionary *)[self.imagesCountingAvailable objectForKey: [NSString stringWithFormat:@"p%d", priority]] objectForKey:[NSString stringWithFormat:@"h%d", (int)round(height)]] intValue];
            temp++;
            //increase counting
            [(NSMutableDictionary *)[self.imagesCountingAvailable objectForKey: [NSString stringWithFormat:@"p%d", priority]] setObject: [NSNumber numberWithInt: temp] forKey:[NSString stringWithFormat:@"h%d", (int)round(height)]];
            imageCount++;
		}
	}
	return imageCount;
}


- (int) countImagesAvailable:(int)imagePriority lowerPriorityAcceptable: (BOOL)lowerPriorityAcceptable imageHeight: (float)imageHeight tallerImageAcceptable: (BOOL) tallerImageAcceptable { // count the total unused images, or the number of unused images of a specific priority and/or height
	int imageCount = 0;
	BOOL thisImageAvailable = false;
    //self.call++;
    //NSLog(@"countImagesAvailable for imagePriority == %d, imageHeight == %f", imagePriority, imageHeight);
    /*
	for (int searchImageIndex=0; searchImageIndex < self.searchResults.count; searchImageIndex++) {
		thisImageAvailable = false;
        NSDictionary *temp = (NSDictionary*)[self.searchResults objectAtIndex: searchImageIndex];
        int priority = [[temp objectForKey: @"priority"] intValue];
        float height = [[temp objectForKey: @"height"] floatValue];
        
		if ( ! [[temp objectForKey:@"used"] boolValue]) {
			thisImageAvailable = true;
            
			if (imagePriority != undefined) {
				if (lowerPriorityAcceptable) {
                    if (priority > imagePriority) {thisImageAvailable = false;}
				} else {
                    if (priority != imagePriority) {thisImageAvailable = false;}
				}
			}
            
			if (imageHeight != undefined) {
				if (tallerImageAcceptable) {
                    if (height < imageHeight) {thisImageAvailable = false;}
				} else {
                    if (height != imageHeight) {thisImageAvailable = false;}
				}
			}
            
		}
		if (thisImageAvailable) {imageCount++;}
	}*/
    
    for(int p = self.imagePriorityIndex ; p < self.imagePriorities.count ; p++) {
        NSDictionary *itemPri = (NSDictionary *)[self.imagesCountingAvailable objectForKey:[NSString stringWithFormat:@"p%d", [[self.imagePriorities objectAtIndex: p] intValue]]];
        int priority = [[itemPri objectForKey:@"priority"] intValue];
        thisImageAvailable = true;
        if (imagePriority != undefined) {
            if (lowerPriorityAcceptable) {
                if (priority > imagePriority) {thisImageAvailable = false;}
            } else {
                if (priority != imagePriority) {thisImageAvailable = false;}
            }
        }
        if (thisImageAvailable) {
            for (int i = 0 ; i < self.imageHeights.count ; i++) {
                int ih = [[self.imageHeights objectAtIndex:i] intValue];
                thisImageAvailable = true;
                if (imageHeight != undefined) {
                    if (tallerImageAcceptable) {
                        if (ih < imageHeight) {thisImageAvailable = false;}
                    } else {
                        if (ih != imageHeight) {thisImageAvailable = false;}
                    }
                }
                if (thisImageAvailable) {
                    imageCount += [[itemPri objectForKey:[NSString stringWithFormat:@"h%d", ih]] intValue];
                }
            }
        }
        //[itemPri release];
    }
    //NSLog(@"imageCount == %d", imageCount);
	return imageCount;
}

- (void) setImageUsed:(int)searchImageIndex imageUsed: (BOOL) used { // record the use of an image, to prevent re-use
    if (used == undefined) {
        used = true;
    }
        
    if ((searchImageIndex != undefined) && (searchImageIndex < self.searchResults.count)) {
        [(NSMutableDictionary*)[self.searchResults objectAtIndex: searchImageIndex] setValue: [NSNumber numberWithBool: used] forKey:@"used"];
        
        
        NSDictionary *item = (NSDictionary *)[self.searchResults objectAtIndex: searchImageIndex];
        int priority = [[item objectForKey:@"priority"] intValue];
        float height = [[item objectForKey:@"height"] floatValue];
        //update
        int temp = [[(NSMutableDictionary *)[self.imagesCountingAvailable objectForKey: [NSString stringWithFormat:@"p%d", priority]] objectForKey:[NSString stringWithFormat:@"h%d", (int)round(height)]] intValue];
        if (used) {
            temp--;
        }else{
            temp++;
        }
        //increase counting
        [(NSMutableDictionary *)[self.imagesCountingAvailable objectForKey: [NSString stringWithFormat:@"p%d", priority]] setObject: [NSNumber numberWithInt: temp] forKey:[NSString stringWithFormat:@"h%d", (int)round(height)]];
    }
}

- (double) getPseudoRandom {
	double pseudoRandomNumber = [[self.pseudoRandomNumbers objectAtIndex: self.pseudoRandomNumbersIndex] doubleValue];
	self.pseudoRandomNumbersIndex++;
	if (self.pseudoRandomNumbersIndex >= [self.pseudoRandomNumbers count]) {
        self.pseudoRandomNumbersIndex = 0;
    }
	return pseudoRandomNumber;
}
@end
