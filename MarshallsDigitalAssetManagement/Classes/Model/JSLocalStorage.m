//
//  JSLocalStorage.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSLocalStorage.h"

@implementation JSLocalStorage

static JSLocalStorage *sharedInstance = nil;


+ (JSLocalStorage *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

- (void) setItem:(NSString *)key value:(NSString *)val {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue: val forKey:key];
    //save data to disk
    [prefs synchronize];
}

- (NSString *) getItem:(NSString *) key {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs stringForKey: key];
}



- (id)init{
    self = [super init];
    
    if (self) {
        // Work your initialising magic here as you normally would
    }
    return self;
}

@end
