//
//  JSPhotoLayout.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 12/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JSPhotoLayout.h"
#import "JSSync.h"

@implementation JSPhotoLayout

@synthesize imageLayout;
@synthesize searchResults;
@synthesize layoutRows;

@synthesize layoutColumns;
@synthesize layoutColumnsIndex;
@synthesize layoutSplits;// pseudo-random sequence of which images will be split vertically into 2 (in a 2-row block only)
@synthesize layoutSplitsIndex;
@synthesize layoutMerges;
@synthesize layoutMergesIndex;
@synthesize pseudoRandomNumbers ;
@synthesize pseudoRandomNumbersIndex;
@synthesize layoutRowsIndex;
@synthesize blockWidths; //block width
@synthesize averageAspectRatios;
@synthesize blockContentsBuffer;
@synthesize currentPhotos;
@synthesize imageHeights;
@synthesize imagesAvailable;
@synthesize maxWidths;
@synthesize minWidths;
@synthesize searchResultsHeightBoundaries;
@synthesize layoutQueryString;

@synthesize _blockIndex;
@synthesize useHTML;
@synthesize photoCount;
@synthesize rowWidthBuffer;
@synthesize totalWidthUsed;
@synthesize adjustImageIndex;
@synthesize blockWidthBuffer;
@synthesize imageBorderWidth;
@synthesize thumbsContainerHeight;
@synthesize averageAspectRatiosCalculated;
@synthesize x;
@synthesize y;
@synthesize previousBlock;
@synthesize blockStartXPos;
@synthesize blockStartYPos;
@synthesize previousBlockWidth;
@synthesize previousMergedRowHeight;
@synthesize searchReset;
@synthesize priorityList;
@synthesize currentPriorityIndex;
//@synthesize endOfCurrentPriorityIndex;

#define undefined -1


static JSPhotoLayout *sharedInstance = nil;


+ (JSPhotoLayout *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (void) internalInit {
    self.imageLayout = [NSMutableArray array];
    self.currentPhotos = [NSMutableArray array];
    
    
    self.thumbsContainerHeight = 748; // height of the tiled thumbnail area (px)
    self.imageBorderWidth = 3; // 3px border round all images = 6px gap between them
    self.useHTML = true; // speed-optimisation avoiding the use of jQuery; not working yet - do not use
    
    self.searchResults = [NSMutableArray array]; // search results to go in here as objects, eg. {filename:"1.jpg", width:500, height:333}, sorted by ascending height then width
    
    self.imageHeights = [NSMutableArray array];
    self.searchResultsHeightBoundaries = [NSMutableArray array];
    self.averageAspectRatios = [NSMutableArray array];
    self.minWidths = [NSMutableArray array];
    self.maxWidths = [NSMutableArray array];
    self.imagesAvailable = [NSMutableArray array];
    self.averageAspectRatiosCalculated = false;
    self._blockIndex = 0;
    self.blockWidthBuffer = 0; // remember width of top row of current block, in order to make all rows as similar in length as possible; they will be exactly equalised later once the block is complete
    self.rowWidthBuffer = 0; // while building each row, compare its width with that of the top one
    //self.blockContentsBuffer = [NSMutableArray array]; // remember the entire contents of the current block, in order to tweak the image sizes (to equalize row lengths) afterwards
    self.totalWidthUsed = 0;
    self.adjustImageIndex = 0;
    
    self.blockWidths = [NSMutableArray array];
    
    self.layoutRows = [NSArray arrayWithObjects:[NSNumber numberWithInt:3], [NSNumber numberWithInt:2], nil];
    self.layoutColumns = [NSArray arrayWithObjects:[NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:1], [NSNumber numberWithInt:3], [NSNumber numberWithInt:1], nil];
    self.layoutSplits = [NSArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0] ,[NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], nil];
    self.layoutMerges = [NSArray arrayWithObjects: [NSNumber numberWithInt:-1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:0], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:0], nil];
    self.pseudoRandomNumbers = [NSMutableArray arrayWithObjects:[NSNumber numberWithDouble:0.46528182671942275], [NSNumber numberWithDouble:0.25146400039806915], [NSNumber numberWithDouble:0.48523483353723973], [NSNumber numberWithDouble:0.03376772038297837], [NSNumber numberWithDouble:0.1730804695886906], [NSNumber numberWithDouble:0.4760373749636351], [NSNumber numberWithDouble:0.880267470298855], [NSNumber numberWithDouble:0.5871465137172949], [NSNumber numberWithDouble:0.7692538208175426], [NSNumber numberWithDouble:0.022155791043057982], [NSNumber numberWithDouble:0.96285739123048], [NSNumber numberWithDouble:0.537058143090162], [NSNumber numberWithDouble:0.17295454101472085], [NSNumber numberWithDouble:0.3753795607520214], [NSNumber numberWithDouble:0.9425579552465277], [NSNumber numberWithDouble:0.27813233250997926], [NSNumber numberWithDouble:0.8410111167008298], [NSNumber numberWithDouble:0.019238634942301602], [NSNumber numberWithDouble:0.2566836040700138], [NSNumber numberWithDouble:0.24890221496609544], [NSNumber numberWithDouble:0.5900169971688878], [NSNumber numberWithDouble:0.7401996453723709], [NSNumber numberWithDouble:0.24506634372629055], [NSNumber numberWithDouble:0.595210221535074], [NSNumber numberWithDouble:0.510938557797881], [NSNumber numberWithDouble:0.4952522767855486], [NSNumber numberWithDouble:0.2050048774951978], [NSNumber numberWithDouble:0.04731068112784764], [NSNumber numberWithDouble:0.3848415778837615], [NSNumber numberWithDouble:0.7607242730678508], [NSNumber numberWithDouble:0.9210416448327311], [NSNumber numberWithDouble:0.21296988112122828], [NSNumber numberWithDouble:0.7319692159919754], [NSNumber numberWithDouble:0.7623804239798945], [NSNumber numberWithDouble:0.8159603287438819], [NSNumber numberWithDouble:0.4234348210160843], [NSNumber numberWithDouble:0.25628665482969815], [NSNumber numberWithDouble:0.6245553273208275], [NSNumber numberWithDouble:0.3274046450229524], [NSNumber numberWithDouble:0.85996270890513], [NSNumber numberWithDouble:0.9837409952656273], [NSNumber numberWithDouble:0.27001591364739796], [NSNumber numberWithDouble:0.9787767666475191], nil]; // pseudo-random numbers to replace Math.random while giving the same results every time
    pseudoRandomNumbersIndex = 0;
    
    
    self.previousBlock = -1;
    self.previousBlockWidth = 0;
    self.blockStartXPos = imageBorderWidth;
    self.blockStartYPos = imageBorderWidth;
    self.x = 0;
    self.y = 0;
    self.previousMergedRowHeight = 0;
}


- (id)init{
    self = [super init];
    if (self) {
        // Work your initialising magic here as you normally would
       [self internalInit];
    }
    
    return self;
}

//photos2: function(query, callback) callback:(void (^)(NSMutableArray *media))successCalback
- (void)photos2: (NSString *)query callback:(void (^)(NSArray *results))successCalback{
    
    NSArray *businessAreas = [NSArray arrayWithObjects:@"domestic", @"commercial", nil];
    
    NSArray *activeContainers = [[[JSSync sharedInstance] getActiveContainers: [businessAreas JSONString]] objectFromJSONString];
    
    NSArray *filterAreas = [[JSMain sharedInstance] getBusinessAreaFilters];
    if (filterAreas != NULL && [filterAreas count] > 0) {
#ifdef DEBUG
        NSLog(@"Apply business filter area %@", filterAreas);
#endif
        activeContainers = filterAreas;
    }
    NSString * placeholders = [JSDBV2 placeholdersForArray: activeContainers];
    //var sql = "SELECT meta FROM media WHERE mt = 1 ORDER BY thumbHeight ASC, thumbWidth ASC";
    NSString * q = @"";
    
    if (query && [allTrim(query) length] > 0) {
        /*
         q = " AND meta LIKE ?";
         params.push("%" + query + "%");
         */
#ifdef DEBUG
        NSLog(@"photos2 query: %@ length == %d", query, [allTrim(query) length]);
#endif
        
        q = [NSString stringWithFormat:@" AND (title like '%%%@%%' or tags like '%%%@%%' or products like '%%%@%%' or location like '%%%@%%' or meta like '%%%@%%') AND mediaType = 'Photos' AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )", allTrim(query),allTrim(query),allTrim(query),allTrim(query),allTrim(query), placeholders];
        
    }else{
        q = [NSString stringWithFormat:@" AND mediaRef in (SELECT DISTINCT mediaRef FROM media_container WHERE container IN ( %@) )",placeholders];
    }
    
    //NSString *sql = [NSString stringWithFormat:@"SELECT distinct mediaRef, mediaType, title, thumbnail, url, thumbWidth, thumbHeight,width,height FROM media WHERE mt = 1 %@ and priority = %d ORDER BY thumbHeight ASC, thumbWidth ASC", q, activePriority];
    
    NSString *sql = [NSString stringWithFormat:@"SELECT distinct mediaRef, mediaType, title, thumbnail, url, thumbWidth, thumbHeight,width,height,priority FROM media WHERE mt = 1 %@ ORDER BY priority DESC, thumbHeight ASC, thumbWidth ASC ", q];//priority DESC, , thumbHeight ASC, thumbWidth ASC
    
    //sql += " LIMIT 1000";
    
#ifdef DEBUG
    NSLog(@"photos2 sql: %@",sql);
#endif
    
    //NSLog(@"photos2 sql: %@ activeContainers = %@",sql, activeContainers);
    
    NSArray *photos = [[JSDBV2 sharedInstance] map: sql params: activeContainers success:nil fail:nil];
    if (successCalback) {
        successCalback(photos);
    }
    /*db.map(sql, params, function(photos){
        callback && callback(photos);
    });*/
}


- (void)photosWithTags: (NSString *)tags callback:(void (^)(NSMutableArray *media))successCalback {
    //        var photos = [];
    //quotedTags = $(tags.split("|")).map(function(index, tag){ return "'" + tag.toUpperCase() + "'"});
    NSArray *temp = [tags componentsSeparatedByString:@"|"];
    NSMutableArray *upperTags = [NSMutableArray array];
    for (NSString *str in temp) {
        [upperTags addObject:[NSString stringWithFormat:@"'%@'", [str uppercaseString]]];
    }
    NSString *tagStr = [upperTags componentsJoinedByString:@","];//$.makeArray(quotedTags).join(",");
    NSString *sql = [NSString stringWithFormat:@"SELECT distinct m.mediaRef, mediaType, title, thumbnail, url, thumbWidth, thumbHeight,width,height FROM media m INNER JOIN media_tags mt ON m.mediaRef = mt.mediaRef WHERE upper(tag) in (%@)  AND mt = 1 ORDER BY priority DESC, thumbHeight ASC, thumbWidth ASC", tagStr];

#ifdef DEBUG
    NSLog(@"photosWithTags == %@", sql);
#endif

    NSArray *rows = [[JSDBV2 sharedInstance] sql:sql params:nil success:nil fail:nil];
    if (successCalback) {
        successCalback(rows);
    }
    /*sql = "SELECT m.mediaRef, mediaType, title, thumbnail, url, thumbWidth, thumbHeight FROM " + sync.mediaTable + " m INNER JOIN " + sync.tagTable;
    sql += " mt ON m.mediaRef = mt.mediaRef WHERE upper(tag) in (" + tagStr + ")"
    sql += " AND mt = 1";
    db.map(sql, [], function(photos){
        callback && callback(photos);
    });*/
}

- (void)photoTags:(id) delegate{
    NSArray *rows = [[JSDBV2 sharedInstance] sql:@"SELECT DISTINCT tag FROM media_tags" params:nil success:nil fail:nil];
    if (rows && [rows count] > 0) {
        NSMutableArray *tagsArray = [[NSMutableArray alloc] init];
        for (NSDictionary *row in rows) {
            [tagsArray addObject: [row objectForKey:@"tag"]];
        }
        
#ifdef DEBUG
        NSLog(@"tagsArray %@", tagsArray);
#endif
        
        [delegate performSelector:@selector(tagsAvailable:) withObject: tagsArray];
        [tagsArray release];
    }
    /*
    db.map("SELECT DISTINCT tag FROM media_tags", [], function(tags){
        var tagsArray = [];
        tagsArray = tags.map(function(row, index){
            return row.tag;
        });
        console.log(tagsArray);
        main.pgFire("DAMTagView", "tagsAvailable", tagsArray);
    });*/
}

- (void) layoutInitWithTags: (id)plugin  tagSpec: (NSString *)_tagSpec {
#ifdef DEBUG
    NSLog(@"photoLayout.layoutInitWithTags plugin: %@", plugin);
#endif
    [self initTiling];
    [self photosWithTags: _tagSpec callback:^(NSMutableArray *photos) {
        
        self.currentPhotos = [NSMutableArray arrayWithArray:photos];
        photoCount = 0;
#ifdef DEBUG
        NSLog(@"photoLayout.layoutInitWithTags photos.length: %d", photos ? [photos count] : 0);
#endif
        if ([photos count] == 0) {
            //[[JSMain sharedInstance] pgFire:plugin selector:@selector(noLayoutResults) withData: nil];
            [plugin performSelector:@selector(noLayoutResults:) withObject: [NSDictionary dictionary]];
            return;
        }
        
        NSMutableArray *photosForTiling = [NSMutableArray array];
        for (NSDictionary *photo in photos) {
            NSMutableDictionary *tmp = [NSMutableDictionary dictionary];
            [tmp setObject: [photo objectForKey:@"mediaRef"] forKey:@"filename"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbWidth"] intValue]] forKey:@"width"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbHeight"] intValue]] forKey:@"height"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"width"] intValue]] forKey:@"originalWidth"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"height"] intValue]] forKey:@"originalHeight"];
            //priority
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"priority"] intValue]] forKey:@"priority"];
            [photosForTiling addObject: tmp];
        }
        //photosForTiling = $.makeArray(photosForTiling);
        //NSLog(@"photosForTiling: %@", photosForTiling);
        self.searchResults = [NSMutableArray arrayWithArray:photosForTiling];
#ifdef DEBUG
        NSLog(@"BEFORE initTiling() searchResults length == %d", [self.searchResults count]);
#endif

        [self initTiling];
        
#ifdef DEBUG
        NSLog(@"AFTER initTiling()");
#endif
        
        [self layoutIterate:plugin iterateCount:200];
    }];
    
}


- (NSArray*) mapTilingResults:(NSArray *) photos {
    NSMutableArray * result = [NSMutableArray array];
    int sequenceNumber = photoCount;
    int i = -1;

#ifdef DEBUG
    NSLog(@"mapTilingResults slicing: photoCount == %d - self.imageLayout count == %d",photoCount, [self.imageLayout count]);
#endif
    
    for (i = photoCount; i < [self.imageLayout count];i++) {
        NSDictionary * photo = [self.imageLayout objectAtIndex: i];
        int imageIndex = [[photo objectForKey:@"imageIndex"] intValue];
        //             console.log("imageIndex:"+imageIndex+", photo.left:"+photo.left);
        NSMutableDictionary *photoModel = [photos objectAtIndex:imageIndex];
        [photoModel setObject:[NSNumber numberWithInt:sequenceNumber] forKey:@"sequenceNumber"];
        [photoModel setObject: [photo objectForKey:@"left"] forKey:@"layoutX"];
        [photoModel setObject: [photo objectForKey:@"top"] forKey:@"layoutY"];
        [photoModel setObject: [photo objectForKey:@"width"] forKey:@"layoutW"];
        [photoModel setObject: [photo objectForKey:@"height"] forKey:@"layoutH"];
        [photoModel setObject: [photo objectForKey:@"priority"] forKey:@"priority"];
        //[photoModel setObject: [photo objectForKey:@"blockIndex"] forKey:@"blockIndex"];
        
        //NSLog(@"added photo model %@", photoModel);
        
        [result addObject:photoModel];
        
        sequenceNumber++;
    }
    photoCount = i;
    return result;
}

- (void)layoutIterate: (id)plugin iterateCount: (int) num {
    //we need to reset xpos
#ifdef DEBUG
    NSLog(@"layoutIterate == search results: %@", self.searchResults);
#endif
    
    BOOL finished =  [self iterateTiling: num > 0 ? num : 50 collectOffset: photoCount];
    
    //NSLog(@"AFTER iterateTiling() finished: %d currentPhotos %d", finished, [self.currentPhotos count]);
    
#ifdef DEBUG
    NSLog(@"AFTER iterateTiling() finished: %d currentPhotos %d", finished, [self.currentPhotos count]);
    for (NSDictionary *photo in self.searchResults) {
        NSLog(@"mediaRef == %@ used == %@", [photo objectForKey:@"filename"], [photo objectForKey:@"used"]);
    }
    
#endif
    
    if ([self.currentPhotos count] == 0) {
        //if has no photo to layout then we need to check new priority list, until we get no data then showing no data showing.
        [plugin performSelector:@selector(noLayoutResults:) withObject: [NSDictionary dictionary]];
        return;
    }
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    BOOL more = NO;
    more = !finished;
    [result setObject: [NSNumber numberWithBool: more] forKey:@"more"];
    [result setObject: [self mapTilingResults: self.currentPhotos] forKey:@"photos"];
    
#ifdef DEBUG
    //NSLog(@"result data count %d", [[result objectForKey:@"photos"] count]);
#endif
    
    //[[JSMain sharedInstance] pgFire:plugin selector:@selector(layoutPartialResult:) withData:result];
    [plugin performSelector:@selector(layoutPartialResult:) withObject: result];
    
    
    /*
    var result = {
    more: !finished,
    photos: photoLayout.mapTilingResults(currentPhotos)
    };
    main.pgFire(plugin, "layoutPartialResult", result);
    */ 
}

- (void) layoutInit:(id) plugin query:(NSString *)strQuery {
    
#ifdef DEBUG
    NSLog(@"photoLayout.layoutInit plugin: %@",plugin);
#endif
    
    [self initTiling];

    self.layoutQueryString = strQuery;
    
    [self photos2: strQuery callback:^(NSArray *photos) {
        
        self.currentPhotos = [NSMutableArray arrayWithArray:photos];
        photoCount = 0;

#ifdef DEBUG
        NSLog(@"photoLayout.layoutInit photos.length: %d", photos ? [photos count] : 0);
#endif
        
        //NSLog(@"photoLayout.layoutInit photos.length: %d", photos ? [photos count] : 0);
        
        if ([photos count] == 0) {
            //[[JSMain sharedInstance] pgFire:plugin selector:@selector(noLayoutResults:) withData: nil];
            [plugin performSelector:@selector(noLayoutResults:) withObject: nil];
            return;
        }
        
        NSMutableArray *photosForTiling = [NSMutableArray array];
        NSString *temp = @"";
        for (NSDictionary *photo in photos) {
            NSMutableDictionary *tmp = [NSMutableDictionary dictionary];
            /*if (i++ < 10) {
                NSLog(@"img name == %@ priority == %d", [photo objectForKey:@"mediaRef"], [[photo objectForKey:@"priority"] intValue]);
            }*/
            //[tmp setObject: [photo objectForKey:@"mediaRef"] forKey:@"filename"];
            [tmp setObject: [photo objectForKey:@"mediaRef"] forKey:@"mediaRef"];
            [tmp setObject: [NSString stringWithFormat:@"%@/%@", [JSMain apiHost], [photo objectForKey:@"thumbnail"]] forKey: @"filename"];
            
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbWidth"] intValue]] forKey:@"width"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"thumbHeight"] intValue]] forKey:@"height"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"width"] intValue]] forKey:@"originalWidth"];
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"height"] intValue]] forKey:@"originalHeight"];
            //priority
            [tmp setObject: [NSNumber numberWithInt:[[photo objectForKey:@"priority"] intValue]] forKey:@"priority"];
            
            [photosForTiling addObject: tmp];
            
            temp = [temp stringByAppendingString: [JSMain stringify: tmp]];
            temp = [temp stringByAppendingString: @","];
        }
        //photosForTiling = $.makeArray(photosForTiling);
        //NSLog(@"photosForTiling: %@", photosForTiling);
        self.searchResults = [NSMutableArray arrayWithArray:photosForTiling];
        
#ifdef DEBUG
        NSLog(@"BEFORE initTiling() searchResults length == %d", [self.searchResults count]);
#endif
        
        NSLog(@"self.searchResults == %@", temp);
        
        //[self initTiling];
        
#ifdef DEBUG
        NSLog(@"AFTER initTiling() searchResults length == %d", [self.searchResults count]);
#endif
        
        [self layoutIterate:plugin iterateCount:200];
    }];
    
    /*
    photoLayout.photos2(query, function(photos) {
        currentPhotos = photos;
        photoCount = 0;
        console.log("photoLayout.layoutInit photos.length:" + photos.length);
        if (photos.length == 0) {
            main.pgFire(plugin, "noLayoutResults");
            return;
        }
        var photosForTiling = $(photos).map(function(i, photo){
            return {
            filename: photo.mediaRef,
            width: parseInt(photo.thumbWidth),
            height: photo.thumbHeight
            };
        });
        photosForTiling = $.makeArray(photosForTiling);
        //            console.log("photosForTiling:" + JSON.stringify(photosForTiling));
        searchResults = photosForTiling;
        console.log("BEFORE initTiling()");
        initTiling();
        console.log("AFTER initTiling()");
        photoLayout.layoutIterate(plugin, 200);
    });*/
}

#pragma start titling.js, we moved titiling.js code into photoLayout.js class because of titling.js is accessing to same variables in photoLayout.js

-(void) initTiling {

#ifdef DEBUG
    NSLog(@"initTiling");
#endif
    
    //console.log("searchResults == " + JSON.stringify(searchResults));
    /*var thumbs = $("div.thumbs");
	if (useHTML) {thumbs.html("");}
     
     */
	self.averageAspectRatiosCalculated = false; // force the stored data from getAverageAspectRatio to be regenerated for the current search results
	[self resetLayout];
    totalWidthUsed = 0;
    self.imageHeights = [NSMutableArray array];
    adjustImageIndex = 0;
    _blockIndex = 0;
    
    self.previousBlock = -1;
    self.searchReset = false;
    self.previousBlockWidth = 0;
    self.blockStartXPos = imageBorderWidth;
    self.blockStartYPos = imageBorderWidth;
    self.x = 0;
    self.y = 0;
    self.previousMergedRowHeight = 0;
    if ([self.imageLayout count] > 0) {
        [self.imageLayout removeAllObjects];
    }
    self.imageLayout = [NSMutableArray array];
    self.blockWidths = [NSMutableArray array];
    self.imagesAvailable = [NSMutableArray array];
    self._blockIndex = 0;
    photoCount = 0;
    
    //need to pickup next priority list
    NSArray *tempList = [[JSDBV2 sharedInstance] map: @"select distinct priority from media order by priority desc" params: nil success:nil fail:nil];
    self.priorityList = [NSMutableArray array];
    for (NSDictionary *item in tempList) {
        [(NSMutableArray *)self.priorityList addObject:  [NSNumber numberWithInt: [[item objectForKey: @"priority"] intValue]]];
    }
    
#ifdef DEBUG
    NSLog(@"new priority list == %@", self.priorityList );
#endif
    
    //NSLog(@"new priority list == %@", self.priorityList);
    
    self.currentPriorityIndex = [[self.priorityList objectAtIndex:0] intValue];
    //self.endOfCurrentPriorityIndex = -1;
}

- (void)resetLayout {
    layoutRowsIndex = 0;
    layoutColumnsIndex = 0;
    layoutSplitsIndex = 0;
    layoutMergesIndex = 0;
	pseudoRandomNumbersIndex = 0;
}

- (int) yPosOfRow: (int) row inBlock:(int)block {
    if (row == 0) {
        return 0;
    }
    int result = 0;
    for (int r = 0 ; r < row ; r++) {
        for (int imageLayoutIndex = 0; imageLayoutIndex < [self.imageLayout count]; imageLayoutIndex++) {
            NSMutableDictionary *image = (NSMutableDictionary*) [self.imageLayout objectAtIndex:imageLayoutIndex];
            if ([[image objectForKey:@"blockIndex"] intValue] == block) {
                if ([[image objectForKey:@"rowIndex"] intValue] == r) {
                    result += [[image objectForKey:@"height"] intValue];
                    break;
                }
            }
        }
    }
    return result + (row * imageBorderWidth);
}


- (int) xPosOfCol: (int) col inBlock:(int)block inRow: (int) row{
    if (col == 0) {
        return 0;
    }
    int result = 0;
    for (int c = 0 ; c < col ; c++) {
        for (int imageLayoutIndex = 0; imageLayoutIndex < [self.imageLayout count]; imageLayoutIndex++) {
            NSMutableDictionary *image = (NSMutableDictionary*) [self.imageLayout objectAtIndex:imageLayoutIndex];
            if ([[image objectForKey:@"blockIndex"] intValue] == block) {
                if ([[image objectForKey:@"rowIndex"] intValue] == row && [[image objectForKey:@"colIndex"] intValue] == c) {
                    result += [[image objectForKey:@"width"] intValue]; 
                }
            }
        }
    }
    return result + (col * imageBorderWidth);
}

-(int)tilingCollectResults:(int)collectOffset{
    int numResults = 0;
#ifdef DEBUG
        NSLog(@"tilingCollectResults %d", collectOffset);
#endif
        
    if ([self.imageLayout count] == 0) {
        
#ifdef DEBUG
        NSLog(@"tilingCollectResults imageLayout empty!!!");
#endif
        
        return numResults;
    }
    //NSLog(@"tilingCollectResults count == %d", imageLayout.count);
    
    
    for (int imageLayoutIndex = collectOffset; imageLayoutIndex < [self.imageLayout count]; imageLayoutIndex++) {
        
        NSMutableDictionary *image = (NSMutableDictionary*) [self.imageLayout objectAtIndex:imageLayoutIndex];
        
        //NSLog(@"thumbnail image %@", image);
        int _block = [[image objectForKey:@"blockIndex"] intValue];
        if (self.previousBlock != _block) {
            self.blockStartXPos += self.previousBlockWidth;
            //NSLog(@"block %d blockStartXPos %d previousBlockWidth == %d", _block, blockStartXPos, self.previousBlockWidth);
            self.x = self.blockStartXPos + imageBorderWidth;
            self.y = self.blockStartYPos;
            self.previousBlock = _block;
            self.previousMergedRowHeight = 0;
            self.previousBlockWidth = [[self.blockWidths objectAtIndex: _block] intValue];//MAX(previousBlockWidth,[[image objectForKey:@"blockWidth"] intValue]);
        }else {
            //self.previousBlockWidth = [[self.blockWidths objectAtIndex: _block] intValue];//MAX(previousBlockWidth,[[image objectForKey:@"blockWidth"] intValue]);
            //NSLog(@"current block width %d", self.previousBlockWidth);
        }
        
        int _rowIndex = [[image objectForKey:@"rowIndex"] intValue];
        int _colIndex = [[image objectForKey:@"colIndex"] intValue];

        self.x = [self xPosOfCol: _colIndex inBlock: _block inRow:_rowIndex] + self.blockStartXPos + (_colIndex * imageBorderWidth);
        self.y = [self yPosOfRow: _rowIndex inBlock: _block] + self.blockStartYPos + (_rowIndex * imageBorderWidth);
        
        
        [image setObject:[NSNumber numberWithInt: self.x] forKey:@"left"];
        [image setObject:[NSNumber numberWithInt: self.y] forKey:@"top"];
#ifdef DEBUG
        NSLog(@"imagename == %@ ; imageLayoutIndex == %d ; left == %d ; top == %d ; rowTop == %d ; rowMerge == %d ; row == %d ; column == %d ; block == %d width == %d ; height == %d", [image objectForKey:@"filename"], imageLayoutIndex, x, y,[[image objectForKey:@"rowTop"] intValue],[[image objectForKey:@"rowMerge"] intValue] , [[image objectForKey:@"rowIndex"] intValue], [[image objectForKey:@"colIndex"] intValue], [[image objectForKey:@"blockIndex"] intValue], [[image objectForKey:@"width"] intValue], [[image objectForKey:@"height"] intValue]);
#endif
        
        //NSLog(@"imagename == %@ ; imageLayoutIndex == %d ; prioirity == %d ; left == %d ; top == %d ; rowTop == %d ; rowMerge == %d ; row == %d ; column == %d ; block == %d width == %d ; height == %d", [image objectForKey:@"filename"], imageLayoutIndex,[[image objectForKey:@"priority"] intValue] , x, y,[[image objectForKey:@"rowTop"] intValue],[[image objectForKey:@"rowMerge"] intValue] , [[image objectForKey:@"rowIndex"] intValue], [[image objectForKey:@"colIndex"] intValue], [[image objectForKey:@"blockIndex"] intValue], [[image objectForKey:@"width"] intValue], [[image objectForKey:@"height"] intValue]);
        
        //NSLog(@"left %d top: %d", x, y);
        if ([[(NSDictionary *)[self.imageLayout objectAtIndex: [self.imageLayout count] - 1] objectForKey:@"imageIndex"] intValue] == undefined) {
            [self.imageLayout removeLastObject];
        }
        /*if ((imageLayout[self.imageLayout.length - 1].hasOwnProperty("imageIndex")) && (imageLayout[self.imageLayout.length - 1].imageIndex == -1)) { // remove any invisible image placeholder (identified by imageIndex of -1)
            imageLayout.pop();
        }*/
    }
    return numResults;
}

-(BOOL)hasImageUnused {
    
    /*NSLog(@"self.searchResults count == %d", self.searchResults.count);
    
    for(NSDictionary *photo in self.searchResults) {
        if ([photo objectForKey:@"used"] == NULL || ![photo objectForKey:@"used"]) {
            NSLog(@"un used filename == %@", [photo objectForKey:@"filename"]);
            return YES;
        }
    }*/
    return NO;
}



- (BOOL)iterateTiling:(int)blockCount collectOffset:(int)co {
    //console.log("iterateTiling:"+blockIndex);

#ifdef DEBUG
    NSLog(@"iterateTiling: %d ; collectOffset == %d", _blockIndex, co);
#endif
    
    int thisTimeCount = 0;
    bool blockEmpty = true;
    
    do {
        if (self.searchReset) {
            break;
        }
        blockEmpty = [self buildBlock: _blockIndex];
        //console.log("iterateTiling blockIndex:"+blockIndex + ", blockCount:" + blockCount + ", blockEmpty: "+blockEmpty, "thisTimeCount:"+thisTimeCount);
#ifdef DEBUG
        NSLog(@"iterateTiling blockIndex: %d, blockCount: %d, blockEmpty: %d, thisTimeCount: %d ; imageLayout result count == %d", _blockIndex, blockCount, blockEmpty, thisTimeCount, [self.imageLayout count]);
#endif
        
        //NSLog(@"iterateTiling blockIndex: %d, blockCount: %d, blockEmpty: %d, thisTimeCount: %d ; imageLayout result count == %d", _blockIndex, blockCount, blockEmpty, thisTimeCount, [self.imageLayout count]);
        
        _blockIndex++;
        thisTimeCount++;
        if (thisTimeCount >= blockCount)
            break;
        
    } while (!blockEmpty && (thisTimeCount < blockCount));
    
    [self tilingCollectResults:co];
    return blockEmpty;
}

-(BOOL) getEmergencyLayoutMode:(int)rowsInCurrentBlock { // NEW: this function // decide whether the supply of images is running low enough to require emergency mode (in which every block consists of 2 rows with 1 split column in each, so that every image position in the layout can accept an image of any size)
    if (rowsInCurrentBlock == 3) {
        if (self.imagesAvailable.count == 4 && (([[self.imagesAvailable objectAtIndex:1] intValue] == 0) || ([[self.imagesAvailable objectAtIndex:3] intValue] == 0))) {return true;}
	} else if (rowsInCurrentBlock == 3) {
        if (self.imagesAvailable.count == 4) {
            if (([[self.imagesAvailable objectAtIndex:0] intValue] == 0) || ([[self.imagesAvailable objectAtIndex:2] intValue] == 0)) {return true;}
        }
	} else {
        if (self.imagesAvailable.count == 4) {
            if (([[self.imagesAvailable objectAtIndex:0] intValue] == 0) || ([[self.imagesAvailable objectAtIndex:1] intValue] == 0) || ([[self.imagesAvailable objectAtIndex:2] intValue] == 0) || ([[self.imagesAvailable objectAtIndex:3] intValue] == 0)) {return true;}
        }
	}
	//if ((imagesAvailable[1] == 0) || (imagesAvailable[2] == 0) || (imagesAvailable[3] == 0)) {return true;}
	return false;
}


/*
 TODO: Build block function
 */
- (BOOL) buildBlock:(int)blockIndex {
#ifdef DEBUG
    NSLog(@"buildBlock: blockIndex == %d ; self.imagesAvailable == %@ ; thumbsContainerHeight == %f", blockIndex, [self.imagesAvailable componentsJoinedByString:@","], thumbsContainerHeight);
#endif
	BOOL blockEmpty = true;
	BOOL blockFull = true;
	self.blockContentsBuffer = [NSMutableArray array];
	self.blockWidthBuffer = 0;
	int blockWidthTarget = 0.0;
    int imagesUsed = 0;
	int rowsInCurrentBlock = [self getLayout:@"rowsInCurrentBlock"];//2 or 3
    
    if (self.imagesAvailable.count >= 4 && ([[self.imagesAvailable objectAtIndex:0] intValue] > 0) && ([[self.imagesAvailable objectAtIndex:1] intValue] + [[self.imagesAvailable objectAtIndex:2] intValue] + [[self.imagesAvailable objectAtIndex:3] intValue] <= 0)) {
        rowsInCurrentBlock = 2;
    } // if only the smallest images are available, force the layout to be 2 rows (which will then be split)
	int rowTop = 0;
	float unmergedRowHeight = /*Math.round*/((thumbsContainerHeight / rowsInCurrentBlock) - (imageBorderWidth * 2)); // default height - that of an unmerged row
	float rowHeight = unmergedRowHeight; // actual height of the current row, taking into account whether it is merged or not
	int rowIndex = 0;
    int imageIndex = undefined;
	int columnsInCurrentBlock = [self getLayout:@"columnsInCurrentBlock"];
	int columnsInCurrentRow = columnsInCurrentBlock;
	int rowMerge = -1;

#ifdef DEBUG
    NSLog(@"rowsInCurrentBlock == %d ; columnsInCurrentBlock == %d", rowsInCurrentBlock, columnsInCurrentBlock);
#endif
    
    if (columnsInCurrentBlock > 1) {
        rowMerge = [self getLayout:@"rowMerge"];
    }
    
    float columnWidth = rowHeight * [self getAverageAspectRatio:rowHeight];//if we move to new priority range, we need to reset avg ratio too
    
#ifdef DEBUG
    NSLog(@"buildBlock: rowHeight %f columnWidth == %f" ,rowHeight, columnWidth);
#endif
    
	int columnIndex = 0;
    BOOL splitBuffer = false;
    if (columnsInCurrentBlock > 1) {
        blockWidthTarget = columnWidth * columnsInCurrentBlock;
    }
    int imageWidth = undefined;
    int imageHeight = undefined;
    int fillerImageWidth = undefined;
    int fillerImageIndex = undefined;
    
    //real row and column index of each thumbnail in block
    int thumbRowIndex = 0;
    int thumbColIndex = 0;
	
	for (rowIndex=0; rowIndex < rowsInCurrentBlock; rowIndex++) {
		rowHeight = unmergedRowHeight; // reset all variables that are affected by row merging, to their default (non-merging) values
		columnsInCurrentRow = columnsInCurrentBlock;
        if (columnsInCurrentBlock > 1) {
            if (rowMerge > -1) {
                if (rowIndex == rowMerge) { // this is the top row of 2 to be merged; build a double-height row here, with fewer columns in it
                    rowHeight = (unmergedRowHeight * 2) + (imageBorderWidth * 2);
                    columnsInCurrentRow = round(columnsInCurrentBlock * 0.5);
                }	else if (rowIndex == rowMerge + 1) { // this is the bottom row of 2 to be merged; there's nothing to build here because the row above it has already been built at double height
                    continue;
                }
            }
        }

#ifdef  DEBUG
        NSLog(@"row: rowIndex == %d; rowHeight == %f; imagesAvailable == %@", rowIndex, rowHeight, [imagesAvailable componentsJoinedByString:@","]);
#endif

		[blockContentsBuffer addObject:[NSMutableArray array]];
		rowWidthBuffer = 0;
        
        //reset thumb col index
        thumbColIndex = 0;
		for (columnIndex=0; columnIndex < columnsInCurrentRow; columnIndex++) {
            
			BOOL columnSplittable = true; // check whether this column can be split into an upper and lower image
            if (rowsInCurrentBlock >= 3) { // block of 3 or more rows; do not split this column
				columnSplittable = false;
			} else if (splitBuffer) { // last column was split; do not split this column
				columnSplittable = false;
			} else if ((columnsInCurrentRow > 1) && (rowIndex > 0) && (columnIndex == columnsInCurrentRow - 1)) { // image is at right end of a lower row so we will prioritize choosing a single image that best fits the remaining space; do not split this column
				columnSplittable = false;
			} else if ((columnsInCurrentRow == 1) && (rowIndex > 0) && (blockWidthBuffer > rowHeight)) { // in a single-column block which is too wide because of wide images on higher rows; do not split this column
				columnSplittable = false;
			}
            
            if ((rowsInCurrentBlock == 2) && (columnsInCurrentBlock == 1) && ([self getEmergencyLayoutMode:rowsInCurrentBlock])) {
                columnSplittable = true;
            } // NEW: this line
            
			if (columnSplittable && [self getLayout:@"columnSplit"] > 0) { // if the top row of a 2-row block was split, split the bottom one too (finding a use for the smallest images)
                //blockContentsBuffer[blockContentsBuffer.length-1].push([]);
                [self.blockContentsBuffer replaceObjectAtIndex: (blockContentsBuffer.count - 1) withObject: [NSMutableArray array]];
                
                int splitHeight = /*Math.round*/((rowHeight - (imageBorderWidth * 2)) * 0.5);
                splitBuffer = true;
                int topImageIndex;
                int bottomImageIndex;
                topImageIndex = [self getImageByAspectRatio: columnWidth / splitHeight excludedImageIndex: undefined imageHeight:splitHeight aspectRatioIsMinimum:FALSE];
                bottomImageIndex = [self getImageByAspectRatio: columnWidth / splitHeight excludedImageIndex:topImageIndex imageHeight:splitHeight aspectRatioIsMinimum:FALSE];
                if ((bottomImageIndex == undefined) && ([[self.imagesAvailable objectAtIndex:0] intValue] + [[self.imagesAvailable objectAtIndex:1] intValue] + [[self.imagesAvailable objectAtIndex:2] intValue] + [[self.imagesAvailable objectAtIndex:3] intValue] == 1)) {
                    bottomImageIndex = -1;
                } // if there is a split in the last block and there is only one image left to share between the 2 image spaces, this special code -1 represents the fact that the bottom image space of the split will be empty
                
                
                if ((topImageIndex != undefined) && (bottomImageIndex != undefined)) {
                    float topImageWidth =  [[(NSDictionary *)[self.searchResults objectAtIndex:topImageIndex] objectForKey:@"width"] floatValue];//searchResults[topImageIndex].width;
                    float bottomImageWidth = topImageWidth;
                    if (bottomImageIndex != undefined) {
                        bottomImageWidth = [[(NSDictionary *)[self.searchResults objectAtIndex:bottomImageIndex] objectForKey:@"width"] floatValue];//searchResults[bottomImageIndex].width;
                        imageWidth = MIN(topImageWidth, bottomImageWidth); // by default, set both of these images to the narrowest width of the two
                    } else {
                        imageWidth = topImageWidth;
                    }
                    
                    if ((blockWidthTarget == 0) || (imageWidth < blockWidthTarget)) {blockWidthTarget = imageWidth;}
                    
                   /* if ((blockWidthTarget == 0) || (blockWidthTarget > (imageWidth + (imageBorderWidth * 2)))) {
                        blockWidthTarget = imageWidth + (imageBorderWidth * 2);
                    }
                    */
                    if ((columnsInCurrentRow == 1) && (rowIndex > 0)) {
                        imageWidth = blockWidthBuffer - (imageBorderWidth * 2);
                    } // in a lower row of a single-column block, force both images to the existing column width
                    
                    [self getThumbHTML:topImageIndex imageWidth:imageWidth imageHeight:splitHeight rowIndex:thumbRowIndex colIndex:thumbColIndex blockIndex:blockIndex rowTop:rowTop rowMerge: 9999];
                    //split.append(getThumbHTML(topImageIndex, imageWidth, splitHeight));
                    imagesUsed++;
                    if (bottomImageIndex > -1) {
                        thumbRowIndex++;
                        //split.append(getThumbHTML(bottomImageIndex, imageWidth, splitHeight));
                        [self getThumbHTML:bottomImageIndex imageWidth:imageWidth imageHeight:splitHeight rowIndex:thumbRowIndex colIndex:thumbColIndex blockIndex:blockIndex rowTop:rowTop rowMerge: 9999];
                        //if (columnsInCurrentBlock == 1) {adjustImageIndex++;}
                        imagesUsed++;
                    }
                    //in crease row index
                    
                    //[self getThumbHTML:bottomImageIndex imageWidth:imageWidth imageHeight:splitHeight rowIndex:thumbRowIndex colIndex:thumbColIndex blockIndex:blockIndex rowTop:rowTop rowMerge: 9999];
                    //split.append(getThumbHTML(bottomImageIndex, imageWidth, splitHeight));
                
                    if (rowIndex == 0) {
                        blockWidthBuffer += imageWidth + (imageBorderWidth * 2);
                        if (columnsInCurrentBlock > 1) {
                            blockWidthTarget = blockWidthBuffer;
                        }
                        //console.log("before adding split: totalWidthUsed == " + totalWidthUsed);
                        totalWidthUsed += imageWidth + (imageBorderWidth * 2);
                        //console.log("after adding split: totalWidthUsed == " + totalWidthUsed);
                    }
                    rowWidthBuffer += imageWidth + (imageBorderWidth * 2);
                    blockEmpty = false;
                    
                    NSMutableDictionary *topImageDict = [NSMutableDictionary dictionary];
                    //{imageIndex:topImageIndex, width:imageWidth, height:splitHeight}
                    [topImageDict setObject:[NSNumber numberWithInt:topImageIndex] forKey:@"imageIndex"];
                    [topImageDict setObject:[NSNumber numberWithFloat: imageWidth] forKey:@"width"];
                    [topImageDict setObject:[NSNumber numberWithFloat: splitHeight] forKey:@"height"];
                    
                    
                    NSMutableDictionary *bottomImageDict = [NSMutableDictionary dictionary];
                    //{imageIndex:bottomImageIndex, width:imageWidth, height:splitHeight}
                    [bottomImageDict setObject:[NSNumber numberWithInt: bottomImageIndex] forKey:@"imageIndex"];
                    [bottomImageDict setObject:[NSNumber numberWithFloat: imageWidth] forKey:@"width"];
                    [bottomImageDict setObject:[NSNumber numberWithFloat: splitHeight] forKey:@"height"]; 
                    
                    NSMutableDictionary *blockImageDict = [NSMutableDictionary dictionary];
                    //{imageIndex:-1, width:imageWidth, height:splitHeight}
                    [blockImageDict setObject:[NSNumber numberWithInt: -1] forKey:@"imageIndex"];
                    [blockImageDict setObject:[NSNumber numberWithFloat: imageWidth] forKey:@"width"];
                    [blockImageDict setObject:[NSNumber numberWithFloat: splitHeight] forKey:@"height"];
                    
                    NSMutableArray * tmp = (NSMutableArray *)[blockContentsBuffer objectAtIndex: [blockContentsBuffer count] - 1];
                    if ([tmp count] == 0) {
                        [tmp addObject: [NSMutableArray array]];
                    }
                    [(NSMutableArray *)[tmp objectAtIndex: [tmp count] - 1] addObject: topImageDict];
                    
                    [self setImageUsed: topImageIndex imageUsed:undefined];
                    
                    if (bottomImageIndex != undefined) {
                        
                        tmp = (NSMutableArray *)[blockContentsBuffer objectAtIndex: [blockContentsBuffer count] - 1];
                        [(NSMutableArray *)[tmp objectAtIndex: [tmp count] - 1] addObject: bottomImageDict];    
                        [self setImageUsed: bottomImageIndex imageUsed:undefined];
                        
                    } else {
                        /*tmp = (NSMutableArray *)[blockContentsBuffer objectAtIndex: [blockContentsBuffer count] - 1];
                        [(NSMutableArray *)[tmp objectAtIndex: [tmp count] - 1] addObject: blockImageDict];    
                        */
                    }
                    
                } else {
					blockFull = false;
				}
                //splitBuffer = true;
            } else {
                splitBuffer = false;
                if ((rowIndex > 0) && (columnIndex == columnsInCurrentRow - 1)) { // end of a lower row of current block; select a final image that keeps the row lengths as similar as possible.
					//int imageIndex = getImageByAspectRatio((blockWidthBuffer - rowWidthBuffer - (imageBorderWidth * 2)) / rowHeight, undefined, rowHeight, true);
                    imageIndex = [self getImageByAspectRatio: ((blockWidthBuffer - rowWidthBuffer - imageBorderWidth * 2) / rowHeight) excludedImageIndex:undefined imageHeight:rowHeight aspectRatioIsMinimum:true];
                    
					if (imageIndex != undefined) {
                        NSDictionary *imgDict = (NSDictionary *)[self.searchResults objectAtIndex:imageIndex];
						imageWidth = [[imgDict objectForKey:@"width"] intValue];
                        imageHeight = [[imgDict objectForKey:@"height"] intValue];
                        //console.log("imageWidth == " + imageWidth);
                        //NSLog(@"imageWidth == %d - imagename %@",imageWidth, [imgDict objectForKey:@"filename"]);
					}
                    
                    
                    float remainingWidth = blockWidthBuffer - (rowWidthBuffer + imageWidth + (imageBorderWidth * 2));
					if (((columnsInCurrentRow > 1) && (remainingWidth > 2)) || (remainingWidth > (blockWidthBuffer * 0.18)) || (remainingWidth > 180)) { // row not long enough
						fillerImageIndex = [self getImageByAspectRatio: (remainingWidth / rowHeight) excludedImageIndex:imageIndex imageHeight:rowHeight aspectRatioIsMinimum:false];
						if (fillerImageIndex != undefined) {
                            NSDictionary *imgDict = (NSDictionary *)[self.searchResults objectAtIndex:fillerImageIndex];
							fillerImageWidth = [[imgDict objectForKey:@"width"] intValue];
                            //console.log("fillerImageWidth == " + fillerImageWidth);
						}
					}
				} else { // default image usage: take the first available image and add it to end of current row
					imageIndex = [self getFirstUnusedImage:0 excludedImageIndex:undefined imageHeight:rowHeight order:@"random"];
					if (imageIndex != undefined) {
						NSDictionary *imgDict = (NSDictionary *)[self.searchResults objectAtIndex:imageIndex];
						imageWidth = [[imgDict objectForKey:@"width"] floatValue];
                        imageHeight = [[imgDict objectForKey:@"height"] intValue];
                        
					}
				}
                
				if (imageIndex != undefined) {
                    
					                    
                    if ((columnsInCurrentRow == 1) && (blockWidthBuffer > 0)) { // in a lower row of a single-column block, crop image to the existing column width
						if (blockWidthBuffer - (imageBorderWidth * 2) < imageWidth) {
							imageWidth = blockWidthBuffer - (imageBorderWidth * 2);
						}
					}
                    //pKt update from rowHeight -> imageHeight
                    [self getThumbHTML:imageIndex imageWidth:imageWidth imageHeight:rowHeight rowIndex:thumbRowIndex colIndex:thumbColIndex blockIndex:blockIndex rowTop:rowTop rowMerge: 8888];
                    imagesUsed++;
                    //row.append(getThumbHTML(imageIndex, imageWidth, rowHeight));
					if (rowIndex == 0) {
                        blockWidthBuffer += imageWidth + (imageBorderWidth * 2);
                        /*if (columnsInCurrentBlock > 1) {*/blockWidthTarget = blockWidthBuffer;//}
                        //console.log("before adding image: totalWidthUsed == " + totalWidthUsed + "; imageWidth == " + imageWidth);
						totalWidthUsed += imageWidth + (imageBorderWidth * 2);
                        
					}
					rowWidthBuffer += imageWidth + (imageBorderWidth * 2);
                    blockEmpty = false;
                    NSMutableDictionary *blockImageDict = [NSMutableDictionary dictionary];
                    //{imageIndex:-1, width:imageWidth, height:splitHeight}
                    [blockImageDict setObject:[NSNumber numberWithInt: imageIndex] forKey:@"imageIndex"];
                    [blockImageDict setObject:[NSNumber numberWithFloat: imageWidth] forKey:@"width"];
                    [blockImageDict setObject:[NSNumber numberWithFloat: rowHeight] forKey:@"height"];
                    
                    [(NSMutableArray *)[blockContentsBuffer objectAtIndex: [blockContentsBuffer count] - 1] addObject: blockImageDict];
					//blockContentsBuffer[blockContentsBuffer.length-1].push({imageIndex:imageIndex, width:imageWidth, height:rowHeight});
                    [self setImageUsed: imageIndex imageUsed:undefined];
				}
                
				if (fillerImageIndex != undefined) { // build filler image here
                    [self getThumbHTML:fillerImageIndex imageWidth:fillerImageWidth imageHeight:rowHeight rowIndex:thumbRowIndex colIndex:(thumbColIndex + 1) blockIndex:blockIndex rowTop:rowTop rowMerge: 7777];
                    //row.append(getThumbHTML(fillerImageIndex, fillerImageWidth, rowHeight));
                    imagesUsed++;
					rowWidthBuffer += fillerImageWidth + (imageBorderWidth * 2);
                    
                    NSMutableDictionary *blockImageDict = [NSMutableDictionary dictionary];
                    //{imageIndex:-1, width:imageWidth, height:splitHeight}
                    [blockImageDict setObject:[NSNumber numberWithInt: fillerImageIndex] forKey:@"imageIndex"];
                    [blockImageDict setObject:[NSNumber numberWithFloat: fillerImageWidth] forKey:@"width"];
                    [blockImageDict setObject:[NSNumber numberWithFloat: rowHeight] forKey:@"height"];
                    
                    [(NSMutableArray *)[blockContentsBuffer objectAtIndex: [blockContentsBuffer count] - 1] addObject:blockImageDict];
					//blockContentsBuffer[blockContentsBuffer.length-1].push({imageIndex:fillerImageIndex, width:fillerImageWidth, height:rowHeight});
                    [self setImageUsed: fillerImageIndex imageUsed:undefined];
				}
				fillerImageIndex = undefined;
				
                /*if ((imageWidth == undefined) && (fillerImageIndex == undefined)) {
                    blockFull = false;
                }*/
			}
			//incre
            thumbColIndex++;
		}
#ifdef DEBUG
        //NSLog(@"row finished: blockIndex == %d; rowIndex == %d; blockWidthBuffer == %f rowWidthBuffer == %f; blockWidthTarget == %d", blockIndex, rowIndex, blockWidthBuffer, rowWidthBuffer, blockWidthTarget);
        //NSLog(@"row finished: blockIndex == %d - %@", blockIndex, imagesAvailable);
#endif
        //console.log("row finished: " + JSON.stringify(imagesAvailable));
        if (/*columnsInCurrentBlock > */1) {
            if (rowWidthBuffer < blockWidthBuffer) {
                blockWidthTarget = rowWidthBuffer;
            }
        }

		rowTop += (rowHeight + (imageBorderWidth * 2));
        thumbRowIndex++;
	}
    
    if (blockFull && blockWidthTarget) { // go back & adjust widths of all images in the block in order to match all rows
        //console.log("blockWidthTarget == " + blockWidthTarget);
        //NSLog(@"block index == %d blockWidthTarget == %d", blockIndex, blockWidthTarget);
        //NSLog(@"blockContentsBuffer == %@ == count == %d", blockContentsBuffer,blockContentsBuffer.count);
        //console.log("blockContentsBuffer == " + JSON.stringify(blockContentsBuffer));
        int rowTop = 0;
        for (int adjustRowIndex=0; adjustRowIndex < [blockContentsBuffer count]; adjustRowIndex++) {
            NSMutableArray *adjustRowDict = (NSMutableArray *)[blockContentsBuffer objectAtIndex:adjustRowIndex];
            float rowWidth = 0.0f;
            float rowWidthUsed = 0.0f;
            //console.log("adjustRowIndex == " + adjustRowIndex);
            for (int adjustColumnIndex=0; adjustColumnIndex < adjustRowDict.count; adjustColumnIndex++) {
                if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                    rowWidth += [[(NSDictionary *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectForKey:@"width"] floatValue];
                }else if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSArray class]]) {
                    rowWidth += [[(NSDictionary *)[(NSArray *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectAtIndex:0] objectForKey:@"width"] floatValue];
                }
                rowWidth += (imageBorderWidth * 2);
            }
            //NSLog(@"rowWidth == %f; rowWidth / blockWidthTarget == %f",rowWidth, (rowWidth / blockWidthTarget));
            //console.log("rowWidth == " + rowWidth + "; rowWidth / blockWidthTarget == " + (rowWidth / blockWidthTarget));
            //var rowRoundingBuffer = 0;
            for (int adjustColumnIndex=0; adjustColumnIndex < adjustRowDict.count; adjustColumnIndex++) {
                //console.log("adjustColumnIndex == " + adjustColumnIndex + " adjustImageIndex == " + adjustImageIndex);
                float oldWidth = 0.0f;
                if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                    if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                        oldWidth = [[(NSDictionary *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectForKey:@"width"] floatValue];
                    }/*else if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSArray class]]) {
                        for (int j = 0 ; j < [(NSArray *)[adjustRowDict objectAtIndex:adjustColumnIndex]].count ; j++) {
                            oldWidth = [[(NSDictionary *)[(NSArray *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectAtIndex:j] objectForKey:@"width"] floatValue];
                        }
                    }*/
                    float newWidth = (oldWidth * blockWidthTarget / rowWidth);
                    
                    if (adjustColumnIndex == adjustRowDict.count - 1) { // final column: use up exactly the remaining available space
                        newWidth = blockWidthTarget - rowWidthUsed - (imageBorderWidth * 2);
                    }
                    if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                        imageHeight = [[(NSDictionary *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectForKey:@"height"] floatValue];
                    }else if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSArray class]]) {
                        imageHeight = [[(NSDictionary *)[(NSArray *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectAtIndex:0] objectForKey:@"height"] floatValue];
                    }
                    
                    if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                        [(NSDictionary *)[(NSMutableArray *)[blockContentsBuffer objectAtIndex:adjustRowIndex] objectAtIndex:adjustColumnIndex] setValue: [NSNumber numberWithFloat: newWidth] forKey:@"width"];
                    }
                    
                    //[(NSMutableDictionary *)[imageLayout objectAtIndex:adjustImageIndex] setObject: [NSNumber numberWithFloat: newWidth] forKey:@"width"];
                    //block.children().eq(adjustRowIndex).children().eq(adjustColumnIndex).find("img").attr("width", Math.floor(newWidth));
                    rowWidthUsed += floor(newWidth);
                    rowWidthUsed += (imageBorderWidth * 2);
                    
                    int backgroundAdjustImageIndex = undefined;
                    if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                        backgroundAdjustImageIndex = [[(NSDictionary *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectForKey:@"imageIndex"] intValue];
                    }else if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSArray class]]) {
                        backgroundAdjustImageIndex = [[(NSDictionary *)[(NSArray *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectAtIndex:0] objectForKey:@"imageIndex"] intValue];
                    }
                    
                    
                    if (newWidth > 0) {
                        [(NSMutableDictionary *)[imageLayout objectAtIndex:adjustImageIndex] setObject: [NSNumber numberWithFloat: newWidth] forKey:@"width"];
                    }
                    
                    //NSLog(@"adjust: adjustImageIndex ==  %d newWidth ==== %f backgroundAdjustImageIndex == %d", adjustImageIndex, newWidth, backgroundAdjustImageIndex);
                    adjustImageIndex++;
                
                }else{
                    NSArray *subRows = (NSArray *)[adjustRowDict objectAtIndex:adjustColumnIndex];
                    
                    for (int adjustSubRow = 0 ; adjustSubRow < subRows.count ; adjustSubRow++) {
                        rowWidth = 0.0f;
                        rowWidthUsed = 0.0f;
                        oldWidth = [[(NSDictionary *)[subRows objectAtIndex: adjustSubRow] objectForKey:@"width"] floatValue];
                        float newWidth = (oldWidth * blockWidthTarget / rowWidth);
                        
                        if (adjustColumnIndex == adjustRowDict.count - 1) { // final column: use up exactly the remaining available space
                            newWidth = blockWidthTarget - rowWidthUsed - (imageBorderWidth * 2);
                        }
                        
                        imageHeight = [[(NSDictionary *)[subRows objectAtIndex:adjustSubRow] objectForKey:@"height"] floatValue];
                        
                        
                        //if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                            [(NSDictionary *)[subRows objectAtIndex:adjustSubRow] setValue: [NSNumber numberWithFloat: newWidth] forKey:@"width"];
                        //}
                        
                        //[(NSMutableDictionary *)[imageLayout objectAtIndex:adjustImageIndex] setObject: [NSNumber numberWithFloat: newWidth] forKey:@"width"];
                        //block.children().eq(adjustRowIndex).children().eq(adjustColumnIndex).find("img").attr("width", Math.floor(newWidth));
                        rowWidthUsed += floor(newWidth);
                        rowWidthUsed += (imageBorderWidth * 2);
                        
                        int backgroundAdjustImageIndex = undefined;
                        if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSDictionary class]]) {
                            backgroundAdjustImageIndex = [[(NSDictionary *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectForKey:@"imageIndex"] intValue];
                        }else if ([[adjustRowDict objectAtIndex:adjustColumnIndex] isKindOfClass:[NSArray class]]) {
                            backgroundAdjustImageIndex = [[(NSDictionary *)[(NSArray *)[adjustRowDict objectAtIndex:adjustColumnIndex] objectAtIndex:0] objectForKey:@"imageIndex"] intValue];
                        }
                        
                        
                        if (newWidth > 0) {
                            [(NSMutableDictionary *)[imageLayout objectAtIndex:adjustImageIndex] setObject: [NSNumber numberWithFloat: newWidth] forKey:@"width"];
                        }
                        
                        //NSLog(@"adjust sub row: adjustImageIndex ==  %d newWidth ==== %f backgroundAdjustImageIndex == %d", adjustImageIndex, newWidth, backgroundAdjustImageIndex);
                        adjustImageIndex++;
                    }
                }
            }//end for column
            rowTop += (imageHeight + (imageBorderWidth * 2));
        }
        //NSLog(@"blockContentsBuffer == %@", blockContentsBuffer);
    }
	if (! blockFull) { // if a block couldn't be filled, delete it and make all its images available again
		if (imagesUsed == 0) {blockEmpty = true;}
        //console.log("block not full: blockContentsBuffer == " + JSON.stringify(blockContentsBuffer));
        
        if ([[self.imagesAvailable objectAtIndex:0] intValue] == 0 && [[self.imagesAvailable objectAtIndex:1] intValue] == 0 && [[self.imagesAvailable objectAtIndex:2] intValue] == 0 && [[self.imagesAvailable objectAtIndex:3] intValue] == 0) {
            blockEmpty = true;
        }else{
            if (imagesUsed == 0) {blockEmpty = true;}
            for (int adjustRowIndex=0; adjustRowIndex < [blockContentsBuffer count]; adjustRowIndex++) {
                NSMutableArray *temp = (NSMutableArray *)[blockContentsBuffer objectAtIndex:adjustRowIndex];
                for (int adjustColumnIndex=0;adjustColumnIndex < [temp count]; adjustColumnIndex++) {
                    NSMutableArray *temp = (NSMutableArray *)[blockContentsBuffer objectAtIndex:adjustRowIndex];
                    id tempObj = [temp objectAtIndex: adjustColumnIndex];
                    if ([tempObj isKindOfClass:[NSArray class]]) {
                        for (int splitIndex=0; splitIndex < [(NSMutableArray *)temp count]; splitIndex++) {
                            //NSMutableDictionary *tempDict = [(NSMutableArray *)temp objectAtIndex: splitIndex];
                            id tempDict = [(NSMutableArray *)temp objectAtIndex: splitIndex];
                            if ([tempDict isKindOfClass:[NSDictionary class]]) {
                                [self setImageUsed:[[tempDict objectForKey:@"imageIndex"] intValue] imageUsed:false];
                                [self.imageLayout removeLastObject];
                                //adjustImageIndex--;
                            }
                        }
                    }else {
                        //single image
                        if ([tempObj isKindOfClass: [NSDictionary class]]) {
                            [self setImageUsed:[[(NSMutableDictionary *)tempObj objectForKey:@"imageIndex"] intValue] imageUsed:false];
                            [self.imageLayout removeLastObject];
                            //adjustImageIndex--;
                        }
                    }
                }
            }
            [self getLayout:@"rowsInCurrentBlock"]; // increment layoutRowsIndex in order to avoid having 2 consecutive blocks with the same number of rows
            [self getLayout: @"columnsInCurrentBlock"]; // increment layoutColumnsIndex to keep it in phase with layoutRowsIndex, so that splits can only occur in a 2-row single-column block
        }
	}
    
    
	
    //need to update block width info
    int bw = (blockWidthTarget > 0 ? blockWidthTarget : blockWidthBuffer);
    [self.blockWidths insertObject:[NSNumber numberWithInt: bw] atIndex: blockIndex];
    /*if ([self.searchResults count] == 1) {
        return YES;
    }*/
    return blockEmpty;
}//end buildBlock function


- (int) getImageByAspectRatio:(double)aspectRatio excludedImageIndex:(int)_excludedImageIndex imageHeight: (float)_imageHeight aspectRatioIsMinimum:(BOOL) _aspectRatioIsMinimum {
    //NSLog(@"getImageByAspectRatio: aspectRatio == %f",aspectRatio);
    float roundedImageHeight = round(_imageHeight);
    //NSLog(@"before: aspectRatio == %f; roundedImageHeight == %f", aspectRatio,roundedImageHeight);
    for (int imageHeightIndex=0; imageHeightIndex < [self.imageHeights count]; imageHeightIndex++) { // if no images available in the specified size, crop down a bigger one
        if ([[self.imageHeights objectAtIndex:imageHeightIndex] floatValue] >= roundedImageHeight) {
            if (imageHeightIndex < self.imagesAvailable.count && [[self.imagesAvailable objectAtIndex:imageHeightIndex] intValue] > 0) {
                if ([[self.imageHeights objectAtIndex:imageHeightIndex] floatValue] >= roundedImageHeight) {
                    //console.log("getImageByAspectRatio - crop: " + imageHeights[imageHeightIndex] + " to " + roundedImageHeight);
                    //NSLog(@"getImageByAspectRatio - crop: %f to %f",[[self.imageHeights objectAtIndex:imageHeightIndex] floatValue], roundedImageHeight);
                    _imageHeight = [[self.imageHeights objectAtIndex:imageHeightIndex] floatValue];
                    roundedImageHeight = _imageHeight;
                }
                break;
            }
        }
    }
    //console.log("after: aspectRatio == " + aspectRatio + "; roundedImageHeight == " + roundedImageHeight);
    //NSLog(@"after: aspectRatio == %f; roundedImageHeight == %f", aspectRatio,roundedImageHeight);
    float idealImageWidth = round(_imageHeight * aspectRatio);
    int startImageIndex = 0; // only search images of the correct height
    int endImageIndex = [self.searchResults count];
    for (int imageHeightIndex=0; imageHeightIndex < self.imageHeights.count; imageHeightIndex++) {
        if (round([[self.imageHeights objectAtIndex:imageHeightIndex] floatValue]) == roundedImageHeight) {
            if (idealImageWidth < [[self.minWidths objectAtIndex:imageHeightIndex] floatValue]) {
                //NSLog(@"getImageByAspectRatio: narrowest");
                return [self getFirstUnusedImage: undefined excludedImageIndex: _excludedImageIndex imageHeight: _imageHeight order: @"ascending"];
            } // no sufficiently narrow image exists; take the narrowest that has not yet been used
            if (idealImageWidth > [[maxWidths objectAtIndex:imageHeightIndex] floatValue]) {
                //NSLog(@"getImageByAspectRatio: widest");
                return [self getFirstUnusedImage: undefined excludedImageIndex: _excludedImageIndex imageHeight: _imageHeight order: @"descending"];
            } // no sufficiently wide image exists; take the widest that has not yet been used
            if (imageHeightIndex < self.searchResultsHeightBoundaries.count && startImageIndex < [[self.searchResultsHeightBoundaries objectAtIndex:imageHeightIndex] floatValue]) {
                startImageIndex = [[self.searchResultsHeightBoundaries objectAtIndex:imageHeightIndex] floatValue];
            }
            if (imageHeightIndex < self.imageHeights.count - 1) {//TODO -1 or -2???
                endImageIndex = [[self.searchResultsHeightBoundaries objectAtIndex:imageHeightIndex + 1] intValue];
            }
            //console.log("getImageByAspectRatio: imagesAvailable == " + JSON.stringify(imagesAvailable));
            //NSLog(@"getImageByAspectRatio: imagesAvailable == %@",imagesAvailable);
            //console.log("getImageByAspectRatio: startImageIndex == " + startImageIndex + "; endImageIndex == " + endImageIndex);
            //NSLog(@"getImageByAspectRatio: startImageIndex == %d; endImageIndex == %d", startImageIndex,endImageIndex);
            break;
        }
    }
    int searchJump = round((endImageIndex - startImageIndex) / 2);
    int searchJumpBuffer;
    int targetImageIndex = undefined;
    float targetImageWidth = 0; // arbitrary start value that the search will be forced to improve on
    int backupImageIndex = -1; // if aspectRatioIsMinimum, return an image with a lower aspect ratio as backup rather than failing entirely
    float backupImageWidth = undefined;
    float searchImageWidth;
    //console.log("searching for: aspectRatio == " + aspectRatio + "; excludedImageIndex == " + excludedImageIndex + "; idealImageWidth == " + idealImageWidth + "; imageHeight == " + imageHeight + "; aspectRatioIsMinimum == " + aspectRatioIsMinimum);
    //NSLog(@"searching for: aspectRatio == %f; excludedImageIndex == %d; idealImageWidth == %f; imageHeight == %f; aspectRatioIsMinimum == %d", aspectRatio, _excludedImageIndex , idealImageWidth,_imageHeight, _aspectRatioIsMinimum);
    int searchImageIndex = startImageIndex + searchJump;
    BOOL searchFinished = false;
    
    do {
        if (searchImageIndex >= self.searchResults.count) {
            break;
        }
        
        NSDictionary *_dict = (NSDictionary*)[self.searchResults objectAtIndex:searchImageIndex];
        float _width = [[_dict objectForKey:@"width"] floatValue];
        int _used = [[_dict objectForKey:@"used"] boolValue];
        int _priority = [[_dict objectForKey:@"priority"] intValue];
        
       
        searchImageWidth =  _width;//searchResults[searchImageIndex].width;
        
        if ((! _used) && (searchImageIndex != _excludedImageIndex)) { // only accept an image other than the one whose index is excludedImageIndex; this allows 2 different images to be picked before either is marked as used
            if (abs(searchImageWidth - idealImageWidth) < abs(targetImageWidth - idealImageWidth)) {
                if ((! _aspectRatioIsMinimum) || (searchImageWidth >= idealImageWidth)) { // if aspectRatioIsMinimum, only accept images that are at least as wide as specified by aspectRatio
                    if (_priority >= self.currentPriorityIndex) { // if image is of high enough priority, use it...
                        targetImageIndex = searchImageIndex;
                        targetImageWidth = searchImageWidth;
                    } else if (backupImageIndex == undefined) { // ...or if not, only use it as the backup if there isn't one already
                        backupImageIndex = searchImageIndex;
                        backupImageWidth = searchImageWidth;
                    }
                    //console.log("improving: targetImageWidth == " + targetImageWidth);
                }
                if (_priority >= self.currentPriorityIndex) { // if image is of high enough priority, use it as the backup...
                    backupImageIndex = searchImageIndex;
                    backupImageWidth = searchImageWidth;
                } else if (backupImageIndex == undefined) { // ...or if not, only use it as the backup if there isn't one already
                    backupImageIndex = searchImageIndex;
                    backupImageWidth = searchImageWidth;
                }
                //console.log("backup: backupImageWidth == " + backupImageWidth);
            } else {
                searchFinished = true;
            }
        }
        
        
        /*
        if ((!_used) && (searchImageIndex != _excludedImageIndex)) { // only accept an image other than the one whose index is excludedImageIndex; this allows 2 different images to be picked before either is marked as used
            if (abs(searchImageWidth - idealImageWidth) < abs(targetImageWidth - idealImageWidth)) {
                if ((! _aspectRatioIsMinimum) || (searchImageWidth >= idealImageWidth)) { // if aspectRatioIsMinimum, only accept images that are at least as wide as specified by aspectRatio
                    targetImageIndex = searchImageIndex;
                    targetImageWidth = searchImageWidth;
                    //console.log("improving: targetImageWidth == " + targetImageWidth);
                }
                backupImageIndex = searchImageIndex;
                backupImageWidth = searchImageWidth;
                //console.log("backup: backupImageWidth == " + backupImageWidth);
            } else {
                searchFinished = true;
            }
        }*/
        searchJumpBuffer = searchJump; // remember the previous value of searchJump in order to stop the search when no finer movement along the array is possible
        searchJump = searchJump / 2;//parseInt(String(searchJump / 2));
        if (searchImageWidth < idealImageWidth) { // image found is too narrow; look for a later (wider) one
            searchJump = abs(searchJump);
            searchImageIndex += searchJump;
            if (searchImageIndex >= endImageIndex) {
                searchFinished = true;
            } // reached endImageIndex; stop search
        } else if (searchImageWidth > idealImageWidth) { // image found is too wide; look for a later (narrower) one
            searchJump = - abs(searchJump);
            searchImageIndex += searchJump;
            if (searchImageIndex < startImageIndex) {
                searchFinished = true;
            } // reached startImageIndex; stop search
        } else {
            if (_used /*&& searchImageIndex + 1 < self.searchResults.count*/) { // image found is correct width but already used; search forwards from here for an unused image of at least the same size
                return [self getFirstUnusedImage: (searchImageIndex + 1) excludedImageIndex: undefined imageHeight: _imageHeight order: @""];
                //return [self getFirstUnusedImage: (searchImageIndex + 1) excludedImageIndex: _excludedImageIndex imageHeight: _imageHeight order: @""];
            } else {
                searchFinished = true;
            }
        }
    } while ((searchJump != searchJumpBuffer) && (!searchFinished));
    if (targetImageIndex != undefined) {
        //console.log("best: targetImageIndex == " + targetImageIndex + "; filename == " + searchResults[targetImageIndex].filename);
        //NSLog(@"best: targetImageIndex == %d filename == %@", targetImageIndex, [(NSDictionary *) [self.searchResults objectAtIndex: targetImageIndex] objectForKey:@"filename"]);
    } else if (backupImageIndex != undefined) {
        //console.log("backup: backupImageIndex == " + backupImageIndex + "; filename == " + searchResults[backupImageIndex].filename);
        //NSLog(@"backup: backupImageIndex == %d filename == %@", backupImageIndex, [(NSDictionary *) [self.searchResults objectAtIndex: backupImageIndex] objectForKey:@"filename"]);
    }
    
    if (targetImageIndex != undefined) {
        return targetImageIndex;
    } else if (backupImageIndex != undefined) {
        return backupImageIndex;
    } else {
        return [self getFirstUnusedImage:searchImageIndex excludedImageIndex: _excludedImageIndex imageHeight:_imageHeight order:@"random"];
        //return [self getFirstUnusedImage:undefined excludedImageIndex: _excludedImageIndex imageHeight:_imageHeight order:@"random"];
    }
}
                                        
                                        
                                        
- (int)getFirstUnusedImage:(int)startImageIndex excludedImageIndex:(int)_excludedImageIndex imageHeight:(float)_imageHeight order: (NSString *)_order {
#ifdef DEBUG
    NSLog(@"searchResultsHeightBoundaries == %@ ; imageHeights == %@", [searchResultsHeightBoundaries componentsJoinedByString:@","], [imageHeights componentsJoinedByString:@","]);
#endif
	BOOL ascendingOrder = true;
    BOOL searchImages = false; // NEW: this line
    
    int targetimageIndex = undefined;
    int backupImageIndex = undefined;
    
	if (_order && [_order isEqualToString:@"descending"]) {
        ascendingOrder = false;
	}
    int roundedImageHeight = round(_imageHeight);
    float minImageWidth = 0;
    
    if (startImageIndex != undefined) {
		if (startImageIndex > searchResults.count - 1) {
            startImageIndex = searchResults.count - 1;
        } // NEW: this line
        minImageWidth = [[(NSDictionary*)[self.searchResults objectAtIndex:startImageIndex] objectForKey:@"width"] floatValue];
	} else {
		startImageIndex = 0;
		ascendingOrder = true; // NEW: this line
	}
    
    
    int endImageIndex = [self.searchResults count]; // start the search with images of the correct height
    int imageHeightIndex = 0;
    
    for (imageHeightIndex=0; imageHeightIndex < self.imageHeights.count; imageHeightIndex++) {
		if (round([[self.imageHeights objectAtIndex: imageHeightIndex] floatValue]) == roundedImageHeight) {
			if ([[imagesAvailable objectAtIndex:imageHeightIndex] intValue] > 0) {searchImages = true;} // NEW: this line
			if (startImageIndex < [[self.searchResultsHeightBoundaries objectAtIndex:imageHeightIndex] intValue]) {
                startImageIndex = [[self.searchResultsHeightBoundaries objectAtIndex:imageHeightIndex] intValue];
            }
			if (imageHeightIndex < imageHeights.count - 1) {
                endImageIndex = [[self.searchResultsHeightBoundaries objectAtIndex:imageHeightIndex + 1] intValue];
            }
			break;
		}
	}
    if (searchResults.count == 1) {// NEW: this if statement
		if (([[imagesAvailable objectAtIndex:imageHeightIndex] intValue] == 1) && (_excludedImageIndex == undefined)) {
			targetimageIndex = 0;
        }
		searchImages = false;
	}
    
    if (searchImages) { // NEW: this line
        if (_order && [_order isEqualToString:@"random"]) { // random mode searches forwards, but starting from a random point within the images of the desired size
            //TODO: fix get random
            //startImageIndex += (floor(getPseudoRandom() * (endImageIndex - startImageIndex)));
            startImageIndex += floor([self getPseudoRandom] * (endImageIndex - startImageIndex));
        }
        
        int searchImageIncrement = 1;
        if (! ascendingOrder) { // if not ascendingOrder, search in reverse order (from widest to narrowest images)
            startImageIndex = endImageIndex - 1;
            endImageIndex = -1;//0; // search through less tall images too
            searchImageIncrement = -1;
        } else {
            endImageIndex = self.searchResults.count; // search through taller images too
        }
        
        for (self.currentPriorityIndex = [[self.priorityList objectAtIndex:0] intValue] ; self.currentPriorityIndex >= 0; self.currentPriorityIndex--) {
        
            for (int searchImageIndex = startImageIndex; searchImageIndex!=endImageIndex ; searchImageIndex+=searchImageIncrement) {
                
                int _height = ([[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"height"] intValue]);
                int _width = ([[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"width"] intValue]);
                BOOL _used = [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"used"] boolValue];
                int _priority = ([[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"priority"] intValue]);
                
                if ((_width >= minImageWidth) && (_height >= roundedImageHeight) && (_priority >= self.currentPriorityIndex) && (!_used) && (searchImageIndex != _excludedImageIndex)) {
                    targetimageIndex = searchImageIndex;
                    break;
                } else if ((_height == roundedImageHeight) && (_priority >= self.currentPriorityIndex) && (! _used) && (searchImageIndex != _excludedImageIndex)) {
                    targetimageIndex = searchImageIndex;
                    break;
                } else if ((_height >= roundedImageHeight) && (_priority >= self.currentPriorityIndex) && (! _used) && (searchImageIndex != _excludedImageIndex)) {
                    targetimageIndex = searchImageIndex;
                    break;
                }else if ((_height >= roundedImageHeight) && (! _used) && (searchImageIndex != _excludedImageIndex)) {
                    backupImageIndex = searchImageIndex;
                }
            }
            //NSLog(@"tilingCollectResults count == %d", imageLayout.count);
            if (targetimageIndex != undefined) {
                break;
            }
        }
    } // NEW: this line
    
    
    //NSLog(@"targetimageIndex == %d", targetimageIndex);
    if (targetimageIndex > undefined) {
        //console.log("getFirstUnusedImage: targetimageIndex == " + targetimageIndex + "; filename == " + searchResults[targetimageIndex].filename);
#ifdef DEBUG
        NSLog(@"getFirstUnusedImage: targetimageIndex == %d; filename == %@",targetimageIndex, [(NSDictionary *)[self.searchResults objectAtIndex:targetimageIndex] objectForKey:@"filename"]);
#endif
        return targetimageIndex;
    }else {
        if (imageHeightIndex < 3) {
            float ih = [[self.imageHeights objectAtIndex: imageHeightIndex + 1] floatValue];
            return [self getFirstUnusedImage: startImageIndex excludedImageIndex: _excludedImageIndex imageHeight: ih order: _order];
        } else if (backupImageIndex > undefined) {
            return backupImageIndex;
        }
    }
    
    return targetimageIndex;
}


// read layout data from arrays, to make it look random but to produce the same layout each time the same search results are returned
-(int) getLayout:(NSString *)layoutParameter { 
    int layout = 0;
    if([layoutParameter isEqualToString:@"rowsInCurrentBlock"]) {
        layout = [[self.layoutRows objectAtIndex:layoutRowsIndex] intValue];
        if ([self getEmergencyLayoutMode: [[self.layoutRows objectAtIndex:layoutRowsIndex] intValue]]) {layout = 2;} // NEW: this line
        layoutRowsIndex++;
        if (layoutRowsIndex >= [self.layoutRows count]) {
            layoutRowsIndex = 0;
        }
    }else if ([layoutParameter isEqualToString:@"columnsInCurrentBlock"]) {
        layout = [[self.layoutColumns objectAtIndex: layoutColumnsIndex] intValue];
        if ([self getEmergencyLayoutMode: [[self.layoutRows objectAtIndex:layoutRowsIndex] intValue]]) {layout = 1;} // NEW: this line
        layoutColumnsIndex++;
        if (layoutColumnsIndex >= [self.layoutColumns count]) {
            layoutColumnsIndex = 0;
        }
    }else if ([layoutParameter isEqualToString: @"columnSplit"])  {
        layout = [[self.layoutSplits objectAtIndex:layoutSplitsIndex] intValue];
        if ([self getEmergencyLayoutMode: [[self.layoutRows objectAtIndex:layoutRowsIndex] intValue]]) {layout = 1;} // NEW: this line
        layoutSplitsIndex++;
        if (layoutSplitsIndex >= [self.layoutSplits count]) {
            layoutSplitsIndex = 0;
        }
    }else if ([layoutParameter isEqualToString:@"rowMerge"]) {
        layout = [[self.layoutMerges objectAtIndex: layoutMergesIndex] intValue];
        if ([self getEmergencyLayoutMode: [[self.layoutRows objectAtIndex:layoutRowsIndex] intValue]]) {layout = 0;} // NEW: this line
        layoutMergesIndex++;
        if (layoutMergesIndex >= [self.layoutMerges count]) {
            layoutMergesIndex = 0;
        }
    }
    return layout;
}

-(float) getAverageAspectRatio:(float)imageHeight { // NEW: this function is completely rewritten // calculate the average aspect ratio of all images in the search results of the given height
    if (imageHeights.count == 0) { // on first use of this function, calculate & cache all relevant data
        //imageHeights = [181, 243, 368, 493]; // hard-coded values, to make it work if not all heights exist in the search results. as currently implemented, only the heights in this array can be used
        self.imageHeights = [NSMutableArray arrayWithObjects:[NSNumber numberWithFloat:181.0],[NSNumber numberWithFloat:243.0],[NSNumber numberWithFloat:368.0],[NSNumber numberWithFloat:493.0], nil];
		self.searchResultsHeightBoundaries = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:-1],[NSNumber numberWithInt:-1],[NSNumber numberWithInt:-1],[NSNumber numberWithInt:-1], nil];
		self.averageAspectRatios = [NSMutableArray arrayWithObjects:[NSNumber numberWithFloat:1],[NSNumber numberWithFloat:1],[NSNumber numberWithFloat:1],[NSNumber numberWithFloat:1], nil];
		
        self.minWidths = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], nil];
        self.maxWidths = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], nil];
        self.imagesAvailable = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], nil];
        
		float searchImageWidth = 0;
		float searchImageHeight = 0;
		float searchImageHeightBuffer = 0;
		float maxWidth = 0;
		float totalWidth = 0;
		float totalHeight = 0;
		int imageCount = 0;
        int imageHeightIndex = 0;
		for (int searchImageIndex=0; searchImageIndex<searchResults.count; searchImageIndex++) {
			//searchImageWidth = searchResults[searchImageIndex].width;
			//searchImageHeight = searchResults[searchImageIndex].height;
            searchImageWidth = [[(NSDictionary *) [self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"width"] floatValue];
            searchImageHeight = [[(NSDictionary *) [self.searchResults objectAtIndex:searchImageIndex] objectForKey:@"height"] floatValue];
            /*
			switch (searchImageHeight) { // hard-coded image heights again - see above
				case 181: imageHeightIndex = 0; break;
				case 243: imageHeightIndex = 1; break;
				case 368: imageHeightIndex = 2; break;
				case 493: imageHeightIndex = 3; break;
			}*/
            if (searchImageHeight <= 181) {
                imageHeightIndex = 0;
            }else if (searchImageHeight <= 243) {
                imageHeightIndex = 1;
            }else if (searchImageHeight <= 368) {
                imageHeightIndex = 2;
            }else if (searchImageHeight <= 493) {
                imageHeightIndex = 3;
            }
            
            
            if ([[searchResultsHeightBoundaries objectAtIndex:imageHeightIndex] intValue] == -1) {
                [self.searchResultsHeightBoundaries replaceObjectAtIndex:imageHeightIndex withObject:[NSNumber numberWithInt: searchImageIndex]];
            }
            
			if ([[averageAspectRatios objectAtIndex:imageHeightIndex] floatValue] == 0.0) {
				[averageAspectRatios replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat: searchImageWidth / searchImageHeight]];
			} else {
				float oldFraction = [[imagesAvailable objectAtIndex:imageHeightIndex] floatValue] / ([[imagesAvailable objectAtIndex:imageHeightIndex] floatValue] + 1);
				float newFraction = 1.0 / ([[imagesAvailable objectAtIndex:imageHeightIndex] floatValue] + 1);
                float newAvgRatio = ([[averageAspectRatios objectAtIndex:imageHeightIndex] floatValue] * oldFraction) + ((searchImageWidth / searchImageHeight) * newFraction);
                [averageAspectRatios replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat: newAvgRatio]];
				//averageAspectRatios[imageHeightIndex] = (averageAspectRatios[imageHeightIndex] * oldFraction) + ((searchImageWidth / searchImageHeight) * newFraction)
			}
			if ([[minWidths objectAtIndex:imageHeightIndex] floatValue] == 0.0) {
				[minWidths replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat: searchImageWidth]];
            } else if (searchImageWidth < [[minWidths objectAtIndex:imageHeightIndex] floatValue]) {
				[minWidths replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat: searchImageWidth]];
			}
			if (searchImageWidth > [[maxWidths objectAtIndex:imageHeightIndex] floatValue]) {
                [maxWidths replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat: searchImageWidth]];
            }
            [imagesAvailable replaceObjectAtIndex: imageHeightIndex withObject: [NSNumber numberWithInt: [[imagesAvailable objectAtIndex:imageHeightIndex] intValue] + 1]];
            totalWidth += searchImageWidth;
			totalHeight += searchImageHeight;
            
            [(NSMutableDictionary*)[self.searchResults objectAtIndex: searchImageIndex] setValue: [NSNumber numberWithBool: FALSE] forKey:@"used"];
			//searchResults[searchImageIndex].used = false;
		}
		for (imageHeightIndex = imageHeights.count-1; imageHeightIndex>=0; imageHeightIndex--) { // correct any values in searchResultsHeightBoundaries that have not been set yet
			if ([[searchResultsHeightBoundaries objectAtIndex:imageHeightIndex] intValue] == -1) {
				if (imageHeightIndex == imageHeights.count - 1) {
					[searchResultsHeightBoundaries replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithInt: self.searchResults.count - 1]];
                    //searchResultsHeightBoundaries[imageHeightIndex] = searchResults.length - 1;
				} else {
                    [searchResultsHeightBoundaries replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithInt: [[searchResultsHeightBoundaries objectAtIndex: imageHeightIndex + 1] intValue]]];
					//searchResultsHeightBoundaries[imageHeightIndex] = searchResultsHeightBoundaries[imageHeightIndex + 1];
				}
			}
		}
	}
    
    /*console.log("getAverageAspectRatio: imageHeights == " + imageHeights);
    console.log("getAverageAspectRatio: searchResultsHeightBoundaries == " + searchResultsHeightBoundaries);
    console.log("getAverageAspectRatio: averageAspectRatios == " + averageAspectRatios);
    console.log("getAverageAspectRatio: minWidths == " + minWidths);
    console.log("getAverageAspectRatio: maxWidths == " + maxWidths);
    console.log("getAverageAspectRatio: imagesAvailable == " + imagesAvailable);
    */
    
	for (int imageHeightIndex=0; imageHeightIndex < imageHeights.count; imageHeightIndex++) {
		if (round([[imageHeights objectAtIndex:imageHeightIndex] floatValue]) == round(imageHeight)) {
            //console.log("getAverageAspectRatio: imageHeight == " + imageHeight + "; averageAspectRatios[imageHeightIndex] == " + averageAspectRatios[imageHeightIndex]);
			return [[self.averageAspectRatios objectAtIndex:imageHeightIndex] floatValue];
		}
	}
    return 0.0;
}

/*
// calculate the average aspect ratio of all images in the search results of the given height
- (float)getAverageAspectRatio:(float)imageHeight {     
    if (!averageAspectRatiosCalculated) { 
        // on first use of this function, calculate & store globally all relevant data
        self.searchResultsHeightBoundaries = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0] , nil];
        self.averageAspectRatios = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], [NSNumber numberWithInt:1], nil];
        self.minWidths = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], nil];
        self.maxWidths = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], nil];
        self.imagesAvailable = [NSMutableArray arrayWithObjects: [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], nil];
        int imageHeightIndex = 0;
            int searchImageWidth = 0;
            int searchImageHeight = 0;
        float searchImageHeightBuffer = [[self.imageHeights objectAtIndex:0] floatValue];
        //float maxWidth = 0;
        float totalWidth = 0.0f;
        float totalHeight = 0.0f;
        int imageCount = 0;
        
        for (int searchImageIndex=0 ; searchImageIndex < [self.searchResults count]; searchImageIndex++) {
            
            searchImageWidth = [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey : @"width"] floatValue];
            searchImageHeight = [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] objectForKey : @"height"] floatValue];
            if (searchImageHeight > searchImageHeightBuffer) {
                if ((totalWidth > 0) && (totalHeight > 0)) {
                    double _temp = totalWidth / totalHeight;
                    [self.averageAspectRatios replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithDouble: _temp]];
                }
                [self.imagesAvailable replaceObjectAtIndex:imageHeightIndex withObject:[NSNumber numberWithInt:imageCount]];
                
                for (int searchImageHeightIndex=0; searchImageHeightIndex < [self.imageHeights count]; searchImageHeightIndex++) {
                    if ([[self.imageHeights objectAtIndex:searchImageHeightIndex] floatValue] == searchImageHeight) {
                        imageHeightIndex = searchImageHeightIndex;
                        break;
                    }
                }
                [self.searchResultsHeightBoundaries replaceObjectAtIndex:imageHeightIndex withObject:[NSNumber numberWithInt: searchImageIndex]];
                [self.minWidths replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat:searchImageWidth]];
                totalWidth = searchImageWidth;
                totalHeight = searchImageHeight;
                imageCount = 1;
            } else {
                if (searchImageWidth < [[self.minWidths objectAtIndex:imageHeightIndex] floatValue]) {
                    [self.minWidths replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat:searchImageWidth]];
                }
                if (searchImageWidth > [[maxWidths objectAtIndex:imageHeightIndex] floatValue]) {
                    [maxWidths replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat:searchImageWidth]];
                }
                totalWidth += searchImageWidth;
                totalHeight += searchImageHeight;
                imageCount++;
            }
            searchImageHeightBuffer = searchImageHeight;
        }
        if ((totalWidth > 0) && (totalHeight > 0)) {
            [averageAspectRatios replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithFloat: totalWidth  / totalHeight]];
        }
        [self.imagesAvailable replaceObjectAtIndex:imageHeightIndex withObject:[NSNumber numberWithInt:imageCount]];    
        
        averageAspectRatiosCalculated = true;
    }
    for (int imageHeightIndex=0; imageHeightIndex < [self.imageHeights count]; imageHeightIndex++) {
        if ([[self.imageHeights objectAtIndex:imageHeightIndex] floatValue]  == round(imageHeight)) {
            return [[self.averageAspectRatios objectAtIndex:imageHeightIndex] floatValue];
        }
    }
    return 0;
}*/

- (void) setImageUsed:(int)searchImageIndex imageUsed: (BOOL) used { // record the use of an image, to prevent re-use
    if (used == undefined) {
        used = true;
    }
    if ((searchImageIndex != undefined) && (searchImageIndex < self.searchResults.count)) {
        [(NSMutableDictionary*)[self.searchResults objectAtIndex: searchImageIndex] setValue: [NSNumber numberWithBool: used] forKey:@"used"];
        
        for (int imageHeightIndex=0; imageHeightIndex < self.imageHeights.count; imageHeightIndex++) {
            if (imageHeightIndex < self.imagesAvailable.count && [[self.imageHeights objectAtIndex:imageHeightIndex] floatValue] == [[(NSDictionary *)[self.searchResults objectAtIndex:searchImageIndex] valueForKey:@"height"] floatValue]) {
                int temp = [[self.imagesAvailable objectAtIndex: imageHeightIndex] intValue];
                [self.imagesAvailable replaceObjectAtIndex:imageHeightIndex withObject: [NSNumber numberWithInt:temp + (used ? -1 : 1)]];
                //[self.imagesAvailable imageHeightIndex] += (used ? -1 : 1);
                break;
            }
        }
    }
}

-(void) getThumbHTML:(int)imageIndex imageWidth: (int)iw imageHeight: (int)ih rowIndex: (int)_rowIndex colIndex:(int)_colIndex blockIndex:(int)_bi rowTop: (int)rt rowMerge: (int) rm{
    //NSLog(@"getThumbHTML: imageIndex == %d ; imageWidth == %d; imageHeight == %d", imageIndex, iw, ih);
    //TODO
    //thumbHTML = "<div class=\"image\"><img src=\"images/spacer.png\" width=\"" + imageWidth + "\" height = \"" + Math.round(imageHeight) + "\" alt=\"\" /></div>";
    NSMutableDictionary *imageData = [NSMutableDictionary dictionary];
    if (imageIndex  != undefined && imageIndex < self.searchResults.count) {
        [imageData setObject: [NSNumber numberWithInt: imageIndex] forKey:@"imageIndex"];
        [imageData setObject: [(NSDictionary *)[self.searchResults objectAtIndex: imageIndex] objectForKey:@"filename"] forKey:@"filename"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"left"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"top"];
        [imageData setObject: [NSNumber numberWithInt: iw] forKey:@"width"];
        [imageData setObject: [NSNumber numberWithInt: ih] forKey:@"height"];
        
        [imageData setObject: [NSNumber numberWithInt: _rowIndex] forKey:@"rowIndex"];
        [imageData setObject: [NSNumber numberWithInt: _colIndex] forKey:@"colIndex"];
        [imageData setObject: [NSNumber numberWithInt: _bi] forKey:@"blockIndex"];
        [imageData setObject: [NSNumber numberWithInt: rt] forKey:@"rowTop"];
        [imageData setObject: [NSNumber numberWithInt: rm] forKey:@"rowMerge"];
        [imageData setObject: [NSNumber numberWithInt: self.currentPriorityIndex] forKey:@"priority"];
        //[imageData setObject: [NSNumber numberWithInt: _blockWidth] forKey:@"blockWidth"];
        //var imageData = {imageIndex:imageIndex, filename:searchResults[imageIndex].filename, left:0, top:0, width:imageWidth, height:imageHeight};
    } else {
        [imageData setObject: [NSNumber numberWithInt: -1] forKey:@"imageIndex"];
        [imageData setObject: @"" forKey:@"filename"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"left"];
        [imageData setObject: [NSNumber numberWithFloat: 0] forKey:@"top"];
        [imageData setObject: [NSNumber numberWithInt: iw] forKey:@"width"];
        [imageData setObject: [NSNumber numberWithInt: ih] forKey:@"height"];
        [imageData setObject: [NSNumber numberWithInt: _rowIndex] forKey:@"rowIndex"];
        [imageData setObject: [NSNumber numberWithInt: _colIndex] forKey:@"colIndex"];
        [imageData setObject: [NSNumber numberWithInt: _bi] forKey:@"blockIndex"];
        [imageData setObject: [NSNumber numberWithInt: rt] forKey:@"rowTop"];
        [imageData setObject: [NSNumber numberWithInt: rm] forKey:@"rowMerge"];
        [imageData setObject: [NSNumber numberWithInt: self.currentPriorityIndex] forKey:@"priority"];
        //[imageData setObject: [NSNumber numberWithInt: _blockWidth] forKey:@"blockWidth"];
        //var imageData = {imageIndex:-1, filename:"", left:0, top:0, width:imageWidth, height:imageHeight};
    }
    [self.imageLayout addObject: imageData];
    //return thumbHTML;
}

- (double) getPseudoRandom {
	double pseudoRandomNumber = [[self.pseudoRandomNumbers objectAtIndex: self.pseudoRandomNumbersIndex] doubleValue];
	self.pseudoRandomNumbersIndex++;
	if (pseudoRandomNumbersIndex >= [self.pseudoRandomNumbers count]) {
        self.pseudoRandomNumbersIndex = 0;
    }
	return pseudoRandomNumber;
}

#pragma end titling.js

@end
