//
//  APICommon.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 19/8/14.
//
//

#import "APICommon.h"
#import "JSONResponseSerializerWithData.h"

@implementation APICommon

static APICommon *sharedInstance = nil;

+ (APICommon *)sharedInstance{
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)init{
    self = [super init];
    
    if (self) {
        // Work your initialising magic here as you normally would
    }
    return self;
}



- (void)performRequest:(NSString *)urlRequest apiName: (NSString *)apiName requestMethod: (NSString *) method params: (NSDictionary *)param onBaseUrl:(NSString *)baseUrl callback:(void (^)(id resp))successCalback failedCallback:(void (^)(NSString *msg))failedCalback{
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", baseUrl, urlRequest];
    
    NSLog(@"Perform request %@ with params %@", requestURL, param);
    
    if([method isEqualToString:kReq_Method_Post]){
        AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
        
        operationManager.responseSerializer = [AFJSONResponseSerializer serializer];
        operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        [operationManager.requestSerializer setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
        [operationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept-Encoding"];
        [operationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
//        [operationManager.responseSerializer setAcceptableContentTypes:[operationManager.responseSerializer.acceptableContentTypes setByAddingObject: @"application/json"]];

        
        //[operationManager.responseSerializer setAcceptableContentTypes:[operationManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"]];
        [operationManager POST: requestURL
                    parameters: param
                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                           NSLog(@"responseObject %@ ",responseObject);
                           NSDictionary *response = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
                           int rspCode = (int)operation.response.statusCode;
                           if (rspCode == 200) {
                               if (successCalback) {
                                   successCalback(response);
                               }
                           }else{
                               failedCalback (@"Server Error");
                           }
                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                           //NSLog(@"POST Failed %@ %@", operation.responseString, error);
                           NSLog(@"request 1 failed == %@ == response == %@  code %d - error %@", operation.request, operation.responseString, operation.response.statusCode, error);
                               if (failedCalback) {
                                   failedCalback(error.description);
                               }
                       }];
    }
    else{
        AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
//        [operationManager.requestSerializer setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
//        operationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];

        operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        [operationManager GET:requestURL
                   parameters:param
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          NSLog(@"responseObject %@ ",operation.responseString);
//                          NSDictionary *response = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
                          int rspCode = (int)operation.response.statusCode;
                          if (rspCode == 200) {
                              if (successCalback) {
                                  successCalback(operation.responseString);
                              }
                          }else{
                              failedCalback (@"Server Error");
                          }
                          
                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          //NSLog(@"POST Failed %@ %@", operation.responseString, error);
                          NSLog(@"request 1 failed == %@ == response == %@  code %d - error %@", operation.request, operation.responseString, operation.response.statusCode, error);
                              if (failedCalback) {
                                  failedCalback(error.description);
                          }
                      }];
    }
}



- (void)jsonPerformRequest:(NSString *)urlRequest apiName: (NSString *)apiName requestMethod: (NSString *) method params: (NSDictionary *)param onBaseUrl:(NSString *)baseUrl callback:(void (^)(id resp))successCalback failedCallback:(void (^)(NSString *msg))failedCalback{
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", baseUrl, urlRequest];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString: requestURL]];
    
    [request setHTTPMethod: method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval: REQUEST_TIMEOUT_SECONDS];
    [request setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
    
    /* add authorization token to header */
    
    NSUserDefaults *prefs   =   [NSUserDefaults standardUserDefaults];
    NSString *authToken     =   [prefs objectForKey:AUTHORIZATION_TOKEN];
    
    if(authToken){
        [request setValue:authToken forHTTPHeaderField:@"Authentication-Token"];
    }
    
    NSLog(@"Perform json request %@ with params %@ - headers[auth_token: %@]", requestURL, param, authToken);
    
    if (param != nil) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param
                                                           options: 0//NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        if (!jsonData) {
            NSLog(@"Got an error: %@", error);
            if (failedCalback) {
                failedCalback(@"Bad input parameters");
                return;
            }
        } else {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding: NSUTF8StringEncoding];
            NSLog(@"json param %@", jsonString);
            //[request appendPostData: [jsonString dataUsingEncoding: NSUTF8StringEncoding]];
            [request setHTTPBody:[NSMutableData dataWithData:[jsonString  dataUsingEncoding:NSUTF8StringEncoding]]];
            [jsonString release];
        }
    }
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setResponseSerializer: [AFJSONResponseSerializer serializer]];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"type of respone %@ resp %@ code %d",[responseObject class], responseObject, [operation.response statusCode]);
        
        int rspCode = [operation.response statusCode];
        if (rspCode == 200) {
            if (successCalback) {
                successCalback(responseObject);
            }
        }else{
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"request failed == %@ == response == %@  code %d - error %@", request, operation.responseString, operation.response.statusCode, error);
        //NSLog(@"request header == %@", [operation.request allHTTPHeaderFields]);
        int rspCode = [operation.response statusCode];
        if (failedCalback) {
            failedCalback(error.description);
        }
    }];
    
    [operation start];
}

- (void)performRequest:(NSString *)urlRequest apiName: (NSString *)apiName requestMethod: (NSString *) method params: (NSDictionary *)param andImageFile:(NSData *)imageData onBaseUrl:(NSString *)baseUrl callback:(void (^)(id resp))successCalback failedCallback:(void (^)(NSString *msg))failedCalback{
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@", baseUrl, urlRequest];
    
    NSLog(@"Perform request %@ wih params %@", requestURL, param);
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString: requestURL]];
    
    /* add authorization token to header */
        
    [request setHTTPMethod: method];
    [request setTimeoutInterval: REQUEST_TIMEOUT_SECONDS];
    [request setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
    
    AFHTTPRequestOperationManager *manager  =   [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFHTTPRequestOperation *operation = [manager POST:requestURL
                                           parameters:param
                            constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                
                                [formData appendPartWithFileData:imageData name:@"FileContent" fileName:[NSString stringWithFormat:@"%@.png", [[param objectForKey:@"title"] stringByReplacingOccurrencesOfString:@" " withString:@""]] mimeType:@"image/png"];
                                
                            } success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                //NSLog(@"type of respone %@ resp %@ code %d",[responseObject class], responseObject, [operation.response statusCode]);
                                
//                                int rspCode = [operation.response statusCode];
//                                if (rspCode == 200) {
                                    if (successCalback) {
                                        successCalback(responseObject);
//                                    }
                                }
                            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                NSLog(@"request failed == %@ == response == %@  code %d", request, operation.responseString, operation.response.statusCode);
                                NSLog(@"request header == %@", [operation.request allHTTPHeaderFields]);
                                int rspCode = [operation.response statusCode];
                                if (rspCode == 400 || rspCode == 500){
                                    //invalid
                                    successCalback(operation.responseObject);
                                }else if (rspCode == 401){
                                    if (failedCalback) {
                                        failedCalback(@"Your session is expired. Please login again.");
                                    }
                                }else {
                                    if (failedCalback) {
                                        failedCalback(@"Server got problem when process request. Please try again!");
                                    }
                                }
                            }];
    [operation start];
}


#pragma mark - Get containerList
- (void)getContainerList:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
    [self jsonPerformRequest: API_ContainersList
                 apiName: API_ContainersList
           requestMethod:kReq_Method_Get
                  params: nil
               onBaseUrl: BASE_URL
                callback:^(id resp) {
                    successCallback (resp);
                } failedCallback:^(NSString *msg) {
                    failedCallback ( msg);
                }];
}

#pragma mark - User Settings
//NSString *url = [NSString stringWithFormat:@"%@/webapi/UserSettings/%@",[JSMain apiHost], userEmail];
- (void)getUserSettings: (NSString *)userEmail successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
    [self jsonPerformRequest: [NSString stringWithFormat:API_UserSettings, userEmail]
                 apiName: API_UserSettings
           requestMethod:kReq_Method_Get
                  params: nil
               onBaseUrl: BASE_URL
                callback:^(id resp) {
                    successCallback (resp);
                } failedCallback:^(NSString *msg) {
                    failedCallback ( msg);
                }];
}

#pragma mark - Sync Assets
//NSString *endpoint = [NSString stringWithFormat:@"%@/api/sync?since=%@&containers=%@", [JSMain apiHost], when, containerName];
- (void)syncAssetsFrom: (NSString *)when containerName : (NSString *)cname successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
    [self jsonPerformRequest: [NSString stringWithFormat:API_SyncAssets, when, cname]
                 apiName: API_SyncAssets
           requestMethod:kReq_Method_Get
                  params: nil
               onBaseUrl: BASE_URL
                callback:^(id resp) {
                    successCallback (resp);
                } failedCallback:^(NSString *msg) {
                    failedCallback ( msg);
                }];
}

#pragma mark - ShareViaEmail
- (void)shareViaEmail:(NSDictionary *)params successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
    [self jsonPerformRequest: API_ShareViaEmail
                 apiName: API_ShareViaEmail
           requestMethod:kReq_Method_Post
                  params: params
               onBaseUrl: BASE_URL
                callback:^(id resp) {
                    successCallback (resp);
                } failedCallback:^(NSString *msg) {
                    failedCallback ( msg);
                }];
}

#pragma mark - iDriver Push
- (void)iDrivePushMethod:(NSDictionary *)params successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
    [self jsonPerformRequest: API_iDrive_PushMethod
                 apiName: API_iDrive_PushMethod
           requestMethod:kReq_Method_Post
                  params: params
               onBaseUrl: BASE_URL
                callback:^(id resp) {
                    successCallback (resp);
                } failedCallback:^(NSString *msg) {
                    failedCallback ( msg);
                }];
}

#pragma mark - iDriver Pull
- (void)iDrivePullMethod:(NSDictionary *)params successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
    [self jsonPerformRequest: API_iDrive_PullMethod
                 apiName: API_iDrive_PullMethod
           requestMethod:kReq_Method_Get
                  params: params
               onBaseUrl: BASE_URL
                callback:^(id resp) {
                    successCallback (resp);
                } failedCallback:^(NSString *msg) {
                    failedCallback ( msg);
                }];
}

#pragma mark - Renew Token
- (void)doRenewAuthToken:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
//    [self jsonPerformRequest: [NSString stringWithFormat: API_Login, kDefaultUserName, kDefaultPassword]
//                 apiName: API_Login
//           requestMethod:kReq_Method_Get
//                  params: nil
//               onBaseUrl: BASE_URL
//                callback:^(id resp) {
//                    //set this token to nsdefault
//                    NSUserDefaults *prefs   =   [NSUserDefaults standardUserDefaults];
//                    [prefs setObject: resp forKey: AUTHORIZATION_TOKEN];
//                    [prefs synchronize];
//                    successCallback (resp);
//                } failedCallback:^(NSString *msg) {
//                    failedCallback ( msg);
//                }];
    
    [self performRequest:[NSString stringWithFormat: API_Login, kDefaultUserName, kDefaultPassword]
                 apiName:API_Login
           requestMethod:kReq_Method_Get
                  params:nil
               onBaseUrl:BASE_URL
                callback:^(id resp) {
                    
                    NSUserDefaults *prefs   =   [NSUserDefaults standardUserDefaults];
                    [prefs setObject: [resp stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey: AUTHORIZATION_TOKEN];
                    [prefs synchronize];

                    successCallback(resp);
                    
                } failedCallback:^(NSString *msg) {
                    failedCallback(msg);
                }];
    
}

#pragma mark - Post Media
/**
 *  Post Media
 *
 *  @param params          list of params details
 Name: The name of the media item being uploaded. string .  Required
 Location: The location the media item relates to. string . None.
 Description: The description of the media item. string . Max length: 140
 Specialities: A comma delimited list of Specialities with which to associate the media item. These Specialities are validated against the list stored on the server and checked for validity. string . None.
 Products: A comma delimited list of products with which to associate the media item. This uses the "Tagging" feature of the DAM library and the supplied list of items do not have to correspond to real products. string . None.
 Latitude: The geographical latitude co-ordinate the media item relates to. decimal number . None.
 Longitude: The geographical longitude co-ordinate the media item relates to. decimal number . None.
 AppUserEmail: The email address of the user who is uploading the media item from the client application. This email address is checked for validity on the server. string . Required
 
 *  @param data            nsdata of media
 *  @param successCallback <#successCallback description#>
 *  @param failedCallback  <#failedCallback description#>
 */
- (void)doPostMedia: (NSDictionary *) params mediaData: (NSData *)data successCallback:(void (^)(id resp))successCallback andFailCallback:(void (^)(id msg))failedCallback {
    [self performRequest:API_PostMedia
                 apiName:API_PostMedia
           requestMethod:kReq_Method_Post
                  params:params
            andImageFile:data
               onBaseUrl:BASE_URL
                callback:^(id resp) {
                    successCallback(resp);
                } failedCallback:^(NSString *msg) {
                    failedCallback(msg);
                }];
}

@end
