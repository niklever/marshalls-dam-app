//
//  JSTiling.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 3/5/13.
//
//

#import <Foundation/Foundation.h>
#import "JSMain.h"

@interface JSTiling : NSObject 

@property (nonatomic, assign) float thumbsContainerHeight; // height of the tiled thumbnail area (px)
@property (nonatomic, assign) float imageBorderWidth; // 3px border round all images = 6px gap between them
@property (nonatomic, retain) NSArray *searchResults; // search results to go in here as objects, eg. {filename:"1.jpg", width:500, height:333, priority:3}, sorted by priority, then height, then width

@property (nonatomic, retain) NSDictionary * imageRanges; // for each priority/height combination in the search results, we need to know the 'range' (ie. first and last image), in order to speed searching later
@property (nonatomic, assign) int rowsInCurrentBlock;
@property (nonatomic, assign) int rowsInPreviousBlock;
@property (nonatomic, assign) float totalWidthUsed;
@property (nonatomic, retain) NSArray *imagePriorities; // all the possible image priority values, from the most to the least important
@property (nonatomic, assign) int imagePriorityIndex;
@property (nonatomic, retain) NSArray * imageHeights; // all the possible image heights, from smallest to largest
@property (nonatomic, assign) int imageHeightIndex;
@property (nonatomic, retain) NSArray *pseudoRandomNumbers ;
@property (nonatomic, retain) NSDictionary *imagesCountingAvailable;
@property (nonatomic, assign) int pseudoRandomNumbersIndex;
@property (nonatomic, assign) BOOL isCancel;

@property (nonatomic) int blockIndex;
/*@property (nonatomic, assign) int previousBlock;
@property (nonatomic, assign) int previousBlockWidth;
@property (nonatomic, assign) int blockStartXPos;
@property (nonatomic, assign) int blockStartYPos;*/
@property (nonatomic, assign) int x;
@property (nonatomic, assign) int y;
@property (nonatomic, assign) int call;
//@property (nonatomic, assign) int previousMergedRowHeight;
@property (nonatomic, retain) NSArray *imageLayout; // keep a record of which image appears where in the search results page layout

+ (JSTiling*)sharedInstance;
- (void) internalInit;
- (NSArray *) getTiling:(NSArray *) data;
- (void) cancelTiling;

@end
