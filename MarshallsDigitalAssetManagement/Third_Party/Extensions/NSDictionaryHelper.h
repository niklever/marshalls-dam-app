//
//  NSDictionaryHelper.h
//  Velocity
//
//  Created by Ky Thanh on 26/4/13.
//  Copyright (c) 2013 GKxIM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionaryHelper)
- (NSString *)nonNullValueForKey:(NSString *)key;
- (id)nonNullObjectForKey:(NSString *)key;
@end