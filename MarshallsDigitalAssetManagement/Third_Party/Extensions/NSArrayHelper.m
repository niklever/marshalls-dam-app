//
//  NSArrayHelper.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 20/8/14.
//
//

#import "NSArrayHelper.h"

@implementation NSArray (NSArrayHelper)

- (id) JSONString {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: self options:NSJSONWritingPrettyPrinted error: nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

@end
