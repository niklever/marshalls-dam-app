//
//  NSStringHelper.m
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 20/8/14.
//
//

#import "NSStringHelper.h"

@implementation NSString (NSStringHelper)

- (id)objectFromJSONString {
    id resp = [NSJSONSerialization JSONObjectWithData: [self dataUsingEncoding: NSUTF8StringEncoding] options:kNilOptions error:nil];
    return resp;
}

@end
