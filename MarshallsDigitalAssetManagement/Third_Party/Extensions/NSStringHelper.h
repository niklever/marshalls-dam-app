//
//  NSStringHelper.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 20/8/14.
//
//

#import <Foundation/Foundation.h>

@interface NSString (NSStringHelper)
- (id)objectFromJSONString;
@end
