//
//  NSArrayHelper.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 20/8/14.
//
//

#import <Foundation/Foundation.h>

@interface NSArray (NSArrayHelper)
- (id) JSONString;
@end
