//
//  NSDictionaryHelper.m
//  Velocity
//
//  Created by Ky Thanh on 26/4/13.
//  Copyright (c) 2013 GKxIM. All rights reserved.
//

#import "NSDictionaryHelper.h"

@implementation NSDictionary (NSDictionaryHelper)

- (NSString *)nonNullValueForKey:(NSString *)key {
    if ([self objectForKey: key] == nil || [[self objectForKey: key] isEqual: [NSNull null]]) {
        return @"";
    }
    return [self objectForKey: key];
}

- (id)nonNullObjectForKey:(NSString *)key {
    if ([[self objectForKey: key] isEqual: [NSNull null]]) {
        return nil;
    }
    return [self objectForKey: key];
}
@end
