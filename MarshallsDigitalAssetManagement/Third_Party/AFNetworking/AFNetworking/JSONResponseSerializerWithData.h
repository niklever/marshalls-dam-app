//
//  JSONResponseSerializerWithData.h
//  MarshallsDigitalAssetManagement
//
//  Created by Ky Thanh on 20/8/14.
//
//

#import <Foundation/Foundation.h>

/// NSError userInfo key that will contain response data
static NSString * const JSONResponseSerializerWithDataKey = @"JSONResponseSerializerWithDataKey";

@interface JSONResponseSerializerWithData : AFJSONResponseSerializer

@end
