//
//  TrackingHelper.h
//  AIASalesTools
//
//  Created by Ky Thanh on 12/9/13.
//  Copyright (c) 2013 GKxIM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAI.h"

@interface TrackingHelper : NSObject

+ (void)sendView:(NSString *)viewName;
+ (void)sendEventWithCategory:(NSString *)cate withAction:(NSString *)action withLabel:(NSString *)label value:(NSNumber *)val;

@end
