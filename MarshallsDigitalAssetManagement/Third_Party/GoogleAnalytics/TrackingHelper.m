//
//  TrackingHelper.m
//
//  Created by Ky Thanh on 12/9/13.
//  Copyright (c) 2013 GKxIM. All rights reserved.
//

#import "TrackingHelper.h"
#import "GAIDictionaryBuilder.h"
#import "GAI.h"
#import "GAIFields.h"

@implementation TrackingHelper


+ (void)sendView:(NSString *)viewName {
    
    id<GAITracker> tracker1 = [[GAI sharedInstance] trackerWithTrackingId: kGAI_APPID];
   
//    // This screen name value will remain set on the tracker and sent with
//    // hits until it is set to a new value or to nil.
    [tracker1 set:kGAIScreenName
            value: viewName];
    [tracker1 send:[[GAIDictionaryBuilder createAppView] build]];
    
}

+ (void)sendEventWithCategory:(NSString *)cate withAction:(NSString *)action withLabel:(NSString *)label value:(NSNumber *)val{
    
    id<GAITracker> tracker1 = [[GAI sharedInstance] trackerWithTrackingId: kGAI_APPID];
//    
    [tracker1 send:[[GAIDictionaryBuilder createEventWithCategory: cate     // Event category (required)
                                                           action: action  // Event action (required)
                                                            label: label          // Event label
                                                            value: val ] build]];
}


@end
