//
//  UIImage+fixOrientation.h
//  MarshallsDigitalAssetManagement
//
//  Created by GKxIM on 10/7/14.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
