//
//  GkximPDFReaderView.h
//  PDFReader
//
//  Created by GKxIM on 2/21/14.
//  Copyright (c) 2014 GKxIM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderMainPagebar.h"

@class ReaderDocument;

@interface GkximPDFReaderView : UIView <UIScrollViewDelegate, ReaderMainPagebarDelegate, UIGestureRecognizerDelegate>
{
    /* UIControl */
    UIScrollView *theScrollView;
    
    /* PDF params */
    ReaderMainPagebar *mainPagebar;
    ReaderDocument *document;
    
    /* internal params */
    NSMutableDictionary *contentViews;
    NSInteger currentPage;
    BOOL isVisible;
}

- (void) loadPDFFileWithFileName:(NSString *)fileName;

@end
