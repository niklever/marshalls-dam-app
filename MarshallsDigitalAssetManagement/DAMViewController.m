//
//  DAMViewController.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMViewController.h"
#import "DAMApplicationModel.h"
#import "DAMHomeView.h"

@interface DAMViewController () {
    
}

@end

@implementation DAMViewController
@synthesize splashImage;
@synthesize homeView;

//TODO - collect constants in one header file?
const float splashFadeAnimationDuration = 0.1f;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.navigationController.navigationBarHidden = YES;
    
    /*self.splashImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Landscape.png"]];
    splashImage.transform = CGAffineTransformMakeTranslation(0, -20);
    [self.view addSubview:splashImage];
    [splashImage release];
    */
    
    self.homeView = [[DAMHomeView alloc] initWithNibName:@"DAMHomeView" bundle:nil];
    
    [UIView beginAnimations:@"splashFade" context:nil];
    [UIView setAnimationDuration:splashFadeAnimationDuration];
    self.splashImage.alpha = 0.0f;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(splashFinished:)];
    [UIView commitAnimations];
    if([[JSMain sharedInstance] isIOS7])
    {
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
        
        //[self.window setFrame:CGRectMake(20, 0, self.window.frame.size.width, self.window.frame.size.height)];
        
        [self setNeedsStatusBarAppearanceUpdate];
    }else {
        //[self.window setFrame:CGRectMake(-20, 0, self.window.frame.size.width, self.window.frame.size.height)];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.splashImage = nil;
    self.homeView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

//Called via PhoneGap when the WebSQL and other bits and pieces are ready.
/*
- (void)webModelReady:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options {
    
    [UIView beginAnimations:@"splashFade" context:nil];
    [UIView setAnimationDuration:splashFadeAnimationDuration];
    splashImage.alpha = 0.0f;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(splashFinished:)];
    [UIView commitAnimations];
    
    DAMApplicationModel *model = [DAMApplicationModel model];
}
*/

- (void)splashFinished:(id)sender {
#ifdef DEBUG
    NSLog(@"DAMViewController.splashFinished");
#endif
    [self.splashImage removeFromSuperview];
    [self.navigationController pushViewController:self.homeView animated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kAppInitDone" object:nil];
    
}

@end
