//
//  DAMAppDelegate.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DAMAppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>

//#import "DAMViewController.h"

@implementation DAMAppDelegate

@synthesize window = _window;
@synthesize damViewController = _damViewController;
@synthesize navigationController = _navigationController;

@synthesize salesPhotoHeight;
@synthesize salesPhotoWidth;

- (void)dealloc
{
    [_window release];
    [_damViewController release];
    [super dealloc];
}


- (void)addSkipBackupAttributeToPath:(NSString*)path {
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    setxattr([path fileSystemRepresentation], attrName, &attrValue, sizeof(attrValue), 0, 0);
}



- (void)appInitDone:(NSNotification *)sender {
    self.startUpOrientation = [UIApplication sharedApplication].statusBarOrientation;
    [self adjustOrientation:self.startUpOrientation];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void) adjustOrientation:(UIInterfaceOrientation)orientation{
    
    CGFloat currentIOS  =   [[UIDevice currentDevice].systemVersion floatValue];
    if(currentIOS >= 8.0){
        originY =   -1 * originY;
        CGRect viewBounds = [self.window bounds];
        viewBounds.origin.y = originY;
        viewBounds.size.height = viewBounds.size.height;
        self.window.frame = viewBounds;
    }
    else{
        if([[JSMain sharedInstance] isIOS7])
        {
            if (orientation == UIDeviceOrientationLandscapeLeft) {
                [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
                CGRect viewBounds = [self.window bounds];
                viewBounds.origin.x = -20;
                viewBounds.size.height = viewBounds.size.height;
                self.window.frame = viewBounds;
                
            }else{
                [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
                CGRect viewBounds = [self.window bounds];
                viewBounds.origin.x = 20;
                viewBounds.size.height = viewBounds.size.height;
                self.window.frame = viewBounds;
            }
        }
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Override point for customization after application launch.
//    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    originY =   -20;
    
    [self createGalleryDirectory];
    
    [GMSServices provideAPIKey:GOOGLE_MAP_KEY];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel: kGAILogLevelNone];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId: kGAI_APPID];
    
    
    [self addSkipBackupAttributeToPath:[NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0], GALLERY]];
    [self addSkipBackupAttributeToPath:[NSString stringWithFormat:@"%@/ThumbMedia",[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0]]];
    [self addSkipBackupAttributeToPath:[NSString stringWithFormat:@"%@/MainMedia",[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask, YES) objectAtIndex:0]]];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    NSString *modelFilePath = [[NSBundle mainBundle] pathForResource: kModelFileBase ofType:@"json"];
    NSLog(@"DAMHomeView.viewDidLoad modelFilePath:%@", modelFilePath);
    NSError *error = nil;
    NSString *modelString = [NSString stringWithContentsOfFile:modelFilePath encoding:NSUTF8StringEncoding error:&error];
    if (!error) {
        self.dataModel = [modelString objectFromJSONString];
    } else {
#ifdef DEBUG
        NSLog(@"DAMHomeView.viewDidLoad.ERROR_NO_MODEL");
#endif
    }
    
    //NSLog(@"wins frame %@", NSStringFromRect(self.window.frame));
    
    self.damViewController = [[[DAMViewController alloc] initWithNibName:@"DAMViewController" bundle:nil] autorelease];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.damViewController];
    //self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];

    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appInitDone:) name:@"kAppInitDone" object:nil];
    
    //NSLog(@"app did finish launching");
    
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    [AFHTTPRequestOperationManager manager].securityPolicy = securityPolicy;
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [[JSMain sharedInstance] goOnWifi];
                [[JSMain sharedInstance] goOnline];
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                // Our connection is fine
                //go online
                [[JSMain sharedInstance] goOnline];
                break;
            case AFNetworkReachabilityStatusNotReachable:
                // We have no active connection - disable all requests and don’t let the user do anything
                [[JSMain sharedInstance] goOffWifi];
                [[JSMain sharedInstance] goOffline];
                break;
            default:
                // If we get here, we’re most likely timing out
                [[JSMain sharedInstance] goOffWifi];
                [[JSMain sharedInstance] goOffline];
                break;
        }
    }];
    
    // Set the reachabilityManager to actively wait for these events
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    
    [DAMApplicationModel model];
    
    [self appInitialize];
    
    return YES;
}


- (void)updateUserDefaultsFromSettingsBundle:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSString *val = nil;
    
	if (standardUserDefaults)
		val = [standardUserDefaults objectForKey:key];
    
	// TODO: / apparent Apple bug: if user hasn't opened Settings for this app yet (as if?!), then
	// the defaults haven't been copied in yet.  So do so here.  Adds another null check
	// for every retrieve, but should only trip the first time
	if (val == nil) {
#ifdef DEBUG
		NSLog(@"user defaults may not have been loaded from Settings.bundle ... doing that now ...");
#endif
		//Get the bundle path
		NSString *bPath = [[NSBundle mainBundle] bundlePath];
		NSString *settingsPath = [bPath stringByAppendingPathComponent:@"Settings.bundle"];
		NSString *plistFile = [settingsPath stringByAppendingPathComponent:@"Root.plist"];
        
		//Get the Preferences Array from the dictionary
		NSDictionary *settingsDictionary = [NSDictionary dictionaryWithContentsOfFile:plistFile];
		NSArray *preferencesArray = [settingsDictionary objectForKey:@"PreferenceSpecifiers"];
        
		//Loop through the array
		NSDictionary *item;
		for(item in preferencesArray)
		{
			//Get the key of the item.
			NSString *keyValue = [item objectForKey:@"Key"];
            
			//Get the default value specified in the plist file.
			id defaultValue = [item objectForKey:@"DefaultValue"];
            
			if (keyValue && defaultValue) {
				[standardUserDefaults setObject:defaultValue forKey:keyValue];
				if ([keyValue compare:key] == NSOrderedSame)
					val = defaultValue;
			}
		}
		[standardUserDefaults synchronize];
	}
    
}

- (void) appInitialize {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //[prefs setInteger:0 forKey:@"dbInitialised"];
    //[prefs setInteger:0 forKey:@"containersInitialised"];
    
    //Check if we have a local database
    NSInteger dbInitialised = [prefs integerForKey:@"dbInitialised"];
    if (dbInitialised == 0){
        //We need to initialise the database
        //pKt update to use JSDB common functions
        
        if ([[JSDBV2 sharedInstance] initialiseDatabase]){
            [prefs setInteger:1 forKey:@"dbInitialised"];
        }
    }
    
    /*if ([self apiHost] == NULL){
     
    }*/
    
    [self updateUserDefaultsFromSettingsBundle:@"server"];
    [self updateUserDefaultsFromSettingsBundle:@"userguide"];
    
    //init share
    [JSShare sharedInstance];
    
    //reset business filter
    [[JSMain sharedInstance] setBusinessAreaFilters: [NSMutableArray array]];
    
    //NSString *clURL = [NSString stringWithFormat:@"%@/API/containerList",[self apiHost]];
    
    //NSLog(@"Open container list API %@", clURL);
    
    [[APICommon sharedInstance] getContainerList:^(id resp) {
        NSArray *containers = resp;
        
        //[kAppDelegate.damViewController.homeView setDisplayActivityIndicatorView: YES];
        //[kAppDelegate.damViewController.homeView setDisplayProgressView:YES];
        
        if (containers.count == 0) {
            return;
        }
        
        __block int tag = 0;
        
        for(NSDictionary *dic in containers){
            NSString *businessArea = [dic objectForKey:@"businessArea"];
            NSString *name = [dic objectForKey:@"container"];
            int state = 0;
            //get current container by name
            [[JSDBV2 sharedInstance] executeQuery:[NSString stringWithFormat:@"select * from containers where name= '%@'", name]
                                           params: nil
                                  successCallback:^(id rows) {
                                      if (rows != NULL && [rows count] > 0) {
                                          //NSDictionary *con = [rows objectAtIndex:0];
                                          //tag = [con objectForKey:@"tag"];
                                      }else{
                                          NSString *sql = [NSString stringWithFormat:@"INSERT INTO containers (businessArea, tag, name, state) VALUES ('%@',%d,'%@',%d)", businessArea, tag, name, state];
                                          [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:sql successCallback:^(BOOL resp) {
                                              
                                          }];
                                      }
                                  }];
            /*
            NSArray *rows = [[JSDBV2 sharedInstance] sql:[NSString stringWithFormat:@"select * from containers where name= '%@'", name] params:nil ];
            if (rows != NULL && [rows count] > 0) {
                //NSDictionary *con = [rows objectAtIndex:0];
                //tag = [con objectForKey:@"tag"];
            }else{
                NSString *sql = [NSString stringWithFormat:@"INSERT INTO containers (businessArea, tag, name, state) VALUES ('%@',%d,'%@',%d)", businessArea, tag, name, state];
                [[JSDBV2 sharedInstance] sqlExecWithSQLStatement:sql];
            }*/
            
            tag++;
        }
    } andFailCallback:^(id msg) {
        //[kAppDelegate.damViewController.homeView setDisplayActivityIndicatorView: YES];
        //[kAppDelegate.damViewController.homeView setDisplayProgressView:YES];
        
        //kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = YES;
        //kAppDelegate.damViewController.homeView.progressView.hidden = YES;
    }];
    //ASIHTTPRequest *request1 = [ASIHTTPRequest requestWithURL:[NSURL URLWithString: clURL]];
    
    [kAppDelegate.damViewController.homeView updateNotificationLogs: @"Checking containers list"];
    //[kAppDelegate.damViewController.homeView setDisplayActivityIndicatorView: NO];
    //[kAppDelegate.damViewController.homeView setDisplayProgressView:YES];
    
    //kAppDelegate.damViewController.homeView.progressView.hidden = YES;
    //kAppDelegate.damViewController.homeView.activityIndicatorView.hidden = NO;
    
    
    /*
     [request1 setRequestMethod:@"GET"];
     [request1 setDefaultResponseEncoding:NSUTF8StringEncoding];
     [request1 setResponseEncoding:NSUTF8StringEncoding];
     [request1 setTimeOutSeconds: 60];
     //[request1 setShowAccurateProgress:YES];
     
     //[request1 setDownloadProgressDelegate: kAppDelegate.damViewController.homeView.progressView];
     
     [request1 setCompletionBlock:^{
     
     }];
     
     [request1 setFailedBlock:^{
     
     }];
     
     [queue addOperation:request1];
     
     [queue go];
     */
    
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    [prefs setObject:[NSString stringWithFormat:@"%@ build:%@", version, build] forKey:@"version"];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //NSLog(@"app did enter bg");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //need to recall sync here
    //NSLog(@"app become active");
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    //NSLog(@"app terminated");
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    //need to abort download queue process
    [[JSSync sharedInstance] abortQueue: nil callback:^{
        NSLog(@"aborted queue when app terminate completed");
        [[DAMApplicationModel model] enableOps];
    }];
    
    //pKt added to close DB
    //[[JSDBV2 sharedInstance] closeDB];
}

- (void) createGalleryDirectory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", GALLERY]];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }
}

@end
