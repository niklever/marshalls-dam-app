//
//  DAMViewController.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DAMHomeView.h"

@class DAMHomeView;

@interface DAMViewController : UIViewController {
    //UIImageView *splashImage;
    
}
@property (nonatomic, retain)IBOutlet UIImageView *splashImage;
@property (nonatomic, retain) DAMHomeView *homeView;

@end
