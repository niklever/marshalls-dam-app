//
//  DAMAppDelegate.h
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//@class DAMViewController;

#import "JSSync.h"
#import "DAMApplicationModel.h"
#import "DAMViewController.h"

@interface DAMAppDelegate : UIResponder <UIApplicationDelegate> {
    int originY;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) UIInterfaceOrientation startUpOrientation;

@property (nonatomic, retain) NSDictionary *dataModel;

@property (strong, nonatomic) DAMViewController *damViewController;
@property (strong, nonatomic) UINavigationController *navigationController;

@property (nonatomic, assign) int inThePressCount;
@property (nonatomic, assign) int salesPhotoWidth;
@property (nonatomic, assign) int salesPhotoHeight;

@property (nonatomic, assign) int photoGalleryCount;


- (void) adjustOrientation:(UIInterfaceOrientation)orientation;

@end
