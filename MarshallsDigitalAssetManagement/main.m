//
//  main.m
//  MarshallsDigitalAssetManagement
//
//  Created by Steve Tickle on 26/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DAMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DAMAppDelegate class]));
    }
}
    